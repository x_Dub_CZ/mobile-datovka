/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QtCore>

#include "ios/src/app_delegate.h"
#include "ios/src/qt_app_delegate.h"
#include "src/qml_interaction/interaction_zfo_file.h"

@implementation QtAppDelegate

/* Pointer to InteractionZfoFile object */
InteractionZfoFile *interactionZfoFilePtr;

+(QtAppDelegate *)sharedQtAppDelegate
{
	static dispatch_once_t pred;
	static QtAppDelegate *shared = nil;
	dispatch_once(&pred, ^{shared = [[super alloc] init];});
	return shared;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
    sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
	Q_UNUSED(application);
	Q_UNUSED(sourceApplication);
	Q_UNUSED(annotation);

	if (url != nil && [url isFileURL]) {
		if ([[url pathExtension] isEqualToString:@"zfo"]) {
			//NSLog(@"URL:%@", [url absoluteString]);
			if (Q_NULLPTR != interactionZfoFilePtr) {
				interactionZfoFilePtr->openZfoFile(QString::fromNSString(url.path));
			}
		}
	}
	return YES;
}

void QtAppDelegateInitialize(InteractionZfoFile *interactionZfoFile)
{
	/* Pointer to InteractionZfoFile object */
	interactionZfoFilePtr = interactionZfoFile;
	/* Init share app delegate with iOS */
	[[UIApplication sharedApplication] setDelegate:[QtAppDelegate sharedQtAppDelegate]];
	//NSLog(@"Created a new appdelegate");
}

@end
