/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QString>
#include <QStringList>
#include <QUrl>

/*!
 * @brief Provides objective-C IO methods for interaction with iCLoud.
 */
class ICloudIo {
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum ICloudResult {
		ICLOUD_NOT_ON = 0, /*!< iCloud is not avalilable/turn on. */
		ICLOUD_FILE_UPLOAD_SUCCESS, /*!< File upload was successful. */
		ICLOUD_FILE_EXISTS, /*!< File exists on iCloud. */
		ICLOUD_FILE_UPLOAD_ERROR, /*!< File upload failed. */
		ICLOUD_TARGET_SAVE_DIR_ERROR /*!< Target dir is missing or not created. */
	};

private:
	/*!
	 * @brief Constructor.
	 */
	ICloudIo(void);

public:
	/*!
	 * @brief Test if iCloud is on.
	 *
	 * @return Return true if iCloud is on.
	 */
	static
	bool isCloudOn(void);

	/*!
	 * @brief Upload single file into iCloud.
	 *
	 * @param[in] srcFilePath Source file path.
	 * @param[in] destFilePath iCloud target path.
	 * @return Return operation error/success code.
	 */
	static
	ICloudResult moveFileToCloud(const QString &srcFilePath,
	    const QString &destFilePath);

	/*!
	 * @brief Create and open document picker controller for export.
	 *
	 * @param[in] exportFilesPath File paths for export (can be empty).
	 * @return True if document picker controller is created and opened.
	 */
	static
	bool openDocumentPickerControllerForExport(const QStringList &exportFilesPath);

	/*!
	 * @brief Create and open document picker controller for import.
	 *
	 * @param[in] allowedUtis Allowed iOS mime extensions (can be empty).
	 * @return True if document picker controller is created and opened.
	 */
	static
	bool openDocumentPickerControllerForImport(const QStringList &allowedUtis);

	/*!
	 * @brief Move file from app temporary inbox to local app sandbox.
	 *
	 * @param[in] sourceFileUrl Source file url from inbox.
	 * @param[in] newFilePath Target path to local app sandbox.
	 * @return Full path where file was moved.
	 */
	static
	QUrl moveFile(const QUrl &sourceFileUrl, const QString &newFilePath);
};
