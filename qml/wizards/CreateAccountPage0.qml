import QtQuick 2.7
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0

Item {

    id: page0
    signal handlerLoader(string pageFile)

    PageHeader {
        id: headerBar
        title: qsTr("Account Creator Wizard")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        AccessibleOverlaidImageButton {
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            image.sourceSize.height: imageActionDimension
            image.source: "qrc:/ui/forward.svg"
            accessibleName: qsTr("Account Creator Wizard")
            onClicked: handlerLoader("CreateAccountPage1.qml")
        }
    }

    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        contentHeight: flickContent.implicitHeight
        height: parent.height * 0.8
        Pane {
            id: flickContent
            anchors.fill: parent
            background: Rectangle {
                color: datovkaPalette.base
            }
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                AccessibleText {
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    color: datovkaPalette.text
                    wrapMode: Text.Wrap
                    text: qsTr("Welcome to the account creator wizard. It will help you to create an account in this application in order to access an existing data box. You should already know valid login credentials (typically a username and a password) for a data box.") + " " +
                          qsTr("The device which this application is running on must have an active internet connection.")
                }
                AccessibleButton {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Continue with the Wizard")
                    height: inputItemHeight
                    font.pointSize: defaultTextFont.font.pointSize
                    onClicked: handlerLoader("CreateAccountPage1.qml")
                }
                AccessibleText {
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    color: datovkaPalette.text
                    wrapMode: Text.Wrap
                    text: qsTr("If you don't want to use the wizard you can skip it and fill in a form.")
                }
                AccessibleButton {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Use Account Form")
                    height: inputItemHeight
                    font.pointSize: defaultTextFont.font.pointSize
                    onClicked: pageView.replace(pageSettingsAccount, {"pageView": pageView, "acntName": null, "acntId": null}, StackView.Immediate)
                }
                AccessibleText {
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    color: datovkaPalette.text
                    wrapMode: Text.Wrap
                    text: qsTr("Note: If you've just freshly obtained your login credentials then you must use them to log into the ISDS web portal for the first time where you will need to change you password. With the new password you can then start using this application.")
                }
                AccessibleText {
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    color: datovkaPalette.text
                    wrapMode: Text.Wrap
                    text: qsTr("Note: This application doesn't allow the creation of new data boxes on the ISDS server.")
                }
                AccessibleText {
                    id: infoLabel
                    width: parent.width
                    textFormat: TextEdit.RichText
                    color: datovkaPalette.text
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    onLinkActivated: Qt.openUrlExternally(link)
                    text: qsTr("Recommendation: Read the <a href=\"%1\">user manual</a> in order to better understand how this application works. The <a href=\"%2\">project web page</a> also contains some useful information.").arg("https://secure.nic.cz/files/datove_schranky/redirect/mobile-manual.html").arg("https://www.datovka.cz/cs/pages/mobilni-datovka.html")
                }
            } // Column
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
}
