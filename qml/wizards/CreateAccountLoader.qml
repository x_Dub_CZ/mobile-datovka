import QtQuick 2.7
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Item {

    id: accountWizard
    anchors.fill: parent

    /* These properties must be set by caller. */
    property var pageView

    /* These properties are set in the wizard pages. */
    property string accountName: ""
    property string accountTypeName: ""
    property string certFilePath: ""
    property int loginMethod: AcntData.LIM_UNAME_PWD
    property string loginMethodName: ""
    property string pwdStr: ""
    property string mepCode: ""

    signal handlerLoader(string pageFile)

    function getFileNameFromPath(filePath) {
        return filePath.replace(/^.*[\\\/]/, '')
    }

    Component.onCompleted: {
        acntId.testing = false
    }

    AcntId {
        id: acntId
    }
    Loader {
        id: pageLoader
        anchors.fill: parent
        source: "CreateAccountPage0.qml"
    }
    Connections {
        target: pageLoader.item
        function onHandlerLoader(pageFile) {
            pageLoader.source = pageFile
        }
    }
}
