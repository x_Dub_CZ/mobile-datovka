/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Window 2.1
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2
import QtQuick.Controls.Material 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

ApplicationWindow {

    // Get deafult color theme and palette from system
    SystemPalette { id: datovkaPalette; colorGroup: SystemPalette.Active }
    id: mainWindow
    visible: true
    title: "Datovka"
    width: 800
    minimumWidth: 320
    height: 600
    minimumHeight: 240

    color: datovkaPalette.window

    // This element set default font (size) and must be hidden. Font is used
    // accross application as default font size. Do not remove it.
    TextMetrics {
        id: defaultTextFont
        text: "W"
    }

    property bool isiOS: iOSHelper.isIos()

    // Set default material design from system palette
    Material.theme: Material.System
    Material.background: datovkaPalette.window
    Material.foreground: datovkaPalette.windowText
    Material.primary: Material.Blue
    Material.accent: Material.Blue

    // UI colors
    property color iOSBlueColor: "#007AFF"
    property color pageHeaderBgColor: (isiOS) ? datovkaPalette.window : Material.color(Material.Blue)
    property color testAccountBgColor: Qt.lighter(datovkaPalette.mid, 1.2)
    property color normalAccountBgColor: Qt.lighter(Material.color(Material.Blue), 1.6)
    property color lightDarkBgColor: Qt.darker(datovkaPalette.base, 1.1)
    property color selectedItemBgColor: (isiOS) ? Qt.lighter(datovkaPalette.dark, 1.3) : Qt.darker(datovkaPalette.base, 1.1)
    property color actionIconColor: (isiOS) ? iOSBlueColor : datovkaPalette.text
    property color imageDarkerColor: Qt.darker(datovkaPalette.dark, 1.5)
    property color textHighlightColor: (isiOS) ? iOSBlueColor : Material.color(Material.Blue)
    property color pageHeaderTextColor: (isiOS) ? datovkaPalette.text : datovkaPalette.base

    // define all pages for stackview
    property Component pageAboutApp: PageAboutApp {}
    property Component pageAccountDetail: PageAccountDetail {}
    property Component pageBackupData: PageBackupData {}
    property Component pageChangePassword: PageChangePassword {}
    property Component pageContactList: PageContactList {}
    property Component pageDataboxDetail: PageDataboxDetail {}
    property Component pageDataboxSearch: PageDataboxSearch {}
    property Component pageGovService: PageGovService {}
    property Component pageGovServiceList: PageGovServiceList {}
    property Component pageImportMessage: PageImportMessage {}
    property Component pageLog: PageLog {}
    property Component pageMenuAccount: PageMenuAccount {}
    property Component pageMenuDatovkaSettings: PageMenuDatovkaSettings {}
    property Component pageMenuMessage: PageMenuMessage {}
    property Component pageMenuMessageDetail: PageMenuMessageDetail {}
    property Component pageMenuMessageList: PageMenuMessageList {}
    property Component pageMessageDetail: PageMessageDetail {}
    property Component pageMessageList: PageMessageList {}
    property Component pageMessageSearch: PageMessageSearch {}
    property Component pageRecordsManagementUpload: PageRecordsManagementUpload {}
    property Component pageRestoreData: PageRestoreData {}
    property Component pageSendMessage: PageSendMessage {}
    property Component pageSettingsAccount: PageSettingsAccount {}
    property Component pageSettingsGeneral: PageSettingsGeneral {}
    property Component pageSettingsPin: PageSettingsPin {}
    property Component pageSettingsRecordsManagement: PageSettingsRecordsManagement {}
    property Component pageSettingsStorage: PageSettingsStorage {}
    property Component pageSettingsSync: PageSettingsSync {}
    property Component pageAccountWizard: CreateAccountLoader {}

    // dimension and style based on font pixel size and screen dpi
    property int textFontSizeInPixels: defaultTextFont.font.pixelSize
    property int textPointSmall: Math.round(defaultTextFont.font.pointSize * 0.7)
    property int textFontSizeSmall: (textPointSmall > 0) ? textPointSmall : 8
    // dimensions of elements a images
    property int baseHeaderHeight: textFontSizeInPixels * 3
    property int imageActionDimension: baseHeaderHeight * 0.6
    property int imageMenuDimension: baseHeaderHeight * 0.5
    property int imageNavigateDimension: imageMenuDimension
    property int imageNextDimension: baseHeaderHeight * 0.3
    property int pictogramDimension: imageNextDimension
    // inputItemHeight holds height of controls elements computed from font size
    property int listItemHeight: baseHeaderHeight * 1.6
    property int inputItemHeight: baseHeaderHeight
    property int defaultMargin: Math.round(Screen.pixelDensity)
    property int formItemVerticalSpacing: defaultMargin

    /* compare message delivery date with current date-90days */
    property int deleteAfterDays: 90
    function compareMsgDate(msgDeliveryTime) {
        if (msgDeliveryTime === "") {
            // message has virus or delivery time missing
            return true
        } else {
            // convert qml date format to ISO date format
            var inputDateFormat = /(\d{2})\.(\d{2})\.(\d{4})/;
            var msgDate = new Date(msgDeliveryTime.replace(inputDateFormat,'$3-$2-$1'));
            var today = new Date()
            today.setDate(today.getDate()-deleteAfterDays)
            // compare both dates in milliseconds
            return (today.getTime() > msgDate.getTime())
        }
    }

    /* Test if string is lowercase without non-printable characters. */
    function isLowerCaseWithoutNonPrintableChars(str) {
        return (str === str.toLowerCase()) && (!(RegExp("\\s").test(str)))
    }

    /* Exposes nested stack to code outside the page component. */
    property var nestedStack: null

    /* Account model */
    property var accountModel: mainAccountModel

    AcntId {
        id: actionAcntId
    }
    Connections {
        target: isds
        function onOpenDialogRequest(isdsAction, loginMethod, userName, testing, title, text, placeholderText, hidePwd) {
            var component = Qt.createComponent("qrc:/qml/dialogues/InputDialogue.qml");
            if (component !== null ) {
                var inputDialog = component.createObject(mainWindow, {});
                if (inputDialog !== null ) {
                    actionAcntId.username = userName
                    actionAcntId.testing = testing
                    inputDialog.openInputDialog(isdsAction, loginMethod, actionAcntId, title, text, placeholderText, hidePwd)
                }
            }
        }
    }
    Connections {
        // Connection is activated when new account was not logged to ISDS.
        target: isds
        function onUnsuccessfulLoginToIsdsSig(userName, testing) {
            actionAcntId.username = userName
            actionAcntId.testing = testing
            if (accounts.removeAccount(accountModel, actionAcntId, false)) {
                settings.saveAllSettings(accountModel)
            }
        }
    }
    MessageDialog {
        id: mepWaitDialog
        property string userName: ""
        title: qsTr("Mobile Key Login")
        text: qsTr("Waiting for acknowledgement from the Mobile key application for the account '%1'.").arg(userName)
        standardButtons: MessageDialog.Cancel
        onRejected: {
            isds.loginMepCancel()
            mepWaitDialog.close()
        }
    }
    Connections {
        target: isds
        function onShowMepWaitDialog(userName, show) {
            mepWaitDialog.userName = userName
            if (show) {
                mepWaitDialog.open()
            } else {
                mepWaitDialog.close()
            }
        }
    }

    StatusBar {
        id: statusBar
    }

    StackView { // Page area.
        id: mainStack
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: (statusBar.visible) ? statusBar.top : parent.bottom
        initialItem: appArea

        Item {
            id: appArea
            objectName: "appArea"
            anchors.fill: parent

            StackView.onActivated: {
                mainStack.forceActiveFocus()
            }

            Item { /* Application page stack. */
                id: mainPage
                anchors.fill: parent
                visible: true
                StackView {
                    id: pageView
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.bottom: (logBar.visible) ? logBar.top : parent.bottom
                    visible: true
                    initialItem: PageAccountList {}

                    Component.onCompleted: {
                        nestedStack = pageView
                    }
                }
                LogBar {
                    id: logBar
                }
            }
        }

        Component { /* Pin lock screen. */
            id: lockScreen
            Rectangle  {
                objectName: "lockScreen"
                anchors.fill: parent
                color: datovkaPalette.base
                Column {
                    anchors.centerIn: parent
                    spacing: formItemVerticalSpacing
                    AccessibleTextField {
                        id: pinCodeInput
                        height: inputItemHeight
                        font.pointSize: defaultTextFont.font.pointSize
                        //focus: true // Forcing focus here causes troubles on mobile devices.
                        echoMode: TextInput.Password
                        passwordMaskDelay: 500 // milliseconds
                        inputMethodHints: Qt.ImhDigitsOnly
                        placeholderText: qsTr("Enter PIN code")
                        anchors.horizontalCenter: parent.horizontalCenter
                        horizontalAlignment: TextInput.AlignHCenter
                        function verifyPin() {
                            settings.verifyPin(pinCodeInput.text.toString())
                        }
                        onEditingFinished: {
                            // This function is called repeatedly when switching
                            // windows. The condition should reduce PIN verification
                            // calls.
                            if (pinCodeInput.text.length > 0) {
                                verifyPin()
                            }
                        }
                    }
                    AccessibleText {
                        id: wrongPin
                        font.bold: true
                        visible: false
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: qsTr("Wrong PIN code.")
                    }
                    AccessibleButton {
                        text: qsTr("Enter")
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.pointSize: defaultTextFont.font.pointSize
                        height: inputItemHeight
                        onClicked: {
                            pinCodeInput.verifyPin()
                        }
                    }
                    Item {
                        id: blankField
                        width: parent.width
                        height: baseHeaderHeight
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    Image {
                        id: datovkaLogo
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: imageActionDimension * 1.4
                        height: imageActionDimension * 1.4
                        source: "qrc:/datovka.png"
                    }
                    AccessibleText {
                        id: versionLabel
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: qsTr("Version") + ": " + settings.appVersion()
                    }
                } // Column
                Connections {
                    target: settings
                    function onSendPinReply(success) {
                        if (success) {
                            Qt.inputMethod.hide()
                            if (mainStack.currentItem.objectName === "lockScreen") {
                                mainStack.pop(StackView.Immediate)
                            }
                        }
                        wrongPin.visible = !success
                        pinCodeInput.text = ""
                    }
                } // Connections
            } // Rectangle
        } // Component

        Connections {
            target: locker
            function onLockApp() {
                if (mainStack.currentItem.objectName === "appArea") {
                    /* Lock application area. */
                    mainStack.push(lockScreen, StackView.Immediate)
                }
            }
        }
        Connections {
            target: interactionZfoFile
            function onDisplayZfoFileContent(filePath) {
                /*
                 * The app should be locked from within C++ code when PIN is
                 * enabled.
                 */
                console.log("Showing zfo file: " + filePath)
                var fileContent = files.rawFileContent(filePath)
                mainStack.push(pageMessageDetail, {
                        "pageView": mainStack,
                        "fromLocalDb": false,
                        "rawZfoContent": fileContent
                    }, StackView.Immediate)
                /*
                 * Next function has effect only for iOS.
                 * Detail info is in the header file.
                 */
                files.deleteTmpFileFromStorage(filePath)
            }
        }

        Connections {
            target: dlgEmitter
            function onDlgGetText(title, message, echoMode, text, placeholderText, inputMethodHints, explicitPasteMenu) {
                /*
                 * Create a dynamic dialogue object set content and connect
                 * functionality.
                 */

                var dlgComponent = Qt.createComponent("qrc:/qml/dialogues/PasteInputDialogue.qml", Component.PreferSynchronous);
                var dlgObj;
                if (dlgComponent.status === Component.Ready) {
                    this.finishCreation();
                } else {
                    console.log("Failed loading dlgComponent:", dlgComponent.errorString());
                    dlgComponent.statusChanged.connect(this.finishCreation);
                }

                function finishCreation() {
                    if (dlgComponent.status === Component.Error) {
                        // Error handling.
                        console.log("Error loading dlgComponent:", dlgComponent.errorString());
                        return;
                    } else if (dlgComponent.status !== Component.Ready) {
                        return;
                    }

                    dlgObj = dlgComponent.createObject(mainWindow, {"objectName": "textInputDialogue"});
                    if (dlgObj === null) {
                        // Error handling.
                        console.log("Error creating dialogue object");
                        return;
                    }

                    dlgObj.dialogue.title = title
                    dlgObj.dialogue.content.messageText.text = message
                    dlgObj.dialogue.content.textInput.inputMethodHints = inputMethodHints /* Must be set before setting echoMode. */
                    dlgObj.dialogue.dlgEchoMode = echoMode
                    dlgObj.dialogue.content.textInput.text = text
                    dlgObj.dialogue.content.textInput.placeholderText = placeholderText
                    dlgObj.dialogue.explicitPasteMenu = explicitPasteMenu

                    /* See PasteInputDialogue.qml for explanation. */
                    dlgObj.dialogue.standardButtons = StandardButton.Ok | StandardButton.Cancel

                    /* Connect signals. */
                    dlgObj.dialogueClosed.connect(deleteObject);
                    dlgObj.dialogue.accepted.connect(emitAccepted);
                    dlgObj.dialogue.rejected.connect(emitRejected);

                    dlgObj.dialogue.open();
                }

                function deleteObject() {
                    console.log("Destroying dialogue.");
                    dlgObj.destroy();

                    mainStack.forceActiveFocus(); /* Dialogue stole focus. */
                }

                function emitAccepted() {
                    console.log("Accepted " + dlgObj.dialogue.content.textInput.text);
                    dlgEmitter.emitAccepted(dlgObj.dialogue.content.textInput.text);
                }

                function emitRejected() {
                    console.log("Rejected");
                    dlgEmitter.emitRejected();
                }
            }

            function onDlgMessage(icon, title, message, infoMessage, buttons) {
                /*
                 * Create a dynamic dialogue object set content and connect
                 * functionality.
                 */

                var dlgComponent = Qt.createComponent("qrc:/qml/dialogues/MessageDialogue.qml", Component.PreferSynchronous);
                var dlgObj;
                if (dlgComponent.status === Component.Ready) {
                    this.finishCreation();
                } else {
                    console.log("Failed loading dlgComponent:", dlgComponent.errorString());
                    dlgComponent.statusChanged.connect(this.finishCreation);
                }

                function finishCreation() {
                    if (dlgComponent.status === Component.Error) {
                        // Error handling.
                        console.log("Error loading dlgComponent:", dlgComponent.errorString());
                        return;
                    } else if (dlgComponent.status !== Component.Ready) {
                        return;
                    }

                    dlgObj = dlgComponent.createObject(mainWindow, {"objectName": "messageDialogue"});
                    if (dlgObj === null) {
                        // Error handling.
                        console.log("Error creating dialogue object");
                        return;
                    }

                    dlgObj.dialogue.icon = icon
                    dlgObj.dialogue.title = title
                    dlgObj.dialogue.content.messageText.text = message
                    dlgObj.dialogue.content.infoMessageText.text = infoMessage
                    dlgObj.dialogue.dlgButtons = buttons

                    /* Connect signals. */
                    dlgObj.dialogueClosed.connect(gatherDataAndDeleteObject);

                    dlgObj.dialogue.open();
                }

                function gatherDataAndDeleteObject(button, customButton) {
                    /* TODO -- Gather pressed button value. */
                    dlgEmitter.emitClosed(button, customButton)

                    console.log("Destroying dialogue.");
                    dlgObj.destroy();

                    mainStack.forceActiveFocus(); /* Dialogue stole focus. */
                }

                function emitClosed() {
                    console.log("Closed");
                    dlgEmitter.emitClosed(1, -1);
                }
            }
        }

        function navigateBack(mainStack, nestedStack, event) {
            if (event.key === Qt.Key_Back) {
                event.accepted = true

                if (nestedStack === null) {
                    return
                }

                if (mainStack.currentItem.objectName === "lockScreen") {
                    console.log("Ignoring back button on lock screen.")
                } else if (mainStack.depth > 1) {
                    mainStack.pop(StackView.Immediate)
                } else if (nestedStack.depth > 1) {
                    nestedStack.pop()
                } else {
                    event.accepted = false
                    Qt.quit()
                }
            }
        }

        /* Android back button. */
        focus: true
        Keys.onReleased: {
            navigateBack(mainStack, nestedStack, event)
        }
    }

    /* We can loose the focus to StackView if any Menu or Popup is opened */
    function resetFocus() {
        pageView.focus = true
    }

    Text {
        id: dummyText
        text: ""
        visible: false
        wrapMode: Text.NoWrap
        elide: Text.ElideNone
    }
    function computeMenuWidth(menu) {
        var w = 0.0
        for (var i = 0; i < menu.contentData.length; i++) {
            dummyText.text = menu.contentData[i].text + "www"
            if (w < dummyText.width) {
                w = dummyText.width
            }
        }
        dummyText.text = ""
        return Math.round(w)
    }
}
