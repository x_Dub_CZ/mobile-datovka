/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

/*
 * Roles are defined in DataboxListModel::roleNames() and are accessed directly
 * via their names.
 */

Item {
    id: pageContactList

    /* These properties must be set by caller. */
    property var pageView
    property AcntId acntId: null
    // recipBoxModel (if not null) holds recipient list of send message page
    property var recipBoxModel: null

    Component.onCompleted: {
        var currentBoxId = accounts.dbId(acntId)
        messages.fillContactList(foundBoxModel, acntId, currentBoxId)
        if (foundBoxModel.rowCount() === 0) {
            emptyList.visible = true
            filterButton.visible = false
        }
        if (recipBoxModel != null) {
            foundBoxModel.selectEntries(recipBoxModel.boxIds(), true)
        }
        proxyDataboxModel.setSourceModel(foundBoxModel)
    }

    DataboxListModel {
        id: foundBoxModel
        Component.onCompleted: {
        }
    }
    ListSortFilterProxyModel {
        id: proxyDataboxModel
        Component.onCompleted: {
            setFilterRoles([DataboxListModel.ROLE_DB_NAME,
                DataboxListModel.ROLE_DB_ADDRESS, DataboxListModel.ROLE_DB_ID])
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("Contacts")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        Row {
            anchors.verticalCenter: parent.verticalCenter
            spacing: defaultMargin
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            AccessibleOverlaidImageButton {
                id: filterButton
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/magnify.svg"
                accessibleName: qsTr("Filter data boxes.")
                onClicked: {
                    databoxList.anchors.top = filterBar.bottom
                    filterBar.visible = true
                    filterBar.filterField.forceActiveFocus()
                    Qt.inputMethod.show()
                }
            }
        }
    } // PageHeader
    FilterBar {
        id: filterBar
        anchors.top: headerBar.bottom
        isAnyItem: (databoxList.count > 0)
        onTextChanged: {
            proxyDataboxModel.setFilterRegExpStr(text)
        }
        onClearClicked: {
            filterBar.visible = false
            databoxList.anchors.top = headerBar.bottom
            Qt.inputMethod.hide()
        }
    }
    DataboxList {
        id: databoxList
        anchors.top: headerBar.bottom
        anchors.bottom: parent.bottom
        visible: true
        width: parent.width
        model: proxyDataboxModel
        canDetailBoxes: recipBoxModel == null
        canSelectBoxes: recipBoxModel != null
        canDeselectBoxes: recipBoxModel != null
        localContactFormat: true
        onBoxSelect: {
            if (recipBoxModel != null) {
                var boxEntry = foundBoxModel.entry(boxId)
                foundBoxModel.selectEntry(boxEntry.dbID, true)
                isds.addRecipient(acntId, boxEntry.dbID, boxEntry.dbName, boxEntry.dbAddress, false, recipBoxModel)
            }
        }
        onBoxDeselect: {
            if (recipBoxModel != null) {
                foundBoxModel.selectEntry(boxId, false)
                recipBoxModel.removeEntry(boxId)
            }
        }
    } // DataboxList
    AccessibleText {
        id: emptyList
        visible: false
        anchors.top: headerBar.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargin
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.Wrap
        text: qsTr("No databox found in contacts.")
    }
} // Item
