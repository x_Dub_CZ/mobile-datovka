/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

/*
 * Roles are defined in AccountListModel::roleNames() and are accessed directly
 * via their names.
 */

Item {
    id: mainPage

    /* Threshold when the dialogue for banner closing will be shown. */
    property int bannerDlgVisibilityThreshold: 4

    /* Set visibility of page's elements.  */
    function setPageElementVisibility() {
        if (accountList.count <= 0) {
            accountList.visible = false
            emptyList.visible = true
            syncAllButton.enabled = false
            syncAllButton.visible = false
        } else if (accountList.count === 1) {
            accountList.visible = true
            emptyList.visible = false
            /* Disable synchronise all button when having e single account. */
            syncAllButton.enabled = false
            syncAllButton.visible = false
        } else {
            accountList.visible = true
            emptyList.visible = false
            syncAllButton.visible = true
            syncAllButton.enabled = true
        }
    }

    function syncAllAccounts() {
        syncAllButton.enabled = false
        syncAllTimer.start()
        isds.syncAllAccounts()
    }

    Component.onCompleted: {
        setPageElementVisibility()
        bannerPanel.visibilityCnt = settings.bannerVisibilityCount()
        bannerPanel.visible = (bannerPanel.visibilityCnt > 0)
    }

    /*
     * Timer enables sync button after 10s. It is temporary solution.
     * TODO - Better sync all accounts solution must be implemented
     *        in the future.
     */
    Timer {
        id: syncAllTimer
        interval: 10000 // milliseconds
        running: false
        repeat: false
        onTriggered: {
            syncAllButton.enabled = true
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("Accounts")
        isFirstPage: true
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        AccessibleOverlaidImageButton {
            id: settingsButton
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: defaultMargin
            image.sourceSize.height: imageActionDimension
            image.source: "qrc:/ui/menu.svg"
            accessibleName: qsTr("Show application preferences.")
            onClicked: {
                statusBar.visible = false
                pageView.push(pageMenuDatovkaSettings, {"pageView": pageView}, StackView.Immediate)
            }
        }
        AccessibleOverlaidImageButton {
            id: syncAllButton
            visible: false
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            image.sourceSize.height: imageActionDimension
            image.source: "qrc:/ui/sync-all.svg"
            accessibleName: qsTr("Synchronise all accounts.")
            onClicked: syncAllAccounts()
        }
    } //PageHeader
    AccessibleTextButton {
        id: emptyList
        visible: false
        color: (isiOS) ? actionIconColor : datovkaPalette.text
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: headerBar.bottom
        anchors.topMargin: 10*defaultMargin
        wrapMode: Text.Wrap
        font.bold: true
        text: qsTr("Add a new account")
        onClicked: {
            statusBar.visible = false
            pageView.push(pageAccountWizard, {"pageView": pageView}, StackView.Immediate)
        }
    }
    AccountList {
        id: accountList
        anchors.top: headerBar.bottom
        anchors.bottom: bannerPanel.visible ? bannerPanel.top : parent.bottom
        width: parent.width
        visible: true
        model: accountModel
        onCountChanged: {
            setPageElementVisibility()
        }
        property bool downloadStart: false
        onMovementEnded: {
            if (downloadStart) {
                downloadStart = false
                syncAllAccounts()
            }
        }
        onDragEnded: {
            downloadStart = contentY < -120
        }
    }
    Rectangle {
        id: bannerPanel
        visible: false
        property int visibilityCnt: 3
        anchors.bottom: parent.bottom
        width: parent.width
        height: parent.height / 3
        color: "#3ea6ff"
        Image {
            anchors.horizontalCenter: parent.horizontalCenter
            sourceSize.height: parent.height
            source: (mainPage.width*0.8 < mainPage.height) ? "qrc:/banners/mojeID_banner_portrait.svg" : "qrc:/banners/mojeID_banner_landscape.svg"
        }
        MouseArea {
            anchors.fill: parent
            onClicked: Qt.openUrlExternally("https://www.mojeid.cz/cs/proc-mojeid/pristup-ke-sluzbam-verejne-spravy/?utm_source=Aplikace%20Datovka&utm_medium=banner%20v%20aplikaci%20Datovka&utm_campaign=mojeID%20banner%20v%20aplikaci%20Datovka")
        }
        AccessibleOverlaidImageButton {
            z: 1
            anchors.top: parent.top
            anchors.right: parent.right
            width: imageActionDimension * 1.2
            height: width
            image.sourceSize.height: width
            image.source: "qrc:/ui/remove.svg"
            accessibleName: qsTr("Hide banner")
            onClicked: {
                if (bannerPanel.visibilityCnt <= bannerDlgVisibilityThreshold) {
                    messageBannerDlg.open()
                } else {
                    bannerPanel.visible = false
                    settings.setBannerVisibilityCount(bannerPanel.visibilityCnt-1)
                }
            }
        }
        MessageDialog {
            id: messageBannerDlg
            title: qsTr("Permanently hide banner")
            text: qsTr("Do you want to permanently hide the banner?")
            standardButtons: MessageDialog.Yes | MessageDialog.No
            onYes: {
                bannerPanel.visible = false
                settings.setBannerVisibilityCount(-1)
            }
            onNo: {
                bannerPanel.visible = false
                settings.setBannerVisibilityCount(bannerPanel.visibilityCnt-1)
            }
        }
    }
    AcntId {
        id: actionAcntId
    }
    Connections {
        target: isds
        function onRunSyncOneAccountSig(userName, testing) {
            actionAcntId.username = userName
            actionAcntId.testing = testing
            isds.syncOneAccount(actionAcntId)
        }
        function onDownloadMessageListFinishedSig(isMsgReceived, userName, testing) {
            actionAcntId.username = userName
            actionAcntId.testing = testing
            accounts.updateOneAccountCounters(accountModel, actionAcntId)
            if (isMsgReceived) {
                settings.setLastUpdateToNow()
                settings.saveAllSettings(accountModel)
            }
        }
    }
    Timer {
        id: syncAllAfterStartup
        interval: 3000;
        running: false;
        repeat: false
        onTriggered: {
            syncAllAccounts()
        }
    }
    Connections {
        target: settings
        function onRunSyncAfterCleanAppStartSig() {
            syncAllAfterStartup.start()
        }
    }
    Connections {
        target: settings
        function onRunGetPasswordExpirationInfoSig() {
            accounts.getPasswordExpirationInfo()
        }
    }
}
