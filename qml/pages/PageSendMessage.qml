/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.files 1.0
import cz.nic.mobileDatovka.iOsHelper 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Item {
    id: pageSendMessage

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null
    property string msgId
    property int msgType
    property string action: "new"

    /*
     * These properties are constants and specifies some attachment size limits.
     * Do not change these! See ISDS documentation about more message limit info.
     */
    property int max_ATTACHMENT_FILES: 900
    property int max_ATTACHMENT_SIZE_BYTES: 20971520 // = 20 * 1024 * 1024.
    property int max_ATTACHMENT_SIZE_MB: 20
    property int max_OVM_ATTACHMENT_SIZE_BYTES: 52428800 // = 50 * 1024 * 1024.
    property int max_OVM_ATTACHMENT_SIZE_MB: 50

    property string text_COLOR_RED: "#ff0000"

    property bool iOS: false
    property bool boxOVM: false

    /* This property holds total attachment size in bytes */
    property int totalAttachmentSizeBytes: 0

    /* This property holds navigation arrows dimension of tabbar */
    property int tabNavImageDimension: imageNavigateDimension * 0.5

    /* Return file name from file path */
    function getFileNameFromPath(filePath) {
        return filePath.replace(/^.*[\\\/]/, '')
    }

    /* Check attachment number ans size. */
    function attachmentSizeOk() {
        attachmentsSizeLabel.color = datovkaPalette.text
        var attachmentNumber = sendMsgAttachmentModel.rowCount()
        if (attachmentNumber === 0) {
            attachmentsSizeLabel.text = qsTr("Total size of attachments is %1 B.").arg(0)
            return false
        }
        if (attachmentNumber >= max_ATTACHMENT_FILES) {
            attachmentsSizeLabel.text = qsTr("The permitted number of (%1) attachments has been exceeded.").arg(max_ATTACHMENT_FILES)
            attachmentsSizeLabel.color = text_COLOR_RED
            return false
        }
        if (totalAttachmentSizeBytes >= max_OVM_ATTACHMENT_SIZE_BYTES) {
            attachmentsSizeLabel.text = qsTr("Total size of attachments exceeds %1 MB.").arg(max_OVM_ATTACHMENT_SIZE_MB)
            attachmentsSizeLabel.color = text_COLOR_RED
            return false
        }
        if (totalAttachmentSizeBytes >= max_ATTACHMENT_SIZE_BYTES) {
            attachmentsSizeLabel.text = qsTr("Total size of attachments exceeds %1 MB. Most of the data boxes cannot receive messages larger than %1 MB. However, some OVM data boxes can receive message up to %2 MB.").arg(max_ATTACHMENT_SIZE_MB).arg(max_OVM_ATTACHMENT_SIZE_MB)
            attachmentsSizeLabel.color = text_COLOR_RED
            return true
        }
        if (totalAttachmentSizeBytes >= 1024) {
            attachmentsSizeLabel.text = qsTr("Total size of attachments is ~%1 kB.").arg(Math.round(totalAttachmentSizeBytes/1024))
        } else {
            attachmentsSizeLabel.text = qsTr("Total size of attachments is %1 B.").arg(totalAttachmentSizeBytes)
        }
        return true
    }

    /* Set mandate fields in QML */
    function setMandate(isdsEnvelope) {
        dmLegalTitleLaw.text = isdsEnvelope.dmLegalTitleLawStr
        dmLegalTitleYear.text = isdsEnvelope.dmLegalTitleYearStr
        dmLegalTitleSect.text = isdsEnvelope.dmLegalTitleSect
        dmLegalTitlePar.text = isdsEnvelope.dmLegalTitlePar
        dmLegalTitlePoint.text = isdsEnvelope.dmLegalTitlePoint
    }

    /* Set reply message data and add recipient to model */
    function setReplyData(acntId, msgId) {
        headerBar.title = qsTr("Reply %1").arg(msgId)
        // get some message envelope data and add recipient to recipient model
        var isdsEnvelope = messages.getMsgEnvelopeDataAndSetRecipient(acntId, msgId)
        dmAnnotation.text = "Re: " + isdsEnvelope.dmAnnotation
        // swap sender ref and ident data to recipient (reply)
        dmSenderRefNumber.text = isdsEnvelope.dmRecipientRefNumber
        dmSenderIdent.text = isdsEnvelope.dmRecipientIdent
        dmRecipientRefNumber.text = isdsEnvelope.dmSenderRefNumber
        dmRecipientIdent.text = isdsEnvelope.dmSenderIdent
        setMandate(isdsEnvelope)
        var canUseInitReply = false
        if (isdsEnvelope.dmType === "I" || isdsEnvelope.dmType === "A") {
            canUseInitReply = true
        }
        isds.addRecipient(acntId, isdsEnvelope.dbIDSender, isdsEnvelope.dmSender, isdsEnvelope.dmSenderAddress, canUseInitReply, recipBoxModel);
    }

    /* Set message envelope data and files to attachment model*/
    function setTemplateData(acntId, msgId) {
        headerBar.title = qsTr("Forward %1").arg(msgId)
        // get some message envelope data, recipient model must be null (no recipient)
        var isdsEnvelope = messages.getMsgEnvelopeDataAndSetRecipient(acntId, msgId)
        dmAnnotation.text = isdsEnvelope.dmAnnotation
        dmSenderRefNumber.text = isdsEnvelope.dmSenderRefNumber
        dmSenderIdent.text = isdsEnvelope.dmSenderIdent
        dmRecipientRefNumber.text = isdsEnvelope.dmRecipientRefNumber
        dmRecipientIdent.text = isdsEnvelope.dmRecipientIdent
        setMandate(isdsEnvelope)
        sendMsgAttachmentModel.setFromDb(acntId, msgId)
        totalAttachmentSizeBytes = sendMsgAttachmentModel.dataSizeSum()
        attachmentSizeOk()
    }

    /* Set message forward data and add ZFO file to model */
    function setForwardZfoData(acntId, msgId) {
        headerBar.title = qsTr("Forward ZFO %1").arg(msgId)
        var isdsEnvelope = messages.getMsgEnvelopeDataAndSetRecipient(acntId, msgId)
        dmAnnotation.text = "Fwd: " + isdsEnvelope.dmAnnotation
        var fileName = (msgType == MessageType.TYPE_SENT) ? "ODZ_" + msgId + ".zfo" : "DDZ_" + msgId + ".zfo"
        var zfoSize = zfo.getZfoSizeFromDb(acntId, msgId)
        // Zfo file must exist in the local database so zfoSize must be > 0
        if (zfoSize > 0) {
            sendMsgAttachmentModel.appendFileFromPath(FileIdType.DB_ZFO_ID, fileName, "", zfoSize)
            totalAttachmentSizeBytes = sendMsgAttachmentModel.dataSizeSum()
        } else {
            mainPanel.visible = false
            errorText.text = qsTr("ZFO file of message %1 is not available. Download complete message to get it before forwarding.").arg(msgId)
            errorText.visible = true
        }
    }

    /* Append one file into send model */
    function appendFileToSendModel(filePath) {
            var isInFiletList = false
            for (var i = 0; i < sendMsgAttachmentModel.rowCount(); i++) {
                if (sendMsgAttachmentModel.filePathFromRow(i) === filePath) {
                    isInFiletList = true
                    break
                }
            }
            if (!isInFiletList) {
                var fileName = getFileNameFromPath(filePath)
                var fileSizeBytes = files.getAttachmentSizeInBytes(filePath)
                sendMsgAttachmentModel.appendFileFromPath(FileIdType.NO_FILE_ID,
                    fileName, filePath, fileSizeBytes)
                totalAttachmentSizeBytes = sendMsgAttachmentModel.dataSizeSum()
            }
    }

    /* Append file list into send model */
    function appendFilesToSendModel(pathListModel) {
        var listLength = pathListModel.count
        for (var j = 0; j < listLength; ++j) {
            appendFileToSendModel(pathListModel.get(j).path)
        }
        pathListModel.clear()
    }

    Component.onCompleted: {
        iOS = iOSHelper.isIos()
        boxOVM = accounts.isOvm(acntId)
        var dbID = accounts.dbId(acntId);
        var dbType = accounts.dbType(acntId);
        databoxInfo.text = qsTr("Databox: %1 (%2)").arg(dbID).arg(dbType)
        dmAllowSubstDelivery.visible = boxOVM

        isds.doIsdsAction("initSendMsgDlg", acntId);

        if (action === "new") {
            headerBar.title = qsTr("New message")
        } else if (action === "reply") {
            setReplyData(acntId, msgId)
        } else if (action === "template") {
            setTemplateData(acntId, msgId)
        } else if (action == "fwdzfo") {
            setForwardZfoData(acntId, msgId)
        }
    }

    Component.onDestruction: {
       if (iOS) {
            iOSHelper.clearSendAndTmpDirs()
       }
    }

    /* File dialog for choose of files from the storage */
    FileDialogue {
        id: fileDialogue
        multiSelect: true
        onFinished: appendFilesToSendModel(pathListModel)
    }

    /* Holds send message recipent list model */
    DataboxListModel {
        id: recipBoxModel
        Component.onCompleted: {
        }
    }

    /* Holds send message attachment list model */
    FileListModel {
        id: sendMsgAttachmentModel
        Component.onCompleted: {
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("Create message")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        AccessibleOverlaidImageButton {
            id: actionButton
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            image.sourceSize.height: imageActionDimension
            image.source: "qrc:/ui/send-msg.svg"
            accessibleName: qsTr("Send message")
            onClicked: {
                if (mainPanel.areReguiredFieldsFilled()) {
                    actionButton.enabled = false
                    mainPanel.doSendRequest()
                } else {
                    var pText = "\n" +
                        qsTr("Before sending a data message you must specify a subject, at least one recipient and at least one attached file. The following fields are missing:") +
                        "\n\n"
                    if (dmAnnotation.text.toString() === "") {
                        pText += "- " + qsTr("message subject") + "\n"
                    }
                    if (recipBoxModel.rowCount() <= 0) {
                        pText += "- " + qsTr("recipient data box") + "\n"
                    }
                    if (!attachmentSizeOk()) {
                        pText += "- " + qsTr("attachment file or files") + "\n"
                    }
                    pText += "\n" + qsTr("Please fill in the respective fields before attempting to send the message.")
                    popupText.text = pText
                    popup.open()
                }
            }
        }
    } // PageHeader
    Popup {
        id: popup
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        width: parent.width / 1.25
        height: parent.height / 1.5
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside | Popup.CloseOnReleaseOutside
        padding: 10
        contentItem: Item {
            AccessibleText {
                id: popupTitle
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.left: parent.left
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
                text: qsTr("Send Message Error")
            }
            Text {
                id: popupText
                anchors.top: popupTitle.bottom
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.bottom: popupButton.top
                wrapMode: Text.Wrap
            }
            Rectangle {
                id: popupButton
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.left: parent.left
                height: inputItemHeight
                color: datovkaPalette.window
                AccessibleButton {
                    text: qsTr("Close")
                    height: inputItemHeight
                    font.pointSize: defaultTextFont.font.pointSize
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: {
                        popup.close()
                    }
                 }
            }
        }
    }
    AccessibleText {
        id: errorText
        visible: false
        anchors.centerIn: parent
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.Wrap
        text: qsTr("Error during message creation.")
    }
    Item {
        id: mainPanel
        anchors.top: headerBar.bottom
        anchors.bottom: parent.bottom
        width: parent.width

        AcntId {
            id: actionAcntId
        }
        Connections {
            target: isds
            function onRunSendMessageSig(userName, testing) {
                actionButton.enabled = false
                actionAcntId.username = userName
                actionAcntId.testing = testing
                isds.sendMessage(actionAcntId, msgId, dmAnnotation.text.toString(),
                    recipBoxModel, sendMsgAttachmentModel,
                    dmLegalTitleLaw.text.toString(), dmLegalTitleYear.text.toString(),
                    dmLegalTitleSect.text.toString(), dmLegalTitlePar.text.toString(),
                    dmLegalTitlePoint.text.toString(), dmToHands.text.toString(),
                    dmRecipientRefNumber.text.toString(), dmRecipientIdent.text.toString(),
                    dmSenderRefNumber.text.toString(), dmSenderIdent.text.toString(),
                    false, dmPublishOwnID.checked, dmAllowSubstDelivery.checked,
                    dmPersonalDelivery.checked)
            }
        }

        function doSendRequest() {
            isds.doIsdsAction("sendMessage", acntId)
        }

        /* Enable send message button if all required fields are filled */
        function areReguiredFieldsFilled() {
            return (attachmentSizeOk() && (dmAnnotation.text.toString() !== "") && (recipBoxModel.rowCount() > 0))
        }

        Rectangle {
            id: previous
            z: 2
            visible: tabBar.currentIndex !== 0
            anchors.top: parent.top
            anchors.left: parent.left
            width: tabNavImageDimension + defaultMargin
            height: tabBar.height
            color: datovkaPalette.window
            OverlaidImage {
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: defaultMargin
                image.sourceSize.height: tabNavImageDimension
                image.source: "qrc:/ui/back.svg"
            }
            MouseArea {
                anchors.fill: parent
                Accessible.role: Accessible.Button
                Accessible.name: qsTr("Previous")
                Accessible.onPressAction: tabBar.decrementCurrentIndex()
                onClicked: tabBar.decrementCurrentIndex()
            }
        }
        TabBar {
            id: tabBar
            z: 1
            width: parent.width
            Repeater {
                // do not change tab order!
                model: [qsTr("General"), qsTr("Recipients"), qsTr("Attachments"), qsTr("Additional"), qsTr("Text")]
                AccessibleTabButton {
                    text: modelData
                    width: Math.max(100, tabBar.width / 5)
                }
            }
        } // TabBar
        Rectangle {
            id: next
            z: 2
            visible: tabBar.currentIndex !== tabBar.count-1
            anchors.top: parent.top
            anchors.right: parent.right
            width: tabNavImageDimension + defaultMargin
            height: tabBar.height
            color: datovkaPalette.window
            OverlaidImage {
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: defaultMargin
                image.sourceSize.height: tabNavImageDimension
                image.source: "qrc:/ui/next.svg"
            }
            MouseArea {
                anchors.fill: parent
                Accessible.role: Accessible.Button
                Accessible.name: qsTr("Next")
                Accessible.onPressAction: tabBar.incrementCurrentIndex()
                onClicked: tabBar.incrementCurrentIndex()
            }
        } // Item
        StackLayout {
            width: parent.width
            anchors.top: tabBar.bottom
            anchors.bottom: parent.bottom
            currentIndex: tabBar.currentIndex
            onCurrentIndexChanged: {
                tabGeneral.contentY = 0
                tabAdditional.contentY = 0
            }
//----SUBJECT SECTION------------
            Flickable {
                id: tabGeneral
                z: 0
                Layout.fillWidth: true
                Layout.fillHeight: true
                contentHeight: generalContent.implicitHeight
                Pane {
                    id: generalContent
                    anchors.fill: parent
                    Column {
                        anchors.right: parent.right
                        anchors.left: parent.left
                        spacing: formItemVerticalSpacing
                        AccessibleText {
                            color: datovkaPalette.mid
                            wrapMode: Text.Wrap
                            horizontalAlignment: Text.AlignHCenter
                            width: parent.width
                            text: qsTr("Fill in the subject of the message, select at least one recipient data box and specify at least one attachment file or put in a short text message before sending the message.")
                        }
                        Text {
                            text: " "
                        }
                        AccessibleText {
                            font.bold: true
                            text: qsTr("Sender account")
                        }
                        AccessibleText {
                            text: acntName + " (" + acntId.username + ")"
                        }
                        AccessibleText {
                            id: databoxInfo
                            width: parent.width
                            wrapMode: Text.Wrap
                        }
                        Text {
                            text: " "
                        }
                        AccessibleText {
                            font.bold: true
                            text: qsTr("Subject")
                        }
                        AccessibleTextField {
                            id: dmAnnotation
                            width: parent.width
                            height: inputItemHeight
                            font.pointSize: defaultTextFont.font.pointSize
                            placeholderText: qsTr("Enter subject. Mandatory")
                            InputLineMenu {
                                id: subjectMenu
                                inputTextControl: dmAnnotation
                                isPassword: false
                            }
                            onPressAndHold: {
                                if (settings.useExplicitClipboardOperations()) {
                                    subjectMenu.implicitWidth = computeMenuWidth(subjectMenu)
                                    subjectMenu.open()
                                }
                            }
                        }
                        AccessibleSwitch {
                            id: dmPublishOwnID
                            width: parent.width
                            text: qsTr("Include sender identification")
                            font.pointSize: defaultTextFont.font.pointSize
                            checked: true
                        }
                        AccessibleSwitch {
                            id: dmPersonalDelivery
                            width: parent.width
                            text: qsTr("Personal delivery")
                            font.pointSize: defaultTextFont.font.pointSize
                            checked: false
                        }
                        AccessibleSwitch {
                            id: dmAllowSubstDelivery
                            visible: false
                            width: parent.width
                            text: qsTr("Allow acceptance through fiction")
                            font.pointSize: defaultTextFont.font.pointSize
                            checked: true
                        }
                    } // Column
                } // Pane
            } // Flickable
//----RECIPIENTS SECTION------------
            Item {
                id: tabRecipients
                Item {
                    id: buttonMenu
                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: inputItemHeight
                    Row {
                        spacing: formItemVerticalSpacing * 5
                        anchors.horizontalCenter: parent.horizontalCenter
                        AccessibleButton {
                            id: addContact
                            height: inputItemHeight
                            font.pointSize: defaultTextFont.font.pointSize
                            text: qsTr("Add")
                            onClicked: {
                                pageView.push(pageContactList, {
                                        "pageView": pageView,
                                        "acntId": acntId,
                                        "recipBoxModel": recipBoxModel
                                    }, StackView.Immediate)
                            }
                        }
                        AccessibleButton {
                            id: findDS
                            height: inputItemHeight
                            font.pointSize: defaultTextFont.font.pointSize
                            text: qsTr("Find")
                            onClicked: {
                                pageView.push(pageDataboxSearch, {
                                        "pageView": pageView,
                                        "acntId": acntId,
                                        "recipBoxModel": recipBoxModel
                                    }, StackView.Immediate)
                            }
                        }
                    } // Row
                } // Item
                DataboxList {
                    id: recipientList
                    anchors.top: buttonMenu.bottom
                    anchors.bottom: parent.bottom
                    visible: true
                    width: parent.width
                    height: 600
                    model: recipBoxModel
                    isSendMsgRecipList: true
                    canRemoveBoxes: true
                    localContactFormat: true
                    onBoxRemove: {
                        recipBoxModel.removeEntry(boxId)
                    }
                    onPdzDmType: {
                        recipBoxModel.setPdzDmType(boxId, dmType)
                        /* Commercial message is initiatory - dmType 73. */
                        if (dmType === 73) {
                            /* Sender reference number must be filled. */
                            if (dmSenderRefNumber.text === "") {
                                tabBar.setCurrentIndex(3)
                                dmSenderRefNumberLabel.color = "red"
                                dmSenderRefNumber.placeholderText = qsTr("Mandatory for initiatory message")
                                dmSenderRefNumber.forceActiveFocus()
                            }
                        /* Commercial message is reply on initiatory - dmType 79. */
                        } else if (dmType === 79) {
                            /* Recipient reference number must be filled. */
                            if (dmRecipientRefNumber.text === "") {
                                tabBar.setCurrentIndex(3)
                                dmRecipientRefNumberLabel.color = "red"
                                dmRecipientRefNumber.placeholderText = qsTr("Mandatory for reply message")
                                dmRecipientRefNumber.forceActiveFocus()
                            }
                        }
                    }
                } // DataboxList
            }
//----ATTACHMENT SECTION------------
            Item {
                id: tabAttachments
                Connections {
                    target: iOSHelper
                    function onSendFilesSelectedSig(sendFilePaths) {
                        for (var j = 0; j < sendFilePaths.length; ++j) {
                            appendFileToSendModel(sendFilePaths[j])
                        }
                    }
                }
                AccessibleButton {
                    id: addFile
                    height: inputItemHeight
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pointSize: defaultTextFont.font.pointSize
                    text: qsTr("Add file")
                    onClicked: {
                        iOS ? iOSHelper.openDocumentPickerControllerForImport(IosImportAction.IMPORT_SEND,[]) : fileDialogue.raise(qsTr("Select files"), ["*.*"], true, "")
                    }
                }
                AccessibleText {
                    id: attachmentsSizeLabel
                    anchors.left: parent.left
                    anchors.leftMargin: defaultMargin
                    anchors.top: addFile.bottom
                    anchors.topMargin: defaultMargin
                    anchors.right: parent.right
                    anchors.rightMargin: defaultMargin
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.Wrap
                    font.pointSize: textFontSizeSmall
                    text: qsTr("Total size of attachments is %1 B.").arg(totalAttachmentSizeBytes)
                }
                ScrollableListView {
                    id: attachmentListSend
                    delegateHeight: baseHeaderHeight * 1.3
                    anchors.top: attachmentsSizeLabel.bottom
                    anchors.bottom: parent.bottom
                    visible: true
                    width: parent.width
                    model: sendMsgAttachmentModel
                    delegate: Rectangle {
                        id: attachmentItem
                        width: parent.width
                        height: attachmentListSend.delegateHeight
                        color: datovkaPalette.base
                        Rectangle {
                            visible: (0 === index)
                            anchors.top: parent.top
                            anchors.right: parent.right
                            height: 1
                            width: parent.width
                            color: datovkaPalette.dark
                        }
                        Image {
                            id: imageAttachment
                            anchors.left: parent.left
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.leftMargin: defaultMargin
                            sourceSize.height: imageActionDimension * 1.6
                            source: files.getAttachmentFileIcon(rFileName)
                        }
                        ColumnLayout {
                            id: databoxColumn
                            anchors.left: imageAttachment.right
                            anchors.leftMargin: defaultMargin
                            anchors.right: parent.right
                            anchors.verticalCenter: parent.verticalCenter
                            spacing: defaultMargin
                            Text {
                                id: fileNameId
                                Layout.preferredWidth: parent.width - nextNavElement.width
                                elide: Text.ElideRight
                                text: rFileName
                                color: datovkaPalette.text
                                font.bold: true
                            }
                            Text {
                                text: if (rFilePath != "") {
                                    iOS ? iOSHelper.getShortSendFilePath(rFilePath) : rFilePath
                                } else {
                                    qsTr("Local database")
                                }
                                color: datovkaPalette.mid
                                font.pointSize: textFontSizeSmall
                                Layout.preferredWidth: parent.width - nextNavElement.width
                                elide: Text.ElideRight
                            }
                            Text {
                                id: fileSizeId
                                Layout.preferredWidth: parent.width - nextNavElement.width
                                text: rFileSizeStr
                                color: imageDarkerColor
                                font.pointSize: textFontSizeSmall
                            }
                        }
                        MouseArea {
                            function handleClick() {
                                // fileId is set and is valid, use files from database
                                if (rFileId > 0) {
                                    if (files.isZfoFile(rFileName)) {
                                        pageView.push(pageMessageDetail, {
                                            "pageView": pageView,
                                            "fromLocalDb": false,
                                            "rawZfoContent": files.getFileRawContentFromDb(acntId, rFileId)})
                                    } else {
                                        files.openAttachmentFromDb(acntId, rFileId)
                                    }
                                } else if (rFileId == FileIdType.DB_ZFO_ID)  {
                                        pageView.push(pageMessageDetail, {
                                            "pageView": pageView,
                                            "fromLocalDb": false,
                                            "rawZfoContent": zfo.getZfoContentFromDb(acntId, msgId)})
                                } else {
                                    if (files.isZfoFile(getFileNameFromPath(rFilePath))) {
                                        pageView.push(pageMessageDetail, {
                                            "pageView": pageView,
                                            "fromLocalDb": false,
                                            "rawZfoContent": files.rawFileContent(rFilePath)})
                                    } else {
                                        files.openAttachment(getFileNameFromPath(rFilePath), files.rawFileContent(rFilePath))
                                    }
                                }
                            }
                            anchors.fill: parent
                            Accessible.role: Accessible.Button
                            Accessible.name: qsTr("Open file '%1'.").arg(rFileName)
                            Accessible.onPressAction: {
                                handleClick()
                            }
                            onClicked: {
                                handleClick()
                            }
                        }
                        NextOverlaidImage {
                            id: nextNavElement
                            image.source: "qrc:/ui/remove.svg"
                            MouseArea {
                                function handleClick() {
                                    sendMsgAttachmentModel.removeItem(index)
                                    totalAttachmentSizeBytes = sendMsgAttachmentModel.dataSizeSum()
                                }
                                anchors.fill: parent
                                Accessible.role: Accessible.Button
                                Accessible.name: qsTr("Remove file '%1' from list.").arg(rFileName)
                                Accessible.onScrollDownAction: attachmentListSend.scrollDown()
                                Accessible.onScrollUpAction: attachmentListSend.scrollUp()
                                Accessible.onPressAction: {
                                    handleClick()
                                }
                                onClicked: {
                                    handleClick()
                                }
                            }
                        } // Rectangle
                        Rectangle {
                            anchors.bottom: parent.bottom
                            anchors.right: parent.right
                            height: 1
                            width: (isiOS && attachmentListSend.count-1 !== index) ? fileNameId.width + nextNavElement.width : parent.width
                            color: (attachmentListSend.count-1 === index) ? datovkaPalette.dark : datovkaPalette.mid
                        }
                    } // Rectangle
                    ScrollIndicator.vertical: ScrollIndicator {}
                } // Listview
            }
//----ADDITIONALS SECTION------------
            Flickable {
                id: tabAdditional
                z: 0
                Layout.fillWidth: true
                Layout.fillHeight: true
                contentHeight: additonalsContent.implicitHeight
                Pane {
                    id: additonalsContent
                    anchors.fill: parent
                    Column {
                        anchors.right: parent.right
                        anchors.left: parent.left
                        spacing: formItemVerticalSpacing
    //---Mandate---------------------
                        AccessibleText {
                            font.bold: true
                            text: qsTr("Mandate")
                        }
                        Row {
                            width: parent.width
                            spacing: defaultMargin
                            AccessibleTextField {
                                id: dmLegalTitleLaw
                                maximumLength: 4
                                width: defaultTextFont.boundingRect.width * this.maximumLength
                                height: inputItemHeight
                                inputMethodHints: Qt.ImhDigitsOnly
                                font.pointSize: defaultTextFont.font.pointSize
                            }
                            AccessibleText {
                                anchors.verticalCenter: parent.verticalCenter
                                text: "/"
                            }
                            AccessibleTextField {
                                id: dmLegalTitleYear
                                maximumLength: 4
                                width: defaultTextFont.boundingRect.width * this.maximumLength
                                height: inputItemHeight
                                inputMethodHints: Qt.ImhDigitsOnly
                                font.pointSize: defaultTextFont.font.pointSize
                            }
                            AccessibleText {
                                anchors.verticalCenter: parent.verticalCenter
                                text: "§"
                            }
                            AccessibleTextField {
                                id: dmLegalTitleSect
                                maximumLength: 4
                                width: defaultTextFont.boundingRect.width * this.maximumLength
                                height: inputItemHeight
                                font.pointSize: defaultTextFont.font.pointSize
                            }
                            AccessibleText {
                                anchors.verticalCenter: parent.verticalCenter
                                text: qsTr("par.")
                            }
                            AccessibleTextField {
                                id: dmLegalTitlePar
                                maximumLength: 2
                                width: defaultTextFont.boundingRect.width * this.maximumLength
                                height: inputItemHeight
                                inputMethodHints: Qt.ImhDigitsOnly
                                font.pointSize: defaultTextFont.font.pointSize
                            }
                            AccessibleText {
                                anchors.verticalCenter: parent.verticalCenter
                                text: qsTr("let.")
                            }
                            AccessibleTextField {
                                id: dmLegalTitlePoint
                                maximumLength: 2
                                width: defaultTextFont.boundingRect.width * this.maximumLength
                                height: inputItemHeight
                                font.pointSize: defaultTextFont.font.pointSize
                            }
                        }
    //---Our ref.number---------------------
                        AccessibleText {
                            id: dmSenderRefNumberLabel
                            font.bold: true
                            text: qsTr("Our reference number")
                        }
                        AccessibleTextField {
                            id: dmSenderRefNumber
                            height: inputItemHeight
                            font.pointSize: defaultTextFont.font.pointSize
                            placeholderText: qsTr("Enter our reference number")
                            width: parent.width
                            InputLineMenu {
                                id: r2c2M
                                inputTextControl: dmSenderRefNumber
                                isPassword: false
                            }
                            onPressAndHold: {
                                if (settings.useExplicitClipboardOperations()) {
                                    r2c2M.implicitWidth = computeMenuWidth(r2c2M)
                                    r2c2M.open()
                                }
                            }
                        }
    //---Our file mark---------------------
                        AccessibleText {
                            font.bold: true
                            text: qsTr("Our file mark")
                        }
                        AccessibleTextField {
                            id: dmSenderIdent
                            height: inputItemHeight
                            font.pointSize: defaultTextFont.font.pointSize
                            placeholderText: qsTr("Enter our file mark")
                            width: parent.width
                            InputLineMenu {
                                id: r3c2M
                                inputTextControl: dmSenderIdent
                                isPassword: false
                            }
                            onPressAndHold: {
                                if (settings.useExplicitClipboardOperations()) {
                                    r3c2M.implicitWidth = computeMenuWidth(r3c2M)
                                    r3c2M.open()
                                }
                            }
                        }
    //---Your ref.number---------------------
                        AccessibleText {
                            id: dmRecipientRefNumberLabel
                            font.bold: true
                            text: qsTr("Your reference number")
                        }
                        AccessibleTextField {
                            id: dmRecipientRefNumber
                            height: inputItemHeight
                            font.pointSize: defaultTextFont.font.pointSize
                            placeholderText: qsTr("Enter your reference number")
                            width: parent.width
                            InputLineMenu {
                                id: r4c2M
                                inputTextControl: dmRecipientRefNumber
                                isPassword: false
                            }
                            onPressAndHold: {
                                if (settings.useExplicitClipboardOperations()) {
                                    r4c2M.implicitWidth = computeMenuWidth(r4c2M)
                                    r4c2M.open()
                                }
                            }
                        }
    //---Your file mark---------------------
                        AccessibleText {
                            font.bold: true
                            text: qsTr("Your file mark")
                        }
                        AccessibleTextField {
                            id: dmRecipientIdent
                            height: inputItemHeight
                            font.pointSize: defaultTextFont.font.pointSize
                            placeholderText: qsTr("Enter your file mark")
                            width: parent.width
                            InputLineMenu {
                                id: r5c2M
                                inputTextControl: dmRecipientIdent
                                isPassword: false
                            }
                            onPressAndHold: {
                                if (settings.useExplicitClipboardOperations()) {
                                    r5c2M.implicitWidth = computeMenuWidth(r5c2M)
                                    r5c2M.open()
                                }
                            }
                        }
    //---To hands---------------------
                        AccessibleText {
                            font.bold: true
                            text: qsTr("To hands")
                        }
                        AccessibleTextField {
                            id: dmToHands
                            height: inputItemHeight
                            font.pointSize: defaultTextFont.font.pointSize
                            placeholderText: qsTr("Enter name")
                            width: parent.width
                            InputLineMenu {
                                id: r6c2M
                                inputTextControl: dmToHands
                                isPassword: false
                            }
                            onPressAndHold: {
                                if (settings.useExplicitClipboardOperations()) {
                                    r6c2M.implicitWidth = computeMenuWidth(r6c2M)
                                    r6c2M.open()
                                }
                            }
                        }
                    } // Column
                 } // Pane
            } // Flickable
//----TEXT MESSAGE SECTION------------
            Flickable {
                id: tabTextMessage
                z: 0
                Layout.fillWidth: true
                Layout.fillHeight: true
                contentHeight: textMessageContent.implicitHeight
                Pane {
                    id: textMessageContent
                    anchors.fill: parent
                    Column {
                        anchors.right: parent.right
                        anchors.left: parent.left
                        spacing: formItemVerticalSpacing
                        AccessibleText {
                            color: datovkaPalette.mid
                            wrapMode: Text.Wrap
                            horizontalAlignment: Text.AlignHCenter
                            width: parent.width
                            text: qsTr("Here you can create a short text message and add it to attachments as a PDF file.") + "\n" +
                                qsTr("Add the message to attachments after you have finished editing the text.")
                        }
                        Text {
                            text: " "
                        }
                        RowLayout {
                            width: parent.width
                            spacing: formItemVerticalSpacing
                            AccessibleText {
                                font.bold: true
                                text: qsTr("Text message")
                            }
                            Item {
                                Layout.fillWidth: true
                            }
                            AccessibleOverlaidImageButton {
                                enabled: textMessage.text.length > 0
                                image.sourceSize.height: imageActionDimension
                                image.source: "qrc:/ui/eye.svg"
                                accessibleName: qsTr("View PDF")
                                onClicked: {
                                    files.viewPDF(textMessage.text)
                                }
                            }
                            AccessibleOverlaidImageButton {
                                id: appendPdfImage
                                enabled: textMessage.text.length > 0
                                image.sourceSize.height: imageActionDimension
                                image.source: "qrc:/ui/paper-clip-plus.svg"
                                accessibleName: qsTr("Append PDF")
                                onClicked: {
                                    var filePath = files.appendPDF(textMessage.text)
                                    if (filePath !== "") {
                                        appendFileToSendModel(filePath)
                                        tabBar.setCurrentIndex(2)
                                        Qt.inputMethod.hide()
                                    }
                                }
                            }
                        }
                        TextArea {
                            id: textMessage
                            width: parent.width
                            color: datovkaPalette.text
                            font.pointSize: defaultTextFont.font.pointSize
                            wrapMode: Text.Wrap
                            placeholderText: qsTr("Enter a short text message.")
                            padding: defaultMargin
                            background: Rectangle {
                                implicitWidth: parent.width
                                implicitHeight: parent.width / 2
                                color: textMessage.activeFocus ? datovkaPalette.base : lightDarkBgColor
                                border.color: textMessage.activeFocus ? textHighlightColor : datovkaPalette.dark
                            }
                        }
                    } // Column
                 } // Pane
            } // Flickable
        } //StackLayout
    } // Item
    Connections {
        target: isds
        function onSentMessageFinished(closePage) {
            if (closePage) {
                pageView.pop(StackView.Immediate)
            } else {
                actionButton.enabled = true
            }
        }
        function onInitSendMsgDlgSig(userName, testing) {
            actionAcntId.username = userName
            actionAcntId.testing = testing
            var boxOVM = accounts.isOvm(actionAcntId)
            if (boxOVM) {
                return
            }
            var dbID = accounts.dbId(actionAcntId);
            isds.getPdzInfo(actionAcntId, dbID);
        }
        function onGetPdzInfoSig(pdzInfo) {
            databoxInfo.text = databoxInfo.text + pdzInfo;
        }
    } // Connections
} // Item
