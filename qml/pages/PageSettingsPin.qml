/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0

Item {
    id: pageSettingsPin

    /* These properties must be set by caller. */
    property var pageView

    Component.onCompleted: {
        currentPIN = settings.pinValue()
        actionName = "unspecified" /* Causes onChanged() to fire. */
    }
    property string currentPIN: ""
    property string actionName: "" /* Initial value left blank. */
    onActionNameChanged : { /* Handles visibility on actionName changes. */
        if (actionName == "unspecified") {
            acceptElement.visible = false
            if (currentPIN == "") {
                topLineText.text = qsTr("Currently there is no PIN code set.")
                setPinButton.visible = true
                changePinButton.visible = false
                disablePinButton.visible = false
            } else {
                topLineText.text = qsTr("You will be asked to enter a PIN code on application start-up.")
                setPinButton.visible = false
                changePinButton.visible = true
                disablePinButton.visible = true
            }
            pinValueField.visible = false
            pinConfirmField1.visible = false
            pinConfirmField2.visible = false
            errLineText.visible = false

            lockIntervalLabel.visible = true
            lockIntervalSpinBox.visible = true

            lockIntervalLabel.enabled = currentPIN != ""
            lockIntervalSpinBox.enabled = currentPIN != ""
            lockIntervalSpinBox.setVal(settings.inactivityInterval())
        } else if (actionName == "new") {
            acceptElement.visible = true
            topLineText.text = qsTr("Enter a new PIN code into both text fields:")
            setPinButton.visible = false
            changePinButton.visible = false
            disablePinButton.visible = false
            pinValueField.visible = false
            pinConfirmField1.visible = true
            pinConfirmField2.visible = true
            errLineText.visible = false

            lockIntervalLabel.visible = false
            lockIntervalSpinBox.visible = false

            pinConfirmField1.focus = true
        } else if (actionName == "change") {
            acceptElement.visible = true
            topLineText.text = qsTr("In order to change the PIN code you must enter the current and a new PIN code:")
            setPinButton.visible = false
            changePinButton.visible = false
            disablePinButton.visible = false
            pinValueField.visible = true
            pinValueField.focus = true
            pinConfirmField1.visible = true
            pinConfirmField2.visible = true
            errLineText.visible = false

            lockIntervalLabel.visible = false
            lockIntervalSpinBox.visible = false

            pinValueField.focus = true
        } else if (actionName == "disable") {
            acceptElement.visible = true
            topLineText.text = qsTr("In order to change the PIN code you must enter the current and a new PIN code:")
            setPinButton.visible = false
            changePinButton.visible = false
            disablePinButton.visible = false
            pinValueField.visible = true
            pinConfirmField1.visible = false
            pinConfirmField2.visible = false
            errLineText.visible = false

            lockIntervalLabel.visible = false
            lockIntervalSpinBox.visible = false

            pinValueField.focus = true
        } else {
            /* This line should not be reached. */
            topLineText.text = qsTr("Something went wrong!")
        }
    }
    PageHeader {
        id: headerBar
        title: qsTr("PIN Settings")
        onBackClicked: {
            if (actionName != "unspecified") {
                /* Navigate to initial action. */
                actionName = "unspecified"
            } else {
                pageView.pop(StackView.Immediate)
            }
        }
        Row {
            anchors.verticalCenter: parent.verticalCenter
            spacing: defaultMargin
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            AccessibleOverlaidImageButton {
                id: acceptElement
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/checkbox-marked-circle.svg"
                accessibleName: qsTr("Accept changes")
                onClicked: {
                    if (actionName == "unspecified") {
                        if (currentPIN == "") {
                            settings.setInactivityInterval(0)
                            locker.setInactivityInterval(0)
                        } else {
                            settings.setInactivityInterval(lockIntervalSpinBox.val())
                            locker.setInactivityInterval(lockIntervalSpinBox.val())
                        }
                        pageView.pop(StackView.Immediate)
                    /* set new pin code */
                    } else if (actionName == "new") {
                        if (pinConfirmField1.text === "" || pinConfirmField2.text === "") {
                            errLineText.text = qsTr("Error: Both new PIN code fields must be filled in!")
                            errLineText.visible = true
                            if (pinConfirmField1.text === "") {
                                pinConfirmField1.focus = true
                            } else if (pinConfirmField2.text === "") {
                                pinConfirmField2.focus = true
                            }
                        } else if (pinConfirmField2.text !== pinConfirmField1.text) {
                            pinConfirmField2.focus = true
                            errLineText.text = qsTr("Error: Newly entered PIN codes are different!")
                            errLineText.visible = true
                        } else {
                            errLineText.visible = false
                            settings.updatePinSettings(pinConfirmField1.text.toString())
                            settings.saveAllSettings(accountModel)
                            pageView.pop(StackView.Immediate)
                        }
                    /* change current pin code */
                    } else if (actionName == "change") {
                        if (pinValueField.text === currentPIN) {
                            errLineText.visible = false
                            if (pinConfirmField1.text === "" || pinConfirmField2.text === "") {
                                errLineText.text = qsTr("Error: Both new PIN code fields must be filled in!")
                                errLineText.visible = true
                                if (pinConfirmField1.text === "") {
                                    pinConfirmField1.focus = true
                                } else if (pinConfirmField2.text === "") {
                                    pinConfirmField2.focus = true
                                }
                            } else if (pinConfirmField2.text !== pinConfirmField1.text) {
                                pinConfirmField2.focus = true
                                errLineText.text = qsTr("Error: Newly entered PIN codes are different!")
                                errLineText.visible = true
                            } else {
                                errLineText.visible = false
                                settings.updatePinSettings(pinConfirmField1.text.toString())
                                settings.saveAllSettings(accountModel)
                                pageView.pop(StackView.Immediate)
                            }
                        } else {
                            pinValueField.text = ""
                            pinValueField.focus = true
                            errLineText.text = qsTr("Error: Current PIN code is wrong!")
                            errLineText.visible = true
                        }
                    /* remove/disable current pin code */
                    } else if (actionName === "disable") {
                        if (pinValueField.text === currentPIN) {
                            errLineText.visible = false
                            settings.updatePinSettings("")
                            settings.saveAllSettings(accountModel)
                            settings.setInactivityInterval(0)
                            locker.setInactivityInterval(0)
                            pageView.pop(StackView.Immediate)
                        } else {
                            pinValueField.text = ""
                            pinValueField.focus = true
                            errLineText.text = qsTr("Error: Current PIN code is wrong!")
                            errLineText.visible = true
                        }
                    }
                }
            }
        }
    } // PageHeader
    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                AccessibleText {
                    id: topLineText
                    anchors.horizontalCenter: parent.horizontalCenter
                    color: datovkaPalette.mid
                    width: parent.width
                    text: ""
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.Wrap
                }
                AccessibleButton {
                    id: setPinButton
                    height: inputItemHeight
                    font.pointSize: defaultTextFont.font.pointSize
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Set PIN")
                    onClicked: {
                        actionName = "new"
                    }
                }
                AccessibleButton {
                    id: changePinButton
                    height: inputItemHeight
                    font.pointSize: defaultTextFont.font.pointSize
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Change PIN")
                    onClicked: {
                        actionName = "change"
                    }
                }
                AccessibleButton {
                    id: disablePinButton
                    height: inputItemHeight
                    font.pointSize: defaultTextFont.font.pointSize
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Disable PIN")
                    onClicked: {
                        actionName = "disable"
                    }
                }
                AccessibleTextField {
                    id: pinValueField
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: inputItemHeight
                    font.pointSize: defaultTextFont.font.pointSize
                    echoMode: TextInput.Password
                    passwordMaskDelay: 500 // milliseconds
                    inputMethodHints: Qt.ImhDigitsOnly
                    placeholderText: qsTr("Current PIN code")
                    horizontalAlignment: TextInput.AlignHCenter
                    text: ""
                }
                AccessibleTextField {
                    id: pinConfirmField1
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: inputItemHeight
                    font.pointSize: defaultTextFont.font.pointSize
                    echoMode: TextInput.Password
                    passwordMaskDelay: 500 // milliseconds
                    inputMethodHints: Qt.ImhDigitsOnly
                    placeholderText: qsTr("New PIN code")
                    horizontalAlignment: TextInput.AlignHCenter
                    text: ""
                }
                AccessibleTextField {
                    id: pinConfirmField2
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: inputItemHeight
                    font.pointSize: defaultTextFont.font.pointSize
                    echoMode: TextInput.Password
                    passwordMaskDelay: 500 // milliseconds
                    inputMethodHints: Qt.ImhDigitsOnly
                    placeholderText: qsTr("Confirm new PIN code")
                    horizontalAlignment: TextInput.AlignHCenter
                    text: ""
                }
                AccessibleText {
                    id: errLineText
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.bold: true
                    width: parent.width
                    text: ""
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.Wrap
                }
                AccessibleText {
                    id: lockIntervalLabel
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    text: qsTr("Lock after seconds of inactivity")
                    horizontalAlignment: Text.AlignHCenter
                }
                AccessibleSpinBoxZeroMax {
                    /* Holds value in seconds. */
                    id: lockIntervalSpinBox

                    anchors.horizontalCenter: parent.horizontalCenter

                    /* Must be a non-decreasing list ending with infinity. */
                    items: [15, 30, 60, 90, 120, 150, 180, qsTr("don't lock")]
                    dfltIdx: 0

                    accessibleDescription: qsTr("Select the number of seconds after which the application is going to be locked.")

                    onValueModified: {
                        /* Enable accept button. Always visible? */
                        acceptElement.visible = val() !== settings.inactivityInterval()
                    }
                }
            } // Column layout
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
} // Item

