/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import cz.nic.mobileDatovka 1.0

Item {
    id: pageSettingsRecordsManagement

    /* These properties must be set by caller. */
    property var pageView
    /* Remember last service url from settings */
    property string lastUrlFromSettings: ""

    /* Enable info and clear buttons if url and token fields are filled */
    function areUrlandTokenFilled() {
        infoButton.enabled = (urlTextField.text.toString() !== "" && tokenTextField.text.toString() !== "")
        clearButton.enabled = infoButton.enabled
    }

    /* Get optimal logo size base on visible screen size */
    function getOptimalLogoSize() {
        var logoSize = pageSettingsRecordsManagement.height / 4
        return (logoSize < imageActionDimension) ? imageActionDimension : logoSize
    }

    /* Clear all data and info */
    function clearAll() {
        urlTextField.clear()
        tokenTextField.clear()
        serviceInfoSection.visible = false
        areUrlandTokenFilled()
        acceptElement.visible = true
    }

    Component.onCompleted: {
        serviceInfoSection.visible = false
        urlTextField.text = settings.rmUrl()
        lastUrlFromSettings = settings.rmUrl()
        tokenTextField.text = settings.rmToken()
        areUrlandTokenFilled()
        recordsManagement.loadStoredServiceInfo()
    }

    PageHeader {
        id: headerBar
        title: qsTr("Records management")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        Row {
            anchors.verticalCenter: parent.verticalCenter
            spacing: defaultMargin
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            AccessibleOverlaidImageButton {
                id: acceptElement
                visible: false
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/checkbox-marked-circle.svg"
                accessibleName: qsTr("Accept changes")
                onClicked: {
                    settings.setRmUrl(urlTextField.text)
                    settings.setRmToken(tokenTextField.text)
                    recordsManagement.updateServiceInfo(urlTextField.text, lastUrlFromSettings, serviceName.text, tokenName.text)
                    pageView.pop(StackView.Immediate)
                }
            }
        }
    } // PageHeader
    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing * 3
                Column {
                    id: serviceInputSection
                    width: parent.width
                    //spacing: formItemVerticalSpacing
                    AccessibleText {
                        id: userNote
                        color: datovkaPalette.mid
                        wrapMode: Text.Wrap
                        horizontalAlignment: Text.AlignHCenter
                        width: parent.width
                        text: qsTr("Please fill in service URL and your identification token. Click the '%1' button to log into records management service. You should receive valid service info. Finally, click the top right button to save the settings.").arg(infoButton.text)
                    }
                    Text {
                        text: " "
                    }
                    AccessibleText {
                        text: qsTr("Service URL")
                    }
                    AccessibleTextField {
                        id: urlTextField
                        width: parent.width
                        height: inputItemHeight
                        font.pointSize: defaultTextFont.font.pointSize
                        placeholderText: qsTr("Enter service url")
                        InputLineMenu {
                            id: urlMenu
                            inputTextControl: urlTextField
                            isPassword: false
                        }
                        onPressAndHold: {
                            if (settings.useExplicitClipboardOperations()) {
                                urlMenu.implicitWidth = computeMenuWidth(urlMenu)
                                urlMenu.open()
                            }
                        }
                        onTextChanged: {
                            areUrlandTokenFilled()
                        }
                    }
                    AccessibleText {
                        text: qsTr("Your token")
                    }
                    AccessibleTextField {
                        id: tokenTextField
                        width: parent.width
                        height: inputItemHeight
                        font.pointSize: defaultTextFont.font.pointSize
                        placeholderText: qsTr("Enter your token")
                        InputLineMenu {
                            id: tokenMenu
                            inputTextControl: tokenTextField
                            isPassword: false
                        }
                        onPressAndHold: {
                            if (settings.useExplicitClipboardOperations()) {
                                tokenMenu.implicitWidth = computeMenuWidth(tokenMenu)
                                tokenMenu.open()
                            }
                        }
                        onTextChanged: {
                            areUrlandTokenFilled()
                        }
                    }
                    Row {
                        spacing: formItemVerticalSpacing * 5
                        anchors.horizontalCenter: parent.horizontalCenter
                        AccessibleButton {
                            id: infoButton
                            height: inputItemHeight
                            font.pointSize: defaultTextFont.font.pointSize
                            text: qsTr("Get service info")
                            onClicked: {
                                serviceInfoSection.visible = recordsManagement.callServiceInfo(urlTextField.text, tokenTextField.text)
                                serviceInfoError.visible = !serviceInfoSection.visible
                                acceptElement.visible = serviceInfoSection.visible
                            }
                        }
                        AccessibleButtonWithImage {
                            id: clearButton
                            height: inputItemHeight
                            accessibleName: qsTr("Clear records management data")
                            source: "qrc:/ui/remove.svg"
                            sourceHeight: imageActionDimension
                            onClicked: {
                                clearAll()
                            }
                        }
                    } // Row
                } // Column
                AccessibleText {
                    id: serviceInfoError
                    visible: false
                    wrapMode: Text.Wrap
                    horizontalAlignment: Text.AlignHCenter
                    width: parent.width
                    text: qsTr("Communication error. Cannot obtain records management info from server. Internet connection failed or service url and identification token can be wrong!")
                }
                Grid {
                    id: serviceInfoSection
                    columns: 2
                    spacing: formItemVerticalSpacing
                    AccessibleText {
                        font.pointSize: defaultTextFont.font.pointSize
                        text: qsTr("Service name") + ":"
                    }
                    AccessibleText {
                        id: serviceName
                        font.pointSize: defaultTextFont.font.pointSize
                        font.bold: true
                        text: ""
                    }
                    AccessibleText {
                        font.pointSize: defaultTextFont.font.pointSize
                        text: qsTr("Service token") + ":"
                    }
                    AccessibleText {
                        id: tokenName
                        font.pointSize: defaultTextFont.font.pointSize
                        font.bold: true
                        text: ""
                    }
                    AccessibleText {
                        height: imageActionDimension
                        font.pointSize: defaultTextFont.font.pointSize
                        verticalAlignment: Text.AlignVCenter
                        text: qsTr("Service logo") + ":"
                    }
                    AccessibleImageButton {
                        id: serviceLogo
                        width: getOptimalLogoSize()
                        height: serviceLogo.width
                        source: "qrc:/ui/briefcase.svg"
                        cache: false
                        accessibleName: qsTr("Records management logo")
                    }
                } // Grid
            } // Column
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
    Connections {
        target: recordsManagement
        function onServiceInfo(srName, srToken) {
            if (srName !== "" && srToken !== "") {
                serviceInfoSection.visible = true
                acceptElement.visible = true
                serviceName.text = srName
                tokenName.text = srToken
                serviceInfoError.visible = false
                serviceLogo.source = "image://images/rmlogo.svg"
            } else {
                serviceInfoSection.visible = false
            }
        }
    }
} // Item
