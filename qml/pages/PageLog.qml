/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import cz.nic.mobileDatovka 1.0

Item {
    id: pageLog

    /* These properties must be set by caller. */
    property var pageView

    property string logFileLocation: ""
    property string logFilePath: ""

    /* Return file name from file path */
    function getFileNameFromPath(filePath) {
        return filePath.replace(/^.*[\\\/]/, '')
    }

    Component.onCompleted: {
        var logTxt = log.loadLogContent(logFilePath)
        logContent.text = qsTr("Log from current run:") + "\n\n" + logTxt
        actionButton.enabled = (logContent.text !== "")
        logFileLocation = log.getLogFileLocation()
    }

    /* File dialog for choose of files from the storage */
    FileDialogue {
        id: fileDialogue
        multiSelect: false
        onFinished: {
            if (pathListModel.count === 1) {
                logContent.clear()
                logFilePath = pathListModel.get(pathListModel.count-1).path
                var logTxt = log.loadLogContent(logFilePath)
                logContent.text = qsTr("Log file") + ": " + getFileNameFromPath(logFilePath) + "\n\n" + logTxt
                actionButton.enabled = (logContent.text !== "")
            }
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("Log Viewer")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        AccessibleOverlaidImageButton {
            id: actionButton
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            image.sourceSize.height: imageActionDimension
            image.source: "qrc:/ui/send-msg.svg"
            accessibleName: qsTr("Send log to helpdesk")
            onClicked: log.sendLogViaEmail(logContent.text)
        }
    }
    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: buttonMenu.top
        contentHeight: flickContent.implicitHeight
        height: parent.height * 0.8
        Pane {
            id: flickContent
            anchors.fill: parent
            background: Rectangle {
                color: datovkaPalette.base
            }
            TextEdit {
                id: logContent
                color: datovkaPalette.text
                anchors.fill: parent
                readOnly: true
                wrapMode: Text.WordWrap
                font.pointSize: textFontSizeSmall
            }
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
    Rectangle {
        id: buttonMenu
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        height: inputItemHeight
        color: datovkaPalette.window
        AccessibleButton {
            visible: true
            text: qsTr("Choose log file")
            height: inputItemHeight
            font.pointSize: defaultTextFont.font.pointSize
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                fileDialogue.raise("Choose log file", ["*.log"], true, logFileLocation)
            }
         }
    }
} // Item
