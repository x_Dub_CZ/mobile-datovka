/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQml 2.2
import QtQuick 2.7
import QtQuick.Controls 2.2
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.iOsHelper 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0
import cz.nic.mobileDatovka.qmlInteraction 1.0

Item {
    id: mainLayout

    /* These properties must be set by caller. */
    property var pageView

    property string restorePath: ""
    property var backupType: BackupRestoreData.BRT_UNKNOWN
    property bool iOS: false

    Component.onCompleted: {
        iOS = iOSHelper.isIos()
        backup.stopWorker(true)
    }

    Component.onDestruction: {
        backup.stopWorker(false)
    }

    BackupRestoreData {
        id: backup
    }

    BackupRestoreSelectionModel {
        id: restoreSelectionModel
        Component.onCompleted: {
        }
        onDataChanged: {
            includeAllAccounts.checked = (restoreSelectionModel.rowCount() > 0) && (restoreSelectionModel.numSelected() == restoreSelectionModel.rowCount())
        }
    }

    function showBusyIndicator(visible) {
        busyElement.visible = visible
        headerBar.enabled = !visible
        selectJsonButton.enabled = !visible
    }

    function canRestore() {
        restoreButton.visible = backup.canRestoreSelectedAccounts(restoreSelectionModel, restorePath, includeZfoDb.checked)
    }

    function parseJSONFile(jsonFilePath) {
        actionInfoText.text = ""
        sizeInfoText.text = ""
        restoreAccountList.visible = false
        includeAllAccounts.visible = false
        restoreButton.visible = false
        restoreSelectionModel.clearAll()
        restorePath = jsonFilePath
        backupType = backup.loadBackupJson(restorePath)
        if (backupType === BackupRestoreData.BRT_BACKUP) {
            backup.fillRestoreModel(restoreSelectionModel)
            restoreAccountList.visible = true
            includeAllAccounts.visible = true
        }
    }


    FileDialogue {
        id: fileDialogue
        onFinished: {
            includeZfoDb.visible = false
            var listLength = pathListModel.count
            if (listLength > 0) {
                if (pathListModel.get(listLength-1).path !== "") {
                    parseJSONFile(pathListModel.get(listLength-1).path)
                }
            }
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("Restore data")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        Row {
            anchors.verticalCenter: parent.verticalCenter
            spacing: defaultMargin
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            AccessibleOverlaidImageButton {
                id: restoreButton
                visible: false
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/checkbox-marked-circle.svg"
                accessibleName: qsTr("Proceed with restoration")
                onClicked: {
                    showBusyIndicator(true)
                    if (backupType === BackupRestoreData.BRT_TRANSFER) {
                        backup.restoreDataFromTransfer()
                    } else if (backupType === BackupRestoreData.BRT_BACKUP) {
                        backup.restoreSelectedAccounts(restoreSelectionModel, restorePath, includeZfoDb.checked)
                    }
                    showBusyIndicator(false)
                }
            }
        }
    }
    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                width: parent.width
                spacing: defaultMargin
                AccessibleText {
                    anchors.topMargin: defaultMargin
                    color: datovkaPalette.mid
                    wrapMode: Text.Wrap
                    horizontalAlignment: Text.AlignHCenter
                    width: parent.width
                    text: qsTr("The action allows to restore data from a backup or transfer file. Select the backup or transfer JSON file.")
                }
                AccessibleButton {
                    id: selectJsonButton
                    text: qsTr("Choose JSON file")
                    height: inputItemHeight
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pointSize: defaultTextFont.font.pointSize
                    onClicked: fileDialogue.raise("Select JSON", ["*.json"], true, "")
                }
                Connections {
                    target: iOSHelper
                    function onJsonFilesSelectedSig(jsonFilePaths) {
                        if (jsonFilePaths.length > 0) {
                            parseJSONFile(jsonFilePaths[0])
                        }
                    }
                }
                Text {
                    id: backupDateText
                    color: datovkaPalette.text
                    width: parent.width
                    wrapMode: Text.Wrap
                }
                AccessibleText {
                    id: actionInfoText
                    width: parent.width
                    wrapMode: Text.Wrap
                    font.bold: true
                }
                AccessibleText {
                    id: sizeInfoText
                    width: parent.width
                }
                AccessibleSwitch {
                    id: includeZfoDb
                    visible: false
                    text: qsTr("Restore ZFO data")
                    font.pointSize: defaultTextFont.font.pointSize
                    checked: false
                    onClicked: canRestore()
                }
                AccessibleSwitch {
                    id: includeAllAccounts
                    visible: false
                    text: qsTr("Select all accounts")
                    font.pointSize: defaultTextFont.font.pointSize
                    checked: false
                    onClicked: {
                        restoreSelectionModel.setAllSelected(includeAllAccounts.checked)
                        canRestore()
                    }
                }
                ScrollableListView {
                    id: restoreAccountList
                    visible: false
                    delegateHeight: baseHeaderHeight
                    width: parent.width
                    height: 300
                    model: restoreSelectionModel
                    delegate: Rectangle {
                        id: uploadItem
                        color: (!rSelected) ? datovkaPalette.base : normalAccountBgColor
                        height: restoreAccountList.delegateHeight
                        width: parent.width
                        Rectangle {
                            visible: (0 === index)
                            anchors.top: parent.top
                            anchors.right: parent.right
                            height: 1
                            width: parent.width
                            color: datovkaPalette.dark
                        }
                        Text {
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.left: parent.left
                            anchors.leftMargin: defaultMargin
                            width: parent.width - defaultMargin
                            elide: Text.ElideRight
                            color: datovkaPalette.text
                            text: rAcntName + "\n" + rUserName + "     boxID: " + rBoxId
                        }
                        MouseArea {
                            function handleClick() {
                                restoreSelectionModel.setSelected(index, !rSelected)
                                canRestore()
                            }
                            anchors.fill: parent
                            Accessible.role: Accessible.Button
                            Accessible.name: (!rSelected) ? qsTr("Select %1.").arg(rAcntName) : qsTr("Deselect %1.").arg(rAcntName)
                            Accessible.onScrollDownAction: restoreAccountList.scrollDown()
                            Accessible.onScrollUpAction: restoreAccountList.scrollUp()
                            Accessible.onPressAction: {
                                handleClick()
                            }
                            onClicked: {
                                handleClick()
                            }
                        }
                        Rectangle {
                            anchors.bottom: parent.bottom
                            anchors.right: parent.right
                            height: 1
                            width: (isiOS && restoreAccountList.count-1 !== index) ? parent.width - defaultMargin : parent.width
                            color: (restoreAccountList.count-1 === index) ? datovkaPalette.dark : datovkaPalette.mid
                        }
                    }
                }
            }
            Connections {
                target: backup
                function onBacupDateTextSig(backupDateTime) {
                    backupDateText.text = backupDateTime
                }
            }
            Connections {
                target: backup
                function onActionTextSig(actionInfo) {
                    actionInfoText.text = actionInfo
                }
            }
            Connections {
                target: backup
                function onSizeTextSig(sizeInfo) {
                    sizeInfoText.text = sizeInfo
                }
            }
            Connections {
                target: backup
                function onVisibleZfoSwitchSig(visible) {
                    includeZfoDb.visible = visible
                }
            }
            Connections {
                target: backup
                function onCanRestoreFromTransferSig(restore) {
                    restoreButton.visible = restore
                }
            }
        }
    }
    Rectangle {
        id: busyElement
        visible: false
        anchors.fill: parent
        color: datovkaPalette.text
        opacity: 0.2
        z: 2
        BusyIndicator {
            anchors.centerIn: parent
            running: true
        }
    }
}
