/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQml 2.2
import QtQuick 2.7
import QtQuick.Controls 2.2
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Item {
    id: mainLayout

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null
    property int msgType
    property string msgId
    property var messageModel: null

    Component.onCompleted: {
        uploadHierarchyProxyModel.setSourceModel(uploadHierarchyListModel)
        uploadHierarchyProxyModel.sortContent()
        timer.start()
    }

    function downloadUploadHierarchy() {
        hierarchyButton.enabled = false
        recordsManagement.callUploadHierarchy(uploadHierarchyListModel)
        hierarchyButton.enabled = true
    }

    Timer {
        id: timer
        interval: 500 // milliseconds
        running: false
        repeat: false
        onTriggered: {
            downloadUploadHierarchy()
        }
    }

    FontMetrics {
        id: fontMetrics
        font: selectedLocations.text
    }

    function viewSelectedNodeNames() {
        var nameList = uploadHierarchyListModel.selectedFullNames(true)
        selectedLocations.text = nameList.join("\n")

        var vertPos = 0.0
        if (selectedLocationsView.contentHeight > selectedLocationsView.height) {
            /* Navigate to bottom. */
            vertPos = 1.0 - (selectedLocationsView.height / selectedLocationsView.contentHeight);
        }
        selectedLocationsView.ScrollBar.vertical.position = vertPos
    }

    UploadHierarchyListModel {
        id: uploadHierarchyListModel
        onDataChanged: {
            uploadButton.enabled = (uploadHierarchyListModel.selectedIds().length > 0)
            viewSelectedNodeNames()
        }
        onModelReset: {
            uploadButton.enabled = (uploadHierarchyListModel.selectedIds().length > 0)
            uploadHierarchyProxyModel.sortContent()
            locationLabel.text = uploadHierarchyListModel.navigatedRootName(true, "/")
            viewSelectedNodeNames()
        }
        Component.onCompleted: {
        }
    }

    UploadHierarchyQmlProxyModel {
        id: uploadHierarchyProxyModel
        Component.onCompleted: {
            setFilterRole(UploadHierarchyListModel.ROLE_FILTER_DATA_RECURSIVE)
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("Upload message %1").arg(msgId)
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        Row {
            anchors.verticalCenter: parent.verticalCenter
            spacing: defaultMargin
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            AccessibleOverlaidImageButton {
                id: searchButton
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/magnify.svg"
                accessibleName: qsTr("Filter upload hierarchy.")
                onClicked: {
                    filterBar.visible = true
                    uploadHierarchyList.anchors.top = filterBar.bottom
                    filterBar.filterField.forceActiveFocus()
                    Qt.inputMethod.show()
                }
            }
        }
    }

    Pane {
        id: controlPane
        anchors.top: headerBar.bottom
        width: parent.width

        Column {
            width: parent.width

            AccessibleText {
                id: topText
                anchors.topMargin: defaultMargin
                color: datovkaPalette.mid
                wrapMode: Text.Wrap
                horizontalAlignment: Text.AlignHCenter
                width: parent.width
                text: qsTr("The message can be uploaded into selected locations in the records management hierarchy.")
            }
            Row {
                id: buttonRow
                spacing: formItemVerticalSpacing * 5
                anchors.horizontalCenter: parent.horizontalCenter
                AccessibleButtonWithImage {
                    id: hierarchyButton
                    height: inputItemHeight
                    accessibleName: qsTr("Update upload hierarchy")
                    source: "qrc:/ui/sync.svg"
                    sourceHeight: imageActionDimension
                    onClicked: {
                        downloadUploadHierarchy()
                    }
                }
                AccessibleButtonWithImage {
                    id: uploadButton
                    enabled: false
                    height: inputItemHeight
                    accessibleName: qsTr("Upload message")
                    source: "qrc:/ui/upload.svg"
                    sourceHeight: imageActionDimension
                    onClicked: {
                        if (recordsManagement.uploadMessage(acntId, msgId, msgType, uploadHierarchyListModel)) {
                            messages.updateRmStatus(messageModel, acntId, msgId, true)
                            pageView.pop(StackView.Immediate)
                        }
                    }
                }
            }
            ScrollView {
                id: selectedLocationsView
                anchors.topMargin: defaultMargin
                anchors.left: parent.left
                anchors.right: parent.right
                clip: true
                height: fontMetrics.height * 5

                ScrollBar.horizontal.policy: ScrollBar.AsNeeded
                ScrollBar.vertical.policy: ScrollBar.AsNeeded

                AccessibleText {
                    id: selectedLocations
                }
            }
            Row {
                id: navigateUpRow
                spacing: formItemVerticalSpacing * 3
                anchors.left: parent.left
                anchors.right: parent.right
                AccessibleButton {
                    id: upButton
                    anchors {
                        verticalCenter: parent.verticalCenter;
                    }
                    text: "<"
                    accessibleName: qsTr("Up") /* Needs to be specified as "<" is not read. */
                    onClicked: {
                        uploadHierarchyListModel.navigateSuper()
                    }
                }
                AccessibleText {
                    id: locationLabel
                    anchors {
                        verticalCenter: parent.verticalCenter;
                    }
                    text: "/"
                }
            }
        }
    }
    Rectangle {
        anchors.top: controlPane.bottom
        anchors.bottom: parent.bottom
        width: parent.width
        color: datovkaPalette.window
        FilterBar {
            id: filterBar
            anchors.top: parent.top
            isAnyItem: (uploadHierarchyList.count > 0)
            onTextChanged: {
                uploadHierarchyProxyModel.setFilterRegExpStr(text)
            }
            onClearClicked: {
                filterBar.visible = false
                uploadHierarchyList.anchors.top = parent.top
                Qt.inputMethod.hide()
            }
        }
        AccessibleText {
            id: emptyHierarchy
            visible: uploadHierarchyList.count <= 0
            anchors.centerIn: parent
            width: parent.width
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.Wrap
            text: (filterBar.filterField.text.length === 0) ?
                qsTr("Upload hierarchy has not been downloaded yet.") :
                qsTr("No hierarchy entry found that matches filter text '%1'.").arg(filterBar.filterField.text)
        }
        ScrollableListView {
            id: uploadHierarchyList
            delegateHeight: baseHeaderHeight
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: parent.width
            model: uploadHierarchyProxyModel
            delegate: Rectangle {
                id: uploadItem
                color: (!rSelected) ? datovkaPalette.base : selectedItemBgColor
                height: uploadHierarchyList.delegateHeight
                width: uploadHierarchyList.width
                Rectangle {
                    visible: (0 === index)
                    anchors.top: parent.top
                    anchors.right: parent.right
                    height: 1
                    width: parent.width
                    color: datovkaPalette.dark
                }
                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: defaultMargin
                    width: parent.width - defaultMargin - nextNavElement.width
                    elide: Text.ElideRight
                    /* Use lighter text if some sub-nodes are selected but not this actual node. */
                    color: (rSelected || (!rSelectedRecursive)) ? datovkaPalette.text : datovkaPalette.mid
                    text: rName
                }
                NextOverlaidImage {
                    id: nextNavElement
                    visible: !rLeaf
                }
                /*
                 * TODO -- Handle situation where non-leaf nodes can also
                 * be selected.
                 */
                MouseArea {
                    function handleClick() {
                        if (!rLeaf) {
                            uploadHierarchyListModel.navigateSub(
                                uploadHierarchyProxyModel.mapToSourceContent(index))
                        } else if (rSelectable) {
                            uploadHierarchyListModel.toggleNodeSelection(
                                uploadHierarchyProxyModel.mapToSourceContent(index))
                        }
                    }

                    anchors.fill: parent

                    Accessible.role: Accessible.Button
                    Accessible.name: {
                        if (!rLeaf) {
                            qsTr("View content of %1.").arg(rName)
                        } else if (rSelectable) {
                            (!rSelected) ? qsTr("Select %1.").arg(rName) : qsTr("Deselect %1.").arg(rName)
                        } else {
                            ""
                        }
                    }
                    Accessible.onScrollDownAction: uploadHierarchyList.scrollDown()
                    Accessible.onScrollUpAction: uploadHierarchyList.scrollUp()
                    Accessible.onPressAction: {
                        handleClick()
                    }
                    onClicked: {
                        handleClick()
                    }
                }
                Rectangle {
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    height: 1
                    width: (isiOS && uploadHierarchyList.count-1 !== index) ? parent.width - defaultMargin : parent.width
                    color: (uploadHierarchyList.count-1 === index) ? datovkaPalette.dark : datovkaPalette.mid
                }
            }
        }
    }
}
