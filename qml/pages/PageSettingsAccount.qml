/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import QtQuick.Dialogs 1.2
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.iOsHelper 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Item {
    id: pageSettingsAccount

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null

    property bool isNewAccount: (acntId == null)
    property int sLoginMethod: AcntData.LIM_UNAME_PWD
    property string oldUserName

    property bool iOS: false
    property bool closePage: false

    property var acntRemembersPwd: false /* Set only once when page content loaded. */

    /* Set some elements on the page based on selected login method */
    function setPageElements(loginMethod) {
        sLoginMethod = loginMethod
        // set default properties
        passwordItem.visible = true
        passwordLabel.visible = passwordItem.visible
        mepCodeItem.visible = !passwordItem.visible
        rememberPassword.visible = passwordItem.visible
        certificateLabel.visible = false
        certPathLabelId.visible = false
        certPathButtonId.visible = false
        if (loginMethod === AcntData.LIM_UNAME_PWD_CRT) {
            certificateLabel.visible = true
            certPathButtonId.visible = true
            certPathLabelId.visible = (certPathLabelId.text !== "")
        } else if (loginMethod === AcntData.LIM_UNAME_MEP) {
            mepCodeItem.visible = true
            passwordItem.visible = !mepCodeItem.visible
            passwordLabel.visible = passwordItem.visible
            rememberPassword.visible = !mepCodeItem.visible
        }
    }

    AcntId {
        id: actionAcntId
    }

    Component.onCompleted: {
            iOS = iOSHelper.isIos()
            if (!isNewAccount) {
                accounts.getAccountData(acntId)
            }
    }

    Component.onDestruction: {
        isds.loginMepCancel()
    }

    FileDialogue {
        id: fileDialogue
        multiSelect: false
        onFinished: {
            var listLength = pathListModel.count
            // set last selected certificate path
            if (listLength > 0) {
                if (pathListModel.get(listLength-1).path === "") {
                    if (certPathLabelId.text === "") {
                        certPathLabelId.visible = false
                    }
                } else {
                    certPathLabelId.visible = true
                    certPathLabelId.text = pathListModel.get(listLength-1).path
                }
            }
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("New account")
        onBackClicked: {
            if (isds.loginMepRunning()) {
                isds.loginMepCancel()
                closePage = true
            } else {
                pageView.pop(StackView.Immediate)
            }
        }
        AccessibleOverlaidImageButton {
            id: actionButton
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            image.sourceSize.height: imageActionDimension
            image.source: "qrc:/ui/checkbox-marked-circle.svg"
            accessibleName: qsTr("Accept changes")
            onClicked: {
                if (Qt.inputMethod.visible) {
                    Qt.inputMethod.commit()
                    Qt.inputMethod.hide()
                }
                if (isNewAccount) {
                    // Create a new account context data and add to model.
                    if (accounts.createAccount(accountModel, sLoginMethod,
                        accountNameItem.textLine.displayText.toString(),
                        userNameItem.textLine.displayText.toString(),
                        passwordItem.pwdTextLine.text.toString(),
                        mepCodeItem.textLine.displayText.toString(),
                        testAccount.checked, rememberPassword.checked,
                        useLS.checked, useSyncWithAll.checked,
                        certPathLabelId.text.toString())) {
                            // Login to new account.
                            actionAcntId.username = userNameItem.textLine.displayText.toString()
                            actionAcntId.testing = testAccount.checked
                            isds.doIsdsAction("addNewAccount", actionAcntId)
                     }
                     if (closePage) {
                        pageView.pop(StackView.Immediate)
                     }
                } else {
                    // Change account user name.
                    if (userNameItem.textLine.displayText.toString() !== oldUserName) {
                        // Create temporary account with a new user name.
                        if (accounts.prepareChangeUserName(accountModel, sLoginMethod,
                              accountNameItem.textLine.displayText.toString(),
                              userNameItem.textLine.displayText.toString(),
                              passwordItem.pwdTextLine.text.toString(),
                              mepCodeItem.textLine.displayText.toString(),
                              testAccount.checked, rememberPassword.checked,
                              useLS.checked, useSyncWithAll.checked,
                              certPathLabelId.text.toString(), oldUserName)) {
                                // Login to isds with the new user name.
                                actionAcntId.username = userNameItem.textLine.displayText.toString()
                                actionAcntId.testing = testAccount.checked
                                isds.doIsdsAction("changeUserName", actionAcntId)
                            }
                    } else {
                        // Update account context data.
                        if (accounts.updateAccount(accountModel, sLoginMethod,
                              accountNameItem.textLine.displayText.toString(),
                              userNameItem.textLine.displayText.toString(),
                              passwordItem.pwdTextLine.text.toString(),
                              mepCodeItem.textLine.displayText.toString(),
                              testAccount.checked, rememberPassword.checked,
                              useLS.checked, useSyncWithAll.checked,
                              certPathLabelId.text.toString())) {
                                  settings.saveAllSettings(accountModel)
                        }
                        pageView.pop(StackView.Immediate)
                    }
                }
            }
            Connections {
                // Connection is activated when account info was downloaded.
                target: isds
                function onDownloadAccountInfoFinishedSig(userName, testing) {
                    settings.saveAllSettings(accountModel)
                    if (isNewAccount)  {
                        pageView.pop(StackView.Immediate)
                    }
                }
                function onRunGetAccountInfoSig(userName, testing) {
                     actionAcntId.username = userName
                     actionAcntId.testing = testing
                     settings.saveAllSettings(accountModel)
                     isds.getAccountInfo(actionAcntId)
                }
                function onRunChangeUserNameSig(userName, testing) {
                    // Login to isds with new user name has been succeeded.
                     actionAcntId.username = userName
                     actionAcntId.testing = testing
                    // Get databox ID of new user name.
                    var dbId = isds.getAccountDbId(actionAcntId)
                    // Change username and rename databases.
                    if (accounts.changeAccountUserName(accountModel,
                        accountNameItem.textLine.text.toString(),
                        userNameItem.textLine.text.toString(),
                        useLS.checked, oldUserName, dbId)) {
                            settings.saveAllSettings(accountModel)
                            isds.getAccountInfo(actionAcntId)
                    }
                }
            }
        }
    } // PageHeader
    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                AccessibleText {
                    id: loginMethodLabel
                    text: qsTr("Login method")
                }
                AccessibleComboBox {
                    id: loginMethodComboBox
                    width: parent.width
                    accessibleDescription: qsTr("Select login method")
                    model: ListModel {
                        id: loginMethodModel
                        /* Key values must be equivalent to constants defined in C++ code! */
                        ListElement { label: qsTr("Username + Password"); key: AcntData.LIM_UNAME_PWD }
                        ListElement { label: qsTr("Certificate + Password"); key: AcntData.LIM_UNAME_PWD_CRT }
                        ListElement { label: qsTr("Password + Security code"); key: AcntData.LIM_UNAME_PWD_HOTP }
                        ListElement { label: qsTr("Password + Security SMS"); key: AcntData.LIM_UNAME_PWD_TOTP }
                        ListElement { label: qsTr("Mobile key"); key: AcntData.LIM_UNAME_MEP }
                    }
                    onCurrentIndexChanged: {
                        setPageElements(currentKey())
                    }
                }
                AccessibleText {
                    id: certificateLabel
                    visible: false
                    text: qsTr("Certificate")
                }
                AccessibleText {
                    id: certPathLabelId
                    visible: false
                    width: parent.width
                    wrapMode: Text.WrapAnywhere
                    text: ""
                    Connections {
                       target: iOSHelper
                        function onCertFilesSelectedSig(certFilePaths) {
                            if (certFilePaths.length > 0) {
                                certPathLabelId.visible = true
                                certPathLabelId.text = certFilePaths[0]
                                fileDialogue.close()
                            }
                        }
                    }
                }
                AccessibleButton {
                    id: certPathButtonId
                    visible: false
                    text: qsTr("Choose file")
                    height: inputItemHeight
                    font.pointSize: defaultTextFont.font.pointSize
                    onClicked: {
                        if (iOS) {
                            iOSHelper.openDocumentPickerControllerForImport(IosImportAction.IMPORT_CERT, [])
                            fileDialogue.raise("Select certificate file", ["*.pem","*.p12","*.pfx"], true, iOSHelper.getCertFileLocation())
                        } else {
                            fileDialogue.raise("Select certificate file", ["*.pem","*.p12","*.pfx"], true, "")
                        }
                    }
                }

                TextLineItem {
                    id: accountNameItem
                    textLineTitle: qsTr("Account title")
                    placeholderText: qsTr("Enter custom account name")
                    isPassword: false
                }
                TextLineItem {
                    id: userNameItem
                    textLineTitle: qsTr("Username")
                    placeholderText: qsTr("Enter the login name")
                    inputMethodHints: Qt.ImhLowercaseOnly | Qt.ImhPreferLowercase | Qt.ImhNoPredictiveText
                    isPassword: false
                    actionButton.image.source: "qrc:/ui/alert.svg"
                    actionButton.accessibleName: qsTr("Wrong username format.")
                    actionButton.onClicked: {
                        messageDlg.title = qsTr("Wrong username format.")
                        messageDlg.text = qsTr("Warning: The username should contain only combinations of lower-case letters and digits.")
                        messageDlg.open()
                    }
                    textLine.onTextChanged: {
                        if (!isLowerCaseWithoutNonPrintableChars(userNameItem.textLine.text)) {
                            userNameItem.textLine.color = "red"
                            userNameItem.actionButtonColor = userNameItem.textLine.color
                            userNameItem.actionButton.visible = true
                        } else {
                            userNameItem.textLine.color = datovkaPalette.text
                            userNameItem.actionButtonColor = actionIconColor
                            userNameItem.actionButton.visible = false
                        }
                    }
                }
                AccessibleText {
                    id: passwordLabel
                    text: qsTr("Password")
                }
                TimedPasswordLine {
                    function askPassword() {
                       pinInputDlg.openInputDialog("showPinRequest", 0, acntId,
                           qsTr("PIN required"), qsTr("Enter PIN code in order to show the password."),
                           qsTr("Enter PIN"), true)
                       return false
                    }

                    id: passwordItem
                    anchors.left: parent.left
                    anchors.right: parent.right
                    placeholderText: qsTr("Enter the password")
                    /* Able to show password: when creating new account, when not remembering password or when PIN is configured. */
                    pwdEyeIconVisible: isNewAccount || (!acntRemembersPwd) || settings.pinConfigured()
                    checkBeforeShowing: (isNewAccount || (!acntRemembersPwd)) ? null : askPassword
                    hideAfterMs: isNewAccount ? 0 : 5000
                }
                TextLineItem {
                    id: mepCodeItem
                    visible: false
                    textLineTitle: qsTr("Communication code")
                    placeholderText: qsTr("Enter the communication code")
                    isPassword: false
                }
                AccessibleSwitch {
                    id: rememberPassword
                    text: qsTr("Remember password")
                    font.pointSize: defaultTextFont.font.pointSize
                    checked: true
                }
                AccessibleSwitch {
                    id: testAccount
                    text: qsTr("Test account")
                    font.pointSize: defaultTextFont.font.pointSize
                }
                AccessibleText {
                    id: testAccountLabel
                    color: datovkaPalette.mid
                    width: parent.width
                    text: qsTr("Test accounts are used to access the ISDS testing environment.")
                    wrapMode: Text.Wrap
                }
                AccessibleSwitch {
                    id: useLS
                    font.pointSize: defaultTextFont.font.pointSize
                    text: qsTr("Use local storage (database)")
                    checked: true
                }
                AccessibleText {
                    color: datovkaPalette.mid
                    width: parent.width
                    text: (useLS.checked ?
                           qsTr("Messages and attachments will be locally stored. No active internet connection is needed to access locally stored data.") :
                           qsTr("Messages and attachments will be stored only temporarily in memory. These data will be lost on application exit."))
                    wrapMode: Text.Wrap
                }
                AccessibleSwitch {
                    id: useSyncWithAll
                    font.pointSize: defaultTextFont.font.pointSize
                    text: qsTr("Synchronise together with all accounts")
                    checked: true
                }
                AccessibleText {
                    color: datovkaPalette.mid
                    width: parent.width
                    text: useSyncWithAll.checked ?
                        qsTr("The account will be included into the synchronisation process of all accounts.") :
                        qsTr("The account won't be included into the synchronisation process of all accounts.")
                    wrapMode: Text.Wrap
                }
                Connections {
                    // Connection is activated when settings of exists account is shown.
                    target: accounts
                    function onSendAccountData(acntName, userName, loginMethod, password,
                        mepToken, isTestAccount, rememberPwd, storeToDisk, syncWithAll, certPath) {
                        accountNameItem.textLine.text = acntName
                        userNameItem.textLine.text = userName
                        passwordItem.pwdTextLine.text = password
                        mepCodeItem.textLine.text = mepToken
                        rememberPassword.checked = rememberPwd
                        testAccount.checked = isTestAccount
                        useLS.checked = storeToDisk
                        useSyncWithAll.checked = syncWithAll
                        certPathLabelId.text = certPath
                        loginMethodComboBox.selectCurrentKey(loginMethod)
                        oldUserName = userName
                        testAccount.enabled = false
                        headerBar.title = qsTr("Account settings")
                        setPageElements(loginMethod)

                        acntRemembersPwd = rememberPwd
                    }
                } // Connection
            } // Column layout
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable

    InputDialogue {
        id: pinInputDlg

        inputAccepted: function checkPinInput() {
            /* Check entered pin and if valid, then show password. */
            if (settings.pinValid(pinInputDlg.inputTextField.text.toString())) {
                passwordItem.show()
            } else {
                /* Notify the user that the password was not valid. */
                messageDlg.title = qsTr("Wrong PIN code.")
                messageDlg.text = qsTr("Entered PIN is not valid.")
                messageDlg.open()
            }
        }
        inputRejected: null /* Do nothing, when dialogue was rejected. */
    }

    MessageDialog {
        id: messageDlg
    }
} // Item
