/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import cz.nic.mobileDatovka 1.0

Item {
    id: pageAboutApp

    /* These properties must be set by caller. */
    property var pageView

    PageHeader {
        id: headerBar
        title: qsTr("About Datovka")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
    }
    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        contentHeight: flickContent.implicitHeight

        /*
         * TODO
         * There are bugs in Qt preventing flicing on iOS when VoiceOver
         * is activated:
         * http://lists.qt-project.org/pipermail/accessibility/2015-March/000073.html
         * https://bugreports.qt.io/browse/QTBUG-41980
         */

        Pane {
            id: flickContent
            anchors.fill: parent
            background: Rectangle {
                color: datovkaPalette.base
            }
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing * 2
                AccessibleImageButton {
                    id: datovkaLogo
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: imageActionDimension * 1.4
                    height: imageActionDimension * 1.4
                    source: "qrc:/datovka.png"
                    accessibleName: qsTr("Open application home page.")
                    onClicked: {
                        Qt.openUrlExternally("http://www.datovka.cz/")
                    }
                }
                AccessibleText {
                    id: pageLabel
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    font.bold: true
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    text: {
                        qsTr("Datovka - free mobile Data-Box client")
                        + "\n" +
                        qsTr("Version: %1").arg(settings.appVersion())
                    }
                }
                AccessibleText {
                    id: infoLabel
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    textFormat: TextEdit.RichText
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    onLinkActivated: Qt.openUrlExternally(link)
                    text: {
                        qsTr("This application provides the means to access data boxes in the ISDS system. It enables you to download and to view the content of messages held within the data box. You can also, with some limitations, send messages from this application.")
                        + "<br/><br/>" +
                        qsTr("You may use this application only at your own risk. The association CZ.NIC is not the operator of the data box system. CZ.NIC is not liable for any damage that may be directly or indirectly caused by using this application.")
                        + "<br/><br/>" +
                        qsTr("If you have problems using this application or want help or you just want more information then start by reading through the information in the <a href=\"%1\">user manual</a> or on the <a href=\"%2\">project web page</a>.")
                            .arg("https://secure.nic.cz/files/datove_schranky/redirect/mobile-manual.html")
                            .arg("https://www.datovka.cz/cs/pages/mobilni-datovka.html")
                     }
                }
                AccessibleText {
                    id: licenceLabel
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    textFormat: TextEdit.RichText
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    onLinkActivated: Qt.openUrlExternally(link)
                    text: qsTr("The application is licensed under the <a href=\"%1\">GNU GPL v3</a>.").arg("https://www.gnu.org/licenses/gpl-3.0-standalone.html")
                }
                AccessibleText {
                    id: cznicLabel
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.bold: true
                    text: qsTr("Powered by")
                }
                AccessibleImageButton {
                    id: cznicLogo
                    anchors.horizontalCenter: parent.horizontalCenter
                    sourceSize.height: imageActionDimension
                    source: "qrc:/cznic.png"
                    accessibleName: qsTr("Open home page of the CZ.NIC association.")
                    onClicked: {
                        Qt.openUrlExternally("http://www.nic.cz/")
                    }
                }
            } // Column layout
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
}
