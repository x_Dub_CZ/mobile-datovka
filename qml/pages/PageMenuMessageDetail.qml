/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.files 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

Item {
    id: mainLayout

    /* These properties must be set by caller. */
    property var pageView
    property string acntName
    property AcntId acntId: null
    property int msgType
    property string msgId
    property var messageModel: null
    property var attachmentModel: null /* The QVariant holds actually a pointer to the model. */

    Component.onCompleted: {
        var index
        if (msgType == MessageType.TYPE_SENT) {
            // Hide some menu items for sent messages
            // Note: remove argument is ListModel item index
            index = messageDetailMenuList.get_model_index(messageDetailMenuListModel, "funcName", "reply");
            if (index >= 0) {
                messageDetailMenuListModel.setProperty(index, "showEntry", false)
            }
        }
        if (!recordsManagement.isValidRecordsManagement()) {
            // Note: remove argument is ListModel item index
            index = messageDetailMenuList.get_model_index(messageDetailMenuListModel, "funcName", "uploadRM");
            if (index >= 0) {
                messageDetailMenuListModel.setProperty(index, "showEntry", false)
            }
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("Message") + ": " + msgId
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
    }

    /* Object (associative array) holding functions. */
    property var funcs: {
        "downlAttch": function callDownlAttch() {
            isds.doIsdsAction("downloadMessage", acntId)
            pageView.pop(StackView.Immediate)
        },
        "reply": function callReply() {
            pageView.replace(pageSendMessage, {
                "pageView": pageView,
                "acntName" : acntName,
                "acntId": acntId,
                "msgId": msgId,
                "msgType": msgType,
                "action": "reply"
            }, StackView.Immediate)
        },
        "forward": function callForward() {
            pageView.replace(pageSendMessage, {
                "pageView": pageView,
                "acntName" : acntName,
                "acntId": acntId,
                "msgId": msgId,
                "msgType": msgType,
                "action": "fwdzfo"
            }, StackView.Immediate)
        },
        "useTemplate": function callUseTemplate() {
            pageView.replace(pageSendMessage, {
                "pageView": pageView,
                "acntName" : acntName,
                "acntId": acntId,
                "msgId": msgId,
                "msgType": msgType,
                "action": "template"
            }, StackView.Immediate)
        },
        "uploadRM": function callUploadRM() {
            pageView.replace(pageRecordsManagementUpload, {
                "pageView": pageView,
                "acntName" : acntName,
                "acntId": acntId,
                "msgId": msgId,
                "msgType": msgType,
                "messageModel": messageModel
            }, StackView.Immediate)
        },
        "sendAttachEmail": function callSendAttachEmail() {
            files.sendMsgFilesWithEmail(acntId, msgId, MsgAttachFlag.MSG_ATTACHS)
            pageView.pop(StackView.Immediate)
        },
        "sendZfoEmail": function callSendZfoEmail() {
            files.sendMsgFilesWithEmail(acntId, msgId, MsgAttachFlag.MSG_ZFO)
            pageView.pop(StackView.Immediate)
        },
        "saveAttachDisk": function callSaveAttachDisk() {
            files.saveMsgFilesToDisk(acntId, msgId, MsgAttachFlag.MSG_ATTACHS)
            pageView.pop(StackView.Immediate)
        },
        "saveZfoDisk": function callSaveZfoDisk() {
            files.saveMsgFilesToDisk(acntId, msgId, MsgAttachFlag.MSG_ZFO)
            pageView.pop(StackView.Immediate)
        },
        "delAttachs": function callDelAttachs() {
            if (files.deleteAttachmentsFromDb(acntId, msgId)) {
                if (attachmentModel != null) {
                    attachmentModel.clearAll()
                }
            }
            pageView.pop(StackView.Immediate)
        }
    }

    ListModel {
        id: messageDetailMenuListModel
        ListElement {
            image: "qrc:/ui/datovka-email-download.svg"
            showEntry: true
            showNext: false
            name: qsTr("Download attachments")
            funcName: "downlAttch"
        }
        ListElement {
            image: "qrc:/ui/reply.svg"
            showEntry: true
            showNext: true
            name: qsTr("Reply")
            funcName: "reply"
        }
        ListElement {
            image: "qrc:/ui/forward.svg"
            showEntry: true
            showNext: true
            name: qsTr("Forward")
            funcName: "forward"
        }
        ListElement {
            image: "qrc:/ui/datovka-email-sample.svg"
            showEntry: true
            showNext: true
            name: qsTr("Use as template")
            funcName: "useTemplate"
        }
        ListElement {
            image: "qrc:/ui/briefcase.svg"
            showEntry: true
            showNext: true
            name: qsTr("Upload to records management")
            funcName: "uploadRM"
        }
        ListElement {
            image: "qrc:/ui/at.svg"
            showEntry: true
            showNext: false
            name: qsTr("Send message zfo by email")
            funcName: "sendZfoEmail"
        }
        ListElement {
            image: "qrc:/ui/at.svg"
            showEntry: true
            showNext: false
            name: qsTr("Send attachments by email")
            funcName: "sendAttachEmail"
        }
        ListElement {
            image: "qrc:/ui/save-to-disk.svg"
            showEntry: true
            showNext: false
            name: qsTr("Save message zfo")
            funcName: "saveZfoDisk"
        }
        ListElement {
            image: "qrc:/ui/save-to-disk.svg"
            showEntry: true
            showNext: false
            name: qsTr("Save attachments")
            funcName: "saveAttachDisk"
        }
        ListElement {
            image: "qrc:/ui/delete.svg"
            showEntry: true
            showNext: false
            name: qsTr("Delete attachments")
            funcName: "delAttachs"
        }
    }

    AccessibleMenu {
        id: messageDetailMenuList
        anchors.top: headerBar.bottom
        anchors.bottom: parent.bottom
        width: parent.width
        funcArr: funcs
        model: messageDetailMenuListModel
    }
}
