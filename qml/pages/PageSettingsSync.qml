/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0

Item {
    id: pageSettingsSync

    /* These properties must be set by caller. */
    property var pageView

    Component.onCompleted: {
        downloadOnlyNewMsgs.checked = settings.downloadOnlyNewMsgs()
        downloadCompleteMsgs.checked = settings.downloadCompleteMsgs()
        syncAfterAppStart.checked = settings.syncAfterCleanAppStart()
    }

    Component.onDestruction: {
        settings.saveAllSettings(accountModel)
    }

    PageHeader {
        id: headerBar
        title: qsTr("Synchronization settings")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
    }
    Flickable {
        id: flickable
        z: 0
        anchors.top: headerBar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        contentHeight: flickContent.implicitHeight
        Pane {
            id: flickContent
            anchors.fill: parent
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                AccessibleSwitch {
                    id: downloadOnlyNewMsgs
                    text: qsTr("Download only newer messages")
                    font.pointSize: defaultTextFont.font.pointSize
                    checked: true
                    onClicked: settings.setDownloadOnlyNewMsgs(downloadOnlyNewMsgs.checked)
                }
                AccessibleText {
                    id: downloadOnlyNewDescription
                    color: datovkaPalette.mid
                    width: parent.width
                    text: (downloadOnlyNewMsgs.checked ?
                           qsTr("Only messages which are not older than 90 days will be downloaded.") :
                           qsTr("All available messages (including those in the data vault) will be downloaded."))
                    wrapMode: Text.Wrap
                }
                AccessibleSwitch {
                    id: downloadCompleteMsgs
                    text: qsTr("Download complete messages")
                    font.pointSize: defaultTextFont.font.pointSize
                    onClicked: settings.setDownloadCompleteMsgs(downloadCompleteMsgs.checked)
                }
                AccessibleText {
                    color: datovkaPalette.mid
                    width: parent.width
                    text: (downloadCompleteMsgs.checked ?
                           qsTr("Complete messages will be downloaded during account synchronisation. This may be slow.") :
                           qsTr("Only message envelopes will be downloaded during account synchronisation."))
                    wrapMode: Text.Wrap
                }
                AccessibleSwitch {
                    id: syncAfterAppStart
                    text: qsTr("Synchronise accounts after start")
                    font.pointSize: defaultTextFont.font.pointSize
                    onClicked: settings.setSyncAfterCleanAppStart(syncAfterAppStart.checked)
                }
                AccessibleText {
                    color: datovkaPalette.mid
                    width: parent.width
                    text: (syncAfterAppStart.checked ?
                           qsTr("Accounts will automatically be synchronised a few seconds after application start-up.") :
                           qsTr("Accounts will be synchronised only when the action is manually invoked."))
                    wrapMode: Text.Wrap
                }
            } // Column layout
        } // Pane
        ScrollIndicator.vertical: ScrollIndicator {}
    } // Flickable
} // Item
