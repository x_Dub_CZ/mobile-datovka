/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.models 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

/*
 * Roles are defined in MessageListModel::roleNames() and are accessed directly
 * via their names.
 */
Item {
    id: pageMessageSearch

    /* These properties must be set by caller. */
    property var pageView

    property int soughtMsgType: MessageType.TYPE_RECEIVED | MessageType.TYPE_SENT

    Component.onCompleted: {
         searchPhraseText.forceActiveFocus()
    }

    MessageListModel {
        id: messageModel
        Component.onCompleted: {
        }
    }

    PageHeader {
        id: headerBar
        title: qsTr("Search message")
        onBackClicked: {
            pageView.pop(StackView.Immediate)
        }
        Row {
            anchors.verticalCenter: parent.verticalCenter
            spacing: defaultMargin
            anchors.right: parent.right
            anchors.rightMargin: defaultMargin
            AccessibleOverlaidImageButton {
                id: actionButton
                anchors.verticalCenter: parent.verticalCenter
                image.sourceSize.height: imageActionDimension
                image.source: "qrc:/ui/magnify.svg"
                accessibleName: qsTr("Search messages.")
                onClicked: {
                    // Set textfield focus on false = hide input keyboard
                    // and obtain textfield value
                    searchPhraseText.focus = false
                    emptyList.visible = searchPanel.doSearchRequest()
                }
            }
        }
    } // PageHeader
    Pane {
        id: searchPanel
        anchors.top: headerBar.bottom
        width: parent.width
        function doSearchRequest() {
            return (messages.searchMsg(messageModel, searchPhraseText.text, soughtMsgType) <= 0)
        }
        Column {
            anchors.right: parent.right
            anchors.left: parent.left
            spacing: formItemVerticalSpacing
            AccessibleComboBox {
                id: searchOptionComboBox
                width: parent.width
                accessibleDescription: qsTr("Select type of sought messages")
                model: ListModel {
                    id: searchOptionComboBoxModel
                    /* Cannot have an expression here because QML complains that it cannot use script for property key. */
                    ListElement { label: qsTr("All messages"); key: -1 }
                    ListElement { label: qsTr("Received messages"); key: MessageType.TYPE_RECEIVED }
                    ListElement { label: qsTr("Sent messages"); key: MessageType.TYPE_SENT }
                }
                onCurrentIndexChanged: {
                    if (currentKey() === -1) {
                        soughtMsgType = MessageType.TYPE_RECEIVED | MessageType.TYPE_SENT
                    } else {
                        soughtMsgType = currentKey()
                    }
                }
            }
            AccessibleTextField {
                id: searchPhraseText
                placeholderText: qsTr("Enter phrase")
                focus: true
                width: parent.width
                font.pointSize: defaultTextFont.font.pointSize
                height: inputItemHeight
                onAccepted: emptyList.visible = searchPanel.doSearchRequest()
                InputLineMenu {
                    id: searchPhraseTextMenu
                    inputTextControl: searchPhraseText
                    isPassword: false
                }
                onPressAndHold: {
                    if (settings.useExplicitClipboardOperations()) {
                        searchPhraseTextMenu.implicitWidth = computeMenuWidth(searchPhraseTextMenu)
                        searchPhraseTextMenu.open()
                    }
                }
            }
        }
    } // Pane
    MessageList {
        id: messageList
        anchors.top: searchPanel.bottom
        anchors.bottom: parent.bottom
        visible: true
        width: parent.width
        model: messageModel
        onMsgClicked: {
            statusBar.visible = false
            messages.markMessageAsLocallyRead(messageModel, acntId, msgId, true)
            pageView.push(pageMessageDetail, {
                    "pageView": pageView,
                    "fromLocalDb": true,
                    "acntName": "",
                    "acntId": acntId,
                    "msgDaysToDeletion": msgDaysToDeletion,
                    "msgType": msgType,
                    "msgId": msgId,
                    "messageModel": messageModel
                })
        }
    } // MessageList
    Text {
        id: emptyList
        visible: false
        anchors.top: searchPanel.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargin
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.Wrap
        text: qsTr("No message found for given search phrase.")
    }
} // Item
