/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.account 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

/*
 * Input dialog for password or OTP code.
 */
Dialog {
    id: root

    property alias inputTextField: rootPwd

    //property int minimumInputSize: parent.width / 2
    property string dIsdsAction
    property var dPwdType
    property var inputAccepted: dfltAccepted
    property var inputRejected: dfltRejected

    AcntId {
        id: actionAcntId
    }

    function openInputDialog(isdsAction, pwdType, acntId, title, text, placeholderText, hidePwd) {
        dIsdsAction = isdsAction
        dPwdType = pwdType
        actionAcntId.username = acntId.username
        actionAcntId.testing = acntId.testing
        root.title = title
        rootText.text = text
        rootPwd.clear()
        rootPwd.placeholderText = placeholderText
        rootPwd.echoMode = (hidePwd) ? TextInput.Password : TextInput.Normal
        if (pwdType === AcntData.LIM_UNAME_PWD_TOTP || pwdType === AcntData.LIM_UNAME_PWD_HOTP) {
            rootPwd.inputMethodHints = Qt.ImhPreferNumbers
        } else {
            rootPwd.inputMethodHints = Qt.ImhNone
        }
        rootPwd.focus = true
        root.open()
    }

    /* Default code to be run when user accepts the dialogue. */
    function dfltAccepted() {
        isds.inputDialogReturnPassword(dIsdsAction, dPwdType, actionAcntId, rootPwd.text.toString())
    }

    /* Default code to be run when user rejects the dialogue. */
    function dfltRejected() {
        isds.inputDialogCancel(dIsdsAction, actionAcntId)
    }

    // center dialogue
    x: parent.width / 2 - width / 2
    y: parent.height / 2 - height / 2

    width: parent.width - 4*defaultMargin

    focus: true
    modal: true
    title: qsTr("QML input dialog")
    standardButtons: Dialog.Ok | Dialog.Cancel

    contentItem: ColumnLayout {
        AccessibleText {
            id: rootText
            Layout.fillWidth: true
            //Layout.minimumWidth: minimumInputSize
            text: ""
            Layout.alignment: Qt.AlignLeft | Qt.AlignBaseline
            wrapMode: Text.WordWrap
        }
        AccessibleTextField {
            id: rootPwd
            focus: true
            Layout.fillWidth: true
            //Layout.minimumWidth: minimumInputSize
            Layout.alignment: Qt.AlignLeft | Qt.AlignBaseline
            echoMode: TextInput.Normal
            horizontalAlignment: TextInput.AlignHCenter
            passwordMaskDelay: 500 // milliseconds
        }
    } // ColumnLayout

    onAccepted: {
        if (root.inputAccepted !== null) {
            root.inputAccepted()
        }
    }
    onRejected: {
        if (root.inputRejected !== null) {
            root.inputRejected()
        }
    }
}
