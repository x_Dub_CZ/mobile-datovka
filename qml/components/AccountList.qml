/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import cz.nic.mobileDatovka 1.0
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

ScrollableListView {
    id: root
    spacing: defaultMargin * 4

    delegate: Item {
        id: accountItem
        width: root.width
        height: baseHeaderHeight * 3

        AcntId {
            id: acntId
            username: rUserName
            testing: rTestAccount
        }

        ColumnLayout {
            spacing: 0
            anchors.fill: parent

            Rectangle { // account description line
                id: accountHeader
                Layout.fillWidth: true
                height: baseHeaderHeight
                color: (isiOS) ? datovkaPalette.base : ((rTestAccount) ? testAccountBgColor : normalAccountBgColor)
                Rectangle {
                    anchors.top: parent.top
                    height: 1
                    width: parent.width
                    color: datovkaPalette.dark
                }
                MenuImage {
                    id: accountImage
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: defaultMargin
                    image.source: "qrc:/ui/account-box.svg"
                }
                Text {
                    id: accountName
                    anchors.left: accountImage.right
                    anchors.leftMargin: defaultMargin
                    anchors.right: syncThisButton.left
                    anchors.verticalCenter: parent.verticalCenter
                    elide: Text.ElideRight
                    text: rAcntName
                    color: (isiOS) ? ((rTestAccount) ? imageDarkerColor : textHighlightColor) : datovkaPalette.text
                    font.bold: true
                    font.italic: !rStoreIntoDb // Ephemeral accounts use italics.
                    font.capitalization: Font.AllUppercase
                }
                Item {
                    anchors.left: parent.left
                    width: parent.width * 0.8
                    height: parent.height
                    MouseArea {
                        function handleClick() {
                            statusBar.visible = false
                            pageView.push(pageMenuAccount, {
                                    "pageView": pageView,
                                    "acntName": rAcntName,
                                    "acntId": acntId
                                }, StackView.Immediate)
                        }
                        anchors.fill: parent
                        Accessible.role: Accessible.Button
                        Accessible.name: !rTestAccount ? qsTr("Account '%1'.").arg(rAcntName) : qsTr("Testing account '%1'.").arg(rAcntName)
                        Accessible.onScrollDownAction: root.scrollDown()
                        Accessible.onScrollUpAction: root.scrollUp()
                        Accessible.onPressAction: {
                            handleClick()
                        }
                        onClicked: {
                            handleClick()
                        }
                    }
                }
                AccessibleOverlaidImageButton {
                    id: syncThisButton
                    anchors.right: parent.right
                    anchors.rightMargin: defaultMargin
                    anchors.verticalCenter: parent.verticalCenter
                    image.sourceSize.height: imageActionDimension
                    image.source: "qrc:/ui/sync.svg"
                    accessibleName: qsTr("Synchronise account '%1'.").arg(rAcntName)
                    onClicked: {
                        isds.doIsdsAction("syncOneAccount", acntId)
                    }
                }
                Rectangle {
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    height: 1
                    color: datovkaPalette.mid
                    width: (isiOS) ? receivedMsgs.width : parent.width
                }
            } // Item account header

            Rectangle { // received messages line
                color: datovkaPalette.base
                Layout.fillWidth: true
                height: baseHeaderHeight
                MenuImage {
                    id: msgRecievedImage
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: defaultMargin
                    image.source: "qrc:/ui/received-msgs.svg"
                }
                Text {
                    id: receivedMsgs
                    anchors.left: msgRecievedImage.right
                    anchors.leftMargin: defaultMargin
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    color: datovkaPalette.text
                    text: qsTr("Received messages") + "   [" + rRcvdUnread + "/" + rRcvdTotal + "]"
                    font.bold: !isiOS
                }
                NextOverlaidImage {}
                MouseArea {
                    function handleClick() {
                        statusBar.visible = false
                        pageView.push(pageMessageList, {
                                "pageView": pageView,
                                "acntName": rAcntName,
                                "acntId": acntId,
                                "msgType": MessageType.TYPE_RECEIVED
                            })
                    }
                    anchors.fill: parent
                    Accessible.role: Accessible.Button
                    Accessible.name: qsTr("Received messages of account '%1'. (Unread %2 of %3 received messages.)").arg(rAcntName).arg(rRcvdUnread).arg(rRcvdTotal)
                    Accessible.onScrollDownAction: root.scrollDown()
                    Accessible.onScrollUpAction: root.scrollUp()
                    Accessible.onPressAction: {
                        handleClick()
                    }
                    onClicked: {
                        handleClick()
                    }
                }
                Rectangle {
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    height: 1
                    width: (isiOS) ? receivedMsgs.width : parent.width
                    color: datovkaPalette.mid
                }
            }
            Rectangle { // sent messages line
                color: datovkaPalette.base
                Layout.fillWidth: true
                height: baseHeaderHeight
                MenuImage {
                    id: msgSentImage
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: defaultMargin
                    image.source: "qrc:/ui/sent-msgs.svg"
                }
                Text {
                    id: sentMsgs
                    anchors.left: msgSentImage.right
                    anchors.leftMargin: defaultMargin
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    color: datovkaPalette.text
                    text: qsTr("Sent messages") + "   [" + rSntTotal + "]"
                    font.bold: !isiOS
                }
                NextOverlaidImage {}
                MouseArea {
                    function handleClick() {
                        statusBar.visible = false
                        pageView.push(pageMessageList, {
                                "pageView": pageView,
                                "acntName": rAcntName,
                                "acntId": acntId,
                                "msgType": MessageType.TYPE_SENT
                            })
                    }
                    anchors.fill: parent
                    Accessible.role: Accessible.Button
                    Accessible.name: qsTr("Sent messages of account '%1'. (Total of %2 sent messages.)").arg(rAcntName).arg(rSntTotal)
                    Accessible.onScrollDownAction: root.scrollDown()
                    Accessible.onScrollUpAction: root.scrollUp()
                    Accessible.onPressAction: {
                        handleClick()
                    }
                    onClicked: {
                        handleClick()
                    }
                }
                Rectangle {
                    anchors.bottom: parent.bottom
                    height: 1
                    width: parent.width
                    color: datovkaPalette.dark
                }
            }

        } //CollumnLayout
    } //Delegate
    ScrollIndicator.vertical: ScrollIndicator {}
}
