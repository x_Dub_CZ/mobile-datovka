/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2
import cz.nic.mobileDatovka 1.0

ScrollableListView {
    id: root
    delegateHeight: listItemHeight

    signal calendarClicked(int index, string dateKey, string dateVal, string dateDescr)

    /*
     * Return asterisks.
     */
    function asterisks(mandatory, userInput, boxInput) {
        if (mandatory && userInput) {
            return " *"
        } else if (boxInput) {
            return " **"
        } else {
            return ""
        }
    }

    delegate: Rectangle {
        id: formItem
        height: root.delegateHeight
        width: parent.width
        enabled: gsUserInput
        color: enabled ? datovkaPalette.base : lightDarkBgColor

        Item {
            anchors.fill: parent
            anchors.margins: defaultMargin
            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                spacing: formItemVerticalSpacing
                anchors.verticalCenter: parent.verticalCenter
                Text {
                    text: gsDescr + asterisks(gsMandatory, gsUserInput, gsBoxInput)
                    color: enabled ? datovkaPalette.text : datovkaPalette.mid // dark
                    font.bold: true
                    width: parent.width
                    wrapMode: Text.Wrap
                }
                AccessibleTextField {
                    id: textField
                    height: inputItemHeight
                    font.pointSize: defaultTextFont.font.pointSize
                    text: gsVal
                    placeholderText: gsPlacehold
                    width: parent.width
                    /*
                     * TODO: Wrong focus on text field when editing date or
                     *       close calendar dialogue. Virtual keyboard is
                     *       always activated. Probably is bug in form model.
                     *       Temporary solution is readonly text field for date.
                     */
                    readOnly: actionButton.visible
                    InputLineMenu {
                        id: icox
                        inputTextControl: textField
                        isPassword: false
                    }
                    onPressAndHold: {
                        if (settings.useExplicitClipboardOperations()) {
                            icox.implicitWidth = computeMenuWidth(icox)
                            icox.open()
                        }
                    }
                    onTextEdited: { // onTextChanged cannot be used here; onEditingFinished ?
                        root.model.setProperty(index, "gsVal", textField.text)
                    }
                    AccessibleOverlaidImageButton {
                        id: actionButton
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.rightMargin: defaultMargin
                        image.sourceSize.height: imageActionDimension
                        image.source: "qrc:/ui/calendar.svg"
                        accessibleName: qsTr("Open calendar")
                        visible: gsTypeDate
                        onClicked: {
                            calendarClicked(index, gsKey, gsVal, gsDescr)
                        }
                    }
                }
            } // Column
        } // Item
    } // Rectangle

    ScrollIndicator.vertical: ScrollIndicator {}
}
