/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

/*
 * Provides a calendar interface.
 * It is defined as a separate component in order not to pollute the namespace
 * with the import of QtQuick.Controls 1.
 */
Calendar {
    id: root
    anchors.centerIn: parent
    focus: true
    weekNumbersVisible: true
    style: CalendarStyle {
        gridVisible: true
        dayDelegate: Rectangle {
            Rectangle {
                anchors.fill: parent
                border.color: "transparent"
                color: styleData.date !== undefined && styleData.selected ? datovkaPalette.highlight : "transparent"
                anchors.margins: styleData.selected ? -1 : 0
            }
            Label {
                text: styleData.date.getDate()
                anchors.centerIn: parent
                font.bold: styleData.selected
                color: styleData.valid ? datovkaPalette.text : datovkaPalette.mid
            }
        }
    }
}
