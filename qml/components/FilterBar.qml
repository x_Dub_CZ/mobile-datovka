/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.1

/*
 * Filter bar component.
 */
Rectangle {
    id: root

    property alias filterField: filterField
    property bool isAnyItem: false

    signal textChanged(string text)
    signal clearClicked()

    z: 1
    visible: false
    width: parent.width
    height: filterField.height
    color: (filterField.text.length === 0) ? datovkaPalette.base : isAnyItem ? "#afffaf" : "#ffafaf"
    border.color: filterField.activeFocus ? textHighlightColor : imageDarkerColor

    AccessibleTextField {
        id: filterField
        anchors.top: parent.top
        width: parent.width - clearFilterButton.width
        placeholderText: qsTr("Set filter")
        font.pointSize: defaultTextFont.font.pointSize
        inputMethodHints: Qt.ImhNoPredictiveText
        Accessible.searchEdit: true
        /*
         * Using explicit top, left, ... padding because plain padding
         * does not centre the text properly.
         */
        topPadding: 8.0
        bottomPadding: 8.0
        leftPadding: 8.0
        rightPadding: 8.0
        /* TODO - remove background if used materials */
        background: Rectangle {
            anchors.fill: parent
            color: "transparent"
            border.color: "transparent"
        }
        onTextChanged: {
            root.textChanged(filterField.text)
        }
    }
    AccessibleOverlaidImageButton {
        id: clearFilterButton
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        image.sourceSize.height: imageNavigateDimension
        image.source: "qrc:/ui/remove.svg"
        accessibleName: qsTr("Clear and hide filter field")
        onClicked: {
            filterField.clear()
            root.clearClicked()
        }
    }
}
