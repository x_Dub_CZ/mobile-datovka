/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2

/*
 * Accessible button component.
 */
Button {
    id: root

    /* These properties must be set by caller. */
    property string accessibleDescription: ""
    property string accessibleName: ""
    property string source: ""
    property int sourceHeight: 0.0

    /*
     * TODO -- Setting the image as root.contentItem causes the image to resize
     * and blur.
     */
    Image {
        anchors.centerIn: parent
        source: root.source
        sourceSize.height: root.sourceHeight
        opacity: enabled || root.highlighted || root.checked ? 1 : 0.3
        /* It looks like the greying-out works also without a ColorOverlay. */
        //ColorOverlay {
        //    anchors.fill: parent
        //    source: parent
        //    color: root.contentItem.color // Current contentItem is a Text item inherited from Button.
        //}
        horizontalAlignment: Image.AlignHCenter
        verticalAlignment: Image.AlignVCenter
    }

    Accessible.role: Accessible.Button
    Accessible.checkable: root.checkable
    Accessible.checked: root.checked
    Accessible.description: root.accessibleDescription
    Accessible.name: (root.accessibleName !== "") ? root.accessibleName : root.text
    Accessible.pressed: root.pressed
    Accessible.onPressAction: {
        root.clicked()
    }
}
