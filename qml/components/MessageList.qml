/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import cz.nic.mobileDatovka.messages 1.0
import cz.nic.mobileDatovka.qmlIdentifiers 1.0

ScrollableListView {
    id: root
    delegateHeight: listItemHeight

    property int msgDeletionNotifyAheadDays: settings.msgDeletionNotifyAheadDays()

    /* These signals should be captured to implement message interaction. */
    signal msgClicked(AcntId acntId, int msgType, string msgId, int msgDaysToDeletion)
    signal msgPressAndHold(AcntId acntId, int msgType, string msgId, bool canDelete)

    delegate: Rectangle {
        id: messageItem

        /* True if listview item is received message. Used for some elements settings. */
        property bool isReceivedMsg : (rMsgType == MessageType.TYPE_RECEIVED)

        height: root.delegateHeight
        width: root.width
        color: (isReceivedMsg && !rReadLocally) ? datovkaPalette.base : lightDarkBgColor

        Component.onCompleted: {
            if ((rMsgDaysToDeletion <= msgDeletionNotifyAheadDays) && (rMsgDaysToDeletion > 0)) {
                r1c2p.source = "qrc:/ui/danger-cross-orange.svg"
                r1c2p.visible = true
            } else if (rMsgDaysToDeletion === 0) {
                r1c2p.source = "qrc:/ui/danger-cross-red.svg"
                r1c2p.visible = true
            }
        }

        GridLayout {
            id: msgEnvelGrid
            anchors.fill: parent
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: defaultMargin
            columns: 2
            rows: 3
            columnSpacing: defaultMargin
            rowSpacing: defaultMargin * 0.6
            Pictogram {
                id: r1c1
                image.source: (isReceivedMsg && !rReadLocally) ? "qrc:/ui/email-outline.svg" : "qrc:/ui/email-open-outline.svg"
            }
            Row {
                spacing: defaultMargin
                Layout.preferredWidth: parent.width
                Image {
                    id: r1c2p
                    visible: false
                    sourceSize.height: pictogramDimension
                    anchors.verticalCenter: parent.verticalCenter
                }
                Text {
                    id: r1c2
                    width: (r1c2p.visible) ? parent.width - r1c1.width - nextNavElement.width - r1c2p.width : parent.width - r1c1.width - nextNavElement.width
                    elide: Text.ElideRight
                    text: (isReceivedMsg) ? rFrom : rTo
                    color: (isReceivedMsg && !rReadLocally) ? textHighlightColor : datovkaPalette.text
                    font.bold: (isReceivedMsg && !rReadLocally)
                }
            }
            Pictogram {
                id: r2c1
                image.source: (rAttachmentsDownloaded) ? "qrc:/ui/paper-clip.svg" : "qrc:/ui/datovka-msg-blank.png"
            }
            Text {
                id: r2c2
                Layout.preferredWidth: parent.width - r2c1.width - nextNavElement.width
                elide: Text.ElideRight
                text: rAnnotation
                color: datovkaPalette.text
                font.bold: (isReceivedMsg && !rReadLocally)
            }
            Pictogram {
                id: r3c1
                image.source: (rRecordsManagement) ? "qrc:/ui/briefcase.svg" : "qrc:/ui/datovka-msg-blank.png"
            }
            Text {
                id: r3c2
                Layout.fillWidth: true
                text: rMsgId + "   " + rDelivTime + "   " + rAcceptTime
                color: imageDarkerColor
                font.pointSize: textFontSizeSmall
            }
        } // GridLayout
        NextOverlaidImage {
            id: nextNavElement
        }
        MouseArea {
            /* Construct string to be presented to accessibility interface. */
            function accessibleText() {
                var aText = "";
                if (isReceivedMsg) {
                    aText += !rReadLocally ? qsTr("Unread message from sender") : qsTr("Read message from sender")
                } else {
                    aText += qsTr("Message to receiver")
                }
                aText += " '" + ((isReceivedMsg) ? rFrom : rTo) + "'. "
                aText += qsTr("Subject") + ": '" + rAnnotation + "'."
                return aText;
            }
            AcntId {
                id: actionAcntId
            }
            function handleClick() {
                actionAcntId.username = rUserName
                actionAcntId.testing = rTesting
                root.msgClicked(actionAcntId, rMsgType, rMsgId, rMsgDaysToDeletion)
            }

            anchors.fill: parent

            Accessible.role: Accessible.Button
            Accessible.name: accessibleText()
            Accessible.onScrollDownAction: {
                root.scrollDown()
            }
            Accessible.onScrollUpAction: {
                root.scrollUp()
            }
            Accessible.onPressAction: {
                handleClick()
            }
            onClicked: {
                handleClick()
            }
            onPressAndHold: {
                /* Accessible doesn't support this type of action. */
                root.msgPressAndHold(acntId, rMsgType, rMsgId, compareMsgDate(rDelivTime))
            }
        }
        Rectangle {
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            height: 1
            width: (isiOS && root.count-1 !== index) ? r3c2.width + defaultMargin : parent.width
            color: (root.count-1 === index) ? datovkaPalette.dark : datovkaPalette.mid
        }
    } // Rectangle
    ScrollIndicator.vertical: ScrollIndicator {}
} // ListView
