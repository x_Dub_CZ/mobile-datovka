/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

ScrollableListView {
    id: root
    delegateHeight: listItemHeight

    /* These signals should be captured to implement message interaction. */
    signal govServiceClicked(string gsInternId, string gsFullName, string gsInstName)

    delegate: Rectangle {
        height: root.delegateHeight
        width: parent.width
        color: datovkaPalette.base
        ColumnLayout {
            anchors.fill: parent
            anchors.margins: defaultMargin
            anchors.verticalCenter: parent.verticalCenter
            spacing: defaultMargin
            Text {
                text: gsFullName
                color: textHighlightColor
                font.bold: true
                Layout.preferredWidth: parent.width - nextNavElement.width
                wrapMode: Text.Wrap
            }
            Text {
                id: instName
                text: "DS: " + gsBoxId + " -- " + gsInstName
                color: datovkaPalette.text
                font.pointSize: textFontSizeSmall
                Layout.preferredWidth: parent.width
                elide: Text.ElideRight
            }
        }
        NextOverlaidImage {
            id: nextNavElement
            visible: (gsInternId !== "")
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                govServiceClicked(gsInternId, gsFullName, instName.text.toString())
            }
        }
        Rectangle {
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            height: 1
            width: (isiOS && root.count-1 !== index) ? parent.width - defaultMargin : parent.width
            color: (root.count-1 === index) ? datovkaPalette.dark : datovkaPalette.mid
        }
    } // Rectangle

    ScrollIndicator.vertical: ScrollIndicator {}
} // ListView
