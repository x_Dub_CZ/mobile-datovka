/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

import QtQuick 2.7
import cz.nic.mobileDatovka 1.0

/*
 * Accessible show password image component.
 */
OverlaidImage {
    id: root

    property bool showPwd: false
    property var checkBeforeShowing: null
    /*
     * The property checkBeforeShowing is treated as a callback function.
     * Set it to a function taking no arguments and which is returning a boolean
     * value. The property is ignored when null.
     */
    signal clicked()
    signal hidden()
    signal shown()

    function hide() {
        /* Hide directly if shown. */
        if (root.showPwd) {
            root.showPwd = false
            root.hidden()
            return true
        }
        return false
    }

    function show() {
        /* Show directly if hidden. */
        if (!root.showPwd) {
            root.showPwd = true
            root.shown()
            return true
        }
        return false
    }

    function showAsk() {
        /* Check before showing. */
        if (!root.showPwd) {
            if ((root.checkBeforeShowing === null) || (root.checkBeforeShowing())) {
                root.showPwd = true
                root.shown()
                return true
            }
        }
        return false
    }

    visible: false
    anchors.verticalCenter: parent.verticalCenter
    image.sourceSize.height: imageNavigateDimension
    image.source: root.showPwd ? "qrc:/ui/eye-pwd-hide.svg" : "qrc:/ui/eye-pwd-show.svg"

    MouseArea {
        function handleClick() {
            if ((root.showPwd && root.hide()) || ((!root.showPwd) && showAsk())) {
                root.clicked()
            }
        }
        anchors.fill: parent
        Accessible.role: Accessible.Button
        Accessible.name: root.showPwd ? qsTr("Hide password") : qsTr("Show password")

        Accessible.onPressAction: {
            handleClick()
        }
        onClicked: {
            handleClick()
        }
    }
}
