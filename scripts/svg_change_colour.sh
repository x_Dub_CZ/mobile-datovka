#!/usr/bin/env sh

if [ "x${GETOPT}" = "x" ]; then
	GETOPT="getopt"
fi

H_SHORT="h"
H_LONG="help"

I_SHORT="i"
I_LONG="input-svg"

O_SHORT="o"
O_LONG="output-svg"

S_SHORT="s"
S_LONG="source-colour"

T_SHORT="t"
T_LONG="target-colour"

SRC_COLOUR=""
DFLT_SRC_COLOUR="#000000"
TGT_COLOUR=""

USAGE="Usage:\n\t$0 [options]\n\n"
USAGE="${USAGE}Supported options:\n"
USAGE="${USAGE}\t-${H_SHORT}, --${H_LONG}\n\t\tPrints help message.\n"
USAGE="${USAGE}\t-${I_SHORT}, --${I_LONG} <svg-file>\n\t\tInput SVG file. Reads from standard input if not specified.\n"
USAGE="${USAGE}\t-${O_SHORT}, --${O_LONG} <svg-file>\n\t\tOutput SVG file. Writes to standard output if not specified.\n"
USAGE="${USAGE}\t-${S_SHORT}, --${S_LONG} <colour>\n\t\tSpecify source colour. Must be in the format '#000000'. Default is '${DFLT_SRC_COLOUR}'.\n"
USAGE="${USAGE}\t-${T_SHORT}, --${T_LONG} <colour>\n\t\tSpecify target colour. Must be in the format '#000000'.\n"
USAGE="${USAGE}\n"
USAGE="${USAGE}Example usage:\n"
USAGE="${USAGE}\tcat input.svg | $0 -t '#00ff00' > output.svg\n"
USAGE="${USAGE}\t$0 -i input.svg -o output.svg -t '#00ff00'\n"

INPUT_FILE=""
DFLT_INPUT_FILE="-"
OUTPUT_FILE=""

if ! "${GETOPT}" -l test: -u -o t: -- --test test > /dev/null; then
	echo "The default getopt does not support long options." >&2
	echo "You may provide such getopt version via the GETOPT variable e.g.:" >&2
	echo "GETOPT=/opt/local/bin/getopt $0" >&2
	exit 1
fi

# Parse rest of command line
set -- $("${GETOPT}" -l ${H_LONG} -l ${I_LONG}: -l ${O_LONG}: -l ${S_LONG}: -l ${T_LONG}: -u -o ${H_SHORT}${I_SHORT}:${O_SHORT}:${S_SHORT}:${T_SHORT}: -- "$@")
if [ $# -lt 1 ]; then
	echo -e ${USAGE} >&2
	exit 1
fi

# Parse rest of command line
while [ $# -gt 0 ]; do
	OPTION="$1"
	PARAM="$2"
	case "${OPTION}" in
	-${H_SHORT}|--${H_LONG})
		echo -e ${USAGE}
		exit 0
		;;
	-${I_SHORT}|--${I_LONG})
		if [ "x${INPUT_FILE}" = "x" ]; then
			INPUT_FILE="${PARAM}"
		else
			echo "Option ${OPTION} already set." >&2
			exit 1
		fi
		shift
		;;
	-${O_SHORT}|--${O_LONG})
		if [ "x${OUTPUT_FILE}" = "x" ]; then
			OUTPUT_FILE="${PARAM}"
		else
			echo "Option ${OPTION} already set." >&2
			exit 1
		fi
		shift
		;;
	-${S_SHORT}|--${S_LONG})
		if [ "x${SRC_COLOUR}" = "x" ]; then
			SRC_COLOUR="${PARAM}"
		else
			echo "Option ${OPTION} already set." >&2
			exit 1
		fi
		shift
		;;
	-${T_SHORT}|--${T_LONG})
		if [ "x${TGT_COLOUR}" = "x" ]; then
			TGT_COLOUR="${PARAM}"
		else
			echo "Option ${OPTION} already set." >&2
			exit 1
		fi
		shift
		;;
	--)
		shift
		break
		;;
	-*|*)
		echo "Unknown option '${KEY}'." >&2
		echo -e ${USAGE} >&2
		exit 1
		;;
	esac
	shift
done
if [ $# -gt 0 ]; then
	echo -e "Unknown options: $@" >&2
	echo -en ${USAGE} >&2
	exit 1
fi

if [ "x${INPUT_FILE}" = "x" ]; then
	INPUT_FILE="${DFLT_INPUT_FILE}"
fi

if [ "x${SRC_COLOUR}" = "x" ]; then
	SRC_COLOUR="${DFLT_SRC_COLOUR}"
fi

# Check colour.
colour_ok() {
	local COLOUR="$1"
	local RESULT=$(echo "${COLOUR}" | grep '^#[0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f]$')
	if [ "x${RESULT}" = "x" ]; then
		return 1
	fi
	return 0
}

if ! colour_ok "${SRC_COLOUR}"; then
	echo "Invalid source colour '${SRC_COLOUR}'." >&2
	exit 1
fi

if ! colour_ok "${TGT_COLOUR}"; then
	echo "Invalid or unspecified target colour '${TGT_COLOUR}'." >&2
	exit 1
fi

SED="sed"
CMD="${SED}"
CMD="${CMD} -e s/fill:${SRC_COLOUR}/fill:${TGT_COLOUR}/g"
CMD="${CMD} -e s/fill=\"${SRC_COLOUR}\"/fill=\"${TGT_COLOUR}\"/g"
CMD="${CMD} -e s/stroke:${SRC_COLOUR}/stroke:${TGT_COLOUR}/g"
CMD="${CMD} -e s/stroke=\"${SRC_COLOUR}\"/stroke=\"${TGT_COLOUR}\"/g"
CMD="${CMD} ${INPUT_FILE}"

if [ "x${OUTPUT_FILE}" = "x" ]; then
	${CMD}
else
	${CMD} > "${OUTPUT_FILE}"
fi
