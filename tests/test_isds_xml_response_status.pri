
QT += xml

DEFINES += \
	TEST_ISDS_XML_RESPONSE_STATUS=1

SOURCES += \
	$${top_srcdir}src/isds/interface/response_status.cpp \
	$${top_srcdir}src/isds/xml/response_status.cpp \
	$${top_srcdir}tests/test_isds_xml_response_status.cpp

HEADERS += \
	$${top_srcdir}src/isds/interface/response_status.h \
	$${top_srcdir}src/isds/xml/response_status.h \
	$${top_srcdir}tests/test_isds_xml_response_status.h
