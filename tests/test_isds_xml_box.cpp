/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QByteArray>
#include <QDate>
#include <QDateTime>
#include <QFile>
#include <QObject>
#include <QString>
#include <QtTest/QtTest>

#include "src/datovka_shared/isds/box_interface2.h"
#include "src/datovka_shared/isds/box_interface.h"
#include "src/isds/xml/box_interface.h"
#include "tests/test_isds_xml_box.h"

static
QByteArray readFileData(const QString &fPath)
{
	QFile f(fPath);
	f.open(QIODevice::ReadOnly);

	QByteArray data = f.readAll();
	f.close();
	return data;
}

class TestIsdsXmlBox : public QObject {
	Q_OBJECT

public:
	TestIsdsXmlBox(void);

private slots:
	void initTestCase(void);

	void cleanupTestCase(void);

	void piXmlTest(void);

	void oiXmlTest(void);

	void uiXmlTest(void);

	void dtiXmlTest(void);

	void pdziXmlTest(void);

	void pdzsiXmlTest(void);

	void dciXmlTest(void);

private:
	const QString m_oiContentPath;
	const QString m_uiContentPath;
	const QString m_piContentPath;
	const QString m_dtiContentPath;
	const QString m_pdziContentPath;
	const QString m_pdzsiContentPath;
	const QString m_dciContentPath;
};

TestIsdsXmlBox::TestIsdsXmlBox(void)
    : QObject(Q_NULLPTR),
    m_oiContentPath("data/soap_GetOwnerInfoFromLogin2Response.xml"),
    m_uiContentPath("data/soap_GetUserInfoFromLogin2Response.xml"),
    m_piContentPath("data/soap_GetPasswordInfoResponse.xml"),
    m_dtiContentPath("data/soap_DTInfoResponse.xml"),
    m_pdziContentPath("data/soap_PDZInfoResponse.xml"),
    m_pdzsiContentPath("data/soap_PDZSendInfoResponse.xml"),
    m_dciContentPath("data/soap_DataBoxCreditInfoResponse.xml")
{
}

void TestIsdsXmlBox::initTestCase(void)
{
}

void TestIsdsXmlBox::cleanupTestCase(void)
{
}

void TestIsdsXmlBox::piXmlTest(void)
{
	const QByteArray data = readFileData(m_piContentPath);
	QVERIFY(data.size() > 0);

	bool iOk = false;
	QDateTime expDate = Isds::Xml::readPasswordInfoExpiration(data, &iOk);
	QVERIFY(iOk == true);
	QVERIFY(!expDate.isNull());

	QVERIFY(expDate > QDateTime::fromString("2011-07-06T11:33:38.999Z",
	    Qt::ISODateWithMs));
	QVERIFY(expDate == QDateTime::fromString("2011-07-06T11:33:39.000Z",
	    Qt::ISODateWithMs));
	QVERIFY(expDate < QDateTime::fromString("2011-07-06T11:33:39.001Z",
	    Qt::ISODateWithMs));
}

void TestIsdsXmlBox::oiXmlTest(void)
{
	const QByteArray data = readFileData(m_oiContentPath);
	QVERIFY(data.size() > 0);

	bool iOk = false;
	Isds::DbOwnerInfoExt2 oi = Isds::Xml::toOwnerInfo(data, &iOk);
	QVERIFY(!oi.isNull());
	QVERIFY(oi.dbID() == QLatin1String("4nwahuq"));
	QVERIFY(oi.aifoIsds() == Isds::Type::BOOL_NULL);
	QVERIFY(oi.dbType() == Isds::Type::BT_OVM);
	QVERIFY(oi.ic() == QLatin1String("70886288"));
	{
		const Isds::PersonName2 &pn = oi.personName();
		//QVERIFY(!pn.isNull());
		QVERIFY(pn.givenNames().isNull());
		QVERIFY(pn.lastName().isNull());
	}
	QVERIFY(oi.firmName() == QString::fromUtf8("Řád rytířů Jedi"));
	{
		const Isds::BirthInfo &bi = oi.birthInfo();
		//QVERIFY(!bi.isNull());
		QVERIFY(bi.date().isNull());
		QVERIFY(bi.city().isNull());
		QVERIFY(bi.county().isNull());
		QVERIFY(bi.state().isNull());
	}
	{
		const Isds::AddressExt2 &a = oi.address();
		QVERIFY(!a.isNull());
		QVERIFY(a.city() == QLatin1String("Jedousov"));
		QVERIFY(a.district().isNull());
		QVERIFY(a.street() == QLatin1String("Nar-Shaddaa"));
		QVERIFY(a.numberInStreet().isNull());
		QVERIFY(a.numberInMunicipality() == QLatin1String("42"));
		QVERIFY(a.zipCode().isNull());
		QVERIFY(a.state() == QLatin1String("CZ"));
	}
	QVERIFY(oi.nationality() == QLatin1String("CZ"));
	QVERIFY(oi.dbIdOVM() == QLatin1String("70886288"));
	QVERIFY(oi.dbState() == Isds::Type::BS_ACCESSIBLE);
	QVERIFY(oi.dbOpenAddressing() == Isds::Type::BOOL_FALSE);
	QVERIFY(oi.dbUpperID().isNull());
}

void TestIsdsXmlBox::uiXmlTest(void)
{
	QByteArray data = readFileData(m_uiContentPath);
	QVERIFY(data.size() > 0);

	bool iOk = false;
	Isds::DbUserInfoExt2 ui = Isds::Xml::toUserInfoExt2(data, &iOk);
	QVERIFY(!ui.isNull());
	QVERIFY(ui.aifoIsds() == Isds::Type::BOOL_FALSE);
	QVERIFY(ui.ic().isNull());
	QVERIFY(ui.firmName().isNull());
	{
		const Isds::PersonName2 &pn = ui.personName();
		QVERIFY(!pn.isNull());
		QVERIFY(pn.givenNames() == QLatin1String("Martin"));
		QVERIFY(pn.lastName() == QString::fromUtf8("Novák"));
	}
	{
		const Isds::AddressExt2 &a = ui.address();
		QVERIFY(!a.isNull());
		QVERIFY(a.code() == QLatin1String("bogus_code1234"));
		QVERIFY(a.city() == QLatin1String("Brno"));
		QVERIFY(a.district().isNull());
		QVERIFY(a.street() == QString::fromUtf8("Husitská"));
		QVERIFY(a.numberInStreet() == QLatin1String("10"));
		QVERIFY(a.numberInMunicipality() == QLatin1String("1843"));
		QVERIFY(a.zipCode() == QLatin1String("61200"));
		QVERIFY(a.state() == QLatin1String("CZ"));
	}
	QVERIFY(ui.biDate() == QDate::fromString("1980-05-29", Qt::ISODate));
	QVERIFY(ui.isdsID() == QLatin1String("DS_rdhtwob7y"));
	QVERIFY(ui.userType() == Isds::Type::UT_PRIMARY);
	QVERIFY(ui.userPrivils() ==
	        (Isds::Type::PRIVIL_READ_NON_PERSONAL |
	         Isds::Type::PRIVIL_READ_ALL |
	         Isds::Type::PRIVIL_CREATE_DM |
	         Isds::Type::PRIVIL_VIEW_INFO |
	         Isds::Type::PRIVIL_SEARCH_DB |
	         Isds::Type::PRIVIL_OWNER_ADM |
	         Isds::Type::PRIVIL_READ_VAULT |
	         Isds::Type::PRIVIL_ERASE_VAULT));
	QVERIFY(ui.ic().isNull());
	QVERIFY(ui.firmName().isNull());
	QVERIFY(ui.caStreet() == QString::fromUtf8("Husitská 1843/10"));
	QVERIFY(ui.caCity() == QLatin1String("Brno"));
	QVERIFY(ui.caZipCode() == QLatin1String("61200"));
	QVERIFY(ui.caState().isNull());
}

void TestIsdsXmlBox::dtiXmlTest(void)
{
	QByteArray data = readFileData(m_dtiContentPath);
	QVERIFY(data.size() > 0);

	bool iOk = false;
	Isds::DTInfoOutput dti = Isds::Xml::toDTInfo(data, &iOk);
	QVERIFY(dti.actDTType() == Isds::Type::DTT_CONTRACTUAL);
	QVERIFY(dti.actDTCapacity() == 100);
	QVERIFY(dti.actDTFrom() == QDate::fromString("2012-04-30", Qt::ISODate));
	QVERIFY(dti.actDTTo() == QDate::fromString("2100-01-01", Qt::ISODate));
	QVERIFY(dti.actDTCapUsed() == 50);
	QVERIFY(dti.futDTType() == Isds::Type::DTT_INACTIVE);
	QVERIFY(dti.futDTCapacity() == -1);
	QVERIFY(dti.futDTFrom().isNull());
	QVERIFY(dti.futDTTo().isNull());
	QVERIFY(dti.futDTPaid() == Isds::Type::FDTPS_NOT_PAID_YET);
}

void TestIsdsXmlBox::pdziXmlTest(void)
{
	const QDateTime cExpire(QDate(2036, 9, 16), QTime(12, 1, 5, 493), Qt::UTC);

	QByteArray data = readFileData(m_pdziContentPath);
	QVERIFY(data.size() > 0);

	bool iOk = false;
	QList<Isds::PDZInfoRec> pil = Isds::Xml::toPdzInfoRecs(data, &iOk);


	QVERIFY(iOk);
	QVERIFY(!pil.isEmpty());
	QVERIFY(pil.count() == 4);

	foreach (const Isds::PDZInfoRec &pi, pil) {
		QVERIFY(pi.pdzType() != Isds::Type::PPT_UNKNOWN);
		if (pi.pdzType() == Isds::Type::PPT_O) {
			QVERIFY(pi.pdzRecip() == "cyde8kw");
			QVERIFY(pi.odzIdent() == "1234567890");
			QVERIFY(pi.pdzExpire() == cExpire);
			QVERIFY(pi.pdzCnt() == 1);
		} else {
			QVERIFY(pi.pdzRecip().isNull());
			QVERIFY(pi.odzIdent().isNull());
		}
		QVERIFY(!pi.pdzPayer().isEmpty());
		if (!pi.pdzExpire().isNull()) {
			QVERIFY(pi.pdzExpire().isValid());
		}
	}

	data = QByteArray();
	pil = Isds::Xml::toPdzInfoRecs(data, &iOk);
	QVERIFY(!iOk);
	QVERIFY(pil.isEmpty());
}

void TestIsdsXmlBox::pdzsiXmlTest(void)
{
	QByteArray data = readFileData(m_pdzsiContentPath);
	QVERIFY(data.size() > 0);

	bool iOk = false;
	bool canPdz = Isds::Xml::readPdzSendInfo(data, &iOk);
	QVERIFY(iOk);
	QVERIFY(canPdz);

	data = QByteArray();
	canPdz = Isds::Xml::readPdzSendInfo(data, &iOk);
	QVERIFY(!iOk);
	QVERIFY(!canPdz);
}

void TestIsdsXmlBox::dciXmlTest(void)
{
	QByteArray data = readFileData(m_dciContentPath);
	QVERIFY(data.size() > 0);

	bool iOk = false;
	qint64 currentCredit = 0;
	QString notifEmail;

	QList<Isds::CreditEvent> evnets =
	    Isds::Xml::toDataBoxCreditInfo(data, currentCredit,
	    notifEmail, &iOk);
	QVERIFY(iOk);
	QVERIFY(notifEmail.isEmpty());
	QVERIFY(currentCredit == 98500);
	QVERIFY(evnets.isEmpty());

	data = QByteArray();
	evnets = Isds::Xml::toDataBoxCreditInfo(data, currentCredit,
	    notifEmail, &iOk);
	QVERIFY(!iOk);
	QVERIFY(notifEmail.isEmpty());
	QVERIFY(currentCredit == 98500);
	QVERIFY(evnets.isEmpty());
}

QObject *newTestIsdsXmlBox(void)
{
	return new (::std::nothrow) TestIsdsXmlBox();
}

//QTEST_MAIN(TestIsdsXmlBox)
#include "test_isds_xml_box.moc"
