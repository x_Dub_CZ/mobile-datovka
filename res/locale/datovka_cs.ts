<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>AccessibleSpinBox</name>
    <message>
        <location filename="../../qml/components/AccessibleSpinBox.qml" line="70"/>
        <location filename="../../qml/components/AccessibleSpinBox.qml" line="70"/>
        <source>Decrease value &apos;%1&apos;.</source>
        <translation>Snížit hodnotu &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccessibleSpinBox.qml" line="103"/>
        <location filename="../../qml/components/AccessibleSpinBox.qml" line="103"/>
        <source>Increase value &apos;%1&apos;.</source>
        <translation>Zvýšit hodnotu &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>AccessibleSpinBoxZeroMax</name>
    <message>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="32"/>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="32"/>
        <source>max</source>
        <translation>max</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="92"/>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="92"/>
        <source>Decrease value &apos;%1&apos;.</source>
        <translation>Snížit hodnotu &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="125"/>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="125"/>
        <source>Increase value &apos;%1&apos;.</source>
        <translation>Zvýšit hodnotu &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>AccountList</name>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="96"/>
        <location filename="../../qml/components/AccountList.qml" line="96"/>
        <source>Account &apos;%1&apos;.</source>
        <translation>Účet &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="96"/>
        <location filename="../../qml/components/AccountList.qml" line="96"/>
        <source>Testing account &apos;%1&apos;.</source>
        <translation>Testovací účet &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="114"/>
        <location filename="../../qml/components/AccountList.qml" line="114"/>
        <source>Synchronise account &apos;%1&apos;.</source>
        <translation>Synchronizovat účet &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="146"/>
        <location filename="../../qml/components/AccountList.qml" line="146"/>
        <source>Received messages</source>
        <translation>Přijaté zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="162"/>
        <location filename="../../qml/components/AccountList.qml" line="162"/>
        <source>Received messages of account &apos;%1&apos;. (Unread %2 of %3 received messages.)</source>
        <translation>Přijaté zprávy účtu &apos;%1&apos;. (Nepřečteno %2 z %3 přijatých zpráv.)</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="198"/>
        <location filename="../../qml/components/AccountList.qml" line="198"/>
        <source>Sent messages</source>
        <translation>Odeslané zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="214"/>
        <location filename="../../qml/components/AccountList.qml" line="214"/>
        <source>Sent messages of account &apos;%1&apos;. (Total of %2 sent messages.)</source>
        <translation>Odeslané zprávy účtu &apos;%1&apos;. (Celkem %2 odeslaných zpráv.)</translation>
    </message>
</context>
<context>
    <name>Accounts</name>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="359"/>
        <source>Remove account: %1</source>
        <translation>Odstranit účet: %1</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="360"/>
        <source>Do you want to remove the account &apos;%1&apos;?</source>
        <translation>Opravdu chcete odstranit účet &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="361"/>
        <source>Note: It will also remove all related local databases and account information.</source>
        <translation>Poznámka: Budou odstraněny také všechny odpovídající lokální databáze a informace o účtu.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="260"/>
        <source>Problem while updating account</source>
        <translation>Problém při vytváření účtu</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="198"/>
        <source>Username, communication code or account name has not been specified. These fields must be filled in.</source>
        <translation>Uživatelské jméno, komunikační kód nebo jméno účtu nebylo uvedeno. Tyto položky musí být vyplněny.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="203"/>
        <source>Username, password or account name has not been specified. These fields must be filled in.</source>
        <translation>Uživatelské jméno, heslo nebo jméno účtu nebylo uvedeno. Tyto položky musí být vyplněny.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="218"/>
        <source>Account with username &apos;%1&apos; already exists.</source>
        <translation>Účet s uživatelským jménem &apos;%1&apos; již existuje.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="261"/>
        <source>Account name has not been specified!</source>
        <translation>Název účtu nebyl vyplněn!</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="262"/>
        <source>This field must be filled in.</source>
        <translation>Tato položka musí být vyplněna.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="450"/>
        <source>Change user name: %1</source>
        <translation>Změna uživatelského jména: %1</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="451"/>
        <source>Do you want to change the username from &apos;%1&apos; to &apos;%2&apos; for the account &apos;%3&apos;?</source>
        <translation>Chcete změnit uživatelské jméno z &apos;%1&apos; na &apos;%2&apos; pro účet &apos;%3&apos;?</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="453"/>
        <source>Note: It will also change all related local database names and account information.</source>
        <translation>Poznámka: Změní se také jména všech odpovídajících databází a informace o účtu.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="500"/>
        <source>Cannot access file database for the username &apos;%1&apos;.</source>
        <translation>Nelze otevřít databázi souborů pro uživatelské jméno &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="509"/>
        <source>Cannot change file database to username &apos;%1&apos;.</source>
        <translation>Nelze změnit databázi souborů na uživatelské jméno &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="535"/>
        <source>Cannot access message database for username &apos;%1&apos;.</source>
        <translation>Nelze otevřít databázi zpráv pro uživatelské jméno &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="544"/>
        <source>Cannot change message database to username &apos;%1&apos;.</source>
        <translation>Nelze změnit databázi zpráv na uživatelské jméno &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="581"/>
        <source>Data box identifier related to the new username &apos;%1&apos; doesn&apos;t correspond with the data box identifier related to the old username &apos;%2&apos;.</source>
        <translation>Identifikátor schránky pro nové uživatelské jméno &quot;%1&quot; neodpovídá identifikátoru schránky starého uživatelského jména &quot;%2&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="587"/>
        <source>Cannot change the file database to match the new username &apos;%1&apos;.</source>
        <translation>Nelze změnit databázi souborů na nové uživatelské jméno &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="593"/>
        <source>Cannot change the message database to match the new username &apos;%1&apos;.</source>
        <translation>Nelze změnit databázi zpráv na nové uživatelské jméno &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="608"/>
        <source>Change username: %1</source>
        <translation>Změna uživatelského jména: %1</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="609"/>
        <source>A new username &apos;%1&apos; for the account &apos;%2&apos; has been set.</source>
        <translation>Nové uživatelské jméno &apos;%1&apos; pro účet &apos;%2&apos; bylo nastaveno.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="611"/>
        <source>Account will use the new settings.</source>
        <translation>Účet použije nová nastavení.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="626"/>
        <source>Username problem: %1</source>
        <translation>Problém s uživatelským jménem: %1</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="627"/>
        <source>The new username &apos;%1&apos; for account &apos;%2&apos; has not been set!</source>
        <translation>Nové uživatelské jméno &apos;%1&apos; pro účet &apos;%2&apos; nebylo nastaveno!</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="630"/>
        <source>Account will use the original settings.</source>
        <translation>Účet použije původní nastavení.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="728"/>
        <source>Password expiration</source>
        <translation>Expirace hesla</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="729"/>
        <source>Passwords of users listed below have expired or are going to expire within few days. You may change the passwords from this application if they haven&apos;t already expired. Expired passwords can only be changed in the ISDS web portal.</source>
        <translation>Uvedená uživatelská hesla expirovala nebo expirují v následujících dnech. Tato hesla, pokud ještě nevypršela, můžete změnit z této aplikace. Expirovaná hesla mohou být změněna pouze na webovém portálu ISDS.</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="229"/>
        <source>Creating account: %1</source>
        <translation>Vytváření účtu: %1</translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="230"/>
        <source>Account &apos;%1&apos; could not be created.</source>
        <translation>Účet &apos;%1&apos; nemohl být vytvořen.</translation>
    </message>
</context>
<context>
    <name>BackupRestoreData</name>
    <message>
        <location filename="../../src/backup.cpp" line="352"/>
        <location filename="../../src/backup.cpp" line="642"/>
        <location filename="../../src/backup.cpp" line="740"/>
        <source>Cannot open or read file &apos;%1&apos;.</source>
        <translation>Nelze otevřít nebo číst soubor &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="367"/>
        <source>Select accounts from the backup JSON file which you want to restore. Data of existing accounts will be replaced by data from the backup.</source>
        <translation>Vyberte účty, které chcete obnovit z JSON souboru obsahujícího popis zálohy. Data existujících účtů budou nahrazena daty ze zálohy.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="630"/>
        <location filename="../../src/backup.cpp" line="730"/>
        <source>File &apos;%1&apos; is not a JSON file.</source>
        <translation>&quot;%1&quot; není JSON soubor.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="653"/>
        <source>JSON file &apos;%1&apos; does not contain valid application information.</source>
        <translation>JSON soubor &quot;%1&quot; neobsahuje platné informace o aplikaci.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="665"/>
        <source>Backup was taken at %1.</source>
        <translation>Záloha byla provedena %1.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="672"/>
        <source>Transfer image was taken at %1.</source>
        <translation>Data pro přenos byla získána %1.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="680"/>
        <source>Unknown backup type. Selected JSON file contains no valid backup data.</source>
        <translation>Neznámý typ zálohy. Vybraný JSON soubor neobsahuje platná data.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="701"/>
        <source>The action will continue with the restoration of the selected accounts. Current data of the selected accounts will be completely rewritten.</source>
        <translation>Akce bude pokračovat obnovením vybraných účtů. Současná data vybraných účtů budou kompletně přepsána.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="702"/>
        <source>Do you wish to restore the selected accounts from the backup?</source>
        <translation>Přejete si obnovit vybrané účty ze zálohy?</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="705"/>
        <source>The action will continue with the restoration of the complete application data. Current application data will be completely rewritten.</source>
        <translation>Akce bude pokračovat obnovením kompletních dat aplikace. Současná data aplikace budou kompletně přepsána.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="706"/>
        <source>Do you wish to restore the application data from the transfer?</source>
        <translation>Přejete si obnovit data aplikace?</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="715"/>
        <source>Proceed?</source>
        <translation>Pokračovat?</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="766"/>
        <location filename="../../src/backup.cpp" line="843"/>
        <location filename="../../src/backup.cpp" line="855"/>
        <source>Importing file &apos;%1&apos;.</source>
        <translation>Importuji soubor &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="773"/>
        <location filename="../../src/backup.cpp" line="862"/>
        <source>Restoration finished.</source>
        <translation>Obnovování dokončeno.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="777"/>
        <location filename="../../src/backup.cpp" line="785"/>
        <location filename="../../src/backup.cpp" line="866"/>
        <location filename="../../src/backup.cpp" line="874"/>
        <source>Restoration finished</source>
        <translation>Obnovování dokončeno</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="778"/>
        <source>All application data have successfully been restored. The application will restart in order to load the imported files.</source>
        <translation>Všechna data byla úspěšně obnovena. Aplikace se restartuje, aby načetla importované soubory.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="779"/>
        <source>Do you want to delete the source transfer folder from the application sandbox before the restart (recommended)?</source>
        <translation>Přejete si před restartem odstranit zdrojový adresář ze sandboxu aplikace (doporučeno)?</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="786"/>
        <source>All application data have successfully been restored.</source>
        <translation>Všechna data byla úspěšně obnovena.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="787"/>
        <location filename="../../src/backup.cpp" line="876"/>
        <source>The application will restart in order to load the imported files.</source>
        <translation>Aplikace se restartuje, aby načetla importované soubory.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="797"/>
        <location filename="../../src/backup.cpp" line="885"/>
        <source>Restoration error</source>
        <translation>Chyba obnovování</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="798"/>
        <location filename="../../src/backup.cpp" line="886"/>
        <source>An error during data restoration has occurred.</source>
        <translation>Nastala chyba v průběhu obnovování.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="799"/>
        <location filename="../../src/backup.cpp" line="887"/>
        <source>Application data restoration has been cancelled.</source>
        <translation>Obnovování bylo přerušeno.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="867"/>
        <source>All selected account data have successfully been restored. The application will restart in order to load the imported files.</source>
        <translation>Data vybraných účtů byla úspěšně obnovena. Aplikace se restartuje, aby načetla importované soubory.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="868"/>
        <source>Do you want to delete the source backup folder from the application sandbox before the restart (recommended)?</source>
        <translation>Přejete si před restartem odstranit zdrojový adresář ze sandboxu aplikace (doporučeno)?</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="875"/>
        <source>All selected account data have successfully been restored.</source>
        <translation>Data vybraných účtů byla úspěšně obnovena.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="929"/>
        <location filename="../../src/backup.cpp" line="1498"/>
        <location filename="../../src/backup.cpp" line="1543"/>
        <source>There is not enough space in the selected storage.</source>
        <translation>Na zvoleném úložišti není dostatek volného místa.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="939"/>
        <location filename="../../src/backup.cpp" line="1549"/>
        <source>One or more files mentioned in JSON file are missing or inaccessible.</source>
        <translation>Jeden nebo více souborů popsaných v JSON souboru chybí nebo nejsou přístupné.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="997"/>
        <source>Backup error</source>
        <translation>Chyba zálohování</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="998"/>
        <source>An error occurred when backing up data to target location. Backup has been cancelled.</source>
        <translation>Nastala chyba při zálohování dat do cílového umístění. Zálohování bylo přerušeno.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="999"/>
        <source>Application data were not backed up to target location.</source>
        <translation>Data nebyla zálohována do cílového umístění.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1016"/>
        <source>Transfer problem</source>
        <translation>Problém přenosu</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1017"/>
        <source>The data transfer requires application data to be secured with a PIN.</source>
        <translation>Přenesení dat vyžaduje, aby byla data v aplikaci zabezpečena PINem.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1018"/>
        <source>Set a PIN in the application settings and try again.</source>
        <translation>Nastavte PIN v nastavení aplikace a zkuste to znovu.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1053"/>
        <source>Transferring file &apos;%1&apos;.</source>
        <translation>Přenáším soubor &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1310"/>
        <source>Restoring file &apos;%1&apos;.</source>
        <translation>Obnovuji soubor &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1093"/>
        <source>Transfer finished.</source>
        <translation>Přenos dokončen.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="77"/>
        <source>Data were exported into the application sandbox. Now you can transfer the exported files to iCloud or use the iTunes application to transfer the backed up files to your Mac or PC manually.</source>
        <translation>Data byla exportována do sandboxu aplikace. Nyní můžete přenést exportovaná data do iCloudu nebo použít aplikaci iTunes k přenosu dat do Vašeho Macu nebo PC.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="78"/>
        <source>Do you want to upload the files to the application iCloud container?</source>
        <translation>Přejete si nahrát data do kontejneru aplikace na iCloud?</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="84"/>
        <source>Exported backup files are available in the application sandbox. Use the iTunes application in your computer to transfer the exported files to your Mac or PC manually. All exported files will automatically be deleted when you start this application again.</source>
        <translation>Data byla exportována do sandboxu aplikace. Nyní můžete přenést exportovaná data do iCloudu nebo použít aplikaci iTunes k přenosu dat do Vašeho Macu nebo PC. Exportovaná data budou automaticky smazána při dalším spuštění této aplikace.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="85"/>
        <source>Preserve the backup in a safe place as it contains private data.</source>
        <translation>Uchovávejte zálohovaná data na bezpečném místě, protože obsahují osobní data.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="103"/>
        <source>unknown</source>
        <translation>neznámý/á</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1102"/>
        <source>Transfer finished</source>
        <translation>Přenos dokončen</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1103"/>
        <source>Application data were transferred successfully to target location.</source>
        <translation>Data byla přenesena do cílového umístění.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1104"/>
        <source>Preserve the transfer data in a safe place as it contains personal data.</source>
        <translation>Uchovávejte přenášená data na bezpečném místě, protože obsahují osobní data.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1165"/>
        <location filename="../../src/backup.cpp" line="1191"/>
        <location filename="../../src/backup.cpp" line="1381"/>
        <location filename="../../src/backup.cpp" line="1403"/>
        <source>Backing up file &apos;%1&apos;.</source>
        <translation>Zálohuji soubor &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1319"/>
        <source>From backup</source>
        <translation>Ze zálohy</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1430"/>
        <source>Backup finished.</source>
        <translation>Záloha dokočena.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="76"/>
        <location filename="../../src/backup.cpp" line="83"/>
        <location filename="../../src/backup.cpp" line="1439"/>
        <source>Backup finished</source>
        <translation>Záloha dokončena</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1440"/>
        <source>All selected accounts have been successfully backed up into target location.</source>
        <translation>Všechny vybrané účty byly úspěšně zálohovány do cílového umístění.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1441"/>
        <source>Preserve the backup in a safe place as it contains personal data.</source>
        <translation>Uchovávejte zálohovaná data na bezpečném místě, protože obsahují osobní data.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1516"/>
        <source>Application version does not match the transfer version.</source>
        <translation>Verze aplikace neodpovídá verzi v přenášených datech.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1518"/>
        <source>Version info</source>
        <translation>Informace o verzi</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1519"/>
        <source>Warning: Application version does not match the version declared in the transfer. Is is recommended to transfer data between same versions of the application to prevent incompatibility issues.</source>
        <translation>Upozornění: Verze aplikace neodpovídá verzi deklarované v transferu. Je doporučeno přenášet data mezi stejnými verzemi aplikace, aby se předešlo problémům s nekompatibilitou.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1520"/>
        <source>Do you want to continue?</source>
        <translation>Chcete pokračovat?</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1540"/>
        <source>Restore all application data from transfer JSON file. The current application data will be complete rewritten with new data from transfer backup.</source>
        <translation>Obnoví kompletní data aplikace z JSON soboru. Současná data v aplikaci budou kompletně přepsána daty z přenosu.</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1559"/>
        <source>Required space</source>
        <translation>Vyžadované místo</translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1560"/>
        <source>Available space</source>
        <translation>Volné místo</translation>
    </message>
</context>
<context>
    <name>CalendarDialogue</name>
    <message>
        <location filename="../../qml/dialogues/CalendarDialogue.qml" line="63"/>
        <location filename="../../qml/dialogues/CalendarDialogue.qml" line="63"/>
        <source>Calendar dialog</source>
        <translation>Kalendář</translation>
    </message>
</context>
<context>
    <name>Connection</name>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="508"/>
        <source>Success.</source>
        <translation>Úspěch.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="511"/>
        <source>Successfully finished.</source>
        <translation>Úspěšně dokončeno.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="514"/>
        <source>Internet connection is probably not available. Check your network settings.</source>
        <translation>Připojení k internetu pravděpodobně není k dispozici. Zkontrolujte nastavení sítě.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="517"/>
        <source>Authorization failed. Server complains about a bad request.</source>
        <translation>Autorizace selhala. Server vrací upozornění na špatný požadavek.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="520"/>
        <source>Error reply.</source>
        <translation>Chybná odezva.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="523"/>
        <source>ISDS server is out of service. Scheduled maintenance in progress. Try again later.</source>
        <translation>Server ISDS je momentálně mimo provoz. Probíhá plánovaná údržba. Zkuste to později.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="526"/>
        <source>Connection with ISDS server timed out. Request was cancelled.</source>
        <translation>Spojení se serverem ISDS vypršelo. Požadavek byl zrušen.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="529"/>
        <source>Authorization failed. Check your credentials in the account settings whether they are correct and try again. It is also possible that your password expired. Check your credentials validity by logging in to the data box using the ISDS web portal.</source>
        <translation>Autorizace selhala. Zkontrolujte své přihlašovací údaje v nastavení účtu. Také je možné, že vypršela platnost hesla. Ověřte správnost Vašich přihlašovacích údajů přihlášením do webového portálu ISDS.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="532"/>
        <source>OTP authorization failed. OTP code is wrong or expired.</source>
        <translation>OTP autorizace selhala. Kód OTP je neplatný nebo vypršel.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="535"/>
        <source>SMS authorization failed. SMS code is wrong or expired.</source>
        <translation>SMS autorizace selhala. Kód SMS je neplatný nebo vypršel.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="538"/>
        <source>SMS authorization failed. SMS code couldn&apos;t be sent. Your order on premium SMS has been exhausted or cancelled.</source>
        <translation>SMS autorizace selhala. SMS kód nemohl být odeslán. Vaše předplatné prémiových SMS bylo vyčerpáno nebo zrušeno.</translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="542"/>
        <source>An communication error. See log for more detail.</source>
        <translation>Chyba komunikace. Více informací naleznete v logu aplikace.</translation>
    </message>
</context>
<context>
    <name>CreateAccountPage0</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="12"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="22"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="12"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="22"/>
        <source>Account Creator Wizard</source>
        <translation>Průvodce vytvořením účtu</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="51"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="51"/>
        <source>Welcome to the account creator wizard. It will help you to create an account in this application in order to access an existing data box. You should already know valid login credentials (typically a username and a password) for a data box.</source>
        <translation>Vítejte v průvodci vytvořením účtu. Pomůže Vám vytvořit účet v této aplikaci, abyste mohli přistupovat do existující datové schránky. Platné přihlašovací údaje (typicky uživatelské jméno a heslo) pro datovou schránku byste měli již znát.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="52"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="52"/>
        <source>The device which this application is running on must have an active internet connection.</source>
        <translation>Zařízení, na kterém běží tato aplikace, musí mít aktivní internetové připojení.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="56"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="56"/>
        <source>Continue with the Wizard</source>
        <translation>Pokračovat s průvodcem</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="66"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="66"/>
        <source>If you don&apos;t want to use the wizard you can skip it and fill in a form.</source>
        <translation>Pokud nechcete použít průvodce, můžete jej přeskočit a vyplnit formulář.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="70"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="70"/>
        <source>Use Account Form</source>
        <translation>Použít uživatelský formulář</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="80"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="80"/>
        <source>Note: If you&apos;ve just freshly obtained your login credentials then you must use them to log into the ISDS web portal for the first time where you will need to change you password. With the new password you can then start using this application.</source>
        <translation>Poznámka: Pokud jste čerstvě obdrželi své přihlašovací údaje, tak se pomocí nich musíte nejprve přihlásit do webového portálu ISDS, kde si musíte změnit heslo. S novým heslem pak můžete začít používat tuto aplikaci.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="87"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="87"/>
        <source>Note: This application doesn&apos;t allow the creation of new data boxes on the ISDS server.</source>
        <translation>Poznámka: Tato aplikace nedovoluje vytvářet nové datové schránky na serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="97"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="97"/>
        <source>Recommendation: Read the &lt;a href=&quot;%1&quot;&gt;user manual&lt;/a&gt; in order to better understand how this application works. The &lt;a href=&quot;%2&quot;&gt;project web page&lt;/a&gt; also contains some useful information.</source>
        <translation>Doporučení: Přečtěte si &lt;a href=&quot;%1&quot;&gt;uživatelskou příručku&lt;/a&gt;, abyste lépe pochopili, jak tato aplikace funguje. Na &lt;a href=&quot;%2&quot;&gt;stránce projektu&lt;/a&gt; také naleznete užitečné informace.</translation>
    </message>
</context>
<context>
    <name>CreateAccountPage1</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="42"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="42"/>
        <source>Create Account: 1/4</source>
        <translation>Vytvořit účet 1/4</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="53"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="53"/>
        <source>Next step</source>
        <translation>Další krok</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="87"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="87"/>
        <source>The account title must be filled in.</source>
        <translation>Musíte vyplnit pojmenování účtu.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="91"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="91"/>
        <source>Account Title</source>
        <translation>Pojmenování účtu</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="92"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="92"/>
        <source>Enter custom account name.</source>
        <translation>Zadejte vlastní název účtu.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="100"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="100"/>
        <source>The account title is a user-specified name used for the identification of the account in the application (e.g. &apos;My Personal Data Box&apos;, &apos;Firm Box&apos;, etc.). The chosen name serves only for your convenience. The entry must be filled in.</source>
        <translation>Pojmenování účtu je uživatelem zvolené jméno, které slouží k identifikaci účtu v této aplikaci (např. &quot;Moje osobní datová schránka&quot;, &quot;Firemní schránka&quot;, atd.). Zvolené jméno slouží jen pro Vaši lepší orientaci. Tato položka musí být vyplněna.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="108"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="108"/>
        <source>Account Type</source>
        <translation>Typ účtu</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="113"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="113"/>
        <source>regular account</source>
        <translation>běžný účet</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="118"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="118"/>
        <source>test account</source>
        <translation>testovací účet</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="126"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="126"/>
        <source>The regular account option is used to access the production (official) ISDS environment (&lt;a href=&quot;https://www.mojedatovaschranka.cz&quot;&gt;www.mojedatovaschranka.cz&lt;/a&gt;). Test accounts are used to access the ISDS testing environment (&lt;a href=&quot;https://www.czebox.cz&quot;&gt;www.czebox.cz&lt;/a&gt;).</source>
        <translation>Volba běžného účtu slouží k přístupu do produkčního (oficiálního) prostředí ISDS (&lt;a href=&quot;https://www.mojedatovaschranka.cz&quot;&gt;www.mojedatovaschranka.cz&lt;/a&gt;). Testovací účty slouží k přístupu do testovacího prostředí ISDS (&lt;a href=&quot;https://www.czebox.cz&quot;&gt;www.czebox.cz&lt;/a&gt;).</translation>
    </message>
</context>
<context>
    <name>CreateAccountPage2</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="28"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="28"/>
        <source>Create Account: 2/4</source>
        <translation>Vytvořit účet 2/4</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="39"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="39"/>
        <source>Next step</source>
        <translation>Další krok</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="70"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="70"/>
        <source>Account title</source>
        <translation>Pojmenování účtu</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="75"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="75"/>
        <source>Account type</source>
        <translation>Typ účtu</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="85"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="85"/>
        <source>Login Method</source>
        <translation>Způsob přihlašování</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="90"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="90"/>
        <source>username + password</source>
        <translation>jméno + heslo</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="95"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="95"/>
        <source>username + Mobile Key</source>
        <translation>jméno + Mobilní klíč</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="100"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="100"/>
        <source>username + password + SMS</source>
        <translation>jméno + heslo + SMS</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="105"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="105"/>
        <source>username + password + security code</source>
        <translation>jméno + heslo + bezpečnostní kód</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="110"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="110"/>
        <source>username + password + certificate</source>
        <translation>jméno + heslo + certifikát</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="117"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="117"/>
        <source>Select the login method which you use to access the data box in the %1 ISDS environment.</source>
        <translation>Zvolte způsob přihlašování, který používíte pro přístup do datové schránky v %1 prostředí ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="117"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="117"/>
        <source>testing</source>
        <translation>testovacím</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="117"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="117"/>
        <source>production</source>
        <translation>produkčním</translation>
    </message>
</context>
<context>
    <name>CreateAccountPage3</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="52"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="52"/>
        <source>The username must be filled in.</source>
        <translation>Uživatelské jméno musí být vyplněno.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="58"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="58"/>
        <source>The communication code must be filled in.</source>
        <translation>Komunikační kód musí být vyplněn.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="65"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="65"/>
        <source>The password must be filled in.</source>
        <translation>Heslo musí být vyplněno.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="73"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="73"/>
        <source>The certificate file must be specified.</source>
        <translation>Musíte určit soubor s certifikátem.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="104"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="104"/>
        <source>Create Account: 3/4</source>
        <translation>Vytvořit účet 3/4</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="115"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="115"/>
        <source>Next step</source>
        <translation>Další krok</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="145"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="145"/>
        <source>Account title</source>
        <translation>Pojmenování účtu</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="150"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="150"/>
        <source>Account type</source>
        <translation>Typ účtu</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="155"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="155"/>
        <source>Login method</source>
        <translation>Způsob přihlašování</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="168"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="184"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="168"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="184"/>
        <source>Fill in your login credentials. All entries must be filled in.</source>
        <translation>Vyplňte své přihlašovací údaje. Všechny položky musí být vyplněny.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="172"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="172"/>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="180"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="180"/>
        <source>Warning: The username should contain only combinations of lower-case letters and digits.</source>
        <translation>Varování: Uživatelské jméno by mělo obsahovat jen kombinaci malých písmen a číslic.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="192"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="192"/>
        <source>The username must consist of at least 6 characters without spaces (only combinations of lower-case letters and digits are allowed). Notification: The username is not a data-box ID.</source>
        <translation>Uživatelské jméno se musí skládat alespoň z 6 znaků bez mezer (jsou povoleny jen kombinace malých písmen a číslic). Upozornění: Uživatelské jméno není ID datové schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="216"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="216"/>
        <source>The password must be valid and non-expired. To check whether you&apos;ve entered the password correctly you may use the icon in the field on the right. Note: You must fist change the password using the ISDS web portal if it is your very first attempt to log into the data box.</source>
        <translation>Heslo musí být validní a neexspirované. Pro ověření, zda jste heslo zadali správně, můžete použít ikonu v poli napravo. Poznámka: Nejrve si musíte změnit heslo ve webovém portálu ISDS, pokud se jedná o Váš zcela první pokus o přihlášení do datové schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="200"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="200"/>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="173"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="173"/>
        <source>Enter the login name.</source>
        <translation>Zadejte přihlašovací jméno.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="207"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="207"/>
        <source>Enter the password.</source>
        <translation>Zadejte heslo.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="221"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="221"/>
        <source>Communication Code</source>
        <translation>Komunikační kód</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="222"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="222"/>
        <source>Enter the communication code.</source>
        <translation>Zadejte komunikační kód.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="234"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="234"/>
        <source>The &lt;a href=&quot;%1&quot;&gt;communication code&lt;/a&gt; is a string which can be generated in the ISDS web portal. You have to have the Mobile Key application installed. The Mobile Key application needs to be paired with the corresponding data-box account on the ISDS web portal.</source>
        <translation>&lt;a href=&quot;%1&quot;&gt;Komunikační kód&lt;/a&gt; je řetězec znaků, který lze vygenerovat ve webovém portálu ISDS. Musíte mít nainstalovanou aplikaci Mobilní klíč. Aplikace Mobilní klíč musí být ve webovém portálu ISDS spárována s odpovídajícím účtem k datové schránce.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="244"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="244"/>
        <source>Certificate File</source>
        <translation>Soubor s certifikátem</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="268"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="268"/>
        <source>Choose file</source>
        <translation>Vybrat soubor</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="286"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="286"/>
        <source>The certificate file is needed for authentication purposes. The supplied file must contain a certificate and its corresponding private key. Only PEM and PFX file formats are accepted.</source>
        <translation>Soubor s certifikátem je vyžadován za účelem autentizace. Dodaný soubor musí obsahovat jak certifikát, tak i odpovídající soukromý klíč. Jen formáty PEM a PFX jsou podporovány.</translation>
    </message>
</context>
<context>
    <name>CreateAccountPage4</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="18"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="18"/>
        <source>During the log-in procedure you will be asked to enter a private key password.</source>
        <translation>V průběhu přihlašování budete vyzváni z zadání hesla k soukromému klíči.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="21"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="21"/>
        <source>During the log-in procedure you will be asked to confirm a notification in the Mobile Key application.</source>
        <translation>V průběhu přihlašování budete vyzváni k potvrzení notifikace v aplikaci Mobilní klíč.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="23"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="23"/>
        <source>During the log-in procedure you will be asked to enter a code which you should obtain via an SMS.</source>
        <translation>V průběhu přihlašování budete vyzváni z zadání kódu, který byste měli obdržet pomocí SMS.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="25"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="25"/>
        <source>During the log-in procedure you will be asked to enter a security code.</source>
        <translation>V průběhu přihlašování budete vyzváni z zadání bezpečnostního kódu.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="59"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="59"/>
        <source>Create Account: 4/4</source>
        <translation>Vytvořit účet 4/4</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="70"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="70"/>
        <source>Create account</source>
        <translation>Vytvořit účet</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="100"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="100"/>
        <source>Account title</source>
        <translation>Pojmenování účtu</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="105"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="105"/>
        <source>Account type</source>
        <translation>Typ účtu</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="110"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="110"/>
        <source>Login method</source>
        <translation>Způsob přihlašování</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="115"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="115"/>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="122"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="122"/>
        <source>Certificate</source>
        <translation>Certifikát</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="132"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="132"/>
        <source>Local Account Settings</source>
        <translation>Místní nastavení účtu</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="136"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="136"/>
        <source>Remember password</source>
        <translation>Zapamatovat si heslo</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="143"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="143"/>
        <source>Use local storage (database)</source>
        <translation>Použít lokální úložiště (databázi)</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="150"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="150"/>
        <source>Copies of messages and attachments will be locally stored. No active internet connection is needed to access locally stored data.</source>
        <translation>Kopie zpráv a příloh budou lokálně ukládány. K přístupu k lokálně uloženým datům není potřeba aktivního připojení k internetu.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="151"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="151"/>
        <source>Copies of messages and attachments will be stored only temporarily in memory. These data will be lost on application exit.</source>
        <translation>Kopie zpráv a příloh budou ukládány pouze dočasně v paměti. Tyto data budou při vypnutí aplikace zahozena.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="157"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="157"/>
        <source>Synchronise together with all accounts</source>
        <translation>Synchronizovat společně se všemi účty</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="164"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="164"/>
        <source>The account will be included into the synchronisation process of all accounts.</source>
        <translation>Účet bude zahrnut do synchronizace všech účtů.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="165"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="165"/>
        <source>The account won&apos;t be included into the synchronisation process of all accounts.</source>
        <translation>Účet nebude zahrnut do synchronizace všech účtů.</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="174"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="174"/>
        <source>Create Account</source>
        <translation>Vytvořit účet</translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="186"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="186"/>
        <source>All done. The account will be created only when the application successfully logs into the data box using the supplied login credentials.</source>
        <translation>Vše hotovo. Účet bude vytvořen jen pokud se aplikace pomocí zadaných přihlašovacích údajů úspěšně přihlásí do datové schánky.</translation>
    </message>
</context>
<context>
    <name>DataboxList</name>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="54"/>
        <location filename="../../qml/components/DataboxList.qml" line="213"/>
        <location filename="../../qml/components/DataboxList.qml" line="54"/>
        <location filename="../../qml/components/DataboxList.qml" line="213"/>
        <source>Public</source>
        <translation>Veřejná</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="56"/>
        <location filename="../../qml/components/DataboxList.qml" line="56"/>
        <source>Response to initiatory</source>
        <translation>Odpověď na iniciační</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="58"/>
        <location filename="../../qml/components/DataboxList.qml" line="58"/>
        <source>Subsidised</source>
        <translation>Dotovaná</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="60"/>
        <location filename="../../qml/components/DataboxList.qml" line="60"/>
        <source>Contractual</source>
        <translation>Smluvní</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="62"/>
        <location filename="../../qml/components/DataboxList.qml" line="62"/>
        <source>Initiatory</source>
        <translation>Iniciační</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="64"/>
        <location filename="../../qml/components/DataboxList.qml" line="64"/>
        <source>Prepaid credit</source>
        <translation>Předplacený kredit</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="154"/>
        <location filename="../../qml/components/DataboxList.qml" line="154"/>
        <source>View details about data box &apos;%1&apos; (identifier &apos;%2&apos;).</source>
        <translation>Zobrazit informace o datové schránce &apos;%1&apos; (identifikátor &apos;%2&apos;).</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="157"/>
        <location filename="../../qml/components/DataboxList.qml" line="157"/>
        <source>Deselect data box &apos;%1&apos; (identifier &apos;%2&apos;).</source>
        <translation>Zrušit výběr datové schránky &apos;%1&apos; (identifikátor &apos;%2&apos;).</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="160"/>
        <location filename="../../qml/components/DataboxList.qml" line="160"/>
        <source>Select data box &apos;%1&apos; (identifier &apos;%2&apos;).</source>
        <translation>Vybrat datovou schránku &apos;%1&apos; (identifikátor &apos;%2&apos;).</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="215"/>
        <location filename="../../qml/components/DataboxList.qml" line="215"/>
        <source>PDZ</source>
        <translation>PDZ</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="217"/>
        <location filename="../../qml/components/DataboxList.qml" line="217"/>
        <source>Unknown</source>
        <translation>Neznámý</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="223"/>
        <location filename="../../qml/components/DataboxList.qml" line="223"/>
        <source>Message type: %1</source>
        <translation>Typ zprávy: %1</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="230"/>
        <location filename="../../qml/components/DataboxList.qml" line="230"/>
        <source>Payment: %1</source>
        <translation>Platba: %1</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="237"/>
        <location filename="../../qml/components/DataboxList.qml" line="237"/>
        <source>Payment:</source>
        <translation>Platba:</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="245"/>
        <location filename="../../qml/components/DataboxList.qml" line="245"/>
        <source>Select commercial message payment method.</source>
        <translation>Vyberte způsob platby za PDZ.</translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="295"/>
        <location filename="../../qml/components/DataboxList.qml" line="295"/>
        <source>Remove data box &apos;%1&apos; (identifier &apos;%2&apos;).</source>
        <translation>Odebrat datovou schránku &apos;%1&apos; (identifikátor &apos;%2&apos;).</translation>
    </message>
</context>
<context>
    <name>DbWrapper</name>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="162"/>
        <location filename="../../src/net/db_wrapper.cpp" line="233"/>
        <source>Message %1 envelope update failed!</source>
        <translation>Aktulizace obálky zprávy  %1 selhala!</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="176"/>
        <source>%1: new messages: %2</source>
        <translation>%1: nových zpráv: %2</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="179"/>
        <source>%1: No new messages.</source>
        <translation>%1: Žádné nové zprávy.</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="135"/>
        <location filename="../../src/net/db_wrapper.cpp" line="226"/>
        <location filename="../../src/net/db_wrapper.cpp" line="310"/>
        <location filename="../../src/net/db_wrapper.cpp" line="338"/>
        <source>Cannot open message database!</source>
        <translation>Nepodařilo se otevřít databázi zpráv!</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="43"/>
        <source>Data box ID</source>
        <translation>ID datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="44"/>
        <source>Data box type</source>
        <translation>Typ datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="47"/>
        <source>IČO</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="54"/>
        <source>Surname</source>
        <translation>Příjmení</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="58"/>
        <source>Date of birth</source>
        <translation>Datum narození</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="62"/>
        <source>City of birth</source>
        <translation>Místo narození</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="66"/>
        <source>County of birth</source>
        <translation>Okres narození</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="70"/>
        <source>State of birth</source>
        <translation>Stát narození</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="74"/>
        <source>Company name</source>
        <translation>Název firmy</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="79"/>
        <source>Street of residence</source>
        <translation>Sídlo - ulice</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="82"/>
        <source>Number in street</source>
        <translation>Číslo orientační</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="84"/>
        <source>Number in municipality</source>
        <translation>Číslo popisné</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="86"/>
        <source>Zip code</source>
        <translation>PSČ</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="93"/>
        <source>District of residence</source>
        <translation>Sídlo - část obce</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="96"/>
        <source>City of residence</source>
        <translation>Sídlo - město</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="99"/>
        <source>State of residence</source>
        <translation>Sídlo - stát</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="103"/>
        <source>Nationality</source>
        <translation>Státní občanství</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="106"/>
        <source>Databox state</source>
        <translation>Stav schránky</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="110"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="110"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="108"/>
        <source>Open addressing</source>
        <translation>Otevřené adresování</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="50"/>
        <source>Given names</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="125"/>
        <location filename="../../src/net/db_wrapper.cpp" line="194"/>
        <location filename="../../src/net/db_wrapper.cpp" line="300"/>
        <location filename="../../src/net/db_wrapper.cpp" line="328"/>
        <source>Internal error!</source>
        <translation>Interní chyba!</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="152"/>
        <source>Message %1 envelope insertion failed!</source>
        <translation>Zpracování obálky zprávy  %1 selhalo!</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="205"/>
        <source>Cannot open file database!</source>
        <translation>Nepodařilo se otevřít databázi souborů!</translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="215"/>
        <source>File %1 insertion failed!</source>
        <translation>Nepodařilo se zpracovat soubor %1!</translation>
    </message>
</context>
<context>
    <name>ErrorEntry</name>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="138"/>
        <source>No error occurred</source>
        <translation>Žádná chyba se nevyskytla</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="141"/>
        <source>Request was malformed</source>
        <translation>Požadavek byl poškozený</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="144"/>
        <source>Identifier is missing</source>
        <translation>Chybějící identifikátor</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="147"/>
        <source>Supplied identifier is wrong</source>
        <translation>Neplatný identifikátor</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="150"/>
        <source>File format is not supported</source>
        <translation>Nepodporovaný formát souboru</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="153"/>
        <source>Data are already present</source>
        <translation>Data již existují</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="156"/>
        <source>Service limit was exceeded</source>
        <translation>Překročen limit služby</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="159"/>
        <source>Unspecified error</source>
        <translation>Blíže neurčená chyba</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="163"/>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
</context>
<context>
    <name>FileDialogue</name>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="36"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="36"/>
        <source>Select path</source>
        <translation>Vybrat cestu</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="41"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="41"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="45"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="45"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="132"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="132"/>
        <source>Select location type</source>
        <translation>Vyberte typ umístění</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="135"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="135"/>
        <source>Desktop</source>
        <translation>Plocha</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="134"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="134"/>
        <source>Documents</source>
        <translation>Dokumenty</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="136"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="136"/>
        <source>Downloads</source>
        <translation>Stažené</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="137"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="137"/>
        <source>Pictures</source>
        <translation>Obrázky</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="138"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="138"/>
        <source>Temp</source>
        <translation>Dočasné</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="160"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="160"/>
        <source>Up</source>
        <translation>Nahoru</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="257"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="257"/>
        <source>Open directory.</source>
        <translation>Otevřít adresář.</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="260"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="260"/>
        <source>File is selected.</source>
        <translation>Soubor je vybrán.</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="262"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="262"/>
        <source>File is not selected.</source>
        <translation>Soubor není vybrán.</translation>
    </message>
</context>
<context>
    <name>Files</name>
    <message>
        <location filename="../../src/files.cpp" line="236"/>
        <location filename="../../src/files.cpp" line="265"/>
        <location filename="../../src/files.cpp" line="274"/>
        <source>Open attachment error</source>
        <translation>Chyba otevření přílohy</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="266"/>
        <location filename="../../src/files.cpp" line="275"/>
        <source>There is no application to open this file format.</source>
        <translation>Neexistuje aplikace, která pracuje s tímto formátem souborů.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="267"/>
        <location filename="../../src/files.cpp" line="276"/>
        <source>File: &apos;%1&apos;</source>
        <translation>Soubor: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="237"/>
        <source>Cannot save selected file to disk for opening.</source>
        <translation>Nelze uložit zvolený soubor pro otevření.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="825"/>
        <source>Path: &apos;%1&apos;</source>
        <translation>Cesta: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="350"/>
        <location filename="../../src/files.cpp" line="725"/>
        <source>ZFO missing</source>
        <translation>Chybějící ZFO</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="351"/>
        <location filename="../../src/files.cpp" line="726"/>
        <source>ZFO message is not present in local database.</source>
        <translation>ZFO se zprávou není uloženo v lokální databázi.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="352"/>
        <location filename="../../src/files.cpp" line="727"/>
        <source>Download complete message again to obtain it.</source>
        <translation>Stáhněte znovu celou datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="406"/>
        <source>Delete files: %1</source>
        <translation>Smazat soubory: %1</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="407"/>
        <source>Do you want to clean up the file database of account &apos;%1&apos;?</source>
        <translation>Opravdu chcete pročistit databázi souborů pro účet &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="408"/>
        <source>Note: All attachment files of messages will be removed from the database.</source>
        <translation>Poznámka: Všechny přílohy zpráv budou odstraněny z databáze.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="450"/>
        <source>Vacuum databases</source>
        <translation>Vakuum databází</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="467"/>
        <source>Operation Vacuum has finished</source>
        <translation>Operace Vakuum byla dokončena</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="824"/>
        <source>Saving Successful</source>
        <translation>Úspěšně uloženo</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="824"/>
        <source>Files have been saved.</source>
        <translation>Soubory byly úspěšně uloženy.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="828"/>
        <source>Saving Failed</source>
        <translation>Ukládání selhalo</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="828"/>
        <source>Files have not been saved.</source>
        <translation>Soubory nebyly uloženy.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="829"/>
        <source>Please check whether the application has permissions to access the storage.</source>
        <translation>Ověřte prosím, zda aplikace má oprávnění k přístupu do úložiště.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="883"/>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="884"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="886"/>
        <source>Attachment size</source>
        <translation>Velikost příloh</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="888"/>
        <source>Personal delivery</source>
        <translation>Doručení do vlastních rukou</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="889"/>
        <location filename="../../src/files.cpp" line="891"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="889"/>
        <location filename="../../src/files.cpp" line="891"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="890"/>
        <source>Delivery by fiction</source>
        <translation>Doručení fikcí</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="893"/>
        <source>Sender</source>
        <translation>Odesílatel</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="894"/>
        <location filename="../../src/files.cpp" line="902"/>
        <source>Databox ID</source>
        <translation>ID schránky</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="896"/>
        <location filename="../../src/files.cpp" line="904"/>
        <source>Name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="898"/>
        <location filename="../../src/files.cpp" line="905"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="901"/>
        <source>Recipient</source>
        <translation>Příjemce</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="908"/>
        <source>To hands</source>
        <translation>K rukám</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="914"/>
        <source>Our file mark</source>
        <translation>Naše spisová značka</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="918"/>
        <source>Our reference number</source>
        <translation>Naše číslo jednací</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="922"/>
        <source>Your file mark</source>
        <translation>Vaše spisová značka</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="926"/>
        <source>Your reference number</source>
        <translation>Vaše číslo jednací</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="930"/>
        <source>Law</source>
        <translation>Zákon</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="934"/>
        <source>Year</source>
        <translation>Rok</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="938"/>
        <source>Section</source>
        <translation>Sekce</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="942"/>
        <source>Paragraph</source>
        <translation>Odstavec</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="946"/>
        <source>Letter</source>
        <translation>Písmeno</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="950"/>
        <source>Additional info</source>
        <translation>Doplňující informace</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="954"/>
        <source>Message state</source>
        <translation>Stav zprávy</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="955"/>
        <source>Delivery time</source>
        <translation>Čas dodání</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="959"/>
        <source>Accetance time</source>
        <translation>Čas doručení</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="963"/>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="967"/>
        <source>Events</source>
        <translation>Události</translation>
    </message>
</context>
<context>
    <name>FilterBar</name>
    <message>
        <location filename="../../qml/components/FilterBar.qml" line="50"/>
        <location filename="../../qml/components/FilterBar.qml" line="50"/>
        <source>Set filter</source>
        <translation>Nastavit filtr</translation>
    </message>
    <message>
        <location filename="../../qml/components/FilterBar.qml" line="78"/>
        <location filename="../../qml/components/FilterBar.qml" line="78"/>
        <source>Clear and hide filter field</source>
        <translation>Vymazat a schovat pole filtru</translation>
    </message>
</context>
<context>
    <name>GlobalSettingsQmlWrapper</name>
    <message>
        <location filename="../../src/setwrapper.cpp" line="370"/>
        <source>Last synchronisation: %1</source>
        <translation>Poslední synchronizace: %1</translation>
    </message>
    <message>
        <location filename="../../src/setwrapper.cpp" line="399"/>
        <source>Select directory</source>
        <translation>Zvolit adresář</translation>
    </message>
    <message>
        <location filename="../../src/setwrapper.cpp" line="415"/>
        <source>New location error</source>
        <translation>Chyba nového umístění</translation>
    </message>
    <message>
        <location filename="../../src/setwrapper.cpp" line="416"/>
        <source>It is not possible to store databases in the new location. Write permission denied.</source>
        <translation>Není možné uložit databáze do nového umístění. Nemáte právo zápisu.</translation>
    </message>
    <message>
        <location filename="../../src/setwrapper.cpp" line="417"/>
        <source>Action will be cancelled.</source>
        <translation>Akce bude zrušena.</translation>
    </message>
</context>
<context>
    <name>GovFormList</name>
    <message>
        <location filename="../../qml/components/GovFormList.qml" line="104"/>
        <location filename="../../qml/components/GovFormList.qml" line="104"/>
        <source>Open calendar</source>
        <translation>Otevřít kalendář</translation>
    </message>
</context>
<context>
    <name>GovWrapper</name>
    <message>
        <location filename="../../src/gov_wrapper.cpp" line="176"/>
        <source>Request: %1</source>
        <translation>Požadavek: %1</translation>
    </message>
    <message>
        <location filename="../../src/gov_wrapper.cpp" line="178"/>
        <source>Recipient: %1</source>
        <translation>Příjemce: %1</translation>
    </message>
    <message>
        <location filename="../../src/gov_wrapper.cpp" line="181"/>
        <source>Send e-gov request</source>
        <translation>Odeslat elektronické podání</translation>
    </message>
    <message>
        <location filename="../../src/gov_wrapper.cpp" line="182"/>
        <source>Do you want to send the e-gov request to data box &apos;%1&apos;?</source>
        <translation>Přejete si odeslat elektronické podání do datové schránky &apos;%1&apos;?</translation>
    </message>
</context>
<context>
    <name>InputDialogue</name>
    <message>
        <location filename="../../qml/dialogues/InputDialogue.qml" line="86"/>
        <location filename="../../qml/dialogues/InputDialogue.qml" line="86"/>
        <source>QML input dialog</source>
        <translation>QML vstupní dialog</translation>
    </message>
</context>
<context>
    <name>InputLineMenu</name>
    <message>
        <location filename="../../qml/components/InputLineMenu.qml" line="37"/>
        <location filename="../../qml/components/InputLineMenu.qml" line="37"/>
        <source>Clear</source>
        <translation>Vymazat</translation>
    </message>
    <message>
        <location filename="../../qml/components/InputLineMenu.qml" line="46"/>
        <location filename="../../qml/components/InputLineMenu.qml" line="46"/>
        <source>Copy</source>
        <translation>Kopírovat</translation>
    </message>
    <message>
        <location filename="../../qml/components/InputLineMenu.qml" line="57"/>
        <location filename="../../qml/components/InputLineMenu.qml" line="57"/>
        <source>Paste</source>
        <translation>Vložit</translation>
    </message>
</context>
<context>
    <name>IosHelper</name>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="59"/>
        <source>iCloud error</source>
        <translation>Chyba iCloudu</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="60"/>
        <source>Unable to access iCloud account. Open the settings and check your iCloud settings.</source>
        <translation>Nelze použít účet pro iCloud. Ověřte nastavení iCloudu na Vašem zařízení.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="81"/>
        <source>Unable to access iCloud!</source>
        <translation>Nelze použít iCloud!</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="86"/>
        <source>Cannot create subdirectory &apos;%1&apos; in iCloud.</source>
        <translation>V iCloudu nelze vytvořit podadresář &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="91"/>
        <source>File &apos;%1&apos; already exists in iCloud.</source>
        <translation>Soubor &apos;%1&apos; již v iCloudu existuje.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="95"/>
        <source>File &apos;%1&apos; upload failed.</source>
        <translation>Nahrávání souboru &apos;%1&apos; selhalo.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="98"/>
        <source>File &apos;%1&apos; has been stored into iCloud.</source>
        <translation>Soubor &apos;%1&apos; byl uložen do iCloudu.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="114"/>
        <source>Saved to iCloud</source>
        <translation>Uloženo do iCloudu</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="115"/>
        <source>Files have been stored into iCloud.</source>
        <translation>Soubory byly uloženy do iCloudu.</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="116"/>
        <source>Path: &apos;%1&apos;</source>
        <translation>Cesta: &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="125"/>
        <source>iCloud Problem</source>
        <translation>Problém s iCloudem</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="126"/>
        <source>Files have not been saved!</source>
        <translation>Soubory nebyly uloženy!</translation>
    </message>
</context>
<context>
    <name>IsdsConversion</name>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="36"/>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="70"/>
        <source>Primary user</source>
        <translation>Oprávněná osoba</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="39"/>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="73"/>
        <source>Entrusted user</source>
        <translation>Pověřená osoba</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="42"/>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="76"/>
        <source>Administrator</source>
        <translation>Administrátor</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="45"/>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="48"/>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="79"/>
        <source>Official</source>
        <translation>Systém ISDS</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="82"/>
        <source>Virtual</source>
        <translation>Virtuální osoba</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="51"/>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="85"/>
        <source>Liquidator</source>
        <translation>Likvidátor</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="54"/>
        <source>Receiver</source>
        <translation>Nucený správce</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="57"/>
        <source>Guardian</source>
        <translation>Opatrovník</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="60"/>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="88"/>
        <source>Unknown</source>
        <translation>Neznámý</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="97"/>
        <source>The data box is accessible. It is possible to send messages into it. It can be looked up on the Portal.</source>
        <translation>Datová schránka je přístupná, lze do ní dodávat zprávy, na Portále lze vyhledat.</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="102"/>
        <source>The data box is temporarily inaccessible (at own request). It may be made accessible again at some point in the future.</source>
        <translation>Datová schránka je dočasně znepřístupněna (na vlastní žádost), může být později opět zpřístupněna.</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="107"/>
        <source>The data box is so far inactive. The owner of the box has to log into the web interface at first in order to activate the box.</source>
        <translation>Datová schránka je dosud neaktivní. Vlastník schránky se musí poprvé přihlásit do webového rozhraní, aby došlo k aktivaci schránky.</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="111"/>
        <source>The data box is permanently inaccessible. It is waiting to be deleted (but it may be made accessible again).</source>
        <translation>Datová schránka je trvale znepřístupněna, čeká na smazání (může být opět zpřístupněna).</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="115"/>
        <source>The data box has been deleted (nonetheless it exists in ISDS).</source>
        <translation>Datová schránka byla smazána (přesto existuje v ISDS).</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="119"/>
        <source>The data box is temporarily inaccessible (because of the reasons enumerated in the law at the time of access denial). It may be made accessible again at some point in the future.</source>
        <translation>Datová schránka je dočasně znepřístupněna (z důvodů vyjmenovaných v zákoně resp. při přerušení činnosti), může být později opět zpřístupněna.</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="124"/>
        <source>An error occurred while checking the status.</source>
        <translation>Došlo k chybě při zjišťování stavu.</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="137"/>
        <source>Subsidised postal data message, initiating reply postal data message</source>
        <translation>Poštovní datová zpráva dotovaná, iniciuje použití odpovědní poštovní datové zprávy</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="141"/>
        <source>Subsidised postal data message, initiating reply postal data message - used for sending reply</source>
        <translation>Poštovní datová zpráva dotovaná, iniciuje použití odpovědní poštovní datové zprávy - využitá pro odeslání odpovědi</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="145"/>
        <source>Subsidised postal data message, initiating reply postal data message - unused for sending reply, expired</source>
        <translation>Poštovní datová zpráva dotovaná, iniciuje použití odpovědní poštovní datové zprávy - nevyužitá a expirovaná</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="149"/>
        <source>Postal data message sent using a subscription (prepaid credit)</source>
        <translation>Poštovní datová zpráva odeslaná pomocí předplacení (kreditu)</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="153"/>
        <source>Postal data message sent in endowment mode by another data box to the benefactor account</source>
        <translation>Poštovní datová zpráva zaslaná v režimu dotování jinou schránkou na účet schránky donátora</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="160"/>
        <source>Initiating postal data message</source>
        <translation>Iniciační poštovní datová zpráva</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="163"/>
        <source>Reply postal data message; sent at the expense of the sender of the initiating postal data message</source>
        <translation>Odpovědní poštovní datová zpráva; zasílaná zdarma na účet odesílatele iniciační poštovní datové zprávy</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="167"/>
        <source>Public message (recipient or sender is a public authority)</source>
        <translation>Veřejná zpráva (adresát nebo odesílatel je OVM)</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="171"/>
        <source>Initiating postal data message - unused for sending reply, expired</source>
        <translation>Iniciační poštovní datová zpráva - nevyužitá a expirovaná</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="175"/>
        <source>Initiating postal data message - used for sending reply</source>
        <translation>Iniciační poštovní datová zpráva - využitá pro odeslání odpovědi</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="179"/>
        <source>Unrecognised message type</source>
        <translation>Nerozpoznaný typ zprávy</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="157"/>
        <source>Postal data message</source>
        <translation>Poštovní datová zpráva</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="189"/>
        <source>Message has been submitted (has been created in ISDS)</source>
        <translation>Zpráva byla podána (vznikla v ISDS)</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="195"/>
        <source>Data message including its attachments signed with time-stamp.</source>
        <translation>Datová zpráva včetně písemností podepsána časovým razítkem.</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="202"/>
        <source>Message did not pass through AV check; infected paper deleted; final status before deletion.</source>
        <translation>Zpráva neprošla AV kontrolou; nakažená písemnost je smazána; konečný stav zprávy před smazáním.</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="207"/>
        <source>Message handed into ISDS (delivery time recorded).</source>
        <translation>Zpráva dodána do ISDS (zapsán čas dodání).</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="216"/>
        <source>10 days have passed since the delivery of the public message which has not been accepted by logging-in (assumption of delivery by fiction in nonOVM DS); this state cannot occur for commercial messages.</source>
        <translation>Uplynulo 10 dnů od dodání veřejné zprávy, která dosud nebyla doručena přihlášením (předpoklad doručení fikcí u ne-OVM DS); tento stav nemůže nastat v případě poštovních datových zpráv.</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="226"/>
        <source>A person authorised to read this message has logged in -- delivered message has been accepted.</source>
        <translation>Osoba oprávněná číst tuto zprávu se přihlásila - dodaná zpráva byla doručena.</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="231"/>
        <source>Message has been read (on the portal or by ESS action).</source>
        <translation>Zpráva byla přečtena (na portále nebo akcí ESS).</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="239"/>
        <source>Message marked as undeliverable because the target databox has been made inaccessible.</source>
        <translation>Zpráva byla označena jako nedoručitelná, protože DS adresáta byla zpětně znepřístupněna.</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="247"/>
        <source>Message content deleted, envelope including hashes has been moved into archive.</source>
        <translation>Obsah zprávy byl smazán, obálka zprávy včetně hashe přesunuta do archívu.</translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="252"/>
        <source>Message resides in data vault.</source>
        <translation>Zpráva je v Datovém trezoru.</translation>
    </message>
</context>
<context>
    <name>IsdsWrapper</name>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="149"/>
        <source>Downloading message %1</source>
        <translation>Stahování zprávy %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="163"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="181"/>
        <source>Finding databoxes</source>
        <translation>Vyhledávání datových schránek</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="988"/>
        <source>Delivery info: %1</source>
        <translation>Informace o doručení: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="482"/>
        <source>Account &apos;%1&apos; requires SMS code for password changing.</source>
        <translation>Účet &apos;%1&apos; vyžaduje SMS kód pro změnu hesla.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1004"/>
        <source>Failed to download complete message %1.</source>
        <translation>Chyba při stahování kompletní zprávy %1.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1009"/>
        <source>Message %1 has been downloaded</source>
        <translation>Zpráva %1 byla stažena</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1003"/>
        <source>Downloading message: %1</source>
        <translation>Stahuji zprávu: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="989"/>
        <source>Failed to download delivery info for message %1.</source>
        <translation>Stažení informací o doručení zprávy %1 selhalo.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="818"/>
        <source>Getting account info %1</source>
        <translation>Získávám info o účtě %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="971"/>
        <source>Account info: %1</source>
        <translation>Informace o doručení: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="972"/>
        <source>Failed to get account info for username %1.</source>
        <translation>Nezdařilo se stáhnout informace o účtu pro uživatelské jméno %1.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1099"/>
        <source>%1: Message has been sent</source>
        <translation>%1: Zpráva byla odeslána</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1100"/>
        <source>Message has successfully been sent to all recipients.</source>
        <translation>Zpráva byla úspěšně odeslána všem příjemcům.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1105"/>
        <source>%1: Error sending message!</source>
        <translation>%1: Chyba při odeslání zprávy!</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1106"/>
        <source>Message has NOT been sent to all recipients.</source>
        <translation>Zpráva nebyla odeslána všem příjemcům.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="879"/>
        <source>Enter password</source>
        <translation>Zadejte heslo</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="484"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="890"/>
        <source>Do you want to send SMS code now?</source>
        <translation>Chcete zaslat SMS kód?</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="265"/>
        <source>No zfo files or selected folder.</source>
        <translation>Žádný zfo soubor nebo vybraná složka.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="214"/>
        <source>Find data box: %1</source>
        <translation>Vyhledat schránku: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="215"/>
        <source>Failed finding data box to phrase &apos;%1&apos;.</source>
        <translation>Vyhledávání schránky k řetězci &quot;%1&quot; selhalo.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="277"/>
        <source>No account for zfo import.</source>
        <translation>Není žádný účet pro import zfo.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="286"/>
        <source>No zfo files in the selected directory.</source>
        <translation>Vybraná složka neobsahuje zfo soubory.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="292"/>
        <source>No zfo file for import.</source>
        <translation>Žádný zfo soubor pro import.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="305"/>
        <source>ZFO messages cannot be imported because there is no active account for their verification.</source>
        <translation>ZFO zprávy nemohou být importovány, protože není k dispozici žádný aktivní účet pro jejich verifikaci.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="309"/>
        <source>ZFO import</source>
        <translation>Import ZFO</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="461"/>
        <source>%1: sending message</source>
        <translation>%1: odesílání zprávy</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="526"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="539"/>
        <source>%1: downloading...</source>
        <translation>%1: stahuji...</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="596"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="602"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="608"/>
        <source>Unknown Message Type</source>
        <translation>Neznámý typ zprávy</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="597"/>
        <source>No information about the recipient data box &apos;%1&apos; could be obtained. It is unknown whether public or commercial messages can be sent to this recipient.</source>
        <translation>Nebylo možné obdržet žádné informace o datové schránce příjemce &quot;%1&quot;. Není známo, zda tomuto příjemci lze poslat veřejnou či poštovní datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="603"/>
        <source>No commercial message to the recipient data box &apos;%1&apos; can be sent. It is unknown whether a public messages can be sent to this recipient.</source>
        <translation>Do datové schránky příjemce &quot;%1&quot; není možné zaslat poštovní datovou zprávu. Není známo, zda tomuto příjemci lze poslat veřejnou datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="609"/>
        <source>No public message to the recipient data box &apos;%1&apos; can be sent. It is unknown whether a commercial messages can be sent to this recipient.</source>
        <translation>Do datové schránky příjemce &quot;%1&quot; není možné zaslat veřejnou datovou zprávu. Není známo, zda tomuto příjemci lze poslat poštovní datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="614"/>
        <source>Cannot send message</source>
        <translation>Zprávu nelze odeslat</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="615"/>
        <source>No public data message nor a commercial data message (PDZ) can be sent to the recipient data box &apos;%1&apos;.</source>
        <translation>Do datové schránky příjemce &quot;%1&quot; nelze zaslat veřejnou ani poštovní datovou zprávu.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="616"/>
        <source>Receiving of PDZs has been disabled in the recipient data box or there are no available payment methods for PDZs.</source>
        <translation>Příjem PDZ byl v datové schránce příjemce zakázán nebo nejsou dostupné platební metody pro PDZ.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="623"/>
        <source>Data box is not active</source>
        <translation>Datová schránka není aktivní</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="624"/>
        <source>Recipient data box ID &apos;%1&apos; isn&apos;t active.</source>
        <translation>Datová schránka příjemce &quot;%1&quot; není aktivní.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="625"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="634"/>
        <source>The message cannot be delivered.</source>
        <translation>Zprávu nelze dodat.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="632"/>
        <source>Wrong Recipient</source>
        <translation>Chybný příjemce</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="633"/>
        <source>Recipient with data box ID &apos;%1&apos; doesn&apos;t exist.</source>
        <translation>Příjemce s datovou schránkou &quot;%1&quot; neexistuje.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="642"/>
        <source>Recipient Search Failed</source>
        <translation>Chyba vyhledávání příjemce</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="643"/>
        <source>Information about recipient data box &apos;%1&apos; couldn&apos;t be obtained.</source>
        <translation>Nebylo možné obdržet informace o datové schránce příjemce &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="644"/>
        <source>Do you still want to add the box &apos;%1&apos; into the recipient list?</source>
        <translation>Chcete stále přidat schránku &quot;%1&quot; do seznamu příjemců?</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="700"/>
        <source>Logging in to data box %1</source>
        <translation>Přihlašování do schránky %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="745"/>
        <source>Internal error.</source>
        <translation>Interní chyba.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="749"/>
        <source>Login failed.</source>
        <translation>Přihlášení selhalo.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="862"/>
        <source>Enter communication code: %1</source>
        <translation>Zadejte komunikační kód: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="863"/>
        <source>Communication code for account &apos;%1&apos; is required.</source>
        <translation>Je vyžadován kód pro účet &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="865"/>
        <source>Enter communication code</source>
        <translation>Zadejte komunikační kód</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="877"/>
        <source>Password for account &apos;%1&apos; missing.</source>
        <translation>Chybí heslo pro účet &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="904"/>
        <source>Certificate password for account &apos;%1&apos; is required.</source>
        <translation>Je vyžadováno heslo k certifikátu pro účet &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="919"/>
        <source>New account problem: %1</source>
        <translation>Chyba vytváření účtu: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="920"/>
        <source>New account could not be created for username &apos;%1&apos;.</source>
        <translation>Nebylo možné vytvořit účet pro uživatelské jméno &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="923"/>
        <source>Note: You must change your password via the ISDS web portal if this is your first login to the data box. You can start using mobile Datovka to access the data box after changing.</source>
        <translation>Poznámka: Pokud se přihlašujete do datové schránky poprvé, tak musíte použit webový portál ISDS, kde si po prvním přihlášení změníte heslo. Po změně hesla pak můžete začít používat Datovku pro přístup do datové schránky.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="927"/>
        <source>Username could not be changed on the new username &apos;%1&apos;.</source>
        <translation>Uživatelské jméno se nepodařilo změnit na nové &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="947"/>
        <source>Security code for &apos;%1&apos; required</source>
        <translation>Je vyžadován bezpečnostní kód pro &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1024"/>
        <source>%1: messages</source>
        <translation>%1: zpráv</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1025"/>
        <source>Failed to download list of messages for user name &apos;%1&apos;.</source>
        <translation>Chyba při stahování seznamu zpráv pro uživatelské jméno &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1038"/>
        <source>ZFO import: %1/%2</source>
        <translation>ZFO import: %1/%2</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1042"/>
        <source>ZFO import has been finished with the result:</source>
        <translation>ZFO import byl dokončen s výsledkem:</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1043"/>
        <source>ZFO import: done</source>
        <translation>ZFO import: Dokončeno</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1078"/>
        <source>Message has been sent to &lt;b&gt;&apos;%1&apos;&lt;/b&gt; &lt;i&gt;(%2)&lt;/i&gt; as message number &lt;b&gt;%3&lt;/b&gt;.</source>
        <translation>Zpráva byla odeslána příjemci &lt;b&gt;&apos;%1&apos;&lt;/b&gt; &lt;i&gt;(%2)&lt;/i&gt; pod číslem &lt;b&gt;%3&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1085"/>
        <source>Message has NOT been sent to &lt;b&gt;&apos;%1&apos;&lt;/b&gt; &lt;i&gt;(%2)&lt;/i&gt;. ISDS returns: %3</source>
        <translation>Zpráva nebyla odeslána příjemci &lt;b&gt;&apos;%1&apos;&lt;/b&gt; &lt;i&gt;(%2)&lt;/i&gt;. ISDS vrací: %3</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1095"/>
        <source>Sending finished</source>
        <translation>Odeslání dokončeno</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1127"/>
        <source>PDZ sending: unavailable</source>
        <translation>Odesílání PDZ: nedostupné</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1142"/>
        <source>PDZ sending: active</source>
        <translation>Odesílání PDZ: aktivní</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1145"/>
        <source>Subsidised by data box &apos;%1&apos;.</source>
        <translation>Dotovaná datovou schránkou &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1148"/>
        <source>Contract with Czech post</source>
        <translation>Smlouva s Českou poštou</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1152"/>
        <source>Remaining credit: %1 Kč</source>
        <translation>Zbývající kredit: %1 Kč</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="926"/>
        <source>Username change: %1</source>
        <translation>Změna uživatelského jména: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="876"/>
        <source>Enter password: %1</source>
        <translation>Zadejte heslo: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="946"/>
        <source>Enter security code: %1</source>
        <translation>Zadejte bezpečnostní kód: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="903"/>
        <source>Enter certificate password: %1</source>
        <translation>Zadejte heslo k certifikátu: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="723"/>
        <source>Waiting for confirmation ...</source>
        <translation>Čekám na potvrzení ...</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="959"/>
        <source>Enter SMS code: %1</source>
        <translation>Zadejte SMS kód: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="960"/>
        <source>SMS code for &apos;%1&apos; required</source>
        <translation>Je vyžadován SMS kód pro &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="961"/>
        <source>Enter SMS code</source>
        <translation>Zadejte SMS kód</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="948"/>
        <source>Enter security code</source>
        <translation>Zadejte bezpečnostní kód</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="906"/>
        <source>Enter certificate password</source>
        <translation>Zadejte heslo k certifikátu</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="932"/>
        <source>Login problem: %1</source>
        <translation>Problém s přihlášením: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="933"/>
        <source>Error while logging in with username &apos;%1&apos;.</source>
        <translation>Chyba při přihlášení uživatelským jménem &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="481"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="887"/>
        <source>SMS code: %1</source>
        <translation>SMS kód: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="888"/>
        <source>Account &apos;%1&apos; requires authentication with SMS code.</source>
        <translation>Účet &apos;%1&apos; vyžaduje přihlášení SMS kódem.</translation>
    </message>
</context>
<context>
    <name>Log</name>
    <message>
        <location filename="../../src/log.cpp" line="89"/>
        <source>Send log via email</source>
        <translation>Odeslat log emailem</translation>
    </message>
    <message>
        <location filename="../../src/log.cpp" line="90"/>
        <source>Do you want to send the log information to developers?</source>
        <translation>Chcete odeslat log vývojářům?</translation>
    </message>
</context>
<context>
    <name>LogBar</name>
    <message>
        <location filename="../../qml/components/LogBar.qml" line="47"/>
        <location filename="../../qml/components/LogBar.qml" line="47"/>
        <source>Close log bar</source>
        <translation>Zavřít panel logu</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../../src/isds/isds_login.cpp" line="328"/>
        <source>MEP confirmation timeout.</source>
        <translation>Vypršela délka MEP požadavku.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_login.cpp" line="333"/>
        <source>MEP login was cancelled.</source>
        <translation>Přihlašování pomocí MEP přerušeno.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_login.cpp" line="338"/>
        <source>Wrong MEP login credentials. Invalid username or communication code.</source>
        <translation>Nesprávné údaje pro přihlášení Mobilním klíčem. Neplatné přihlašovací jméno nebo komunikační kód.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_login.cpp" line="343"/>
        <source>MEP login failed.</source>
        <translation>Přihlašování pomocí MEP selhalo.</translation>
    </message>
</context>
<context>
    <name>MessageDialogue</name>
    <message>
        <location filename="../../qml/dialogues/MessageDialogue.qml" line="84"/>
        <location filename="../../qml/dialogues/MessageDialogue.qml" line="84"/>
        <source>Question</source>
        <translation>Dotaz</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/MessageDialogue.qml" line="85"/>
        <location filename="../../qml/dialogues/MessageDialogue.qml" line="85"/>
        <source>Information</source>
        <translation>Informace</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/MessageDialogue.qml" line="86"/>
        <location filename="../../qml/dialogues/MessageDialogue.qml" line="86"/>
        <source>Warning</source>
        <translation>Varování</translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/MessageDialogue.qml" line="87"/>
        <location filename="../../qml/dialogues/MessageDialogue.qml" line="87"/>
        <source>Critical</source>
        <translation>Kritické</translation>
    </message>
</context>
<context>
    <name>MessageList</name>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="123"/>
        <location filename="../../qml/components/MessageList.qml" line="123"/>
        <source>Unread message from sender</source>
        <translation>Nepřečtená zpráva od odesílatele</translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="123"/>
        <location filename="../../qml/components/MessageList.qml" line="123"/>
        <source>Read message from sender</source>
        <translation>Přečtená zpráva od odesílatele</translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="125"/>
        <location filename="../../qml/components/MessageList.qml" line="125"/>
        <source>Message to receiver</source>
        <translation>Zpráva příjemci</translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="128"/>
        <location filename="../../qml/components/MessageList.qml" line="128"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
</context>
<context>
    <name>Messages</name>
    <message>
        <location filename="../../src/messages.cpp" line="313"/>
        <source>Delete message: %1</source>
        <translation>Smazat zprávu: %1</translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="314"/>
        <source>Do you want to delete the message &apos;%1&apos;?</source>
        <translation>Přejete si smazat zprávu &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="315"/>
        <source>Note: It will delete all attachments and message information from the local database.</source>
        <translation>Poznámka: Z místní databáze budou smazány všechny přílohy a informace o zprávě.</translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="404"/>
        <source>New location error</source>
        <translation>Chyba nového umístění</translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="405"/>
        <source>It is not possible to store databases in the new location because there is not enough free space left.</source>
        <translation>Není možné uložit databáze do nového umístění, protože se tam nenachází dostatek volného místa.</translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="406"/>
        <source>Databases size is %1 MB.</source>
        <translation>Velikost databáze je %1 MB.</translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="478"/>
        <source>Change database location</source>
        <translation>Změna umístění databází</translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="479"/>
        <source>What do you want to do with the currently used database files?</source>
        <translation>Co chcete udělat se stávajícími databázovými soubory?</translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="480"/>
        <source>You have the option to move the current database files to a new location or you can delete them and create new empty databases.</source>
        <translation>Máte možnost přesunout soubory současných databází do nového umístění, nebo je můžete smazat a vytvořit nové prázdné databáze.</translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="473"/>
        <source>Move</source>
        <translation>Přesunout</translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="474"/>
        <source>Create new</source>
        <translation>Nově vytvořit</translation>
    </message>
</context>
<context>
    <name>MvCrrVbhData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="91"/>
        <source>Driving licence ID</source>
        <translation>Číslo řidičského oprávnění</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="92"/>
        <source>Enter driving licence ID without spaces</source>
        <translation>Zadejte číslo řidičského průkazu bez mezer</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="102"/>
        <source>Data-box owner name</source>
        <translation>Jméno vlastníka datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="112"/>
        <source>Data-box owner surname</source>
        <translation>Příjmení vlastníka datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="122"/>
        <source>Birth date</source>
        <translation>Datum narození</translation>
    </message>
</context>
<context>
    <name>MvIrVpData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_ir_vp.cpp" line="516"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_ir_vp.cpp" line="517"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>MvRtVtData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="106"/>
        <source>Data-box owner name</source>
        <translation>Jméno vlastníka datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="116"/>
        <source>Data-box owner surname</source>
        <translation>Příjmení vlastníka datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="126"/>
        <source>Birth date</source>
        <translation>Datum narození</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="136"/>
        <source>Birth region</source>
        <translation>Okres (narození)</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="146"/>
        <source>City</source>
        <translation>Město</translation>
    </message>
</context>
<context>
    <name>MvRtpoVtData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.cpp" line="521"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.cpp" line="522"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>MvSkdVpData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_skd_vp.cpp" line="517"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_skd_vp.cpp" line="518"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>MvVrVpData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_vr_vp.cpp" line="550"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_vr_vp.cpp" line="551"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>MvZrVpData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_zr_vp.cpp" line="516"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_zr_vp.cpp" line="517"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>PageAboutApp</name>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="37"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="37"/>
        <source>About Datovka</source>
        <translation>O Datovce</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="88"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="88"/>
        <source>Datovka - free mobile Data-Box client</source>
        <translation>Datovka - svobodný mobilní klient pro Datové schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="75"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="75"/>
        <source>Open application home page.</source>
        <translation>Otevřít domovskou stránku aplikace.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="90"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="90"/>
        <source>Version: %1</source>
        <translation>Verze: %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="102"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="102"/>
        <source>This application provides the means to access data boxes in the ISDS system. It enables you to download and to view the content of messages held within the data box. You can also, with some limitations, send messages from this application.</source>
        <translation>Tato aplikace poskytuje přístup k datovým schránkám v systému ISDS. Dovoluje Vám stahovat a prohlížet obsah zpráv uvnitř datové schránky. Můžete také, s jistými omezeními, z této aplikace zprávy odesílat.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="104"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="104"/>
        <source>You may use this application only at your own risk. The association CZ.NIC is not the operator of the data box system. CZ.NIC is not liable for any damage that may be directly or indirectly caused by using this application.</source>
        <translation>Tuto aplikaci můžete používat pouze na vlastní nebezpečí. Sdružení CZ.NIC není provozovatelem systému datových schránek. CZ.NIC neručí za zádné škody, které mohou být přímo či nepřímo způsobeny použitím této aplikace.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="106"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="106"/>
        <source>If you have problems using this application or want help or you just want more information then start by reading through the information in the &lt;a href=&quot;%1&quot;&gt;user manual&lt;/a&gt; or on the &lt;a href=&quot;%2&quot;&gt;project web page&lt;/a&gt;.</source>
        <translation>Máte-li potíže s používáním této aplikace, potřebujete-li pomoc, či jen chcete další informace, tak začněte pročítáním informací v &lt;a href=&quot;%1&quot;&gt;uživatelské příručce&lt;/a&gt; nebo na &lt;a href=&quot;%2&quot;&gt;webových stránkách projektu&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="119"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="119"/>
        <source>The application is licensed under the &lt;a href=&quot;%1&quot;&gt;GNU GPL v3&lt;/a&gt;.</source>
        <translation>Aplikace je distribuovaná pod licencí &lt;a href=&quot;%1&quot;&gt;GNU GPL v3&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="125"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="125"/>
        <source>Powered by</source>
        <translation>Vyvíjeno</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="132"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="132"/>
        <source>Open home page of the CZ.NIC association.</source>
        <translation>Otevřít domovskou stránku sdružení CZ.NIC.</translation>
    </message>
</context>
<context>
    <name>PageAccountDetail</name>
    <message>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="49"/>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="49"/>
        <source>Account info</source>
        <translation>Informace o účtu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="60"/>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="60"/>
        <source>Refresh information about the account.</source>
        <translation>Obnovit informace o účtu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="102"/>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="102"/>
        <source>Account info has not been downloaded yet.</source>
        <translation>Informace o účtu nebyly doposud staženy.</translation>
    </message>
</context>
<context>
    <name>PageAccountList</name>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="120"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="120"/>
        <source>Synchronise all accounts.</source>
        <translation>Synchronizovat všechny účty.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="106"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="106"/>
        <source>Show application preferences.</source>
        <translation>Zobrazit nastavení aplikace.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="94"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="94"/>
        <source>Accounts</source>
        <translation>Účty</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="133"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="133"/>
        <source>Add a new account</source>
        <translation>Přidat nový účet</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="185"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="185"/>
        <source>Hide banner</source>
        <translation>Skrýt banner</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="197"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="197"/>
        <source>Permanently hide banner</source>
        <translation>Trvale skrýt banner</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="198"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="198"/>
        <source>Do you want to permanently hide the banner?</source>
        <translation>Přejete si banner skrýt natrvalo?</translation>
    </message>
</context>
<context>
    <name>PageBackupData</name>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="67"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="215"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="67"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="215"/>
        <source>Target</source>
        <translation>Cíl</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="67"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="67"/>
        <source>Application sandbox</source>
        <translation>Sandbox aplikace</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="73"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="73"/>
        <source>Transfer application data</source>
        <translation>Přenos dat aplikace</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="74"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="74"/>
        <source>The action allows to transfer complete application data to another device. Preserve the files in a safe place as it contains login and private data. Transferred data require to be secured with a PIN. Set PIN in the application settings if it not set.</source>
        <translation>Akce dovoluje přenést kompletní data aplikace na jiné zařízení. Uchovávejte tyto data na bezpečném místě, protože obsahují přihlašovací a jiné osobní údaje. Přenášená data vyžadují, aby byla zabezpečena PINem. Nastavte PIN v nastavení aplikace, pokud není nastaven.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="76"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="84"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="76"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="84"/>
        <source>Data will be stored into the application sandbox and, subsequently, you can upload them into the iCloud or you can use the iTunes application in order to copy them to your Mac or PC.</source>
        <translation>Data budou uložena do sandboxu aplikace, odkud je můžete následně přenést do iCloudu nebo můžete použít iTunes pro přenesení dat do Vašeho Macu nebo PC.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="78"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="86"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="78"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="86"/>
        <source>Specify the location where you want to store the data.</source>
        <translation>Určete umístění, kam si přejete zapsat data.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="81"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="135"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="81"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="135"/>
        <source>Back up accounts</source>
        <translation>Záloha účtů</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="81"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="81"/>
        <source>Back up account</source>
        <translation>Záloha účtu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="82"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="82"/>
        <source>The action allows to back up selected accounts. Preserve the backup in a safe place as it contains private data.</source>
        <translation>Akce dovoluje zálohovat data zvolených účtů. Uchovávejte tyto data na bezpečném místě, protože obsahují osobní údaje.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="150"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="150"/>
        <source>Proceed with operation</source>
        <translation>Pokračovat v operaci</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="190"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="190"/>
        <source>Specify the location where you want to back up your data. Preserve the backup in a safe place as it contains private data.</source>
        <translation>Určete umístění, kam si přejete zálohovat data. Uchovávejte tyto data na bezpečném místě, protože obsahují osobní údaje.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="194"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="194"/>
        <source>Back up ZFO data</source>
        <translation>Zálohovat ZFO data</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="202"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="202"/>
        <source>Choose location</source>
        <translation>Vybrat umístění</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="230"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="230"/>
        <source>Select all accounts</source>
        <translation>Vybrat všechny účty</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="274"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="274"/>
        <source>Select %1.</source>
        <translation>Vybrat %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="274"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="274"/>
        <source>Deselect %1.</source>
        <translation>Zrušit výběr %1.</translation>
    </message>
</context>
<context>
    <name>PageChangePassword</name>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="46"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="46"/>
        <source>Enter current password, twice a new password and SMS code.</source>
        <translation>Zadejte aktuální heslo, dvakrát nové heslo a kód SMS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="46"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="52"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="57"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="46"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="52"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="57"/>
        <source>Account is logged in to ISDS.</source>
        <translation>Účet je přihlášen do ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="47"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="47"/>
        <source>Enter SMS code</source>
        <translation>Zadejte SMS kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="52"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="52"/>
        <source>Enter current password, twice a new password and security code.</source>
        <translation>Zadejte aktuální heslo, dvakrát nové heslo a bezpečnostní kód.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="53"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="53"/>
        <source>Enter security code</source>
        <translation>Zadejte bezpečnostní kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="57"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="57"/>
        <source>Enter current password and twice a new password.</source>
        <translation>Zadejte aktuální heslo a dvakrát nové heslo.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="77"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="77"/>
        <source>Account must be logged in to the data box to be able to change the password.</source>
        <translation>Účet musí být přihlášen do datové schránky, aby bylo možné změnit heslo.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="85"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="85"/>
        <source>Change password</source>
        <translation>Změnit heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="112"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="112"/>
        <source>Accept changes</source>
        <translation>Přijmout změny</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="115"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="115"/>
        <source>Old and new password must both be filled in.</source>
        <translation>Staré a nové heslo musí být obě vyplněna.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="120"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="120"/>
        <source>The new password fields do not match.</source>
        <translation>Obsah polí s novým heslem si neodpovídá.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="125"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="125"/>
        <source>Wrong password format. The new password must contain at least 8 characters including at least 1 number and at least 1 upper-case letter.</source>
        <translation>Špatný formát hesla. Nové heslo musí být alepoň 8 znaků dlouhé včetně nejméně 1 čísla a nejméně 1 velkého písmene.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="131"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="131"/>
        <source>Security/SMS code must be filled in.</source>
        <translation>Bezpečnostní/SMS kód musí být vyplněn.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="141"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="141"/>
        <source>Password change failed.</source>
        <translation>Změna hesla selhala.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="267"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="267"/>
        <source>Enter the current password to be able to send the SMS code.</source>
        <translation>Zadejte současné heslo, aby bylo možné zaslat SMS kód.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="278"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="278"/>
        <source>SMS code cannot be sent. Entered wrong current password or ISDS server is out of order.</source>
        <translation>Nelze zaslat SMS kód. Bylo zadáno špatné současné heslo nebo ISDS server je mimo provoz.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="101"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="101"/>
        <source>Hide passwords</source>
        <translation>Skrýt hesla</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="101"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="101"/>
        <source>Show passwords</source>
        <translation>Zobrazit hesla</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="168"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="168"/>
        <source>Enter the current password and a new password.</source>
        <translation>Zadejte aktuální heslo a nové heslo.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="178"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="178"/>
        <source>Log in now</source>
        <translation>Přihlásit se nyní</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="194"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="194"/>
        <source>Current password</source>
        <translation>Aktuální heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="218"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="218"/>
        <source>New password</source>
        <translation>Nové heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="242"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="242"/>
        <source>Confirm the new password</source>
        <translation>Potvrďte nové heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="264"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="264"/>
        <source>Send SMS code</source>
        <translation>Zaslat kód SMS</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="273"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="273"/>
        <source>SMS code was sent...</source>
        <translation>SMS kód byl odeslán...</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="296"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="296"/>
        <source>Enter OTP code</source>
        <translation>Zadejte OTP kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="316"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="316"/>
        <source>This will change the password on the ISDS server. You won&apos;t be able to log in to ISDS without the new password. If your current password has already expired then you must log into the ISDS web portal to change it.</source>
        <translation>Tato operace změní heslo na serveru ISDS. Bez nového hesla se nebudete moci přihlásit do ISDS. Pokud již vypršela platnost vašeho aktuálního hesla, tak se pro jeho změnu musíte přihlásit do webového portálu ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="325"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="325"/>
        <source>If you just want to update the password because it has been changed elsewhere then go to the </source>
        <translation>Pokud chcete jen opravit nastavení hesla, protože bylo změněno odjinud, tak použijte </translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="325"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="325"/>
        <source>Account settings</source>
        <translation>Nastavení účtu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="334"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="334"/>
        <source>Keep a copy of the login credentials and store it on a safe place which only an authorised person has access to.</source>
        <translation>Udržujte si kopii přihlašovacích údajů a uchovávejte ji na bezpečném místě, kam má přístup jen oprávněná osoba.</translation>
    </message>
</context>
<context>
    <name>PageContactList</name>
    <message>
        <location filename="../../qml/pages/PageContactList.qml" line="72"/>
        <location filename="../../qml/pages/PageContactList.qml" line="72"/>
        <source>Contacts</source>
        <translation>Kontakty</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageContactList.qml" line="86"/>
        <location filename="../../qml/pages/PageContactList.qml" line="86"/>
        <source>Filter data boxes.</source>
        <translation>Filtrovat datové schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageContactList.qml" line="145"/>
        <location filename="../../qml/pages/PageContactList.qml" line="145"/>
        <source>No databox found in contacts.</source>
        <translation>Nenalezeny žádné schránky.</translation>
    </message>
</context>
<context>
    <name>PageDataboxDetail</name>
    <message>
        <location filename="../../qml/pages/PageDataboxDetail.qml" line="44"/>
        <location filename="../../qml/pages/PageDataboxDetail.qml" line="44"/>
        <source>Databox info</source>
        <translation>Informace o schránce</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxDetail.qml" line="83"/>
        <location filename="../../qml/pages/PageDataboxDetail.qml" line="83"/>
        <source>Information about the data box &apos;%1&apos; are not available. The data box may have been temporarily disabled. It is likely that you cannot send data message to this data box.</source>
        <translation>Informace o datové schránce &quot;%1&quot; nejsou dostupné. Datová schránka mohla být dočasně znepřístupněna. Je možné, že do této datové schránky nebudete moci odeslat datovou zprávu.</translation>
    </message>
</context>
<context>
    <name>PageDataboxSearch</name>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="75"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="75"/>
        <source>Find databox</source>
        <translation>Vyhledat schránku</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="144"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="144"/>
        <source>IČO</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="151"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="151"/>
        <source>Enter sought phrase</source>
        <translation>Zadejte hledaný text</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="203"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="203"/>
        <source>Previous</source>
        <translation>Předchozí</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="244"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="244"/>
        <source>Next</source>
        <translation>Následující</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="128"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="128"/>
        <source>All databoxes</source>
        <translation>Všechny schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="90"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="90"/>
        <source>Filter data boxes.</source>
        <translation>Filtrovat datové schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="103"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="103"/>
        <source>Search data boxes.</source>
        <translation>Vyhledat datové schránky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="125"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="125"/>
        <source>Select type of sought data box</source>
        <translation>Vyberte typ hledané datové schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="129"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="129"/>
        <source>OVM</source>
        <translation>OVM</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="130"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="130"/>
        <source>PO</source>
        <translation>PO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="131"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="131"/>
        <source>PFO</source>
        <translation>PFO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="132"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="132"/>
        <source>FO</source>
        <translation>FO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="139"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="139"/>
        <source>Select entries to search in</source>
        <translation>Vyberte položky, ve kterých se bude vyhledávat</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="142"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="142"/>
        <source>All fields</source>
        <translation>Všechny položky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="143"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="143"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="145"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="145"/>
        <source>Databox ID</source>
        <translation>ID schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="342"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="342"/>
        <source>from</source>
        <translation>z</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="344"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="344"/>
        <source>Found</source>
        <translation>Nalezeno</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="342"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="342"/>
        <source>Shown</source>
        <translation>Zobrazeno</translation>
    </message>
</context>
<context>
    <name>PageGovService</name>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="76"/>
        <location filename="../../qml/pages/PageGovService.qml" line="76"/>
        <source>Service:</source>
        <translation>Služba:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="87"/>
        <location filename="../../qml/pages/PageGovService.qml" line="87"/>
        <source>Send request</source>
        <translation>Odeslat podání</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="108"/>
        <location filename="../../qml/pages/PageGovService.qml" line="108"/>
        <source>Account:</source>
        <translation>Účet:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="114"/>
        <location filename="../../qml/pages/PageGovService.qml" line="114"/>
        <source>Request:</source>
        <translation>Podání:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="140"/>
        <location filename="../../qml/pages/PageGovService.qml" line="140"/>
        <source>Required information</source>
        <translation>Požadované informace</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="147"/>
        <location filename="../../qml/pages/PageGovService.qml" line="147"/>
        <source>Acquired from data box</source>
        <translation>Obdrženo z datové schránky</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="156"/>
        <location filename="../../qml/pages/PageGovService.qml" line="156"/>
        <source>No user data needed.</source>
        <translation>Nejsou vyžadována uživatelská data.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="169"/>
        <location filename="../../qml/pages/PageGovService.qml" line="169"/>
        <source>Select date</source>
        <translation>Zvolit datum</translation>
    </message>
</context>
<context>
    <name>PageGovServiceList</name>
    <message>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="61"/>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="61"/>
        <source>E-Gov Services</source>
        <translation>Elektronická podání</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="75"/>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="75"/>
        <source>Set filter</source>
        <translation>Nastavit filtr</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="131"/>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="131"/>
        <source>No available e-government service.</source>
        <translation>Žádné dostupné elektronické podání.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="132"/>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="132"/>
        <source>No e-government service found that matches filter text &apos;%1&apos;.</source>
        <translation>Nenalezeno žádné elektronické podání odpovídající textu &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>PageHeader</name>
    <message>
        <location filename="../../qml/components/PageHeader.qml" line="78"/>
        <location filename="../../qml/components/PageHeader.qml" line="78"/>
        <source>Back</source>
        <translation>Zpět</translation>
    </message>
</context>
<context>
    <name>PageImportMessage</name>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="74"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="74"/>
        <source>ZFO message import</source>
        <translation>Import ZFO zpráv</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="86"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="86"/>
        <source>Select ZFO files</source>
        <translation>Vyberte soubory ZFO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="95"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="95"/>
        <source>Select import directory</source>
        <translation>Vyberte adresář pro import</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="126"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="126"/>
        <source>Import ZFO files from directory</source>
        <translation>Importovat ZFO soubory z adresáře</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="151"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="151"/>
        <source>Here you can import messages from ZFO files into the local database.</source>
        <translation>Zde můžete importovat zprávy ze souborů ZFO do místní databáze.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="155"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="155"/>
        <source>Verify messages on the ISDS server</source>
        <translation>Verifikovat zprávy na serveru ISDS</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="177"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="177"/>
        <source>Imported messages are going to be sent to the ISDS server where they are going to be checked. Messages failing this test won&apos;t be imported. We don&apos;t recommend using this option when you are using a mobile or slow internet connection.</source>
        <translation>Importované zprávy budou nejdříve zaslány na ISDS server, kde budou ověřeny. Zprávy, které se nepodaří ověřit, nebudou importovány. Nedoporučujeme používat tuto možnost, když používáte mobilní nebo pomalé internetové připojení.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="186"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="186"/>
        <source>Account must be logged in to the data box to be able to verify messages.</source>
        <translation>Účet musí být přihlášen do datové schránky, aby bylo možné ověřit zprávy.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="194"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="194"/>
        <source>Log in now</source>
        <translation>Přihlásit se nyní</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="231"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="231"/>
        <source>Account is logged in to ISDS.</source>
        <translation>Účet je přihlášen do ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="231"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="231"/>
        <source>Select ZFO files or a directory.</source>
        <translation>Vyberte soubory ZFO nebo adresář.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="178"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="178"/>
        <source>Imported messages won&apos;t be validated on the ISDS server.</source>
        <translation>Importované zprávy nebudou ověřeny na serveru ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="107"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="119"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="107"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="119"/>
        <source>Import selected ZFO files</source>
        <translation>Importovat vybrané ZFO soubory</translation>
    </message>
</context>
<context>
    <name>PageLog</name>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="45"/>
        <location filename="../../qml/pages/PageLog.qml" line="45"/>
        <source>Log from current run:</source>
        <translation>Log současného běhu:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="59"/>
        <location filename="../../qml/pages/PageLog.qml" line="59"/>
        <source>Log file</source>
        <translation>Soubor s logem</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="67"/>
        <location filename="../../qml/pages/PageLog.qml" line="67"/>
        <source>Log Viewer</source>
        <translation>Zobrazení logu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="78"/>
        <location filename="../../qml/pages/PageLog.qml" line="78"/>
        <source>Send log to helpdesk</source>
        <translation>Odeslat log na podporu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="117"/>
        <location filename="../../qml/pages/PageLog.qml" line="117"/>
        <source>Choose log file</source>
        <translation>Vybrat soubor s logem</translation>
    </message>
</context>
<context>
    <name>PageMenuAccount</name>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="53"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="53"/>
        <source>Account properties</source>
        <translation>Vlastnosti účtu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="133"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="133"/>
        <source>Account settings</source>
        <translation>Nastavení účtu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="140"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="140"/>
        <source>View account info</source>
        <translation>Zobrazit info o účtu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="147"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="147"/>
        <source>Create message</source>
        <translation>Vytvořit zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="154"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="154"/>
        <source>Send e-gov request</source>
        <translation>Odeslat elektronické podání</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="161"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="161"/>
        <source>Import message</source>
        <translation>Importovat zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="168"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="168"/>
        <source>Find databox</source>
        <translation>Vyhledat schránku</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="175"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="175"/>
        <source>Back up downloaded data</source>
        <translation>Zálohovat stažená data</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="182"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="182"/>
        <source>Clean up the file database</source>
        <translation>Vyčistit databázi souborů</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="189"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="189"/>
        <source>Change password</source>
        <translation>Změnit heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="196"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="196"/>
        <source>Delete account</source>
        <translation>Odstranit účet</translation>
    </message>
</context>
<context>
    <name>PageMenuDatovkaSettings</name>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="68"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="68"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="139"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="139"/>
        <source>Add account</source>
        <translation>Přidat účet</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="146"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="146"/>
        <source>Search message</source>
        <translation>Hledat zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="153"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="153"/>
        <source>Import message</source>
        <translation>Importovat zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="160"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="160"/>
        <source>Update list of uploaded files</source>
        <translation>Aktualizovat seznam nahraných zpráv</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="174"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="174"/>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="181"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="181"/>
        <source>Synchronization</source>
        <translation>Synchronizace</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="188"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="188"/>
        <source>Storage</source>
        <translation>Úložiště</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="195"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="195"/>
        <source>Security and PIN</source>
        <translation>Zabezpečení a PIN</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="202"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="202"/>
        <source>Transfer all application data</source>
        <translation>Přenést všechna data aplikace</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="209"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="209"/>
        <source>Back up downloaded data</source>
        <translation>Zálohovat stažená data</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="216"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="216"/>
        <source>Restore data</source>
        <translation>Obnovit data</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="244"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="244"/>
        <source>About Datovka</source>
        <translation>O Datovce</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="167"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="167"/>
        <source>Records Management</source>
        <translation>Spisová služba</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="223"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="223"/>
        <source>Log Viewer</source>
        <translation>Zobrazení logu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="230"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="230"/>
        <source>Show log panel</source>
        <translation>Zobrazit panel logu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="237"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="237"/>
        <source>User Guide</source>
        <translation>Uživatelská příručka</translation>
    </message>
</context>
<context>
    <name>PageMenuMessage</name>
    <message>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="52"/>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="52"/>
        <source>Message</source>
        <translation>Zpráva</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="84"/>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="84"/>
        <source>Download attachments</source>
        <translation>Stáhnout přílohy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="98"/>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="98"/>
        <source>Mark as read</source>
        <translation>Označit jako přečtené</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="105"/>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="105"/>
        <source>Mark as unread</source>
        <translation>Označit jako nepřečtené</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="91"/>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="91"/>
        <source>Delete message</source>
        <translation>Smazat zprávu</translation>
    </message>
</context>
<context>
    <name>PageMenuMessageDetail</name>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="65"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="65"/>
        <source>Message</source>
        <translation>Zpráva</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="149"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="149"/>
        <source>Download attachments</source>
        <translation>Stáhnout přílohy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="156"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="156"/>
        <source>Reply</source>
        <translation>Odpověď</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="163"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="163"/>
        <source>Forward</source>
        <translation>Přeposlat</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="170"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="170"/>
        <source>Use as template</source>
        <translation>Použít jako šablonu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="177"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="177"/>
        <source>Upload to records management</source>
        <translation>Nahrát do spisové služby</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="184"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="184"/>
        <source>Send message zfo by email</source>
        <translation>Odeslat ZFO zprávu emailem</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="191"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="191"/>
        <source>Send attachments by email</source>
        <translation>Odeslat přílohy emailem</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="198"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="198"/>
        <source>Save message zfo</source>
        <translation>Uložit ZFO zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="205"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="205"/>
        <source>Save attachments</source>
        <translation>Uložit přílohy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="212"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="212"/>
        <source>Delete attachments</source>
        <translation>Smazat přílohy</translation>
    </message>
</context>
<context>
    <name>PageMenuMessageList</name>
    <message>
        <location filename="../../qml/pages/PageMenuMessageList.qml" line="42"/>
        <location filename="../../qml/pages/PageMenuMessageList.qml" line="42"/>
        <source>Message operations</source>
        <translation>Operace se zprávami</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageList.qml" line="66"/>
        <location filename="../../qml/pages/PageMenuMessageList.qml" line="66"/>
        <source>Mark messages as read</source>
        <translation>Označit vše jako přečtené</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageList.qml" line="73"/>
        <location filename="../../qml/pages/PageMenuMessageList.qml" line="73"/>
        <source>Mark messages as unread</source>
        <translation>Označit vše jako nepřečtené</translation>
    </message>
</context>
<context>
    <name>PageMessageDetail</name>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="103"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="103"/>
        <source>File doesn&apos;t contain a valid message nor does it contain valid delivery information or the file is corrupt.</source>
        <translation>Soubor neobsahuje platnou datovou zprávu ani platné údaje o doručení a nebo je poškozen.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="159"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="159"/>
        <source>Received</source>
        <translation>Přijatá</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="161"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="161"/>
        <source>Sent</source>
        <translation>Odeslaná</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="163"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="165"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="163"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="165"/>
        <source>ZFO</source>
        <translation>ZFO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="169"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="169"/>
        <source>Unknown ZFO content</source>
        <translation>Neznámy obsah souboru ZFO</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="167"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="167"/>
        <source>Message ID</source>
        <translation>ID zprávy</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qml/pages/PageMessageDetail.qml" line="75"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="75"/>
        <source>The message will be deleted from ISDS in %n day(s).</source>
        <translation>
            <numerusform>Zpráva bude smazána z ISDS za %n den.</numerusform>
            <numerusform>Zpráva bude smazána z ISDS za %n dny.</numerusform>
            <numerusform>Zpráva bude smazána z ISDS za %n dnů.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qml/pages/PageMessageDetail.qml" line="78"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="78"/>
        <source>The message will be deleted from ISDS in %n day(s) because the long term storage is full.</source>
        <translation>
            <numerusform>Zpráva bude smazána z ISDS za %n den, protože datový trezor je plný.</numerusform>
            <numerusform>Zpráva bude smazána z ISDS za %n dny, protože datový trezor je plný.</numerusform>
            <numerusform>Zpráva bude smazána z ISDS za %n dnů, protože datový trezor je plný.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qml/pages/PageMessageDetail.qml" line="80"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="80"/>
        <source>The message will be moved to the long term storage in %n day(s) if the storage is not full. The message will be deleted from the ISDS server if the storage is full.</source>
        <translation>
            <numerusform>Zpráva bude za %n den přesunuta do datového trezoru pokud není trezor zaplněn. Zpráva bude smazána ze serveru ISDS pokud je trezor plný.</numerusform>
            <numerusform>Zpráva bude za %n dny přesunuta do datového trezoru pokud není trezor zaplněn. Zpráva bude smazána ze serveru ISDS pokud je trezor plný.</numerusform>
            <numerusform>Zpráva bude za %n dnů přesunuta do datového trezoru pokud není trezor zaplněn. Zpráva bude smazána ze serveru ISDS pokud je trezor plný.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="84"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="84"/>
        <source>The message has already been deleted from the ISDS.</source>
        <translation>Zpráva byla smazána z ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="184"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="184"/>
        <source>Email attachments</source>
        <translation>Poslat přílohy emailem</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="194"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="194"/>
        <source>Save attachments.</source>
        <translation>Uložit přílohy.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="208"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="208"/>
        <source>Download message</source>
        <translation>Stáhnout zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="220"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="220"/>
        <source>Show menu of available operations</source>
        <translation>Zobrazit menu s dostupnými operacemi</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="283"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="283"/>
        <source>Attachments</source>
        <translation>Přílohy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="374"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="374"/>
        <source>Open attachment &apos;%1&apos;.</source>
        <translation>Otevřít přílohu &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="418"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="418"/>
        <source>Attachments have not been downloaded yet.
Click the icon or this text for their download.</source>
        <translation>Přílohy nebyly doposud staženy.
Klikněte na ikonu nebo na tento text pro jejich stažení.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="418"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="418"/>
        <source>No attachments present.</source>
        <translation>Přílohy nejsou přítomny.</translation>
    </message>
</context>
<context>
    <name>PageMessageList</name>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="85"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="85"/>
        <source>Sent messages</source>
        <translation>Odeslané zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="85"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="85"/>
        <source>Received messages</source>
        <translation>Přijaté zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="99"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="99"/>
        <source>Filter messages.</source>
        <translation>Filtrovat zprávy.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="113"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="113"/>
        <source>Create and send message.</source>
        <translation>Vytvořit a odeslat zprávu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="129"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="129"/>
        <source>Show menu of available operations.</source>
        <translation>Zobrazit menu s dostupnými operacemi.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="259"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="259"/>
        <source>No messages or have not been downloaded yet.</source>
        <translation>Žádné zprávy nebo doposud nebyly žádné staženy.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="260"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="260"/>
        <source>No message found that matches filter text &apos;%1&apos;.</source>
        <translation>Nenalezena žádná zpráva odpovídající textu filtru &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>PageMessageSearch</name>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="56"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="56"/>
        <source>Search message</source>
        <translation>Hledat zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="70"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="70"/>
        <source>Search messages.</source>
        <translation>Hledat zprávy.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="94"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="94"/>
        <source>Select type of sought messages</source>
        <translation>Vyberte typ hledaných zpráv</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="98"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="98"/>
        <source>All messages</source>
        <translation>Všechny zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="99"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="99"/>
        <source>Received messages</source>
        <translation>Přijaté zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="100"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="100"/>
        <source>Sent messages</source>
        <translation>Odeslané zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="112"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="112"/>
        <source>Enter phrase</source>
        <translation>Zadejte text</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="165"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="165"/>
        <source>No message found for given search phrase.</source>
        <translation>Nenalezena žádná zpráva pro uvedený hledaný text.</translation>
    </message>
</context>
<context>
    <name>PageRecordsManagementUpload</name>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="106"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="106"/>
        <source>Upload message %1</source>
        <translation>Nahrát zprávu %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="120"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="120"/>
        <source>Filter upload hierarchy.</source>
        <translation>Filtrovat hierarchii pro nahrávání.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="146"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="146"/>
        <source>The message can be uploaded into selected locations in the records management hierarchy.</source>
        <translation>Zprávu můžete nahrát do zvolených umístění v hierarchii spisové služby.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="155"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="155"/>
        <source>Update upload hierarchy</source>
        <translation>Aktualizovat hierarchii</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="166"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="166"/>
        <source>Upload message</source>
        <translation>Nahrát zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="203"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="203"/>
        <source>Up</source>
        <translation>Nahoru</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="244"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="244"/>
        <source>Upload hierarchy has not been downloaded yet.</source>
        <translation>Hierarchie spisové služby nebyla doposud stažena.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="245"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="245"/>
        <source>No hierarchy entry found that matches filter text &apos;%1&apos;.</source>
        <translation>Nenalezena žádná položka hierarchie odpovídající textu &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="301"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="301"/>
        <source>View content of %1.</source>
        <translation>Zobrazit obsah %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="303"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="303"/>
        <source>Select %1.</source>
        <translation>Vybrat %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="303"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="303"/>
        <source>Deselect %1.</source>
        <translation>Zrušit výběr %1.</translation>
    </message>
</context>
<context>
    <name>PageRestoreData</name>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="107"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="107"/>
        <source>Restore data</source>
        <translation>Obnova dat</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="122"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="122"/>
        <source>Proceed with restoration</source>
        <translation>Pokračovat s obnovením</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="155"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="155"/>
        <source>The action allows to restore data from a backup or transfer file. Select the backup or transfer JSON file.</source>
        <translation>Akce umožňuje obnovit data ze zálohy nebo načíst data z přenosu. Vyberte JSON soubor.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="159"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="159"/>
        <source>Choose JSON file</source>
        <translation>Vybrat JSON soubor</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="192"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="192"/>
        <source>Restore ZFO data</source>
        <translation>Obnovit ZFO data</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="200"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="200"/>
        <source>Select all accounts</source>
        <translation>Vybrat všechny účty</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="244"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="244"/>
        <source>Select %1.</source>
        <translation>Vybrat %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="244"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="244"/>
        <source>Deselect %1.</source>
        <translation>Zrušit výběr %1.</translation>
    </message>
</context>
<context>
    <name>PageSendMessage</name>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="113"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="113"/>
        <source>Reply %1</source>
        <translation>Odpověď %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="132"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="132"/>
        <source>Forward %1</source>
        <translation>Přeposlat %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="148"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="148"/>
        <source>Forward ZFO %1</source>
        <translation>Přeposlat ZFO %1</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="159"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="159"/>
        <source>ZFO file of message %1 is not available. Download complete message to get it before forwarding.</source>
        <translation>ZFO zpráva %1 není k dispozici. Stáhněte znovu kompletní zprávu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="196"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="196"/>
        <source>Databox: %1 (%2)</source>
        <translation>Datová schránka: %1 (%2)</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="202"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="202"/>
        <source>New message</source>
        <translation>Nová zpráva</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="241"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="241"/>
        <source>Create message</source>
        <translation>Vytvořit zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="252"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="252"/>
        <source>Send message</source>
        <translation>Odeslat zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="259"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="259"/>
        <source>Before sending a data message you must specify a subject, at least one recipient and at least one attached file. The following fields are missing:</source>
        <translation>Před odesláním datové zprávy je potřeba uvést předmět, nejméně jednoho příjemce a nejméně jeden soubor přílohy. Následující položky chybí:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="262"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="262"/>
        <source>message subject</source>
        <translation>předmět zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="265"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="265"/>
        <source>recipient data box</source>
        <translation>datová schránka příjemce</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="268"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="268"/>
        <source>attachment file or files</source>
        <translation>soubor nebo soubory příloh</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="270"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="270"/>
        <source>Please fill in the respective fields before attempting to send the message.</source>
        <translation>Před odesláním zprávy vyplňte prosím odpovídající položky.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="295"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="295"/>
        <source>Send Message Error</source>
        <translation>Chyba odesílání zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="313"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="313"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="331"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="331"/>
        <source>Error during message creation.</source>
        <translation>Chyba při vytváření zprávy.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <source>Recipients</source>
        <translation>Příjemci</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <source>Attachments</source>
        <translation>Přílohy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="465"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="465"/>
        <source>Sender account</source>
        <translation>Účet odesílatele</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="480"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="480"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="503"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="503"/>
        <source>Include sender identification</source>
        <translation>Přidat identifikaci odesílatele</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="510"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="510"/>
        <source>Personal delivery</source>
        <translation>Doručení do vlastních rukou</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="518"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="518"/>
        <source>Allow acceptance through fiction</source>
        <translation>Povolit doručení fikcí</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="540"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="540"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="553"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="553"/>
        <source>Find</source>
        <translation>Najít</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="618"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="618"/>
        <source>Add file</source>
        <translation>Přidat soubor</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="620"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="620"/>
        <source>Select files</source>
        <translation>Vybrat soubory</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="729"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="729"/>
        <source>Open file &apos;%1&apos;.</source>
        <translation>Otevřít soubor &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="684"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="684"/>
        <source>Local database</source>
        <translation>Lokální databáze</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="487"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="487"/>
        <source>Enter subject. Mandatory</source>
        <translation>Zadejte předmět. Povinné</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="586"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="586"/>
        <source>Mandatory for initiatory message</source>
        <translation>Povinné pro iniciační datovou zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="595"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="595"/>
        <source>Mandatory for reply message</source>
        <translation>Povinné pro odpovědní datovou zprávu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="747"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="747"/>
        <source>Remove file &apos;%1&apos; from list.</source>
        <translation>Odebrat soubor &apos;%1&apos; ze seznamu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="786"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="786"/>
        <source>Mandate</source>
        <translation>Pověření</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="850"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="850"/>
        <source>Our reference number</source>
        <translation>Naše číslo jednací</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="873"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="873"/>
        <source>Our file mark</source>
        <translation>Naše spisová značka</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="897"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="897"/>
        <source>Your reference number</source>
        <translation>Vaše číslo jednací</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="920"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="920"/>
        <source>Your file mark</source>
        <translation>Vaše spisová značka</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="943"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="943"/>
        <source>To hands</source>
        <translation>K rukám</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="824"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="824"/>
        <source>par.</source>
        <translation>odst.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="76"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="97"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="634"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="76"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="97"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="634"/>
        <source>Total size of attachments is %1 B.</source>
        <translation>Celková velikost příloh je %1 B.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="80"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="80"/>
        <source>The permitted number of (%1) attachments has been exceeded.</source>
        <translation>Povolený počet (%1) příloh byl překročen.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="85"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="85"/>
        <source>Total size of attachments exceeds %1 MB.</source>
        <translation>Celková velikost příloh přesahuje %1 MB.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="90"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="90"/>
        <source>Total size of attachments exceeds %1 MB. Most of the data boxes cannot receive messages larger than %1 MB. However, some OVM data boxes can receive message up to %2 MB.</source>
        <translation>Celková velikost příloh přesahuje %1 MB. Většina datových schránek nemůže přijímat zprávy větší než %1 MB. Nicméně některé datové schránky typu OVM mohou přijímat zprávy až do %2 MB.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="95"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="95"/>
        <source>Total size of attachments is ~%1 kB.</source>
        <translation>Celková velikost příloh je ~%1 kB.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="388"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="388"/>
        <source>Previous</source>
        <translation>Předchozí</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <source>Additional</source>
        <translation>Doplňující</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <source>Text</source>
        <translation>Text</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="425"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="425"/>
        <source>Next</source>
        <translation>Následující</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="458"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="458"/>
        <source>Fill in the subject of the message, select at least one recipient data box and specify at least one attachment file or put in a short text message before sending the message.</source>
        <translation>Před odesláním zprávy vyplňte předmět zprávy, vyberte alespoň jednu datovou schránku příjemce a uveďtě alespoň jeden soubor s přílohou nebo vložte krátkou textovou zprávu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="836"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="836"/>
        <source>let.</source>
        <translation>písm.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="856"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="856"/>
        <source>Enter our reference number</source>
        <translation>Zadejte jednací číslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="879"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="879"/>
        <source>Enter our file mark</source>
        <translation>Zadejte spisovou značku</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="903"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="903"/>
        <source>Enter your reference number</source>
        <translation>Zadejte jednací číslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="926"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="926"/>
        <source>Enter your file mark</source>
        <translation>Zadejte spisovou značku</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="949"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="949"/>
        <source>Enter name</source>
        <translation>Zadejte jméno</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="985"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="985"/>
        <source>Here you can create a short text message and add it to attachments as a PDF file.</source>
        <translation>Zde můžete napsat krátkou textovou zprávu a vložit ji do příloh jako PDF soubor.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="986"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="986"/>
        <source>Add the message to attachments after you have finished editing the text.</source>
        <translation>Přidejte zprávu do příloh poté, co jste ukončili editaci textu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="996"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="996"/>
        <source>Text message</source>
        <translation>Textová zpráva</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1005"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1005"/>
        <source>View PDF</source>
        <translation>Zobrazit PDF</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1015"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1015"/>
        <source>Append PDF</source>
        <translation>Připojit PDF</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1032"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1032"/>
        <source>Enter a short text message.</source>
        <translation>Zadejte krátkou textovou zprávu.</translation>
    </message>
</context>
<context>
    <name>PageSettingsAccount</name>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="254"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="254"/>
        <source>Certificate</source>
        <translation>Certifikát</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="276"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="276"/>
        <source>Choose file</source>
        <translation>Vybrat soubor</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="232"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="232"/>
        <source>Login method</source>
        <translation>Způsob přihlašování</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="109"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="109"/>
        <source>New account</source>
        <translation>Nový účet</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="241"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="241"/>
        <source>Username + Password</source>
        <translation>Jméno + Heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="242"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="242"/>
        <source>Certificate + Password</source>
        <translation>Certifikát + Heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="243"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="243"/>
        <source>Password + Security code</source>
        <translation>Heslo + Bezpečnostní kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="244"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="244"/>
        <source>Password + Security SMS</source>
        <translation>Heslo + SMS kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="291"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="291"/>
        <source>Account title</source>
        <translation>Pojmenování účtu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="297"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="297"/>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="322"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="322"/>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="298"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="298"/>
        <source>Enter the login name</source>
        <translation>Zadejte přihlašovací jméno</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="125"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="125"/>
        <source>Accept changes</source>
        <translation>Přijmout změny</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="237"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="237"/>
        <source>Select login method</source>
        <translation>Vyberte přihlašovací metodu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="245"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="245"/>
        <source>Mobile key</source>
        <translation>Mobilní klíč</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="292"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="292"/>
        <source>Enter custom account name</source>
        <translation>Zadejte vlastní název účtu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="302"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="304"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="302"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="304"/>
        <source>Wrong username format.</source>
        <translation>Špatný formát uživatelského jména.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="305"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="305"/>
        <source>Warning: The username should contain only combinations of lower-case letters and digits.</source>
        <translation>Varování: Uživatelské jméno by mělo obsahovat jen kombinaci malých písmen a číslic.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="327"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="327"/>
        <source>PIN required</source>
        <translation>Vyžadován PIN</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="327"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="327"/>
        <source>Enter PIN code in order to show the password.</source>
        <translation>Zadejte PIN kód, aby bylo možné zobrazit heslo.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="328"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="328"/>
        <source>Enter PIN</source>
        <translation>Zadejte PIN</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="335"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="335"/>
        <source>Enter the password</source>
        <translation>Zadejte heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="344"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="344"/>
        <source>Communication code</source>
        <translation>Komunikační kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="345"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="345"/>
        <source>Enter the communication code</source>
        <translation>Zadejte komunikační kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="350"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="350"/>
        <source>Remember password</source>
        <translation>Zapamatovat si heslo</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="356"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="356"/>
        <source>Test account</source>
        <translation>Testovací účet</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="363"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="363"/>
        <source>Test accounts are used to access the ISDS testing environment.</source>
        <translation>Testovací účty jsou využívány k přístupu do testovacího prostředí ISDS.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="369"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="369"/>
        <source>Use local storage (database)</source>
        <translation>Použít lokální úložiště (databázi)</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="376"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="376"/>
        <source>Messages and attachments will be locally stored. No active internet connection is needed to access locally stored data.</source>
        <translation>Zprávy a přílohy budou lokálně ukládány. K přístupu k lokálně uloženým datům není potřeba aktivního připojení k internetu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="377"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="377"/>
        <source>Messages and attachments will be stored only temporarily in memory. These data will be lost on application exit.</source>
        <translation>Zprávy a přílohy budou ukládány pouze dočasně v paměti. Tyto data budou při vypnutí aplikace zahozena.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="383"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="383"/>
        <source>Synchronise together with all accounts</source>
        <translation>Synchronizovat společně se všemi účty</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="390"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="390"/>
        <source>The account will be included into the synchronisation process of all accounts.</source>
        <translation>Účet bude zahrnut do synchronizace všech účtů.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="391"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="391"/>
        <source>The account won&apos;t be included into the synchronisation process of all accounts.</source>
        <translation>Účet nebude zahrnut do synchronizace všech účtů.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="411"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="411"/>
        <source>Account settings</source>
        <translation>Nastavení účtu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="431"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="431"/>
        <source>Wrong PIN code.</source>
        <translation>Špatný PIN kód.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="432"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="432"/>
        <source>Entered PIN is not valid.</source>
        <translation>Zadaný PIN je neplatný.</translation>
    </message>
</context>
<context>
    <name>PageSettingsGeneral</name>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="49"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="49"/>
        <source>General settings</source>
        <translation>Obecné nastavení</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="71"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="71"/>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="76"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="76"/>
        <source>Select language</source>
        <translation>Vyberte jazyk</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="80"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="80"/>
        <source>System</source>
        <translation>Ze systému</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="81"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="81"/>
        <source>Czech</source>
        <translation>Český</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="82"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="82"/>
        <source>English</source>
        <translation>Anglický</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="89"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="89"/>
        <source>Note: Language will be changed after application restart.</source>
        <translation>Poznámka: Jazyk bude změněn až po restartu aplikace.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="94"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="94"/>
        <source>Font</source>
        <translation>Font</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="99"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="99"/>
        <source>Select font</source>
        <translation>Vyberte font</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="102"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="102"/>
        <source>System Default</source>
        <translation>Systémový výchozí</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="103"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="103"/>
        <source>Roboto</source>
        <translation>Roboto</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="104"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="104"/>
        <source>Source Sans Pro</source>
        <translation>Source Sans Pro</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="111"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="111"/>
        <source>Note: Font will be changed after application restart.</source>
        <translation>Poznámka: Font bude změněn až po restartu aplikace.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="117"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="117"/>
        <source>Font size and application scale</source>
        <translation>Velikost písma a měřítko</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="128"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="128"/>
        <source>Set application font size</source>
        <translation>Nastavit velikost fontu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="139"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="139"/>
        <source>Note: Font size will be changed after application restart. Default is %1.</source>
        <translation>Poznámka: Změna velikosti se projeví až po restartu aplikace. Výchozí hodnota je %1.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="151"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="151"/>
        <source>Debug verbosity level</source>
        <translation>Úroveň ladících výpisů</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="159"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="159"/>
        <source>Set the amount of logged debugging information</source>
        <translation>Nastavit množství zaznamenávaných ladících informací</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="178"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="178"/>
        <source>Set the verbosity of logged entries</source>
        <translation>Nastavit podrobnost zaznamenávaných položek</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="184"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="184"/>
        <source>Log verbosity controls the level of detail of the logged entries.</source>
        <translation>Podrobnost záznamů určuje úroveň detailu zaznamenávaných informací.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="165"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="165"/>
        <source>Debug verbosity controls the amount of debugging information written to the log file.</source>
        <translation>Úroveň ladících výpisů řídí množství zaznamenávaných ladících informací.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="170"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="170"/>
        <source>Log verbosity level</source>
        <translation>Podrobnost záznamů</translation>
    </message>
</context>
<context>
    <name>PageSettingsPin</name>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="45"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="45"/>
        <source>Currently there is no PIN code set.</source>
        <translation>Momentálně není nastaven žádný PIN kód.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="50"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="50"/>
        <source>You will be asked to enter a PIN code on application start-up.</source>
        <translation>Při startu aplikace budete vyzváni k zadání PIN kódu.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="68"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="68"/>
        <source>Enter a new PIN code into both text fields:</source>
        <translation>Zadejte nový PIN kód do obou textových polí:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="83"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="99"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="83"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="99"/>
        <source>In order to change the PIN code you must enter the current and a new PIN code:</source>
        <translation>Pro změnu PIN kódu musíte zadat současný a nový PIN kód:</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="114"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="114"/>
        <source>Something went wrong!</source>
        <translation>Někde se stala chyba!</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="119"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="119"/>
        <source>PIN Settings</source>
        <translation>Nastavení PINu</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="138"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="138"/>
        <source>Accept changes</source>
        <translation>Přijmout změny</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="152"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="174"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="152"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="174"/>
        <source>Error: Both new PIN code fields must be filled in!</source>
        <translation>Chyba: Obě pole s novým PIN kódem musí být vyplněna!</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="161"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="183"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="161"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="183"/>
        <source>Error: Newly entered PIN codes are different!</source>
        <translation>Chyba: Nově zadané PIN kódy se liší!</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="194"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="209"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="194"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="209"/>
        <source>Error: Current PIN code is wrong!</source>
        <translation>Chyba: Současný PIN kód je nesprávný!</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="246"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="246"/>
        <source>Set PIN</source>
        <translation>Nastavit PIN</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="256"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="256"/>
        <source>Change PIN</source>
        <translation>Změnit PIN</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="266"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="266"/>
        <source>Disable PIN</source>
        <translation>Zrušit PIN</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="279"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="279"/>
        <source>Current PIN code</source>
        <translation>Současný PIN kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="291"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="291"/>
        <source>New PIN code</source>
        <translation>Nový PIN kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="303"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="303"/>
        <source>Confirm new PIN code</source>
        <translation>Potvrdit nový PIN kód</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="320"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="320"/>
        <source>Lock after seconds of inactivity</source>
        <translation>Zamknout po sekundách neaktivity</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="330"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="330"/>
        <source>don&apos;t lock</source>
        <translation>nezamykat</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="333"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="333"/>
        <source>Select the number of seconds after which the application is going to be locked.</source>
        <translation>Vybrat dobu (v sekundách), za jak dlouho se aplikace uzamkne.</translation>
    </message>
</context>
<context>
    <name>PageSettingsRecordsManagement</name>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="179"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="179"/>
        <source>Get service info</source>
        <translation>Stáhnout info</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="69"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="69"/>
        <source>Records management</source>
        <translation>Spisová služba</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="84"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="84"/>
        <source>Accept changes</source>
        <translation>Přijmout změny</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="125"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="125"/>
        <source>Service URL</source>
        <translation>URL služby</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="132"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="132"/>
        <source>Enter service url</source>
        <translation>Zadejte URL služby</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="149"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="149"/>
        <source>Your token</source>
        <translation>Token</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="156"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="156"/>
        <source>Enter your token</source>
        <translation>Zadejte token</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="189"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="189"/>
        <source>Clear records management data</source>
        <translation>Vymazat data o spisové službe</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="212"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="212"/>
        <source>Service name</source>
        <translation>Název služby</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="222"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="222"/>
        <source>Service token</source>
        <translation>Token služby</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="234"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="234"/>
        <source>Service logo</source>
        <translation>Logo služby</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="204"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="204"/>
        <source>Communication error. Cannot obtain records management info from server. Internet connection failed or service url and identification token can be wrong!</source>
        <translation>Chyba komunikace. Nelze obdržet informace o spisové službě. Připojení k internetu nefunguje nebo URL služby a/nebo identifikační token jsou neplatné!</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="119"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="119"/>
        <source>Please fill in service URL and your identification token. Click the &apos;%1&apos; button to log into records management service. You should receive valid service info. Finally, click the top right button to save the settings.</source>
        <translation>Vyplňte prosím URL služby a identifikační token. Pro přihlášení do spisové služby stiskněte tlačítko &quot;%1&quot;. Měli byste obdržet informace o službě. Pro uložení nastavení stiskněte tlačíto vpravo nahoře.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="242"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="242"/>
        <source>Records management logo</source>
        <translation>Logo spisové služby</translation>
    </message>
</context>
<context>
    <name>PageSettingsStorage</name>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="60"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="60"/>
        <source>Storage settings</source>
        <translation>Nastavení úložiště</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="82"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="82"/>
        <source>Number of days to keep messages</source>
        <translation>Počet dní pro uchování zpráv</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="89"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="89"/>
        <source>don&apos;t delete</source>
        <translation>nemazat</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="92"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="92"/>
        <source>Select the number of days how long downloaded messages are kept in local storage after download.</source>
        <translation>Vybrat dobu (ve dnech), jak dlouho po stažení budou zprávy uchovávány v lokálním úložišti.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="100"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="100"/>
        <source>Messages won&apos;t automatically be deleted from the local storage.</source>
        <translation>Zprávy nebudou automaticky mazány z místního úložiště.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="108"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="108"/>
        <source>Number of days to keep attachments</source>
        <translation>Počet dní pro uchování příloh</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="115"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="115"/>
        <source>like messages</source>
        <translation>jako zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="118"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="118"/>
        <source>Select the number of days how long message content is kept in local storage after download.</source>
        <translation>Vybrat dobu (ve dnech), jak dlouho po stažení bude v lokálním úložišti uchováván obsah zpráv.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="126"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="126"/>
        <source>Attachments won&apos;t be deleted from the local storage as long as the corresponding messages won&apos;t be deleted.</source>
        <translation>Přílohy nebudou smazány z místního úložiště po dobu, po kterou nebudou smazány odpovídající zprávy.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="102"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="102"/>
        <source>Messages will be locally stored for a period of %1 days since their acceptance time. By default they are not deleted.</source>
        <translation>Zprávy budou uchovávány po dobu %1 dnů od doby dodání. Ve výchozím nastavení nejsou mazány.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="128"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="128"/>
        <source>Attachments will be locally stored, but no longer than corresponding messages, for a period of %1 days since their download time. Default value corresponds with message settings.</source>
        <translation>Přílohy budou uchovány po dobu %1 dnů od jejich stažení, ale ne déle než odpovídající zprávy. Výchozí hodnota odpovídá nastavení zpráv.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="134"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="134"/>
        <source>ZFO storage size limit in MB</source>
        <translation>Velikost úložiště ZFO v MB</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="144"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="144"/>
        <source>Specify the maximal amount of memory for preserving recently downloaded messages.</source>
        <translation>Určit maximální množství paměti určené pro uchovávání nedávno stažených zpráv.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="152"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="152"/>
        <source>Message ZFO data won&apos;t be automatically stored.</source>
        <translation>Zpráva ZFO nebude automaticky uložena.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="154"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="154"/>
        <source>Maximum size of stored message ZFO data is set to %1 MB. Default is %2 MB.</source>
        <translation>Maximální velikost úložiště ZFO zpráv je %1 MB. Výchozí nastavení je %2 MB.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="160"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="160"/>
        <source>Databases location</source>
        <translation>Umístění databází</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="173"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="173"/>
        <source>Change location</source>
        <translation>Změnit umístění</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="189"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="189"/>
        <source>Set default</source>
        <translation>Nastavit výchozí</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="207"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="207"/>
        <source>Clean up all databases</source>
        <translation>Pročistit všechny databáze</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="213"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="213"/>
        <source>Clean now</source>
        <translation>Vyčistit teď</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="223"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="223"/>
        <source>The action performs a clean-up in local databases in order to optimise their speed and size. Note: It may take a while to complete this action.</source>
        <translation>Akce provede pročištění místních databází za účelem optimalizace rycholsti přístupu a jejich velikosti. Poznámka: Dokončení této akce může chvíli trvat.</translation>
    </message>
</context>
<context>
    <name>PageSettingsSync</name>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="47"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="47"/>
        <source>Synchronization settings</source>
        <translation>Nastavení synchronizace</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="69"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="69"/>
        <source>Download only newer messages</source>
        <translation>Stahovat pouze novější zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="79"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="79"/>
        <source>Only messages which are not older than 90 days will be downloaded.</source>
        <translation>Budou stahovány jen zprávy, které nejsou starší 90 dnů.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="80"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="80"/>
        <source>All available messages (including those in the data vault) will be downloaded.</source>
        <translation>Budou stahovány všechny zprávy (včetně těch v datovém trezoru).</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="85"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="85"/>
        <source>Download complete messages</source>
        <translation>Stahovat kompletní zprávy</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="93"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="93"/>
        <source>Complete messages will be downloaded during account synchronisation. This may be slow.</source>
        <translation>V průběhu synchronizace účtů budou stahovány kompletní zprávy. To může být pomalé.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="94"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="94"/>
        <source>Only message envelopes will be downloaded during account synchronisation.</source>
        <translation>V průběhu synchronizace účtů budou stahovány pouze obálky zpráv.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="99"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="99"/>
        <source>Synchronise accounts after start</source>
        <translation>Po startu synchronizovat účty</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="107"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="107"/>
        <source>Accounts will automatically be synchronised a few seconds after application start-up.</source>
        <translation>Účty budou automaticky synchronizovány několik sekund po startu aplikace.</translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="108"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="108"/>
        <source>Accounts will be synchronised only when the action is manually invoked.</source>
        <translation>Účty budou synchronizovány jen po ručním vyvolání akce.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="108"/>
        <source>FROM</source>
        <translation>OD</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="110"/>
        <source>TO</source>
        <translation>KOMU</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="112"/>
        <source>DELIVERED</source>
        <translation>DORUČENO</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="440"/>
        <source>General</source>
        <translation>Obecné informace</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="459"/>
        <source>Personal delivery</source>
        <translation>Doručení do vlastních rukou</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="243"/>
        <source>Databox info</source>
        <translation>Informace o DS</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="268"/>
        <location filename="../../src/sqlite/message_db.cpp" line="460"/>
        <location filename="../../src/sqlite/message_db.cpp" line="462"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="269"/>
        <location filename="../../src/sqlite/message_db.cpp" line="460"/>
        <location filename="../../src/sqlite/message_db.cpp" line="462"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="342"/>
        <source>Full control</source>
        <translation>Plný přístup</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="343"/>
        <source>Restricted control</source>
        <translation>Omezený přístup</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="850"/>
        <source>Password of username &apos;%1&apos; expires on %2.</source>
        <translation>Heslo uživatele &quot;%1&quot; vyprší %2.</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="852"/>
        <source>Password of username &apos;%1&apos; expired on %2.</source>
        <translation>Heslo uživatele &quot;%1&quot; vypršelo %2.</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="461"/>
        <source>Delivery by fiction</source>
        <translation>Doručení fikcí</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="475"/>
        <location filename="../../src/sqlite/message_db.cpp" line="488"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="464"/>
        <source>Message type</source>
        <translation>Typ zprávy</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="478"/>
        <source>Message author</source>
        <translation>Odesílající osoba</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="580"/>
        <source>Records Management Location</source>
        <translation>Umístění ve spisové službě</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="582"/>
        <source>Location</source>
        <translation>Umístění</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="518"/>
        <source>Message state</source>
        <translation>Stav zprávy</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="559"/>
        <source>Events</source>
        <translation>Události</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="106"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="470"/>
        <source>Sender</source>
        <translation>Odesílatel</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="483"/>
        <source>Recipient</source>
        <translation>Příjemce</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="457"/>
        <source>Attachment size</source>
        <translation>Velikost příloh</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="473"/>
        <location filename="../../src/sqlite/message_db.cpp" line="486"/>
        <source>Name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="454"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="114"/>
        <source>This email has been generated with Datovka application based on a data message (%1) delivered to databox.</source>
        <translation>Tento email byl vytvořen aplikací Datovka na základě datové zprávy (%1) doručené do datové schránky.</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="471"/>
        <location filename="../../src/sqlite/message_db.cpp" line="484"/>
        <source>Databox ID</source>
        <translation>ID schránky</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="245"/>
        <source>Data box application</source>
        <translation>Aplikace pro datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="250"/>
        <source>ZFO file to be viewed.</source>
        <translation>Soubor zfo, který má být zobrazen.</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="881"/>
        <source>Last synchronisation: %1</source>
        <translation>Poslední synchronizace: %1</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="887"/>
        <source>Security problem</source>
        <translation>Bezpečnostní problém</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="888"/>
        <source>OpenSSL support is required!</source>
        <translation>Je nezbytná podpora OpenSSL!</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="889"/>
        <source>The device does not support OpenSSL. The application won&apos;t work correctly.</source>
        <translation>Vaše zařízení nepodporuje OpenSSL. Aplikace nebude pracovat správně.</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="318"/>
        <source>User info</source>
        <translation>Informace o uživateli</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="287"/>
        <source>Owner info</source>
        <translation>Informace o majiteli DS</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="95"/>
        <source>Cannot open ZFO file &apos;%1&apos;.</source>
        <translation>Nelze otevřít ZFO soubor &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="103"/>
        <source>Wrong message format of ZFO file &apos;%1&apos;.</source>
        <translation>Špatný formát ZFO souboru &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="107"/>
        <source>Wrong message data of ZFO file &apos;%1&apos;.</source>
        <translation>Špatný obsah ZFO souboru &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="128"/>
        <source>Relevant account does not exist for ZFO file &apos;%1&apos;.</source>
        <translation>Neexistuje příslušný účet pro import ZFO souboru &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="134"/>
        <source>Import internal error.</source>
        <translation>Interní chyba během importu.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="159"/>
        <source>Cannot authenticate the ZFO file &apos;%1&apos;.</source>
        <translation>Nelze ověřit pravost ZFO souboru &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="164"/>
        <source>ZFO file &apos;%1&apos; is not authentic (returns ISDS)!</source>
        <translation>ZFO Soubor &apos;%1&apos; není autentický (vrací ISDS)!</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="175"/>
        <source>Cannot store the ZFO file &apos;%1&apos; to local database.</source>
        <translation>Nepodařilo se uložit ZFO soubor &apos;%1&apos; do místní databáze.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="186"/>
        <source>ZFO file &apos;%1&apos; has been imported.</source>
        <translation>ZFO soubor &apos;%1&apos; byl importován.</translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="194"/>
        <source>File has not been imported!</source>
        <translation>Soubor nebyl importován!</translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="62"/>
        <source>Message &apos;%1&apos; could not be uploaded.</source>
        <translation>Zpráva &apos;%1&apos; nemohla být nahrána.</translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="65"/>
        <source>Received error</source>
        <translation>Obržena chyba</translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="70"/>
        <source>File Upload Error</source>
        <translation>Chyba nahrávání souboru</translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="75"/>
        <source>Successful File Upload</source>
        <translation>Úspěšné nahrání souboru</translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="76"/>
        <source>Message &apos;%1&apos; was successfully uploaded into the records management service.</source>
        <translation>Zpráva &apos;%1&apos; byla úspěšně nahrána do spisové služby.</translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="78"/>
        <source>It can be now found in the records management service in these locations:</source>
        <translation>Lze ji teď nalézt ve spisové službě v těchto položkách:</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="650"/>
        <source>You can export files into iCloud or into device local storage.</source>
        <translation>Můžete exportovat soubory do iCloudu nebo do úložiště v zařízení.</translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="651"/>
        <source>Do you want to export files to Datovka iCloud container?</source>
        <translation>Přejete si exportovat soubory do kontejneru Datovky v iCloudu?</translation>
    </message>
</context>
<context>
    <name>RecordsManagement</name>
    <message>
        <location filename="../../src/records_management.cpp" line="121"/>
        <source>Get service info</source>
        <translation>Stáhnout info o službě</translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="137"/>
        <location filename="../../src/records_management.cpp" line="185"/>
        <location filename="../../src/records_management.cpp" line="412"/>
        <source>Done</source>
        <translation>Hotovo</translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="143"/>
        <location filename="../../src/records_management.cpp" line="191"/>
        <location filename="../../src/records_management.cpp" line="418"/>
        <source>Communication error</source>
        <translation>Chyba komunikace</translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="168"/>
        <source>Upload hierarchy</source>
        <translation>Hierarchie služby</translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="211"/>
        <source>Sync service</source>
        <translation>Synchronizuji</translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="220"/>
        <source>Sync failed</source>
        <translation>Synchronizace selhala</translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="367"/>
        <source>Update account &apos;%1&apos; (%2/%3)</source>
        <translation>Aktualizuji účet &apos;%1&apos; (%2/%3)</translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="370"/>
        <source>Update done</source>
        <translation>Aktualizace dokončena</translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="400"/>
        <source>Upload message</source>
        <translation>Nahrát zprávu</translation>
    </message>
</context>
<context>
    <name>SQLiteTbls</name>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="77"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="198"/>
        <source>Data box ID</source>
        <translation>ID datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="78"/>
        <source>Data box type</source>
        <translation>Typ datové schránky</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="79"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="158"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="80"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="147"/>
        <source>Given name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="81"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="148"/>
        <source>Middle name</source>
        <translation>Prostření jméno</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="82"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="149"/>
        <source>Surname</source>
        <translation>Příjmení</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="83"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="150"/>
        <source>Surname at birth</source>
        <translation>Rodné příjmení</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="84"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="159"/>
        <source>Company name</source>
        <translation>Název firmy</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="85"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="157"/>
        <source>Date of birth</source>
        <translation>Datum narození</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="86"/>
        <source>City of birth</source>
        <translation>Místo narození</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="87"/>
        <source>County of birth</source>
        <translation>Okres narození</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="88"/>
        <source>State of birth</source>
        <translation>Stát narození</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="89"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="161"/>
        <source>City of residence</source>
        <translation>Sídlo - město</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="90"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="160"/>
        <source>Street of residence</source>
        <translation>Sídlo - ulice</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="91"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="153"/>
        <source>Number in street</source>
        <translation>Číslo orientační</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="92"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="154"/>
        <source>Number in municipality</source>
        <translation>Číslo popisné</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="93"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="155"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="162"/>
        <source>Zip code</source>
        <translation>PSČ</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="94"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="163"/>
        <source>State of residence</source>
        <translation>Sídlo - stát</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="95"/>
        <source>Nationality</source>
        <translation>Státní občanství</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="98"/>
        <source>Databox state</source>
        <translation>Stav schránky</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="99"/>
        <source>Effective OVM</source>
        <translation>Efektivní OVM</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="100"/>
        <source>Open addressing</source>
        <translation>Otevřené adresování</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="145"/>
        <source>User type</source>
        <translation>Typ uživatele</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="146"/>
        <source>Permissions</source>
        <translation>Oprávnění</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="151"/>
        <source>City</source>
        <translation>Město</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="152"/>
        <source>Street</source>
        <translation>Ulice</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="156"/>
        <source>State</source>
        <translation>Stát</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="164"/>
        <source>Password expiration</source>
        <translation>Expirace hesla</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="199"/>
        <source>Long term storage type</source>
        <translation>Typ datového trezoru</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="200"/>
        <source>Long term storage capacity</source>
        <translation>Kapacita datového trezoru</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="201"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="206"/>
        <source>Active from</source>
        <translation>Aktivní od</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="202"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="207"/>
        <source>Active to</source>
        <translation>Aktivní do</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="203"/>
        <source>Capacity used</source>
        <translation>Použitá kapacita</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="204"/>
        <source>Future long term storage type</source>
        <translation>Budoucí typ datového trezoru</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="205"/>
        <source>Future long term storage capacity</source>
        <translation>Budoucí kapacita datového trezoru</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="208"/>
        <source>Payment state</source>
        <translation>Stav platby</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/file_db_tables.cpp" line="67"/>
        <source>File name</source>
        <translation>Název souboru</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/file_db_tables.cpp" line="70"/>
        <source>Mime type</source>
        <translation>Typ MIME</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="92"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="94"/>
        <source>Sender</source>
        <translation>Odesílatel</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="95"/>
        <source>Sender address</source>
        <translation>Adresa odesílatele</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="97"/>
        <source>Recipient</source>
        <translation>Příjemce</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="98"/>
        <source>Recipient address</source>
        <translation>Adresa příjemce</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="105"/>
        <source>To hands</source>
        <translation>K rukám</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="106"/>
        <source>Subject</source>
        <translation>Předmět</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="107"/>
        <source>Your reference number</source>
        <translation>Vaše číslo jednací</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="108"/>
        <source>Our reference number</source>
        <translation>Naše číslo jednací</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="109"/>
        <source>Your file mark</source>
        <translation>Vaše spisová značka</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="110"/>
        <source>Our file mark</source>
        <translation>Naše spisová značka</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="111"/>
        <source>Law</source>
        <translation>Zákon</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="112"/>
        <source>Year</source>
        <translation>Rok</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="113"/>
        <source>Section</source>
        <translation>Sekce</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="114"/>
        <source>Paragraph</source>
        <translation>Odstavec</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="115"/>
        <source>Letter</source>
        <translation>Písmeno</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="119"/>
        <source>Delivered</source>
        <translation>Čas dodání</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="120"/>
        <source>Accepted</source>
        <translation>Čas doručení</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="121"/>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="122"/>
        <source>Attachment size</source>
        <translation>Velikost příloh</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/io/prefs_db_tables.cpp" line="55"/>
        <source>Preference Name</source>
        <translation>Název předvolby</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/io/prefs_db_tables.cpp" line="56"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/io/prefs_db_tables.cpp" line="57"/>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
</context>
<context>
    <name>Service</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="175"/>
        <source>Cannot access date field.</source>
        <translation>Nelze získat datum.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="184"/>
        <source>The field &apos;%1&apos; contains an invalid date &apos;%2&apos;.</source>
        <translation>Pole &apos;%1&apos; obsahuje neplatné datum &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="204"/>
        <source>Cannot access IČO field.</source>
        <translation>Nelze získat IČO.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="213"/>
        <source>The field &apos;%1&apos; contains an invalid value &apos;%2&apos;.</source>
        <translation>Pole &apos;%1&apos; obsahuje neplatnou hodnotu &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="233"/>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="261"/>
        <source>Cannot access string.</source>
        <translation>Nelze získat řetězec.</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="241"/>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="269"/>
        <source>The field &apos;%1&apos; contains no value.</source>
        <translation>Pole &apos;%1&apos; neobsahuje žádnou hodnotu.</translation>
    </message>
</context>
<context>
    <name>Session</name>
    <message>
        <location filename="../../src/isds/session/isds_session.cpp" line="183"/>
        <source>Cannot open certificate &apos;%1&apos;.</source>
        <translation>Nelze otevřít certifikát &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/session/isds_session.cpp" line="209"/>
        <source>Cannot parse certificate &apos;%1&apos;.</source>
        <translation>Nelze načíst certifikát &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/isds/session/isds_session.cpp" line="217"/>
        <source>Unknown format of certificate &apos;%1&apos;.</source>
        <translation>Neznámý formát certifikátu &quot;%1&quot;.</translation>
    </message>
</context>
<context>
    <name>ShowPasswordOverlaidImage</name>
    <message>
        <location filename="../../qml/components/ShowPasswordOverlaidImage.qml" line="89"/>
        <location filename="../../qml/components/ShowPasswordOverlaidImage.qml" line="89"/>
        <source>Hide password</source>
        <translation>Skrýt heslo</translation>
    </message>
    <message>
        <location filename="../../qml/components/ShowPasswordOverlaidImage.qml" line="89"/>
        <location filename="../../qml/components/ShowPasswordOverlaidImage.qml" line="89"/>
        <source>Show password</source>
        <translation>Zobrazit heslo</translation>
    </message>
</context>
<context>
    <name>SrvcMvCrrVbh</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="152"/>
        <source>Printout from the driver penalty point system</source>
        <translation>Výpis bodového hodnocení z centrálního registru řidičů</translation>
    </message>
</context>
<context>
    <name>SrvcMvIrVp</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_ir_vp.cpp" line="545"/>
        <source>Printout from the insolvency register</source>
        <translation>Výpis z insolvenčního rejstříku</translation>
    </message>
</context>
<context>
    <name>SrvcMvRtVt</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="175"/>
        <source>Printout from the criminal records</source>
        <translation>Výpis z rejstříku trestů</translation>
    </message>
</context>
<context>
    <name>SrvcMvRtpoVt</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.cpp" line="550"/>
        <source>Printout from the criminal records of legal persons</source>
        <translation>Výpis z rejstříku trestů právnických osob</translation>
    </message>
</context>
<context>
    <name>SrvcMvSkdVp</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_skd_vp.cpp" line="546"/>
        <source>Printout from the list of qualified suppliers</source>
        <translation>Výpis ze seznamu kvalifikovaných dodavatelů</translation>
    </message>
</context>
<context>
    <name>SrvcMvVrVp</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_vr_vp.cpp" line="579"/>
        <source>Printout from the public register</source>
        <translation>Výpis z veřejného rejstříku</translation>
    </message>
</context>
<context>
    <name>SrvcMvZrVp</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_zr_vp.cpp" line="545"/>
        <source>Printout from the company register</source>
        <translation>Výpis z živnostenského rejstříku</translation>
    </message>
</context>
<context>
    <name>SrvcSzrRobVu</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vu.cpp" line="94"/>
        <source>Printout from the resident register</source>
        <translation>Výpis z registru obyvatel</translation>
    </message>
</context>
<context>
    <name>SrvcSzrRobVvu</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="134"/>
        <source>Printout about the usage of entries from the resident register</source>
        <translation>Výpis o využití údajů z registru obyvatel</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="234"/>
        <source>The date of start cannot be later than the date of end.</source>
        <translation>Datum začátku nemůže být později než datum konce.</translation>
    </message>
</context>
<context>
    <name>SrvcSzrRosVv</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_ros_vv.cpp" line="131"/>
        <source>Public printout from the person register</source>
        <translation>Veřejný výpis z registru osob</translation>
    </message>
</context>
<context>
    <name>SzrRobVvuData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="93"/>
        <source>From</source>
        <translation>Od</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="94"/>
        <source>Select start date</source>
        <translation>Zvolte počáteční datum</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="105"/>
        <source>Select end date</source>
        <translation>Zvolte koncové datum</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="104"/>
        <source>To</source>
        <translation>Do</translation>
    </message>
</context>
<context>
    <name>SzrRosVvData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_ros_vv.cpp" line="102"/>
        <source>Identification number (IČO)</source>
        <translation>IČO</translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_ros_vv.cpp" line="103"/>
        <source>Enter identification number (IČO)</source>
        <translation>Zadejte IČO</translation>
    </message>
</context>
<context>
    <name>TaskDownloadMessageList</name>
    <message>
        <location filename="../../src/worker/task_download_message_list.cpp" line="160"/>
        <source>%1: No new messages.</source>
        <translation>%1: Žádné nové zprávy.</translation>
    </message>
</context>
<context>
    <name>Tasks</name>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="76"/>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="77"/>
        <source>Wrong username</source>
        <translation>Špatné uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="82"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="390"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="399"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="458"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="495"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="502"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="82"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="391"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="400"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="459"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="503"/>
        <source>Internal error</source>
        <translation>Interní chyba</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="110"/>
        <source>Change password error: %1</source>
        <translation>Chyba při změně hesla: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="111"/>
        <source>Failed to change password for username &apos;%1&apos;.</source>
        <translation>Nezdařilo se změnit heslo pro uživatelské jméno %1.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="112"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="118"/>
        <source>ISDS returns: %1</source>
        <translation>ISDS vrací: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="116"/>
        <source>Change password: %1</source>
        <translation>Změna hesla: %1</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="117"/>
        <source>Password for username &apos;%1&apos; has changed.</source>
        <translation>Heslo pro uživatelské jméno &quot;%1&quot; bylo změněno.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="313"/>
        <source>ZFO import is running... Wait until import will be finished.</source>
        <translation>ZFO import běží... Počkejte, dokud import neskončí.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="390"/>
        <source>Cannot access data box model.</source>
        <translation>Nelze získat data ze seznamu příjemců.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="399"/>
        <source>Cannot access attachment model.</source>
        <translation>Nelze získat data ze seznamu příloh.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="412"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="420"/>
        <source>Error sending message</source>
        <translation>Chyba při odesílání zprávy</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="413"/>
        <source>Pre-paid transfer charges for message reply have been enabled.</source>
        <translation>Bylo povoleno předplatit poplatky za odpověď na tuto zprávu.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="414"/>
        <source>The sender reference number must be specified in the additional section in order to send the message.</source>
        <translation>Pro odeslání inicializační poštovní zprávy musí být zadáno referenční číslo odesílatele.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="421"/>
        <source>Message uses the offered payment of transfer charges by the recipient.</source>
        <translation>Využít nabízenou platbu za zasílací poplatky příjemcem.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="422"/>
        <source>The recipient reference number must be specified in the additional section in order to send the message.</source>
        <translation>Pro odeslání předplacené odpovědní poštovní zprávy musí být zadáno referenční číslo příjemce.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="458"/>
        <source>Cannot access file database.</source>
        <translation>Nelze zpřístupnit databázi příloh.</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="495"/>
        <source>Cannot load file content from path:</source>
        <translation>Nelze načíst obsah souboru z cesty:</translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="502"/>
        <source>No file to send.</source>
        <translation>Žádný soubor k odeslání.</translation>
    </message>
</context>
<context>
    <name>TextLineItem</name>
    <message>
        <location filename="../../qml/components/TextLineItem.qml" line="82"/>
        <location filename="../../qml/components/TextLineItem.qml" line="82"/>
        <source>Input text action button.</source>
        <translation>Tlačítko akce vkládání textu.</translation>
    </message>
</context>
<context>
    <name>TimedPasswordLine</name>
    <message>
        <location filename="../../qml/components/TimedPasswordLine.qml" line="36"/>
        <location filename="../../qml/components/TimedPasswordLine.qml" line="36"/>
        <source>Enter the password</source>
        <translation>Zadejte heslo</translation>
    </message>
</context>
<context>
    <name>Utility</name>
    <message>
        <location filename="../../src/datovka_shared/utility/strings.cpp" line="59"/>
        <source>unknown</source>
        <translation>neznámý</translation>
    </message>
</context>
<context>
    <name>WidgetInputDialogue</name>
    <message>
        <location filename="../../src/dialogues/widget_input_dialogue.cpp" line="127"/>
        <source>Paste</source>
        <translation>Vložit</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../../qml/main.qml" line="283"/>
        <location filename="../../qml/main.qml" line="283"/>
        <source>Wrong PIN code.</source>
        <translation>Špatný PIN kód.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="310"/>
        <location filename="../../qml/main.qml" line="310"/>
        <source>Version</source>
        <translation>Verze</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="263"/>
        <location filename="../../qml/main.qml" line="263"/>
        <source>Enter PIN code</source>
        <translation>Zadejte PIN kód</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="182"/>
        <location filename="../../qml/main.qml" line="182"/>
        <source>Mobile Key Login</source>
        <translation>Přihlášení Mobilním klíčem</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="183"/>
        <location filename="../../qml/main.qml" line="183"/>
        <source>Waiting for acknowledgement from the Mobile key application for the account &apos;%1&apos;.</source>
        <translation>Čekání na potvrzení z Mobilního klíče pro účet &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="286"/>
        <location filename="../../qml/main.qml" line="286"/>
        <source>Enter</source>
        <translation>Vstoupit</translation>
    </message>
</context>
</TS>
