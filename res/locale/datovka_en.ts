<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>AccessibleSpinBox</name>
    <message>
        <location filename="../../qml/components/AccessibleSpinBox.qml" line="70"/>
        <location filename="../../qml/components/AccessibleSpinBox.qml" line="70"/>
        <source>Decrease value &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/AccessibleSpinBox.qml" line="103"/>
        <location filename="../../qml/components/AccessibleSpinBox.qml" line="103"/>
        <source>Increase value &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccessibleSpinBoxZeroMax</name>
    <message>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="32"/>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="32"/>
        <source>max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="92"/>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="92"/>
        <source>Decrease value &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="125"/>
        <location filename="../../qml/components/AccessibleSpinBoxZeroMax.qml" line="125"/>
        <source>Increase value &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountList</name>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="96"/>
        <location filename="../../qml/components/AccountList.qml" line="96"/>
        <source>Account &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="96"/>
        <location filename="../../qml/components/AccountList.qml" line="96"/>
        <source>Testing account &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="114"/>
        <location filename="../../qml/components/AccountList.qml" line="114"/>
        <source>Synchronise account &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="146"/>
        <location filename="../../qml/components/AccountList.qml" line="146"/>
        <source>Received messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="162"/>
        <location filename="../../qml/components/AccountList.qml" line="162"/>
        <source>Received messages of account &apos;%1&apos;. (Unread %2 of %3 received messages.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="198"/>
        <location filename="../../qml/components/AccountList.qml" line="198"/>
        <source>Sent messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/AccountList.qml" line="214"/>
        <location filename="../../qml/components/AccountList.qml" line="214"/>
        <source>Sent messages of account &apos;%1&apos;. (Total of %2 sent messages.)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Accounts</name>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="359"/>
        <source>Remove account: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="360"/>
        <source>Do you want to remove the account &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="361"/>
        <source>Note: It will also remove all related local databases and account information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="260"/>
        <source>Problem while updating account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="198"/>
        <source>Username, communication code or account name has not been specified. These fields must be filled in.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="261"/>
        <source>Account name has not been specified!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="262"/>
        <source>This field must be filled in.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="450"/>
        <source>Change user name: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="451"/>
        <source>Do you want to change the username from &apos;%1&apos; to &apos;%2&apos; for the account &apos;%3&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="453"/>
        <source>Note: It will also change all related local database names and account information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="500"/>
        <source>Cannot access file database for the username &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="509"/>
        <source>Cannot change file database to username &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="535"/>
        <source>Cannot access message database for username &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="544"/>
        <source>Cannot change message database to username &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="581"/>
        <source>Data box identifier related to the new username &apos;%1&apos; doesn&apos;t correspond with the data box identifier related to the old username &apos;%2&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="587"/>
        <source>Cannot change the file database to match the new username &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="593"/>
        <source>Cannot change the message database to match the new username &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="608"/>
        <source>Change username: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="609"/>
        <source>A new username &apos;%1&apos; for the account &apos;%2&apos; has been set.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="611"/>
        <source>Account will use the new settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="626"/>
        <source>Username problem: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="627"/>
        <source>The new username &apos;%1&apos; for account &apos;%2&apos; has not been set!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="630"/>
        <source>Account will use the original settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="728"/>
        <source>Password expiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="729"/>
        <source>Passwords of users listed below have expired or are going to expire within few days. You may change the passwords from this application if they haven&apos;t already expired. Expired passwords can only be changed in the ISDS web portal.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="229"/>
        <source>Creating account: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="203"/>
        <source>Username, password or account name has not been specified. These fields must be filled in.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="218"/>
        <source>Account with username &apos;%1&apos; already exists.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/wrap_accounts.cpp" line="230"/>
        <source>Account &apos;%1&apos; could not be created.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BackupRestoreData</name>
    <message>
        <location filename="../../src/backup.cpp" line="352"/>
        <location filename="../../src/backup.cpp" line="642"/>
        <location filename="../../src/backup.cpp" line="740"/>
        <source>Cannot open or read file &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="367"/>
        <source>Select accounts from the backup JSON file which you want to restore. Data of existing accounts will be replaced by data from the backup.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="630"/>
        <location filename="../../src/backup.cpp" line="730"/>
        <source>File &apos;%1&apos; is not a JSON file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="653"/>
        <source>JSON file &apos;%1&apos; does not contain valid application information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="665"/>
        <source>Backup was taken at %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="672"/>
        <source>Transfer image was taken at %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="680"/>
        <source>Unknown backup type. Selected JSON file contains no valid backup data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="701"/>
        <source>The action will continue with the restoration of the selected accounts. Current data of the selected accounts will be completely rewritten.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="702"/>
        <source>Do you wish to restore the selected accounts from the backup?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="705"/>
        <source>The action will continue with the restoration of the complete application data. Current application data will be completely rewritten.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="706"/>
        <source>Do you wish to restore the application data from the transfer?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="715"/>
        <source>Proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="766"/>
        <location filename="../../src/backup.cpp" line="843"/>
        <location filename="../../src/backup.cpp" line="855"/>
        <source>Importing file &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="773"/>
        <location filename="../../src/backup.cpp" line="862"/>
        <source>Restoration finished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="777"/>
        <location filename="../../src/backup.cpp" line="785"/>
        <location filename="../../src/backup.cpp" line="866"/>
        <location filename="../../src/backup.cpp" line="874"/>
        <source>Restoration finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="778"/>
        <source>All application data have successfully been restored. The application will restart in order to load the imported files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="779"/>
        <source>Do you want to delete the source transfer folder from the application sandbox before the restart (recommended)?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="786"/>
        <source>All application data have successfully been restored.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="787"/>
        <location filename="../../src/backup.cpp" line="876"/>
        <source>The application will restart in order to load the imported files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="797"/>
        <location filename="../../src/backup.cpp" line="885"/>
        <source>Restoration error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="798"/>
        <location filename="../../src/backup.cpp" line="886"/>
        <source>An error during data restoration has occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="799"/>
        <location filename="../../src/backup.cpp" line="887"/>
        <source>Application data restoration has been cancelled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="867"/>
        <source>All selected account data have successfully been restored. The application will restart in order to load the imported files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="868"/>
        <source>Do you want to delete the source backup folder from the application sandbox before the restart (recommended)?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="875"/>
        <source>All selected account data have successfully been restored.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="929"/>
        <location filename="../../src/backup.cpp" line="1498"/>
        <location filename="../../src/backup.cpp" line="1543"/>
        <source>There is not enough space in the selected storage.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="939"/>
        <location filename="../../src/backup.cpp" line="1549"/>
        <source>One or more files mentioned in JSON file are missing or inaccessible.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="997"/>
        <source>Backup error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="998"/>
        <source>An error occurred when backing up data to target location. Backup has been cancelled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="999"/>
        <source>Application data were not backed up to target location.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1016"/>
        <source>Transfer problem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1017"/>
        <source>The data transfer requires application data to be secured with a PIN.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1018"/>
        <source>Set a PIN in the application settings and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1053"/>
        <source>Transferring file &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1310"/>
        <source>Restoring file &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1093"/>
        <source>Transfer finished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="77"/>
        <source>Data were exported into the application sandbox. Now you can transfer the exported files to iCloud or use the iTunes application to transfer the backed up files to your Mac or PC manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="78"/>
        <source>Do you want to upload the files to the application iCloud container?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="84"/>
        <source>Exported backup files are available in the application sandbox. Use the iTunes application in your computer to transfer the exported files to your Mac or PC manually. All exported files will automatically be deleted when you start this application again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="85"/>
        <source>Preserve the backup in a safe place as it contains private data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="103"/>
        <source>unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1102"/>
        <source>Transfer finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1103"/>
        <source>Application data were transferred successfully to target location.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1104"/>
        <source>Preserve the transfer data in a safe place as it contains personal data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1165"/>
        <location filename="../../src/backup.cpp" line="1191"/>
        <location filename="../../src/backup.cpp" line="1381"/>
        <location filename="../../src/backup.cpp" line="1403"/>
        <source>Backing up file &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1319"/>
        <source>From backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1430"/>
        <source>Backup finished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="76"/>
        <location filename="../../src/backup.cpp" line="83"/>
        <location filename="../../src/backup.cpp" line="1439"/>
        <source>Backup finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1440"/>
        <source>All selected accounts have been successfully backed up into target location.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1441"/>
        <source>Preserve the backup in a safe place as it contains personal data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1516"/>
        <source>Application version does not match the transfer version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1518"/>
        <source>Version info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1519"/>
        <source>Warning: Application version does not match the version declared in the transfer. Is is recommended to transfer data between same versions of the application to prevent incompatibility issues.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1520"/>
        <source>Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1540"/>
        <source>Restore all application data from transfer JSON file. The current application data will be complete rewritten with new data from transfer backup.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1559"/>
        <source>Required space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/backup.cpp" line="1560"/>
        <source>Available space</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CalendarDialogue</name>
    <message>
        <location filename="../../qml/dialogues/CalendarDialogue.qml" line="63"/>
        <location filename="../../qml/dialogues/CalendarDialogue.qml" line="63"/>
        <source>Calendar dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Connection</name>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="508"/>
        <source>Success.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="511"/>
        <source>Successfully finished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="514"/>
        <source>Internet connection is probably not available. Check your network settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="517"/>
        <source>Authorization failed. Server complains about a bad request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="520"/>
        <source>Error reply.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="523"/>
        <source>ISDS server is out of service. Scheduled maintenance in progress. Try again later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="526"/>
        <source>Connection with ISDS server timed out. Request was cancelled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="529"/>
        <source>Authorization failed. Check your credentials in the account settings whether they are correct and try again. It is also possible that your password expired. Check your credentials validity by logging in to the data box using the ISDS web portal.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="532"/>
        <source>OTP authorization failed. OTP code is wrong or expired.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="535"/>
        <source>SMS authorization failed. SMS code is wrong or expired.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="538"/>
        <source>SMS authorization failed. SMS code couldn&apos;t be sent. Your order on premium SMS has been exhausted or cancelled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/io/connection.cpp" line="542"/>
        <source>An communication error. See log for more detail.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateAccountPage0</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="12"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="22"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="12"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="22"/>
        <source>Account Creator Wizard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="51"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="51"/>
        <source>Welcome to the account creator wizard. It will help you to create an account in this application in order to access an existing data box. You should already know valid login credentials (typically a username and a password) for a data box.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="52"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="52"/>
        <source>The device which this application is running on must have an active internet connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="56"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="56"/>
        <source>Continue with the Wizard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="66"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="66"/>
        <source>If you don&apos;t want to use the wizard you can skip it and fill in a form.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="70"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="70"/>
        <source>Use Account Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="80"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="80"/>
        <source>Note: If you&apos;ve just freshly obtained your login credentials then you must use them to log into the ISDS web portal for the first time where you will need to change you password. With the new password you can then start using this application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="87"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="87"/>
        <source>Note: This application doesn&apos;t allow the creation of new data boxes on the ISDS server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="97"/>
        <location filename="../../qml/wizards/CreateAccountPage0.qml" line="97"/>
        <source>Recommendation: Read the &lt;a href=&quot;%1&quot;&gt;user manual&lt;/a&gt; in order to better understand how this application works. The &lt;a href=&quot;%2&quot;&gt;project web page&lt;/a&gt; also contains some useful information.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateAccountPage1</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="42"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="42"/>
        <source>Create Account: 1/4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="53"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="53"/>
        <source>Next step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="87"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="87"/>
        <source>The account title must be filled in.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="91"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="91"/>
        <source>Account Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="92"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="92"/>
        <source>Enter custom account name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="100"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="100"/>
        <source>The account title is a user-specified name used for the identification of the account in the application (e.g. &apos;My Personal Data Box&apos;, &apos;Firm Box&apos;, etc.). The chosen name serves only for your convenience. The entry must be filled in.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="108"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="108"/>
        <source>Account Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="113"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="113"/>
        <source>regular account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="118"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="118"/>
        <source>test account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="126"/>
        <location filename="../../qml/wizards/CreateAccountPage1.qml" line="126"/>
        <source>The regular account option is used to access the production (official) ISDS environment (&lt;a href=&quot;https://www.mojedatovaschranka.cz&quot;&gt;www.mojedatovaschranka.cz&lt;/a&gt;). Test accounts are used to access the ISDS testing environment (&lt;a href=&quot;https://www.czebox.cz&quot;&gt;www.czebox.cz&lt;/a&gt;).</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateAccountPage2</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="28"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="28"/>
        <source>Create Account: 2/4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="39"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="39"/>
        <source>Next step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="70"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="70"/>
        <source>Account title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="75"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="75"/>
        <source>Account type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="85"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="85"/>
        <source>Login Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="90"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="90"/>
        <source>username + password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="95"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="95"/>
        <source>username + Mobile Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="100"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="100"/>
        <source>username + password + SMS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="105"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="105"/>
        <source>username + password + security code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="110"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="110"/>
        <source>username + password + certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="117"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="117"/>
        <source>Select the login method which you use to access the data box in the %1 ISDS environment.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="117"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="117"/>
        <source>testing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="117"/>
        <location filename="../../qml/wizards/CreateAccountPage2.qml" line="117"/>
        <source>production</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateAccountPage3</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="52"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="52"/>
        <source>The username must be filled in.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="58"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="58"/>
        <source>The communication code must be filled in.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="65"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="65"/>
        <source>The password must be filled in.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="73"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="73"/>
        <source>The certificate file must be specified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="104"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="104"/>
        <source>Create Account: 3/4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="115"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="115"/>
        <source>Next step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="145"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="145"/>
        <source>Account title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="150"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="150"/>
        <source>Account type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="155"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="155"/>
        <source>Login method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="168"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="184"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="168"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="184"/>
        <source>Fill in your login credentials. All entries must be filled in.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="172"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="172"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="173"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="173"/>
        <source>Enter the login name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="180"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="180"/>
        <source>Warning: The username should contain only combinations of lower-case letters and digits.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="192"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="192"/>
        <source>The username must consist of at least 6 characters without spaces (only combinations of lower-case letters and digits are allowed). Notification: The username is not a data-box ID.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="200"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="200"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="207"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="207"/>
        <source>Enter the password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="216"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="216"/>
        <source>The password must be valid and non-expired. To check whether you&apos;ve entered the password correctly you may use the icon in the field on the right. Note: You must fist change the password using the ISDS web portal if it is your very first attempt to log into the data box.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="221"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="221"/>
        <source>Communication Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="222"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="222"/>
        <source>Enter the communication code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="234"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="234"/>
        <source>The &lt;a href=&quot;%1&quot;&gt;communication code&lt;/a&gt; is a string which can be generated in the ISDS web portal. You have to have the Mobile Key application installed. The Mobile Key application needs to be paired with the corresponding data-box account on the ISDS web portal.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="244"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="244"/>
        <source>Certificate File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="268"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="268"/>
        <source>Choose file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="286"/>
        <location filename="../../qml/wizards/CreateAccountPage3.qml" line="286"/>
        <source>The certificate file is needed for authentication purposes. The supplied file must contain a certificate and its corresponding private key. Only PEM and PFX file formats are accepted.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateAccountPage4</name>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="18"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="18"/>
        <source>During the log-in procedure you will be asked to enter a private key password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="21"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="21"/>
        <source>During the log-in procedure you will be asked to confirm a notification in the Mobile Key application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="23"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="23"/>
        <source>During the log-in procedure you will be asked to enter a code which you should obtain via an SMS.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="25"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="25"/>
        <source>During the log-in procedure you will be asked to enter a security code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="59"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="59"/>
        <source>Create Account: 4/4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="70"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="70"/>
        <source>Create account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="100"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="100"/>
        <source>Account title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="105"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="105"/>
        <source>Account type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="110"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="110"/>
        <source>Login method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="115"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="115"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="122"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="122"/>
        <source>Certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="132"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="132"/>
        <source>Local Account Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="136"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="136"/>
        <source>Remember password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="143"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="143"/>
        <source>Use local storage (database)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="150"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="150"/>
        <source>Copies of messages and attachments will be locally stored. No active internet connection is needed to access locally stored data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="151"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="151"/>
        <source>Copies of messages and attachments will be stored only temporarily in memory. These data will be lost on application exit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="157"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="157"/>
        <source>Synchronise together with all accounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="164"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="164"/>
        <source>The account will be included into the synchronisation process of all accounts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="165"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="165"/>
        <source>The account won&apos;t be included into the synchronisation process of all accounts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="174"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="174"/>
        <source>Create Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="186"/>
        <location filename="../../qml/wizards/CreateAccountPage4.qml" line="186"/>
        <source>All done. The account will be created only when the application successfully logs into the data box using the supplied login credentials.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DataboxList</name>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="54"/>
        <location filename="../../qml/components/DataboxList.qml" line="213"/>
        <location filename="../../qml/components/DataboxList.qml" line="54"/>
        <location filename="../../qml/components/DataboxList.qml" line="213"/>
        <source>Public</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="56"/>
        <location filename="../../qml/components/DataboxList.qml" line="56"/>
        <source>Response to initiatory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="58"/>
        <location filename="../../qml/components/DataboxList.qml" line="58"/>
        <source>Subsidised</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="60"/>
        <location filename="../../qml/components/DataboxList.qml" line="60"/>
        <source>Contractual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="62"/>
        <location filename="../../qml/components/DataboxList.qml" line="62"/>
        <source>Initiatory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="64"/>
        <location filename="../../qml/components/DataboxList.qml" line="64"/>
        <source>Prepaid credit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="154"/>
        <location filename="../../qml/components/DataboxList.qml" line="154"/>
        <source>View details about data box &apos;%1&apos; (identifier &apos;%2&apos;).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="157"/>
        <location filename="../../qml/components/DataboxList.qml" line="157"/>
        <source>Deselect data box &apos;%1&apos; (identifier &apos;%2&apos;).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="160"/>
        <location filename="../../qml/components/DataboxList.qml" line="160"/>
        <source>Select data box &apos;%1&apos; (identifier &apos;%2&apos;).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="215"/>
        <location filename="../../qml/components/DataboxList.qml" line="215"/>
        <source>PDZ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="217"/>
        <location filename="../../qml/components/DataboxList.qml" line="217"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="223"/>
        <location filename="../../qml/components/DataboxList.qml" line="223"/>
        <source>Message type: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="230"/>
        <location filename="../../qml/components/DataboxList.qml" line="230"/>
        <source>Payment: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="237"/>
        <location filename="../../qml/components/DataboxList.qml" line="237"/>
        <source>Payment:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="245"/>
        <location filename="../../qml/components/DataboxList.qml" line="245"/>
        <source>Select commercial message payment method.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/DataboxList.qml" line="295"/>
        <location filename="../../qml/components/DataboxList.qml" line="295"/>
        <source>Remove data box &apos;%1&apos; (identifier &apos;%2&apos;).</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DbWrapper</name>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="162"/>
        <location filename="../../src/net/db_wrapper.cpp" line="233"/>
        <source>Message %1 envelope update failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="176"/>
        <source>%1: new messages: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="179"/>
        <source>%1: No new messages.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="135"/>
        <location filename="../../src/net/db_wrapper.cpp" line="226"/>
        <location filename="../../src/net/db_wrapper.cpp" line="310"/>
        <location filename="../../src/net/db_wrapper.cpp" line="338"/>
        <source>Cannot open message database!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="43"/>
        <source>Data box ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="44"/>
        <source>Data box type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="47"/>
        <source>IČO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="54"/>
        <source>Surname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="58"/>
        <source>Date of birth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="62"/>
        <source>City of birth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="66"/>
        <source>County of birth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="70"/>
        <source>State of birth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="74"/>
        <source>Company name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="79"/>
        <source>Street of residence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="82"/>
        <source>Number in street</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="84"/>
        <source>Number in municipality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="86"/>
        <source>Zip code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="93"/>
        <source>District of residence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="96"/>
        <source>City of residence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="99"/>
        <source>State of residence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="103"/>
        <source>Nationality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="106"/>
        <source>Databox state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="110"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="110"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="108"/>
        <source>Open addressing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="50"/>
        <source>Given names</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="125"/>
        <location filename="../../src/net/db_wrapper.cpp" line="194"/>
        <location filename="../../src/net/db_wrapper.cpp" line="300"/>
        <location filename="../../src/net/db_wrapper.cpp" line="328"/>
        <source>Internal error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="152"/>
        <source>Message %1 envelope insertion failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="205"/>
        <source>Cannot open file database!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/net/db_wrapper.cpp" line="215"/>
        <source>File %1 insertion failed!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ErrorEntry</name>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="138"/>
        <source>No error occurred</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="141"/>
        <source>Request was malformed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="144"/>
        <source>Identifier is missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="147"/>
        <source>Supplied identifier is wrong</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="150"/>
        <source>File format is not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="153"/>
        <source>Data are already present</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="156"/>
        <source>Service limit was exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="159"/>
        <source>Unspecified error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/records_management/json/entry_error.cpp" line="163"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileDialogue</name>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="36"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="36"/>
        <source>Select path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="41"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="41"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="45"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="45"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="132"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="132"/>
        <source>Select location type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="135"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="135"/>
        <source>Desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="134"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="134"/>
        <source>Documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="136"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="136"/>
        <source>Downloads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="137"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="137"/>
        <source>Pictures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="138"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="138"/>
        <source>Temp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="160"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="160"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="257"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="257"/>
        <source>Open directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="260"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="260"/>
        <source>File is selected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="262"/>
        <location filename="../../qml/dialogues/FileDialogue.qml" line="262"/>
        <source>File is not selected.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Files</name>
    <message>
        <location filename="../../src/files.cpp" line="236"/>
        <location filename="../../src/files.cpp" line="265"/>
        <location filename="../../src/files.cpp" line="274"/>
        <source>Open attachment error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="266"/>
        <location filename="../../src/files.cpp" line="275"/>
        <source>There is no application to open this file format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="267"/>
        <location filename="../../src/files.cpp" line="276"/>
        <source>File: &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="237"/>
        <source>Cannot save selected file to disk for opening.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="825"/>
        <source>Path: &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="350"/>
        <location filename="../../src/files.cpp" line="725"/>
        <source>ZFO missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="351"/>
        <location filename="../../src/files.cpp" line="726"/>
        <source>ZFO message is not present in local database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="352"/>
        <location filename="../../src/files.cpp" line="727"/>
        <source>Download complete message again to obtain it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="406"/>
        <source>Delete files: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="407"/>
        <source>Do you want to clean up the file database of account &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="408"/>
        <source>Note: All attachment files of messages will be removed from the database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="450"/>
        <source>Vacuum databases</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="467"/>
        <source>Operation Vacuum has finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="824"/>
        <source>Saving Successful</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="824"/>
        <source>Files have been saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="828"/>
        <source>Saving Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="828"/>
        <source>Files have not been saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="829"/>
        <source>Please check whether the application has permissions to access the storage.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="883"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="884"/>
        <source>Subject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="886"/>
        <source>Attachment size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="888"/>
        <source>Personal delivery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="889"/>
        <location filename="../../src/files.cpp" line="891"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="889"/>
        <location filename="../../src/files.cpp" line="891"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="890"/>
        <source>Delivery by fiction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="893"/>
        <source>Sender</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="894"/>
        <location filename="../../src/files.cpp" line="902"/>
        <source>Databox ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="896"/>
        <location filename="../../src/files.cpp" line="904"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="898"/>
        <location filename="../../src/files.cpp" line="905"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="901"/>
        <source>Recipient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="908"/>
        <source>To hands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="914"/>
        <source>Our file mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="918"/>
        <source>Our reference number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="922"/>
        <source>Your file mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="926"/>
        <source>Your reference number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="930"/>
        <source>Law</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="934"/>
        <source>Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="938"/>
        <source>Section</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="942"/>
        <source>Paragraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="946"/>
        <source>Letter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="950"/>
        <source>Additional info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="954"/>
        <source>Message state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="955"/>
        <source>Delivery time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="959"/>
        <source>Accetance time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="963"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="967"/>
        <source>Events</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FilterBar</name>
    <message>
        <location filename="../../qml/components/FilterBar.qml" line="50"/>
        <location filename="../../qml/components/FilterBar.qml" line="50"/>
        <source>Set filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/FilterBar.qml" line="78"/>
        <location filename="../../qml/components/FilterBar.qml" line="78"/>
        <source>Clear and hide filter field</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GlobalSettingsQmlWrapper</name>
    <message>
        <location filename="../../src/setwrapper.cpp" line="370"/>
        <source>Last synchronisation: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/setwrapper.cpp" line="399"/>
        <source>Select directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/setwrapper.cpp" line="415"/>
        <source>New location error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/setwrapper.cpp" line="416"/>
        <source>It is not possible to store databases in the new location. Write permission denied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/setwrapper.cpp" line="417"/>
        <source>Action will be cancelled.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GovFormList</name>
    <message>
        <location filename="../../qml/components/GovFormList.qml" line="104"/>
        <location filename="../../qml/components/GovFormList.qml" line="104"/>
        <source>Open calendar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GovWrapper</name>
    <message>
        <location filename="../../src/gov_wrapper.cpp" line="176"/>
        <source>Request: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gov_wrapper.cpp" line="178"/>
        <source>Recipient: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gov_wrapper.cpp" line="181"/>
        <source>Send e-gov request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gov_wrapper.cpp" line="182"/>
        <source>Do you want to send the e-gov request to data box &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InputDialogue</name>
    <message>
        <location filename="../../qml/dialogues/InputDialogue.qml" line="86"/>
        <location filename="../../qml/dialogues/InputDialogue.qml" line="86"/>
        <source>QML input dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InputLineMenu</name>
    <message>
        <location filename="../../qml/components/InputLineMenu.qml" line="37"/>
        <location filename="../../qml/components/InputLineMenu.qml" line="37"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/InputLineMenu.qml" line="46"/>
        <location filename="../../qml/components/InputLineMenu.qml" line="46"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/InputLineMenu.qml" line="57"/>
        <location filename="../../qml/components/InputLineMenu.qml" line="57"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IosHelper</name>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="59"/>
        <source>iCloud error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="60"/>
        <source>Unable to access iCloud account. Open the settings and check your iCloud settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="81"/>
        <source>Unable to access iCloud!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="86"/>
        <source>Cannot create subdirectory &apos;%1&apos; in iCloud.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="91"/>
        <source>File &apos;%1&apos; already exists in iCloud.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="95"/>
        <source>File &apos;%1&apos; upload failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="98"/>
        <source>File &apos;%1&apos; has been stored into iCloud.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="114"/>
        <source>Saved to iCloud</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="115"/>
        <source>Files have been stored into iCloud.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="116"/>
        <source>Path: &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="125"/>
        <source>iCloud Problem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/ios_helper.cpp" line="126"/>
        <source>Files have not been saved!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IsdsConversion</name>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="36"/>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="70"/>
        <source>Primary user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="39"/>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="73"/>
        <source>Entrusted user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="42"/>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="76"/>
        <source>Administrator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="45"/>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="48"/>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="79"/>
        <source>Official</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="82"/>
        <source>Virtual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="51"/>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="85"/>
        <source>Liquidator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="54"/>
        <source>Receiver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="57"/>
        <source>Guardian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="60"/>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="88"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="97"/>
        <source>The data box is accessible. It is possible to send messages into it. It can be looked up on the Portal.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="102"/>
        <source>The data box is temporarily inaccessible (at own request). It may be made accessible again at some point in the future.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="107"/>
        <source>The data box is so far inactive. The owner of the box has to log into the web interface at first in order to activate the box.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="111"/>
        <source>The data box is permanently inaccessible. It is waiting to be deleted (but it may be made accessible again).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="115"/>
        <source>The data box has been deleted (nonetheless it exists in ISDS).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="119"/>
        <source>The data box is temporarily inaccessible (because of the reasons enumerated in the law at the time of access denial). It may be made accessible again at some point in the future.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="124"/>
        <source>An error occurred while checking the status.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="137"/>
        <source>Subsidised postal data message, initiating reply postal data message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="141"/>
        <source>Subsidised postal data message, initiating reply postal data message - used for sending reply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="145"/>
        <source>Subsidised postal data message, initiating reply postal data message - unused for sending reply, expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="149"/>
        <source>Postal data message sent using a subscription (prepaid credit)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="153"/>
        <source>Postal data message sent in endowment mode by another data box to the benefactor account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="160"/>
        <source>Initiating postal data message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="163"/>
        <source>Reply postal data message; sent at the expense of the sender of the initiating postal data message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="167"/>
        <source>Public message (recipient or sender is a public authority)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="171"/>
        <source>Initiating postal data message - unused for sending reply, expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="175"/>
        <source>Initiating postal data message - used for sending reply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="179"/>
        <source>Unrecognised message type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="157"/>
        <source>Postal data message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="189"/>
        <source>Message has been submitted (has been created in ISDS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="195"/>
        <source>Data message including its attachments signed with time-stamp.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="202"/>
        <source>Message did not pass through AV check; infected paper deleted; final status before deletion.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="207"/>
        <source>Message handed into ISDS (delivery time recorded).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="216"/>
        <source>10 days have passed since the delivery of the public message which has not been accepted by logging-in (assumption of delivery by fiction in nonOVM DS); this state cannot occur for commercial messages.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="226"/>
        <source>A person authorised to read this message has logged in -- delivered message has been accepted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="231"/>
        <source>Message has been read (on the portal or by ESS action).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="239"/>
        <source>Message marked as undeliverable because the target databox has been made inaccessible.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="247"/>
        <source>Message content deleted, envelope including hashes has been moved into archive.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/conversion/isds_conversion.cpp" line="252"/>
        <source>Message resides in data vault.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IsdsWrapper</name>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1003"/>
        <source>Downloading message: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="149"/>
        <source>Downloading message %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="988"/>
        <source>Delivery info: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1009"/>
        <source>Message %1 has been downloaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="989"/>
        <source>Failed to download delivery info for message %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="818"/>
        <source>Getting account info %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="971"/>
        <source>Account info: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="972"/>
        <source>Failed to get account info for username %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1099"/>
        <source>%1: Message has been sent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1100"/>
        <source>Message has successfully been sent to all recipients.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1105"/>
        <source>%1: Error sending message!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1106"/>
        <source>Message has NOT been sent to all recipients.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="960"/>
        <source>SMS code for &apos;%1&apos; required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="961"/>
        <source>Enter SMS code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="948"/>
        <source>Enter security code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="482"/>
        <source>Account &apos;%1&apos; requires SMS code for password changing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1004"/>
        <source>Failed to download complete message %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1024"/>
        <source>%1: messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1025"/>
        <source>Failed to download list of messages for user name &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="932"/>
        <source>Login problem: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="933"/>
        <source>Error while logging in with username &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="484"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="890"/>
        <source>Do you want to send SMS code now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="265"/>
        <source>No zfo files or selected folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="277"/>
        <source>No account for zfo import.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="286"/>
        <source>No zfo files in the selected directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="292"/>
        <source>No zfo file for import.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="309"/>
        <source>ZFO import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="163"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="181"/>
        <source>Finding databoxes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="214"/>
        <source>Find data box: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="215"/>
        <source>Failed finding data box to phrase &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="305"/>
        <source>ZFO messages cannot be imported because there is no active account for their verification.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="461"/>
        <source>%1: sending message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="481"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="887"/>
        <source>SMS code: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="526"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="539"/>
        <source>%1: downloading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="596"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="602"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="608"/>
        <source>Unknown Message Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="597"/>
        <source>No information about the recipient data box &apos;%1&apos; could be obtained. It is unknown whether public or commercial messages can be sent to this recipient.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="603"/>
        <source>No commercial message to the recipient data box &apos;%1&apos; can be sent. It is unknown whether a public messages can be sent to this recipient.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="609"/>
        <source>No public message to the recipient data box &apos;%1&apos; can be sent. It is unknown whether a commercial messages can be sent to this recipient.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="614"/>
        <source>Cannot send message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="615"/>
        <source>No public data message nor a commercial data message (PDZ) can be sent to the recipient data box &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="616"/>
        <source>Receiving of PDZs has been disabled in the recipient data box or there are no available payment methods for PDZs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="623"/>
        <source>Data box is not active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="624"/>
        <source>Recipient data box ID &apos;%1&apos; isn&apos;t active.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="625"/>
        <location filename="../../src/isds/isds_wrapper.cpp" line="634"/>
        <source>The message cannot be delivered.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="632"/>
        <source>Wrong Recipient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="633"/>
        <source>Recipient with data box ID &apos;%1&apos; doesn&apos;t exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="642"/>
        <source>Recipient Search Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="643"/>
        <source>Information about recipient data box &apos;%1&apos; couldn&apos;t be obtained.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="644"/>
        <source>Do you still want to add the box &apos;%1&apos; into the recipient list?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="700"/>
        <source>Logging in to data box %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="745"/>
        <source>Internal error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="749"/>
        <source>Login failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="862"/>
        <source>Enter communication code: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="863"/>
        <source>Communication code for account &apos;%1&apos; is required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="865"/>
        <source>Enter communication code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="877"/>
        <source>Password for account &apos;%1&apos; missing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="904"/>
        <source>Certificate password for account &apos;%1&apos; is required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="919"/>
        <source>New account problem: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="920"/>
        <source>New account could not be created for username &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="923"/>
        <source>Note: You must change your password via the ISDS web portal if this is your first login to the data box. You can start using mobile Datovka to access the data box after changing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="927"/>
        <source>Username could not be changed on the new username &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="947"/>
        <source>Security code for &apos;%1&apos; required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1038"/>
        <source>ZFO import: %1/%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1042"/>
        <source>ZFO import has been finished with the result:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1043"/>
        <source>ZFO import: done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1078"/>
        <source>Message has been sent to &lt;b&gt;&apos;%1&apos;&lt;/b&gt; &lt;i&gt;(%2)&lt;/i&gt; as message number &lt;b&gt;%3&lt;/b&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1085"/>
        <source>Message has NOT been sent to &lt;b&gt;&apos;%1&apos;&lt;/b&gt; &lt;i&gt;(%2)&lt;/i&gt;. ISDS returns: %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1095"/>
        <source>Sending finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1127"/>
        <source>PDZ sending: unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1142"/>
        <source>PDZ sending: active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1145"/>
        <source>Subsidised by data box &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1148"/>
        <source>Contract with Czech post</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="1152"/>
        <source>Remaining credit: %1 Kč</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="926"/>
        <source>Username change: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="876"/>
        <source>Enter password: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="946"/>
        <source>Enter security code: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="903"/>
        <source>Enter certificate password: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="723"/>
        <source>Waiting for confirmation ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="959"/>
        <source>Enter SMS code: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="879"/>
        <source>Enter password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="888"/>
        <source>Account &apos;%1&apos; requires authentication with SMS code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_wrapper.cpp" line="906"/>
        <source>Enter certificate password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Log</name>
    <message>
        <location filename="../../src/log.cpp" line="89"/>
        <source>Send log via email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/log.cpp" line="90"/>
        <source>Do you want to send the log information to developers?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LogBar</name>
    <message>
        <location filename="../../qml/components/LogBar.qml" line="47"/>
        <location filename="../../qml/components/LogBar.qml" line="47"/>
        <source>Close log bar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../../src/isds/isds_login.cpp" line="328"/>
        <source>MEP confirmation timeout.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_login.cpp" line="333"/>
        <source>MEP login was cancelled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_login.cpp" line="338"/>
        <source>Wrong MEP login credentials. Invalid username or communication code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_login.cpp" line="343"/>
        <source>MEP login failed.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageDialogue</name>
    <message>
        <location filename="../../qml/dialogues/MessageDialogue.qml" line="84"/>
        <location filename="../../qml/dialogues/MessageDialogue.qml" line="84"/>
        <source>Question</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/MessageDialogue.qml" line="85"/>
        <location filename="../../qml/dialogues/MessageDialogue.qml" line="85"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/MessageDialogue.qml" line="86"/>
        <location filename="../../qml/dialogues/MessageDialogue.qml" line="86"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/dialogues/MessageDialogue.qml" line="87"/>
        <location filename="../../qml/dialogues/MessageDialogue.qml" line="87"/>
        <source>Critical</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageList</name>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="123"/>
        <location filename="../../qml/components/MessageList.qml" line="123"/>
        <source>Unread message from sender</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="123"/>
        <location filename="../../qml/components/MessageList.qml" line="123"/>
        <source>Read message from sender</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="125"/>
        <location filename="../../qml/components/MessageList.qml" line="125"/>
        <source>Message to receiver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/MessageList.qml" line="128"/>
        <location filename="../../qml/components/MessageList.qml" line="128"/>
        <source>Subject</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Messages</name>
    <message>
        <location filename="../../src/messages.cpp" line="313"/>
        <source>Delete message: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="314"/>
        <source>Do you want to delete the message &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="315"/>
        <source>Note: It will delete all attachments and message information from the local database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="404"/>
        <source>New location error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="405"/>
        <source>It is not possible to store databases in the new location because there is not enough free space left.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="406"/>
        <source>Databases size is %1 MB.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="478"/>
        <source>Change database location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="479"/>
        <source>What do you want to do with the currently used database files?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="480"/>
        <source>You have the option to move the current database files to a new location or you can delete them and create new empty databases.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="473"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/messages.cpp" line="474"/>
        <source>Create new</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MvCrrVbhData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="91"/>
        <source>Driving licence ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="92"/>
        <source>Enter driving licence ID without spaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="102"/>
        <source>Data-box owner name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="112"/>
        <source>Data-box owner surname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="122"/>
        <source>Birth date</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MvIrVpData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_ir_vp.cpp" line="516"/>
        <source>Identification number (IČO)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_ir_vp.cpp" line="517"/>
        <source>Enter identification number (IČO)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MvRtVtData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="106"/>
        <source>Data-box owner name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="116"/>
        <source>Data-box owner surname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="126"/>
        <source>Birth date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="136"/>
        <source>Birth region</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="146"/>
        <source>City</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MvRtpoVtData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.cpp" line="521"/>
        <source>Identification number (IČO)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.cpp" line="522"/>
        <source>Enter identification number (IČO)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MvSkdVpData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_skd_vp.cpp" line="517"/>
        <source>Identification number (IČO)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_skd_vp.cpp" line="518"/>
        <source>Enter identification number (IČO)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MvVrVpData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_vr_vp.cpp" line="550"/>
        <source>Identification number (IČO)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_vr_vp.cpp" line="551"/>
        <source>Enter identification number (IČO)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MvZrVpData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_zr_vp.cpp" line="516"/>
        <source>Identification number (IČO)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_zr_vp.cpp" line="517"/>
        <source>Enter identification number (IČO)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageAboutApp</name>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="37"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="37"/>
        <source>About Datovka</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="88"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="88"/>
        <source>Datovka - free mobile Data-Box client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="75"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="75"/>
        <source>Open application home page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="90"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="90"/>
        <source>Version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="102"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="102"/>
        <source>This application provides the means to access data boxes in the ISDS system. It enables you to download and to view the content of messages held within the data box. You can also, with some limitations, send messages from this application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="104"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="104"/>
        <source>You may use this application only at your own risk. The association CZ.NIC is not the operator of the data box system. CZ.NIC is not liable for any damage that may be directly or indirectly caused by using this application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="106"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="106"/>
        <source>If you have problems using this application or want help or you just want more information then start by reading through the information in the &lt;a href=&quot;%1&quot;&gt;user manual&lt;/a&gt; or on the &lt;a href=&quot;%2&quot;&gt;project web page&lt;/a&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="119"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="119"/>
        <source>The application is licensed under the &lt;a href=&quot;%1&quot;&gt;GNU GPL v3&lt;/a&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="125"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="125"/>
        <source>Powered by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAboutApp.qml" line="132"/>
        <location filename="../../qml/pages/PageAboutApp.qml" line="132"/>
        <source>Open home page of the CZ.NIC association.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageAccountDetail</name>
    <message>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="49"/>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="49"/>
        <source>Account info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="60"/>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="60"/>
        <source>Refresh information about the account.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="102"/>
        <location filename="../../qml/pages/PageAccountDetail.qml" line="102"/>
        <source>Account info has not been downloaded yet.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageAccountList</name>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="120"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="120"/>
        <source>Synchronise all accounts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="106"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="106"/>
        <source>Show application preferences.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="94"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="94"/>
        <source>Accounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="133"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="133"/>
        <source>Add a new account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="185"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="185"/>
        <source>Hide banner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="197"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="197"/>
        <source>Permanently hide banner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageAccountList.qml" line="198"/>
        <location filename="../../qml/pages/PageAccountList.qml" line="198"/>
        <source>Do you want to permanently hide the banner?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageBackupData</name>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="67"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="215"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="67"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="215"/>
        <source>Target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="67"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="67"/>
        <source>Application sandbox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="73"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="73"/>
        <source>Transfer application data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="74"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="74"/>
        <source>The action allows to transfer complete application data to another device. Preserve the files in a safe place as it contains login and private data. Transferred data require to be secured with a PIN. Set PIN in the application settings if it not set.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="76"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="84"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="76"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="84"/>
        <source>Data will be stored into the application sandbox and, subsequently, you can upload them into the iCloud or you can use the iTunes application in order to copy them to your Mac or PC.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="78"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="86"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="78"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="86"/>
        <source>Specify the location where you want to store the data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="81"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="135"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="81"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="135"/>
        <source>Back up accounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="81"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="81"/>
        <source>Back up account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="82"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="82"/>
        <source>The action allows to back up selected accounts. Preserve the backup in a safe place as it contains private data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="150"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="150"/>
        <source>Proceed with operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="190"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="190"/>
        <source>Specify the location where you want to back up your data. Preserve the backup in a safe place as it contains private data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="194"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="194"/>
        <source>Back up ZFO data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="202"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="202"/>
        <source>Choose location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="230"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="230"/>
        <source>Select all accounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="274"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="274"/>
        <source>Select %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageBackupData.qml" line="274"/>
        <location filename="../../qml/pages/PageBackupData.qml" line="274"/>
        <source>Deselect %1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageChangePassword</name>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="46"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="46"/>
        <source>Enter current password, twice a new password and SMS code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="46"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="52"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="57"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="46"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="52"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="57"/>
        <source>Account is logged in to ISDS.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="47"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="47"/>
        <source>Enter SMS code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="52"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="52"/>
        <source>Enter current password, twice a new password and security code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="53"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="53"/>
        <source>Enter security code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="57"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="57"/>
        <source>Enter current password and twice a new password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="77"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="77"/>
        <source>Account must be logged in to the data box to be able to change the password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="85"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="85"/>
        <source>Change password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="112"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="112"/>
        <source>Accept changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="115"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="115"/>
        <source>Old and new password must both be filled in.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="120"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="120"/>
        <source>The new password fields do not match.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="125"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="125"/>
        <source>Wrong password format. The new password must contain at least 8 characters including at least 1 number and at least 1 upper-case letter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="131"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="131"/>
        <source>Security/SMS code must be filled in.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="141"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="141"/>
        <source>Password change failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="267"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="267"/>
        <source>Enter the current password to be able to send the SMS code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="278"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="278"/>
        <source>SMS code cannot be sent. Entered wrong current password or ISDS server is out of order.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="101"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="101"/>
        <source>Hide passwords</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="101"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="101"/>
        <source>Show passwords</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="168"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="168"/>
        <source>Enter the current password and a new password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="178"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="178"/>
        <source>Log in now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="194"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="194"/>
        <source>Current password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="218"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="218"/>
        <source>New password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="242"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="242"/>
        <source>Confirm the new password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="264"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="264"/>
        <source>Send SMS code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="273"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="273"/>
        <source>SMS code was sent...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="296"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="296"/>
        <source>Enter OTP code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="316"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="316"/>
        <source>This will change the password on the ISDS server. You won&apos;t be able to log in to ISDS without the new password. If your current password has already expired then you must log into the ISDS web portal to change it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="325"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="325"/>
        <source>If you just want to update the password because it has been changed elsewhere then go to the </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="325"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="325"/>
        <source>Account settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageChangePassword.qml" line="334"/>
        <location filename="../../qml/pages/PageChangePassword.qml" line="334"/>
        <source>Keep a copy of the login credentials and store it on a safe place which only an authorised person has access to.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageContactList</name>
    <message>
        <location filename="../../qml/pages/PageContactList.qml" line="72"/>
        <location filename="../../qml/pages/PageContactList.qml" line="72"/>
        <source>Contacts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageContactList.qml" line="86"/>
        <location filename="../../qml/pages/PageContactList.qml" line="86"/>
        <source>Filter data boxes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageContactList.qml" line="145"/>
        <location filename="../../qml/pages/PageContactList.qml" line="145"/>
        <source>No databox found in contacts.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageDataboxDetail</name>
    <message>
        <location filename="../../qml/pages/PageDataboxDetail.qml" line="44"/>
        <location filename="../../qml/pages/PageDataboxDetail.qml" line="44"/>
        <source>Databox info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxDetail.qml" line="83"/>
        <location filename="../../qml/pages/PageDataboxDetail.qml" line="83"/>
        <source>Information about the data box &apos;%1&apos; are not available. The data box may have been temporarily disabled. It is likely that you cannot send data message to this data box.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageDataboxSearch</name>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="75"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="75"/>
        <source>Find databox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="144"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="144"/>
        <source>IČO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="151"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="151"/>
        <source>Enter sought phrase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="203"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="203"/>
        <source>Previous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="244"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="244"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="128"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="128"/>
        <source>All databoxes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="90"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="90"/>
        <source>Filter data boxes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="103"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="103"/>
        <source>Search data boxes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="125"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="125"/>
        <source>Select type of sought data box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="129"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="129"/>
        <source>OVM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="130"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="130"/>
        <source>PO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="131"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="131"/>
        <source>PFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="132"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="132"/>
        <source>FO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="139"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="139"/>
        <source>Select entries to search in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="142"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="142"/>
        <source>All fields</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="143"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="143"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="145"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="145"/>
        <source>Databox ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="342"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="342"/>
        <source>from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="344"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="344"/>
        <source>Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="342"/>
        <location filename="../../qml/pages/PageDataboxSearch.qml" line="342"/>
        <source>Shown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageGovService</name>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="76"/>
        <location filename="../../qml/pages/PageGovService.qml" line="76"/>
        <source>Service:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="87"/>
        <location filename="../../qml/pages/PageGovService.qml" line="87"/>
        <source>Send request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="108"/>
        <location filename="../../qml/pages/PageGovService.qml" line="108"/>
        <source>Account:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="114"/>
        <location filename="../../qml/pages/PageGovService.qml" line="114"/>
        <source>Request:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="140"/>
        <location filename="../../qml/pages/PageGovService.qml" line="140"/>
        <source>Required information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="147"/>
        <location filename="../../qml/pages/PageGovService.qml" line="147"/>
        <source>Acquired from data box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="156"/>
        <location filename="../../qml/pages/PageGovService.qml" line="156"/>
        <source>No user data needed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovService.qml" line="169"/>
        <location filename="../../qml/pages/PageGovService.qml" line="169"/>
        <source>Select date</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageGovServiceList</name>
    <message>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="61"/>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="61"/>
        <source>E-Gov Services</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="75"/>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="75"/>
        <source>Set filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="131"/>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="131"/>
        <source>No available e-government service.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="132"/>
        <location filename="../../qml/pages/PageGovServiceList.qml" line="132"/>
        <source>No e-government service found that matches filter text &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageHeader</name>
    <message>
        <location filename="../../qml/components/PageHeader.qml" line="78"/>
        <location filename="../../qml/components/PageHeader.qml" line="78"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageImportMessage</name>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="74"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="74"/>
        <source>ZFO message import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="86"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="86"/>
        <source>Select ZFO files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="95"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="95"/>
        <source>Select import directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="126"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="126"/>
        <source>Import ZFO files from directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="151"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="151"/>
        <source>Here you can import messages from ZFO files into the local database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="155"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="155"/>
        <source>Verify messages on the ISDS server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="177"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="177"/>
        <source>Imported messages are going to be sent to the ISDS server where they are going to be checked. Messages failing this test won&apos;t be imported. We don&apos;t recommend using this option when you are using a mobile or slow internet connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="186"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="186"/>
        <source>Account must be logged in to the data box to be able to verify messages.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="194"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="194"/>
        <source>Log in now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="231"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="231"/>
        <source>Account is logged in to ISDS.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="231"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="231"/>
        <source>Select ZFO files or a directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="178"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="178"/>
        <source>Imported messages won&apos;t be validated on the ISDS server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageImportMessage.qml" line="107"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="119"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="107"/>
        <location filename="../../qml/pages/PageImportMessage.qml" line="119"/>
        <source>Import selected ZFO files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageLog</name>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="45"/>
        <location filename="../../qml/pages/PageLog.qml" line="45"/>
        <source>Log from current run:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="59"/>
        <location filename="../../qml/pages/PageLog.qml" line="59"/>
        <source>Log file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="67"/>
        <location filename="../../qml/pages/PageLog.qml" line="67"/>
        <source>Log Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="78"/>
        <location filename="../../qml/pages/PageLog.qml" line="78"/>
        <source>Send log to helpdesk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageLog.qml" line="117"/>
        <location filename="../../qml/pages/PageLog.qml" line="117"/>
        <source>Choose log file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageMenuAccount</name>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="53"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="53"/>
        <source>Account properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="133"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="133"/>
        <source>Account settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="140"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="140"/>
        <source>View account info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="147"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="147"/>
        <source>Create message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="154"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="154"/>
        <source>Send e-gov request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="161"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="161"/>
        <source>Import message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="168"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="168"/>
        <source>Find databox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="175"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="175"/>
        <source>Back up downloaded data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="182"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="182"/>
        <source>Clean up the file database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="189"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="189"/>
        <source>Change password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="196"/>
        <location filename="../../qml/pages/PageMenuAccount.qml" line="196"/>
        <source>Delete account</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageMenuDatovkaSettings</name>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="68"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="68"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="139"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="139"/>
        <source>Add account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="146"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="146"/>
        <source>Search message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="153"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="153"/>
        <source>Import message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="160"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="160"/>
        <source>Update list of uploaded files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="174"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="174"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="181"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="181"/>
        <source>Synchronization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="188"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="188"/>
        <source>Storage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="195"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="195"/>
        <source>Security and PIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="202"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="202"/>
        <source>Transfer all application data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="209"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="209"/>
        <source>Back up downloaded data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="216"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="216"/>
        <source>Restore data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="244"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="244"/>
        <source>About Datovka</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="167"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="167"/>
        <source>Records Management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="223"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="223"/>
        <source>Log Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="230"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="230"/>
        <source>Show log panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="237"/>
        <location filename="../../qml/pages/PageMenuDatovkaSettings.qml" line="237"/>
        <source>User Guide</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageMenuMessage</name>
    <message>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="52"/>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="52"/>
        <source>Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="84"/>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="84"/>
        <source>Download attachments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="98"/>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="98"/>
        <source>Mark as read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="105"/>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="105"/>
        <source>Mark as unread</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="91"/>
        <location filename="../../qml/pages/PageMenuMessage.qml" line="91"/>
        <source>Delete message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageMenuMessageDetail</name>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="65"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="65"/>
        <source>Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="149"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="149"/>
        <source>Download attachments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="156"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="156"/>
        <source>Reply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="163"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="163"/>
        <source>Forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="170"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="170"/>
        <source>Use as template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="177"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="177"/>
        <source>Upload to records management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="184"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="184"/>
        <source>Send message zfo by email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="191"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="191"/>
        <source>Send attachments by email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="198"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="198"/>
        <source>Save message zfo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="205"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="205"/>
        <source>Save attachments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="212"/>
        <location filename="../../qml/pages/PageMenuMessageDetail.qml" line="212"/>
        <source>Delete attachments</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageMenuMessageList</name>
    <message>
        <location filename="../../qml/pages/PageMenuMessageList.qml" line="42"/>
        <location filename="../../qml/pages/PageMenuMessageList.qml" line="42"/>
        <source>Message operations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageList.qml" line="66"/>
        <location filename="../../qml/pages/PageMenuMessageList.qml" line="66"/>
        <source>Mark messages as read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMenuMessageList.qml" line="73"/>
        <location filename="../../qml/pages/PageMenuMessageList.qml" line="73"/>
        <source>Mark messages as unread</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageMessageDetail</name>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="103"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="103"/>
        <source>File doesn&apos;t contain a valid message nor does it contain valid delivery information or the file is corrupt.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="159"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="159"/>
        <source>Received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="161"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="161"/>
        <source>Sent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="163"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="165"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="163"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="165"/>
        <source>ZFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="169"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="169"/>
        <source>Unknown ZFO content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="167"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="167"/>
        <source>Message ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../qml/pages/PageMessageDetail.qml" line="75"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="75"/>
        <source>The message will be deleted from ISDS in %n day(s).</source>
        <translation>
            <numerusform>The message will be deleted from ISDS in %n day.</numerusform>
            <numerusform>The message will be deleted from ISDS in %n days.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qml/pages/PageMessageDetail.qml" line="78"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="78"/>
        <source>The message will be deleted from ISDS in %n day(s) because the long term storage is full.</source>
        <translation>
            <numerusform>The message will be deleted from ISDS in %n day because the long term storage is full.</numerusform>
            <numerusform>The message will be deleted from ISDS in %n days because the long term storage is full.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../qml/pages/PageMessageDetail.qml" line="80"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="80"/>
        <source>The message will be moved to the long term storage in %n day(s) if the storage is not full. The message will be deleted from the ISDS server if the storage is full.</source>
        <translation>
            <numerusform>The message will be moved to the long term storage in %n day if the storage is not full. The message will be deleted from the ISDS server if the storage is full.</numerusform>
            <numerusform>The message will be moved to the long term storage in %n days if the storage is not full. The message will be deleted from the ISDS server if the storage is full.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="84"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="84"/>
        <source>The message has already been deleted from the ISDS.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="184"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="184"/>
        <source>Email attachments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="194"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="194"/>
        <source>Save attachments.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="208"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="208"/>
        <source>Download message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="220"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="220"/>
        <source>Show menu of available operations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="283"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="283"/>
        <source>Attachments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="374"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="374"/>
        <source>Open attachment &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="418"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="418"/>
        <source>Attachments have not been downloaded yet.
Click the icon or this text for their download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="418"/>
        <location filename="../../qml/pages/PageMessageDetail.qml" line="418"/>
        <source>No attachments present.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageMessageList</name>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="85"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="85"/>
        <source>Sent messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="85"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="85"/>
        <source>Received messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="99"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="99"/>
        <source>Filter messages.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="113"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="113"/>
        <source>Create and send message.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="129"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="129"/>
        <source>Show menu of available operations.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="259"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="259"/>
        <source>No messages or have not been downloaded yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageList.qml" line="260"/>
        <location filename="../../qml/pages/PageMessageList.qml" line="260"/>
        <source>No message found that matches filter text &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageMessageSearch</name>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="56"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="56"/>
        <source>Search message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="70"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="70"/>
        <source>Search messages.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="94"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="94"/>
        <source>Select type of sought messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="98"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="98"/>
        <source>All messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="99"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="99"/>
        <source>Received messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="100"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="100"/>
        <source>Sent messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="112"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="112"/>
        <source>Enter phrase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="165"/>
        <location filename="../../qml/pages/PageMessageSearch.qml" line="165"/>
        <source>No message found for given search phrase.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageRecordsManagementUpload</name>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="106"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="106"/>
        <source>Upload message %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="120"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="120"/>
        <source>Filter upload hierarchy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="146"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="146"/>
        <source>The message can be uploaded into selected locations in the records management hierarchy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="155"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="155"/>
        <source>Update upload hierarchy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="166"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="166"/>
        <source>Upload message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="203"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="203"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="244"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="244"/>
        <source>Upload hierarchy has not been downloaded yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="245"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="245"/>
        <source>No hierarchy entry found that matches filter text &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="301"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="301"/>
        <source>View content of %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="303"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="303"/>
        <source>Select %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="303"/>
        <location filename="../../qml/pages/PageRecordsManagementUpload.qml" line="303"/>
        <source>Deselect %1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageRestoreData</name>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="107"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="107"/>
        <source>Restore data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="122"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="122"/>
        <source>Proceed with restoration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="155"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="155"/>
        <source>The action allows to restore data from a backup or transfer file. Select the backup or transfer JSON file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="159"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="159"/>
        <source>Choose JSON file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="192"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="192"/>
        <source>Restore ZFO data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="200"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="200"/>
        <source>Select all accounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="244"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="244"/>
        <source>Select %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageRestoreData.qml" line="244"/>
        <location filename="../../qml/pages/PageRestoreData.qml" line="244"/>
        <source>Deselect %1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageSendMessage</name>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="113"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="113"/>
        <source>Reply %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="132"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="132"/>
        <source>Forward %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="148"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="148"/>
        <source>Forward ZFO %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="159"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="159"/>
        <source>ZFO file of message %1 is not available. Download complete message to get it before forwarding.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="196"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="196"/>
        <source>Databox: %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="202"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="202"/>
        <source>New message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="241"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="241"/>
        <source>Create message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="252"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="252"/>
        <source>Send message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="259"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="259"/>
        <source>Before sending a data message you must specify a subject, at least one recipient and at least one attached file. The following fields are missing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="262"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="262"/>
        <source>message subject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="265"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="265"/>
        <source>recipient data box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="268"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="268"/>
        <source>attachment file or files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="270"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="270"/>
        <source>Please fill in the respective fields before attempting to send the message.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="295"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="295"/>
        <source>Send Message Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="313"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="313"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="331"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="331"/>
        <source>Error during message creation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="388"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="388"/>
        <source>Previous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <source>Recipients</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <source>Attachments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <source>Additional</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="399"/>
        <source>Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="425"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="425"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="458"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="458"/>
        <source>Fill in the subject of the message, select at least one recipient data box and specify at least one attachment file or put in a short text message before sending the message.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="465"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="465"/>
        <source>Sender account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="480"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="480"/>
        <source>Subject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="503"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="503"/>
        <source>Include sender identification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="510"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="510"/>
        <source>Personal delivery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="518"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="518"/>
        <source>Allow acceptance through fiction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="540"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="540"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="553"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="553"/>
        <source>Find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="586"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="586"/>
        <source>Mandatory for initiatory message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="595"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="595"/>
        <source>Mandatory for reply message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="618"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="618"/>
        <source>Add file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="620"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="620"/>
        <source>Select files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="729"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="729"/>
        <source>Open file &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="985"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="985"/>
        <source>Here you can create a short text message and add it to attachments as a PDF file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="986"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="986"/>
        <source>Add the message to attachments after you have finished editing the text.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="996"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="996"/>
        <source>Text message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1005"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1005"/>
        <source>View PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1015"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1015"/>
        <source>Append PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1032"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="1032"/>
        <source>Enter a short text message.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="684"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="684"/>
        <source>Local database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="487"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="487"/>
        <source>Enter subject. Mandatory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="747"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="747"/>
        <source>Remove file &apos;%1&apos; from list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="786"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="786"/>
        <source>Mandate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="850"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="850"/>
        <source>Our reference number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="873"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="873"/>
        <source>Our file mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="897"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="897"/>
        <source>Your reference number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="920"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="920"/>
        <source>Your file mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="943"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="943"/>
        <source>To hands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="824"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="824"/>
        <source>par.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="76"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="97"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="634"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="76"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="97"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="634"/>
        <source>Total size of attachments is %1 B.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="80"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="80"/>
        <source>The permitted number of (%1) attachments has been exceeded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="85"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="85"/>
        <source>Total size of attachments exceeds %1 MB.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="90"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="90"/>
        <source>Total size of attachments exceeds %1 MB. Most of the data boxes cannot receive messages larger than %1 MB. However, some OVM data boxes can receive message up to %2 MB.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="95"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="95"/>
        <source>Total size of attachments is ~%1 kB.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="836"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="836"/>
        <source>let.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="856"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="856"/>
        <source>Enter our reference number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="879"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="879"/>
        <source>Enter our file mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="903"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="903"/>
        <source>Enter your reference number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="926"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="926"/>
        <source>Enter your file mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSendMessage.qml" line="949"/>
        <location filename="../../qml/pages/PageSendMessage.qml" line="949"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageSettingsAccount</name>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="254"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="254"/>
        <source>Certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="276"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="276"/>
        <source>Choose file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="232"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="232"/>
        <source>Login method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="109"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="109"/>
        <source>New account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="125"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="125"/>
        <source>Accept changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="237"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="237"/>
        <source>Select login method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="241"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="241"/>
        <source>Username + Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="242"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="242"/>
        <source>Certificate + Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="243"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="243"/>
        <source>Password + Security code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="244"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="244"/>
        <source>Password + Security SMS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="245"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="245"/>
        <source>Mobile key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="292"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="292"/>
        <source>Enter custom account name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="297"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="297"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="298"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="298"/>
        <source>Enter the login name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="302"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="304"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="302"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="304"/>
        <source>Wrong username format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="305"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="305"/>
        <source>Warning: The username should contain only combinations of lower-case letters and digits.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="322"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="322"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="328"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="328"/>
        <source>Enter PIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="335"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="335"/>
        <source>Enter the password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="344"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="344"/>
        <source>Communication code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="345"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="345"/>
        <source>Enter the communication code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="390"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="390"/>
        <source>The account will be included into the synchronisation process of all accounts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="391"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="391"/>
        <source>The account won&apos;t be included into the synchronisation process of all accounts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="431"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="431"/>
        <source>Wrong PIN code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="432"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="432"/>
        <source>Entered PIN is not valid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="291"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="291"/>
        <source>Account title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="327"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="327"/>
        <source>PIN required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="327"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="327"/>
        <source>Enter PIN code in order to show the password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="350"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="350"/>
        <source>Remember password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="356"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="356"/>
        <source>Test account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="363"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="363"/>
        <source>Test accounts are used to access the ISDS testing environment.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="369"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="369"/>
        <source>Use local storage (database)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="376"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="376"/>
        <source>Messages and attachments will be locally stored. No active internet connection is needed to access locally stored data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="377"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="377"/>
        <source>Messages and attachments will be stored only temporarily in memory. These data will be lost on application exit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="383"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="383"/>
        <source>Synchronise together with all accounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="411"/>
        <location filename="../../qml/pages/PageSettingsAccount.qml" line="411"/>
        <source>Account settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageSettingsGeneral</name>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="49"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="49"/>
        <source>General settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="71"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="71"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="76"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="76"/>
        <source>Select language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="80"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="80"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="81"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="81"/>
        <source>Czech</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="82"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="82"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="89"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="89"/>
        <source>Note: Language will be changed after application restart.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="94"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="94"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="99"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="99"/>
        <source>Select font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="102"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="102"/>
        <source>System Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="103"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="103"/>
        <source>Roboto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="104"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="104"/>
        <source>Source Sans Pro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="111"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="111"/>
        <source>Note: Font will be changed after application restart.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="117"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="117"/>
        <source>Font size and application scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="128"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="128"/>
        <source>Set application font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="139"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="139"/>
        <source>Note: Font size will be changed after application restart. Default is %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="151"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="151"/>
        <source>Debug verbosity level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="159"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="159"/>
        <source>Set the amount of logged debugging information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="165"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="165"/>
        <source>Debug verbosity controls the amount of debugging information written to the log file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="170"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="170"/>
        <source>Log verbosity level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="178"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="178"/>
        <source>Set the verbosity of logged entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="184"/>
        <location filename="../../qml/pages/PageSettingsGeneral.qml" line="184"/>
        <source>Log verbosity controls the level of detail of the logged entries.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageSettingsPin</name>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="45"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="45"/>
        <source>Currently there is no PIN code set.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="50"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="50"/>
        <source>You will be asked to enter a PIN code on application start-up.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="68"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="68"/>
        <source>Enter a new PIN code into both text fields:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="83"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="99"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="83"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="99"/>
        <source>In order to change the PIN code you must enter the current and a new PIN code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="114"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="114"/>
        <source>Something went wrong!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="119"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="119"/>
        <source>PIN Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="138"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="138"/>
        <source>Accept changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="152"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="174"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="152"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="174"/>
        <source>Error: Both new PIN code fields must be filled in!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="161"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="183"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="161"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="183"/>
        <source>Error: Newly entered PIN codes are different!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="194"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="209"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="194"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="209"/>
        <source>Error: Current PIN code is wrong!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="246"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="246"/>
        <source>Set PIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="256"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="256"/>
        <source>Change PIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="266"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="266"/>
        <source>Disable PIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="279"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="279"/>
        <source>Current PIN code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="291"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="291"/>
        <source>New PIN code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="303"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="303"/>
        <source>Confirm new PIN code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="320"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="320"/>
        <source>Lock after seconds of inactivity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="330"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="330"/>
        <source>don&apos;t lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="333"/>
        <location filename="../../qml/pages/PageSettingsPin.qml" line="333"/>
        <source>Select the number of seconds after which the application is going to be locked.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageSettingsRecordsManagement</name>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="179"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="179"/>
        <source>Get service info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="69"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="69"/>
        <source>Records management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="84"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="84"/>
        <source>Accept changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="125"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="125"/>
        <source>Service URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="132"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="132"/>
        <source>Enter service url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="149"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="149"/>
        <source>Your token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="156"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="156"/>
        <source>Enter your token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="189"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="189"/>
        <source>Clear records management data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="212"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="212"/>
        <source>Service name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="222"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="222"/>
        <source>Service token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="234"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="234"/>
        <source>Service logo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="204"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="204"/>
        <source>Communication error. Cannot obtain records management info from server. Internet connection failed or service url and identification token can be wrong!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="119"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="119"/>
        <source>Please fill in service URL and your identification token. Click the &apos;%1&apos; button to log into records management service. You should receive valid service info. Finally, click the top right button to save the settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="242"/>
        <location filename="../../qml/pages/PageSettingsRecordsManagement.qml" line="242"/>
        <source>Records management logo</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageSettingsStorage</name>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="60"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="60"/>
        <source>Storage settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="82"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="82"/>
        <source>Number of days to keep messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="89"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="89"/>
        <source>don&apos;t delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="92"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="92"/>
        <source>Select the number of days how long downloaded messages are kept in local storage after download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="100"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="100"/>
        <source>Messages won&apos;t automatically be deleted from the local storage.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="102"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="102"/>
        <source>Messages will be locally stored for a period of %1 days since their acceptance time. By default they are not deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="108"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="108"/>
        <source>Number of days to keep attachments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="115"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="115"/>
        <source>like messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="118"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="118"/>
        <source>Select the number of days how long message content is kept in local storage after download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="126"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="126"/>
        <source>Attachments won&apos;t be deleted from the local storage as long as the corresponding messages won&apos;t be deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="128"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="128"/>
        <source>Attachments will be locally stored, but no longer than corresponding messages, for a period of %1 days since their download time. Default value corresponds with message settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="134"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="134"/>
        <source>ZFO storage size limit in MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="144"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="144"/>
        <source>Specify the maximal amount of memory for preserving recently downloaded messages.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="152"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="152"/>
        <source>Message ZFO data won&apos;t be automatically stored.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="154"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="154"/>
        <source>Maximum size of stored message ZFO data is set to %1 MB. Default is %2 MB.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="160"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="160"/>
        <source>Databases location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="173"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="173"/>
        <source>Change location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="189"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="189"/>
        <source>Set default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="207"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="207"/>
        <source>Clean up all databases</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="213"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="213"/>
        <source>Clean now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="223"/>
        <location filename="../../qml/pages/PageSettingsStorage.qml" line="223"/>
        <source>The action performs a clean-up in local databases in order to optimise their speed and size. Note: It may take a while to complete this action.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageSettingsSync</name>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="47"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="47"/>
        <source>Synchronization settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="69"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="69"/>
        <source>Download only newer messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="79"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="79"/>
        <source>Only messages which are not older than 90 days will be downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="80"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="80"/>
        <source>All available messages (including those in the data vault) will be downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="85"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="85"/>
        <source>Download complete messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="93"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="93"/>
        <source>Complete messages will be downloaded during account synchronisation. This may be slow.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="94"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="94"/>
        <source>Only message envelopes will be downloaded during account synchronisation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="99"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="99"/>
        <source>Synchronise accounts after start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="107"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="107"/>
        <source>Accounts will automatically be synchronised a few seconds after application start-up.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="108"/>
        <location filename="../../qml/pages/PageSettingsSync.qml" line="108"/>
        <source>Accounts will be synchronised only when the action is manually invoked.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="108"/>
        <source>FROM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="110"/>
        <source>TO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="112"/>
        <source>DELIVERED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="440"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="459"/>
        <source>Personal delivery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="243"/>
        <source>Databox info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="268"/>
        <location filename="../../src/sqlite/message_db.cpp" line="460"/>
        <location filename="../../src/sqlite/message_db.cpp" line="462"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="269"/>
        <location filename="../../src/sqlite/message_db.cpp" line="460"/>
        <location filename="../../src/sqlite/message_db.cpp" line="462"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="342"/>
        <source>Full control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="343"/>
        <source>Restricted control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="850"/>
        <source>Password of username &apos;%1&apos; expires on %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="852"/>
        <source>Password of username &apos;%1&apos; expired on %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="461"/>
        <source>Delivery by fiction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="475"/>
        <location filename="../../src/sqlite/message_db.cpp" line="488"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="464"/>
        <source>Message type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="478"/>
        <source>Message author</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="580"/>
        <source>Records Management Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="582"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="518"/>
        <source>Message state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="559"/>
        <source>Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="106"/>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="470"/>
        <source>Sender</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="483"/>
        <source>Recipient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="457"/>
        <source>Attachment size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="473"/>
        <location filename="../../src/sqlite/message_db.cpp" line="486"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="454"/>
        <source>Subject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/auxiliaries/email_helper.cpp" line="114"/>
        <source>This email has been generated with Datovka application based on a data message (%1) delivered to databox.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db.cpp" line="471"/>
        <location filename="../../src/sqlite/message_db.cpp" line="484"/>
        <source>Databox ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="245"/>
        <source>Data box application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="250"/>
        <source>ZFO file to be viewed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="881"/>
        <source>Last synchronisation: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="887"/>
        <source>Security problem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="888"/>
        <source>OpenSSL support is required!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="889"/>
        <source>The device does not support OpenSSL. The application won&apos;t work correctly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="318"/>
        <source>User info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db.cpp" line="287"/>
        <source>Owner info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="95"/>
        <source>Cannot open ZFO file &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="103"/>
        <source>Wrong message format of ZFO file &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="107"/>
        <source>Wrong message data of ZFO file &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="128"/>
        <source>Relevant account does not exist for ZFO file &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="134"/>
        <source>Import internal error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="159"/>
        <source>Cannot authenticate the ZFO file &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="164"/>
        <source>ZFO file &apos;%1&apos; is not authentic (returns ISDS)!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="175"/>
        <source>Cannot store the ZFO file &apos;%1&apos; to local database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="186"/>
        <source>ZFO file &apos;%1&apos; has been imported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/worker/task_import_zfo.cpp" line="194"/>
        <source>File has not been imported!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="62"/>
        <source>Message &apos;%1&apos; could not be uploaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="65"/>
        <source>Received error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="70"/>
        <source>File Upload Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="75"/>
        <source>Successful File Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="76"/>
        <source>Message &apos;%1&apos; was successfully uploaded into the records management service.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="78"/>
        <source>It can be now found in the records management service in these locations:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="650"/>
        <source>You can export files into iCloud or into device local storage.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/files.cpp" line="651"/>
        <source>Do you want to export files to Datovka iCloud container?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordsManagement</name>
    <message>
        <location filename="../../src/records_management.cpp" line="121"/>
        <source>Get service info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="137"/>
        <location filename="../../src/records_management.cpp" line="185"/>
        <location filename="../../src/records_management.cpp" line="412"/>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="143"/>
        <location filename="../../src/records_management.cpp" line="191"/>
        <location filename="../../src/records_management.cpp" line="418"/>
        <source>Communication error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="168"/>
        <source>Upload hierarchy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="211"/>
        <source>Sync service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="220"/>
        <source>Sync failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="367"/>
        <source>Update account &apos;%1&apos; (%2/%3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="370"/>
        <source>Update done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/records_management.cpp" line="400"/>
        <source>Upload message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SQLiteTbls</name>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="77"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="198"/>
        <source>Data box ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="78"/>
        <source>Data box type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="79"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="158"/>
        <source>Identification number (IČO)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="80"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="147"/>
        <source>Given name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="81"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="148"/>
        <source>Middle name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="82"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="149"/>
        <source>Surname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="83"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="150"/>
        <source>Surname at birth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="84"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="159"/>
        <source>Company name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="85"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="157"/>
        <source>Date of birth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="86"/>
        <source>City of birth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="87"/>
        <source>County of birth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="88"/>
        <source>State of birth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="89"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="161"/>
        <source>City of residence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="90"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="160"/>
        <source>Street of residence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="91"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="153"/>
        <source>Number in street</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="92"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="154"/>
        <source>Number in municipality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="93"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="155"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="162"/>
        <source>Zip code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="94"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="163"/>
        <source>State of residence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="95"/>
        <source>Nationality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="98"/>
        <source>Databox state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="99"/>
        <source>Effective OVM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="100"/>
        <source>Open addressing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="145"/>
        <source>User type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="146"/>
        <source>Permissions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="151"/>
        <source>City</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="152"/>
        <source>Street</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="156"/>
        <source>State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="164"/>
        <source>Password expiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="199"/>
        <source>Long term storage type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="200"/>
        <source>Long term storage capacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="201"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="206"/>
        <source>Active from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="202"/>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="207"/>
        <source>Active to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="203"/>
        <source>Capacity used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="204"/>
        <source>Future long term storage type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="205"/>
        <source>Future long term storage capacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/account_db_tables.cpp" line="208"/>
        <source>Payment state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/file_db_tables.cpp" line="67"/>
        <source>File name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/file_db_tables.cpp" line="70"/>
        <source>Mime type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="92"/>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="94"/>
        <source>Sender</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="95"/>
        <source>Sender address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="97"/>
        <source>Recipient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="98"/>
        <source>Recipient address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="105"/>
        <source>To hands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="106"/>
        <source>Subject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="107"/>
        <source>Your reference number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="108"/>
        <source>Our reference number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="109"/>
        <source>Your file mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="110"/>
        <source>Our file mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="111"/>
        <source>Law</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="112"/>
        <source>Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="113"/>
        <source>Section</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="114"/>
        <source>Paragraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="115"/>
        <source>Letter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="119"/>
        <source>Delivered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="120"/>
        <source>Accepted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="121"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlite/message_db_tables.cpp" line="122"/>
        <source>Attachment size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/io/prefs_db_tables.cpp" line="55"/>
        <source>Preference Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/io/prefs_db_tables.cpp" line="56"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/io/prefs_db_tables.cpp" line="57"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Service</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="175"/>
        <source>Cannot access date field.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="184"/>
        <source>The field &apos;%1&apos; contains an invalid date &apos;%2&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="204"/>
        <source>Cannot access IČO field.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="213"/>
        <source>The field &apos;%1&apos; contains an invalid value &apos;%2&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="233"/>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="261"/>
        <source>Cannot access string.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="241"/>
        <location filename="../../src/datovka_shared/gov_services/service/gov_service.cpp" line="269"/>
        <source>The field &apos;%1&apos; contains no value.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Session</name>
    <message>
        <location filename="../../src/isds/session/isds_session.cpp" line="183"/>
        <source>Cannot open certificate &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/session/isds_session.cpp" line="209"/>
        <source>Cannot parse certificate &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/session/isds_session.cpp" line="217"/>
        <source>Unknown format of certificate &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowPasswordOverlaidImage</name>
    <message>
        <location filename="../../qml/components/ShowPasswordOverlaidImage.qml" line="89"/>
        <location filename="../../qml/components/ShowPasswordOverlaidImage.qml" line="89"/>
        <source>Hide password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/components/ShowPasswordOverlaidImage.qml" line="89"/>
        <location filename="../../qml/components/ShowPasswordOverlaidImage.qml" line="89"/>
        <source>Show password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SrvcMvCrrVbh</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_crr_vbh.cpp" line="152"/>
        <source>Printout from the driver penalty point system</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SrvcMvIrVp</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_ir_vp.cpp" line="545"/>
        <source>Printout from the insolvency register</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SrvcMvRtVt</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rt_vt.cpp" line="175"/>
        <source>Printout from the criminal records</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SrvcMvRtpoVt</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_rtpo_vt.cpp" line="550"/>
        <source>Printout from the criminal records of legal persons</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SrvcMvSkdVp</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_skd_vp.cpp" line="546"/>
        <source>Printout from the list of qualified suppliers</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SrvcMvVrVp</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_vr_vp.cpp" line="579"/>
        <source>Printout from the public register</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SrvcMvZrVp</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_mv_zr_vp.cpp" line="545"/>
        <source>Printout from the company register</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SrvcSzrRobVu</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vu.cpp" line="94"/>
        <source>Printout from the resident register</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SrvcSzrRobVvu</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="134"/>
        <source>Printout about the usage of entries from the resident register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="234"/>
        <source>The date of start cannot be later than the date of end.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SrvcSzrRosVv</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_ros_vv.cpp" line="131"/>
        <source>Public printout from the person register</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SzrRobVvuData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="93"/>
        <source>From</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="94"/>
        <source>Select start date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="105"/>
        <source>Select end date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_rob_vvu.cpp" line="104"/>
        <source>To</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SzrRosVvData</name>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_ros_vv.cpp" line="102"/>
        <source>Identification number (IČO)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/datovka_shared/gov_services/service/gov_szr_ros_vv.cpp" line="103"/>
        <source>Enter identification number (IČO)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TaskDownloadMessageList</name>
    <message>
        <location filename="../../src/worker/task_download_message_list.cpp" line="160"/>
        <source>%1: No new messages.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tasks</name>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="76"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="77"/>
        <source>Wrong username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="82"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="390"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="399"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="458"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="495"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="502"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="82"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="391"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="400"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="459"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="503"/>
        <source>Internal error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="110"/>
        <source>Change password error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="111"/>
        <source>Failed to change password for username &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="112"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="118"/>
        <source>ISDS returns: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="116"/>
        <source>Change password: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="117"/>
        <source>Password for username &apos;%1&apos; has changed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="313"/>
        <source>ZFO import is running... Wait until import will be finished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="390"/>
        <source>Cannot access data box model.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="399"/>
        <source>Cannot access attachment model.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="412"/>
        <location filename="../../src/isds/isds_tasks.cpp" line="420"/>
        <source>Error sending message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="413"/>
        <source>Pre-paid transfer charges for message reply have been enabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="414"/>
        <source>The sender reference number must be specified in the additional section in order to send the message.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="421"/>
        <source>Message uses the offered payment of transfer charges by the recipient.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="422"/>
        <source>The recipient reference number must be specified in the additional section in order to send the message.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="458"/>
        <source>Cannot access file database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="495"/>
        <source>Cannot load file content from path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/isds/isds_tasks.cpp" line="502"/>
        <source>No file to send.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextLineItem</name>
    <message>
        <location filename="../../qml/components/TextLineItem.qml" line="82"/>
        <location filename="../../qml/components/TextLineItem.qml" line="82"/>
        <source>Input text action button.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TimedPasswordLine</name>
    <message>
        <location filename="../../qml/components/TimedPasswordLine.qml" line="36"/>
        <location filename="../../qml/components/TimedPasswordLine.qml" line="36"/>
        <source>Enter the password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Utility</name>
    <message>
        <location filename="../../src/datovka_shared/utility/strings.cpp" line="59"/>
        <source>unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WidgetInputDialogue</name>
    <message>
        <location filename="../../src/dialogues/widget_input_dialogue.cpp" line="127"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../../qml/main.qml" line="283"/>
        <location filename="../../qml/main.qml" line="283"/>
        <source>Wrong PIN code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="310"/>
        <location filename="../../qml/main.qml" line="310"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="263"/>
        <location filename="../../qml/main.qml" line="263"/>
        <source>Enter PIN code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="182"/>
        <location filename="../../qml/main.qml" line="182"/>
        <source>Mobile Key Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="183"/>
        <location filename="../../qml/main.qml" line="183"/>
        <source>Waiting for acknowledgement from the Mobile key application for the account &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qml/main.qml" line="286"/>
        <location filename="../../qml/main.qml" line="286"/>
        <source>Enter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
