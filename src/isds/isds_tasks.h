/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS */

#include "src/datovka_shared/isds/box_interface.h"
#include "src/datovka_shared/isds/box_interface2.h"
#include "src/messages.h"

class AccountListModel; /* Forward declaration. */
class AcntId; /* Forward declaration. */
class DataboxListModel; /* Forward declaration. */
class FileListModel; /* Forward declaration. */

namespace Isds {

	class Tasks {
		Q_DECLARE_TR_FUNCTIONS(Tasks)

	public:
		/*!
		 * @brief Change ISDS login password.
		 *
		 * @param[in] acntId Account identifier.
		 * @param[in] oldPwd Current/old password string.
		 * @param[in] newPwd New password string.
		 * @param[in] otpCode OTP code string (may be null).
		 * @return True if password was changed.
		 */
		static
		bool changePassword(const AcntId &acntId, const QString &oldPwd,
		    const QString &newPwd, const QString &otpCode);

		/*!
		 * @brief Downloads complete message.
		 *
		 * @param[in] acntId Account identifier.
		 * @param[in] messageType Message orientation.
		 * @param[in] msgId Message ID.
		 */
		static
		void downloadMessage(const AcntId &acntId,
		    enum Messages::MessageType messageType, qint64 msgId);

		/*!
		 * @brief Find data box and return its detail info string.
		 *
		 * @param[in] acntId Account identifier.
		 * @param[in] dbID Data box ID.
		 * @param[in] dbType Data box Type.
		 * @return Data box detail info string.
		 */
		static
		QString findDatabox(const AcntId &acntId, const QString &dbID,
		    enum Type::DbType dbType);

		/*!
		 * @brief Full-text data-box search based on search criteria.
		 *
		 * @param[in] acntId Account identifier.
		 * @param[in] phrase World to be found.
		 * @param[in] searchType Data box field specification for search.
		 * @param[in] searchScope Data box type search restriction.
		 * @param[in] page Page number to be shown.
		 * @param[out] dbList List of results.
		 * @param[out] totalCount Total number of data boxes.
		 * @param[out] currentCount Number of data boxes on the current page.
		 * @param[out] position Page number.
		 * @param[out] lastPage True if search result are last page.
		 * @param[out] lastError last error description.
		 * @return True if success.
		 */
		static
		bool findDataboxFulltext(const AcntId &acntId,
		    const QString &phrase,
		    enum Type::FulltextSearchType searchType,
		    enum Type::DbType searchScope, long int page,
		    QList<Isds::DbResult2> &dbList, long int &totalCount,
		    long int &currentCount, long int &position, bool &lastPage,
		    QString &lastError);

		/*!
		 * @brief Get data box ID from ISDS.
		 *
		 * @param[in] acntId Account identifier.
		 * @return Return data box ID.
		 */
		static
		QString getAccountDbId(const AcntId &acntId);

		/*!
		 * @brief Download account info.
		 *
		 * @param[in] acntId Account identifier.
		 */
		static
		void getAccountInfo(const AcntId &acntId);

		/*!
		 * @brief Download long term storage info.
		 *
		 * @param[in] acntId Account identifier.
		 * @param[in] dbID Data box id.
		 */
		static
		void downloadDTInfo(const AcntId &acntId, const QString &dbID);

		/*!
		 * @brief Downloads delivery info.
		 *
		 * Note: Task is not use now. But may be use in future.
		 *
		 * @param[in] acntId Account identifier.
		 * @param[in] msgId Message ID.
		 */
		static
		void getDeliveryInfo(const AcntId &acntId, qint64 msgId);

		/*!
		 * @brief Import ZFO files to local database.
		 *
		 * @param[in] acntIdList List of account identifiers.
		 * @param[in] filePathList File list or dir path.
		 * @param[in] authenticate True if message must be authenticate.
		 * @return Info/error text.
		 */
		static
		QString importZfoMessages(const QList<AcntId> &acntIdList,
		    const QStringList &filePathList, bool authenticate);

		/*!
		 * @brief Add recipient and set pdz payment methods.
		 *
		 * @param[in] acntId Account identifier.
		 * @param[in] dbID Recipient data box id.
		 * @param[in] senderOvm True if sender is OVM.
		 * @param[in] canUseInitReply True if can use reply
		 *                            on initiatory message.
		 * @param[in] pdzInfos List of pdz info.
		 */
		static
		void recipientInfo(const AcntId &acntId, const QString &dbID,
		    bool senderOvm, bool canUseInitReply,
		    const QList<Isds::PDZInfoRec> &pdzInfos);

		/*!
		 * @brief Sent GOV request.
		 *
		 * @param[in] acntId Account identifier.
		 * @param[in] msg Gov message.
		 * @param[in,out] transactIds Send transaction ids.
		 * @return True if message was sent.
		 */
		static
		bool sendGovRequest(const AcntId &acntId,
		    const Isds::Message &msg, QSet<QString> &transactIds);

		/*!
		 * @brief Send message.
		 *
		 * @param[in] acntId Account identifier.
		 * @param[in] dmID Message ID.
		 * @param[in] dmAnnotation Message subject string.
		 * @param[in] databoxModel Recipient data box model.
		 * @param[in] attachModel Attachment model.
		 * @param[in] dmLegalTitleLaw Mandate Law string.
		 * @param[in] dmLegalTitleYear Mandate Year string.
		 * @param[in] dmLegalTitleSect Mandate Section string.
		 * @param[in] dmLegalTitlePar Mandate Paragraph string.
		 * @param[in] dmLegalTitlePoint Mandate Letter string.
		 * @param[in] dmToHands Mesasge to hands string.
		 * @param[in] dmRecipientRefNumber Your ref. number string.
		 * @param[in] dmRecipientIdent Your identification string.
		 * @param[in] dmSenderRefNumber Our ref. number string.
		 * @param[in] dmSenderIdent Our identification string.
		 * @param[in] dmOVM True if send message as OVM.
		 * @param[in] dmPublishOwnID True if add sender name to message.
		 * @param[in] dmAllowSubstDelivery True if allow acceptance by fiction.
		 * @param[in] dmPersonalDelivery True if personal delivery.
		 * @param[in,out] transactIds Send transaction ids.
		 */
		static
		void sendMessage(const AcntId &acntId, qint64 dmID,
		    const QString &dmAnnotation, const DataboxListModel *databoxModel,
		    const FileListModel *attachModel,
		    const QString &dmLegalTitleLaw, const QString &dmLegalTitleYear,
		    const QString &dmLegalTitleSect, const QString &dmLegalTitlePar,
		    const QString &dmLegalTitlePoint, const QString &dmToHands,
		    const QString &dmRecipientRefNumber, const QString &dmRecipientIdent,
		    const QString &dmSenderRefNumber, const QString &dmSenderIdent,
		    bool dmOVM, bool dmPublishOwnID, bool dmAllowSubstDelivery,
		    bool dmPersonalDelivery, QSet<QString> &transactIds);

		/*!
		 * @brief Sent SMS request.
		 *
		 * @param[in] acntId Account identifier.
		 * @param[in] oldPwd Current/old password string.
		 * @return True if SMS was sent.
		 */
		static
		bool sendSMS(const AcntId &acntId, const QString &oldPwd);

		/*!
		 * @brief Download received or sent message list of one account.
		 *
		 * @param[in] acntId Account identifier.
		 * @param[in] msgDirect Message orientation.
		 */
		static
		void syncSingleAccount(const AcntId &acntId,
		    enum Messages::MessageType msgDirect);

		/*!
		 * @brief Download pdz info.
		 *
		 * @param[in] acntId Account identifier.
		 * @param[in] dbID Data box id.
		 */
		static
		void downloadPdzInfo(const AcntId &acntId, const QString &dbID);

		/*!
		 * @brief Download credit event information.
		 *
		 * @param[in]  acntId Account identifier.
		 * @param[in]  dbId Data box identifier.
		 * @param[in]  fromDate From date.
		 * @param[in]  toDate To date.
		 * @param[out] currentCredit Remaining credit in CZK.
		 * @param[out] notifEmail Notification email address.
		 * @return Credit event list.
		 */
		static
		QList<Isds::CreditEvent> downloadCreditInfo(const AcntId &acntId,
		    const QString &dbID, const QDate &fromDate,
		    const QDate &toDate, qint64 &currentCredit,
		    QString &notifEmail);
	};
}
