/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/isds/services/helper.h"

enum Isds::Type::Error Isds::errCode2Error(enum Connection::ErrCode errCode) {
	switch (errCode) {
	case Connection::ERR_NO_ERROR:
		return Type::ERR_SUCCESS;
		break;
	case Connection::ERR_NO_CONNECTION:
		return Type::ERR_NETWORK;
		break;
	case Connection::ERR_REPLY:
		return Type::ERR_SOAP;
		break;
	case Connection::ERR_SERVER_UNAVAILABLE:
		return Type::ERR_NETWORK;
		break;
	case Connection::ERR_TIMEOUT:
		return Type::ERR_TIMED_OUT;
		break;
	case Connection::ERR_BAD_REQUEST:
	case Connection::ERR_UNAUTHORIZED:
	case Connection::ERR_UNAUTHORIZED_HOTP:
	case Connection::ERR_UNAUTHORIZED_TOTP:
	case Connection::ERR_UNAUTHORIZED_TOTP_SMS:
		return Type::ERR_NOT_LOGGED_IN;
		break;
	case Connection::ERR_UNSPECIFIED:
		return Type::ERR_ERROR;
		break;
	default:
		Q_ASSERT(0);
		return Type::ERR_ERROR;
		break;
	}
}
