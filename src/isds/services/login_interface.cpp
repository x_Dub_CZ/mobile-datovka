/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <utility> /* ::std::move */

#include "src/isds/conversion/isds_conversion.h"
#include "src/isds/io/connection.h"
#include "src/isds/isds_const.h"
#include "src/isds/services/login_interface.h"
#include "src/isds/services/helper.h" /* errCode2Error() */
#include "src/isds/session/isds_session.h"
#include "src/isds/xml/login_interface.h"
#include "src/isds/xml/message_interface.h"
#include "src/isds/xml/response_status.h"

#ifdef Q_COMPILER_RVALUE_REFS
#  define macroStdMove(x) ::std::move(x)
#else /* Q_COMPILER_RVALUE_REFS */
#  define macroStdMove(x) (x)
#endif /* Q_COMPILER_RVALUE_REFS */

enum Isds::Type::Error Isds::changePasswordOTP(Session &session,
    enum Type::OtpMethod otpMethod, const QString &oldPwd,
    const QString &newPwd, DbStatus *status, QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(session,
	    OTP_CHNG_PWD_SERVICE,
	    Xml::soapRequestChangePasswordOTP(otpMethod, oldPwd, newPwd),
	    response);
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DbStatus dbStatus = Xml::toDbStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dbStatus.message();
	}
	iOk = dbStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dbStatus);
	}
	/* Cannot use dbStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::changeISDSPassword(Session &session,
    const QString &oldPwd, const QString &newPwd, DbStatus *status,
    QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(session,
	    DB_SERVICE, Xml::soapRequestChangeISDSPassword(oldPwd, newPwd),
	    response);
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DbStatus dbStatus = Xml::toDbStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dbStatus.message();
	}
	iOk = dbStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dbStatus);
	}
	/* Cannot use dbStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}
	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::sendSMSCode(Session &session, DbStatus *status,
    QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(session,
	    OTP_CHNG_PWD_SERVICE, Xml::soapRequestSendSmsCode(), response);
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DbStatus dbStatus = Xml::toDbStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dbStatus.message();
	}
	iOk = dbStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dbStatus);
	}
	/* Cannot use dbStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}
	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::loginOperation(Session &session,
    const QString &soapEndPoint, DmStatus *status, QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(session,
	    soapEndPoint, Xml::soapRequestDummyOperation(), response);
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DmStatus dmStatus = Xml::toDmStatus(response, &iOk);
	/* For some login requests can be status empty. Like 302 redirect. */
	if (dmStatus.isNull()) {
		return Type::ERR_SUCCESS;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dmStatus.message();
	}
	iOk = dmStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dmStatus);
	}
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}
	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::logoutMepOtp(Session &session)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(session,
	    LOGIN_SERVICE, QByteArray(), response);
	if (errCode != Connection::ERR_NO_ERROR) {
		return errCode2Error(errCode);
	}
	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::waitForMep(Session &session,
    enum Type::MepResolution &rsCode)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(session,
	    QByteArray(), QByteArray(), response);
	if (errCode != Connection::ERR_NO_ERROR) {
		return errCode2Error(errCode);
	}

	bool iOk = false;
	int statusNum = response.toInt(&iOk);
	if (Q_UNLIKELY(!iOk)) {
		Q_ASSERT(0);
		return Type::ERR_ERROR;
	}
	rsCode = IsdsConversion::isdsMepStatusToMepResolution(statusNum);
	return Type::ERR_SUCCESS;
}
