/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDomDocument>

#include "src/isds/services/message_interface_offline.h"
#include "src/isds/xml/cms.h"
#include "src/isds/xml/helper.h"
#include "src/isds/xml/message_interface.h"

#ifdef Q_COMPILER_RVALUE_REFS
#  define macroStdMove(x) ::std::move(x)
#else /* Q_COMPILER_RVALUE_REFS */
#  define macroStdMove(x) (x)
#endif /* Q_COMPILER_RVALUE_REFS */

/*!
 * @brief Read DOM from data.
 *
 * @param[in]  data Message data. May also be CMS data.
 * @param[out] dom DOM document obtained from data.
 * @param[out] rawType Determined raw data type.
 * @return True on success, false on error.
 */
static
bool getDom(const QByteArray &data, QDomDocument &dom,
    enum Isds::Type::RawType &rawType)
{
	const QByteArray *dataPtr = &data;
	QByteArray cmsData = Isds::Xml::cmsContent(data);
	if (!cmsData.isNull()) {
		/* Probably CMS data. */
		dataPtr = &cmsData;
	}

	bool iOk = false;
	dom = Isds::Xml::toDomDocument(*dataPtr, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		rawType = Isds::Type::RT_UNKNOWN;
		return false;
	}

	cmsData.clear(); /* Save some memory. */

	rawType = Isds::Xml::guessRawType(dom);
	if (Q_UNLIKELY(rawType == Isds::Type::RT_UNKNOWN)) {
		return false;
	}
	if (dataPtr == &cmsData) {
		switch (rawType) {
		case Isds::Type::RT_PLAIN_SIGNED_INCOMING_MESSAGE:
			rawType = Isds::Type::RT_CMS_SIGNED_INCOMING_MESSAGE;
			break;
		case Isds::Type::RT_PLAIN_SIGNED_OUTGOING_MESSAGE:
			rawType = Isds::Type::RT_CMS_SIGNED_OUTGOING_MESSAGE;
			break;
		case Isds::Type::RT_PLAIN_SIGNED_DELIVERYINFO:
			rawType= Isds::Type::RT_CMS_SIGNED_DELIVERYINFO;
			break;
		default:
			/* Do nothing. */
			break;
		}
	}

	return true;
}

/*!
 * @brief Set message content except for raw data.
 *
 * @param[in]  dom DOM data.
 * @param[in]  rawType Raw data type.
 * @param[out] message Message to be updated.
 * @return True on success, false on error.
 */
static
bool setMessageExceptRaw(const QDomDocument &dom,
    enum Isds::Type::RawType rawType, Isds::Message &message)
{
	bool iOk = false;

	message.setRawType(rawType);

	message.setEnvelope(Isds::Xml::readEnvelope(dom, &iOk));
	if (Q_UNLIKELY(!iOk)) {
		return false;
	}

	switch (rawType) {
	case Isds::Type::RT_PLAIN_SIGNED_INCOMING_MESSAGE:
	case Isds::Type::RT_CMS_SIGNED_INCOMING_MESSAGE:
	case Isds::Type::RT_PLAIN_SIGNED_OUTGOING_MESSAGE:
	case Isds::Type::RT_CMS_SIGNED_OUTGOING_MESSAGE:
		message.setDocuments(Isds::Xml::readDocuments(dom, &iOk));
		if (Q_UNLIKELY(!iOk)) {
			return false;
		}
		break;
	default:
		/* Ignore other. */
		break;
	}

	return true;
}

Isds::Message Isds::toMessage(const QByteArray &data, bool *ok)
{
	if (Q_UNLIKELY(data.isEmpty())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return Message();
	}

	QDomDocument dom;
	enum Type::RawType rawType = Type::RT_UNKNOWN;
	Message message;

	if (Q_UNLIKELY(!getDom(data, dom, rawType))) {
		goto fail;
	}

	message.setRaw(data);

	if (Q_UNLIKELY(!setMessageExceptRaw(dom, rawType, message))) {
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return message;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return Message();
}

Isds::Message Isds::toMessage(QByteArray &&data, bool *ok)
{
	if (Q_UNLIKELY(data.isEmpty())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return Message();
	}

	QDomDocument dom;
	enum Type::RawType rawType = Type::RT_UNKNOWN;
	Message message;

	if (Q_UNLIKELY(!getDom(data, dom, rawType))) {
		goto fail;
	}

	message.setRaw(macroStdMove(data));

	if (Q_UNLIKELY(!setMessageExceptRaw(dom, rawType, message))) {
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return message;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return Message();
}
