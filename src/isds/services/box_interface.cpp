/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <utility> /* ::std::move */

#include "src/isds/io/connection.h"
#include "src/isds/isds_const.h"
#include "src/isds/services/box_interface.h"
#include "src/isds/services/helper.h" /* errCode2Error() */
#include "src/isds/session/isds_session.h"
#include "src/isds/xml/box_interface.h"
#include "src/isds/xml/response_status.h"

#ifdef Q_COMPILER_RVALUE_REFS
#  define macroStdMove(x) ::std::move(x)
#else /* Q_COMPILER_RVALUE_REFS */
#  define macroStdMove(x) (x)
#endif /* Q_COMPILER_RVALUE_REFS */

enum Isds::Type::Error Isds::getPasswordInfo(Session &session,
     QDateTime &expiration, DbStatus *status, QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(
	    session, DB_SERVICE, Xml::soapRequestGetPasswordInfo(), response);
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DbStatus dbStatus = Xml::toDbStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dbStatus.message();
	}
	iOk = dbStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dbStatus);
	}
	/* Cannot use dbStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	/* Read user password info content. */
	expiration = Xml::readPasswordInfoExpiration(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::getOwnerInfo2(Session &session,
    DbOwnerInfoExt2 &dbOwnerInfo, DbStatus *status, QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(
	    session, DB_SERVICE, Xml::soapRequestGetOwnerInfoFromLogin2(),
	    response);
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DbStatus dbStatus = Xml::toDbStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dbStatus.message();
	}
	iOk = dbStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dbStatus);
	}
	/* Cannot use dbStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	/* Read owner info content. */
	dbOwnerInfo = Xml::toOwnerInfo(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::getUserInfo2(Session &session,
    DbUserInfoExt2 &dbUserInfo, DbStatus *status, QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(
	    session, DB_SERVICE, Xml::soapRequestGetUserInfoFromLogin2(),
	    response);
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DbStatus dbStatus = Xml::toDbStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dbStatus.message();
	}
	iOk = dbStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dbStatus);
	}
	/* Cannot use dbStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	/* Read user info content. */
	dbUserInfo = Xml::toUserInfoExt2(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::findDatabox2(Session &session,
    const DbOwnerInfoExt2 &criteria, QList<DbOwnerInfoExt2> &foundBoxes,
    DbStatus *status, QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(
	    session, FIND_SERVICE, Xml::soapRequestFindDataBox2(criteria),
	    response);
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DbStatus dbStatus = Xml::toDbStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dbStatus.message();
	}
	iOk = dbStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dbStatus);
	}
	/* Cannot use dbStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	/*
	 * Read owner info content.
	 * FIXME -- Using this function here is a crude hack. List may be empty
	 * in which case the function fails.
	 */
	DbOwnerInfoExt2 found = Xml::toOwnerInfo(response, &iOk);
	if (iOk) {
		foundBoxes.append(found);
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::isdsSearch3(Session &session,
    const QString &query, enum Type::FulltextSearchType searchType,
    enum Type::DbType boxType, long int page, long int pageSize,
    enum Type::NilBool highlighting, QList<DbResult2> &foundBoxes,
    long int &totalCount, long int &currentCount, long int &position,
    bool &lastPage, DbStatus *status, QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(
	    session, FIND_SERVICE, Xml::soapRequestIsdsSearch3(query, searchType,
	        boxType, page, pageSize, highlighting), response);
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DbStatus dbStatus = Xml::toDbStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dbStatus.message();
	}
	iOk = dbStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dbStatus);
	}
	/* Cannot use dbStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	/* Read data box list content and other search results. */
	foundBoxes = Xml::toDbResult2(response, totalCount, currentCount,
	    position, lastPage, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::dtInfo(Session &session, const QString &dbID,
    DTInfoOutput &dtInfo, DbStatus *status, QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(
	    session, DB_SERVICE, Xml::soapRequestDTInfo(dbID), response);
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DbStatus dbStatus = Xml::toDbStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dbStatus.message();
	}
	iOk = dbStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dbStatus);
	}
	/* Cannot use dbStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	/* Read long term storage info content. */
	dtInfo = Xml::toDTInfo(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::pdzInfo(Session &session, const QString &dbID,
    QList<PDZInfoRec> &pdzInfoRecs, DbStatus *status, QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(
	    session, DB_SERVICE, Xml::soapRequestPdzInfo(dbID), response);
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DbStatus dbStatus = Xml::toDbStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dbStatus.message();
	}
	iOk = dbStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dbStatus);
	}
	/* Cannot use dbStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	/* Read pdz info content. */
	pdzInfoRecs = Xml::toPdzInfoRecs(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::pdzSendInfo(Session &session, const QString &dbID,
    enum Type::PdzMessageType type, bool &canSend, DbStatus *status,
    QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(
	    session, DB_SERVICE, Xml::soapRequestPdzSendInfo(dbID, type),
	    response);
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DbStatus dbStatus = Xml::toDbStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dbStatus.message();
	}
	iOk = dbStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dbStatus);
	}
	/* Cannot use dbStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	canSend = Xml::readPdzSendInfo(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::dataBoxCreditInfo(Session &session,
    const QString &dbID, const QDate &fromDate, const QDate &toDate,
    qint64 &currentCredit, QString &notifEmail, QList<CreditEvent> &history,
    DbStatus *status, QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(
	    session, DB_SERVICE, Xml::soapRequestDataBoxCerditInfo(dbID,
	    fromDate, toDate), response);
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DbStatus dbStatus = Xml::toDbStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dbStatus.message();
	}
	iOk = dbStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dbStatus);
	}
	/* Cannot use dbStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	/* Read data box credit info content. */
	history = Xml::toDataBoxCreditInfo(response, currentCredit,
	    notifEmail, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}

	return Type::ERR_SUCCESS;
}
