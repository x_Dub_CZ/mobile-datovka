/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include "src/datovka_shared/isds/message_interface.h"
#include "src/datovka_shared/isds/types.h"

class QDateTime; /* Forward declaration. */
class QString; /* Forward declaration. */

namespace Isds {

	class DmStatus; /* Forward declaration. */
	class Session; /* Forward declaration. */

	/*!
	 * @brief Download list of received messages using <GetListOfReceivedMessages>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     fromTime Delivery time window start.
	 * @param[in]     toTime Delivery time window end.
	 * @param[in]     recipientOrgUnitNum Recipient organisation unit number.
	 *                                    Use 0 or negative to indicates empty value.
	 *                                    At the moment the value has no effect.
	 * @param[in]     statusFilter Bit mask determining requested message types.
	 * @param[in]     offset Sequence number of the first requested record,
	 *                       starts from 1. Use 0 to ignore.
	 * @param[in]     limit Number of requested records.
	 *                      Use 0 to indicate default which is 1000.
	 *                      Values greater than 1000 are acceptable.
	 * @param[out]    envelopes List of received message envelopes.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error getListOfReceivedMessages(Session &session,
	    const QDateTime &fromTime, const QDateTime &toTime,
	    long int recipientOrgUnitNum, Type::DmFiltStates statusFilter,
	    unsigned long int offset, unsigned long int limit,
	    QList<Envelope> &envelopes, DmStatus *status = Q_NULLPTR,
	    QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Download list of sent messages using <GetListOfSentMessages>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     fromTime Delivery time window start.
	 * @param[in]     toTime Delivery time window end.
	 * @param[in]     senderOrgUnitNum Sender organisation unit number.
	 *                                 Use 0 or negative to indicates empty value.
	 *                                 At the moment the value has no effect.
	 * @param[in]     statusFilter Bit mask determining requested message types.
	 * @param[in]     offset Sequence number of the first requested record,
	 *                       starts from 1. Use 0 or negative to ignore.
	 * @param[in]     limit Number of requested records.
	 *                      Use 0 or negative to indicate default which is 1000.
	 *                      Values greater than 1000 are acceptable.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    envelopes List of sent message envelopes.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error getListOfSentMessages(Session &session,
	    const QDateTime &fromTime, const QDateTime &toTime,
	    long int senderOrgUnitNum, Type::DmFiltStates statusFilter,
	    unsigned long int offset, unsigned long int limit,
	    QList<Envelope> &envelopes, DmStatus *status = Q_NULLPTR,
	    QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Download signed delivery info using <GetSignedDeliveryInfo>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     dmId Message identifier.
	 * @param[out]    message Downloaded message.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error getSignedDeliveryInfo(Session &session, qint64 dmId,
	    Message &message, DmStatus *status = Q_NULLPTR,
	    QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Download signed received message using <SignedMessageDownload>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     dmId Message identifier.
	 * @param[out]    message Downloaded message.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error getSignedReceivedMessage(Session &session, qint64 dmId,
	    Message &message, DmStatus *status = Q_NULLPTR,
	    QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Download signed sent message using <SignedSentMessageDownload>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     dmId Message identifier.
	 * @param[out]    message Downloaded message.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error getSignedSentMessage(Session &session, qint64 dmId,
	    Message &message, DmStatus *status = Q_NULLPTR,
	    QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Send message using <CreateMessage>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in,out] message Message to be sent. The value of dmAnnotation,
	 *                        dbIDRecipient and at least one attachment with
	 *                        FMT_MAIN must be set.
	 *                        The dmID value is set upon successful sending.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error sendMessage(Session &session, Message &message,
	    DmStatus *status = Q_NULLPTR, QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Authenticate message using <AuthenticateMessage>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     rawData Message or delivery info in raw binary format.
	 * @param[out]    valid True if is valid.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error authenticateMessage(Session &session,
	    const QByteArray &rawData, bool &valid, DmStatus *status = Q_NULLPTR,
	    QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Download message author info using <GetMessageAuthor>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     dmId Message identifier.
	 * @param[out]    userType Sender type.
	 * @param[out]    authorName Author name.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error getMessageAuthorInfo(Session &session, qint64 dmId,
	    enum Type::SenderType &userType, QString &authorName,
	    DmStatus *status = Q_NULLPTR, QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Mark message as downloaded using <MarkMessageAsDownloaded>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[in]     dmId Message identifier.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error markMessageAsDownloaded(Session &session,
	    qint64 dmId, DmStatus *status = Q_NULLPTR,
	    QString *errMsg = Q_NULLPTR);

	/*!
	 * @brief Ping ISDS using <DummyOperation>.
	 *
	 * @param[in,out] session Communication session.
	 * @param[out]    status Status as reported by the ISDS.
	 * @param[out]    errMsg Error message.
	 * @return Error status.
	 */
	enum Type::Error dummyOperation(Session &session,
	    DmStatus *status = Q_NULLPTR, QString *errMsg = Q_NULLPTR);
}
