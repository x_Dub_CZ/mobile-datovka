/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <utility> /* ::std::move */

#include "src/isds/io/connection.h"
#include "src/isds/isds_const.h"
#include "src/isds/services/helper.h" /* errCode2Error() */
#include "src/isds/services/message_interface.h"
#include "src/isds/services/message_interface_offline.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/xml/message_interface.h"
#include "src/isds/xml/response_status.h"
#include "src/isds/xml/services_message.h"

#ifdef Q_COMPILER_RVALUE_REFS
#  define macroStdMove(x) ::std::move(x)
#else /* Q_COMPILER_RVALUE_REFS */
#  define macroStdMove(x) (x)
#endif /* Q_COMPILER_RVALUE_REFS */

/*!
 * @brief Download list of sent messages using <GetListOfReceivedMessages> or
 *     <GetListOfSentMessages>.
 *
 * @param[in]     rml Determines request type.
 * @param[in,out] session Communication session.
 * @param[in]     fromTime Delivery time window start.
 * @param[in]     toTime Delivery time window end.
 * @param[in]     orgUnitNum Recipient or sender organisation unit number.
 *                           Use 0 or negative to indicates empty value.
 *                           At the moment the value has no effect.
 * @param[in]     statusFilter Bit mask determining requested message types.
 * @param[in]     offset Sequence number of the first requested record,
 *                       starts from 1. Use 0 to ignore.
 * @param[in]     limit Number of requested records.
 *                      Use 0 to indicate default which is 1000.
 *                      Values greater than 1000 are acceptable.
 * @param[out]    envelopes List of message envelopes.
 * @param[out]    status Status as reported by the ISDS.
 * @param[out]    errMsg Error message.
 * @return Error status.
 */
static
enum Isds::Type::Error getListOfMessages(enum Isds::Xml::RequestMessageList rml,
    Isds::Session &session, const QDateTime &fromTime, const QDateTime &toTime,
    long int orgUnitNum, Isds::Type::DmFiltStates statusFilter,
    unsigned long int offset, unsigned long int limit,
    QList<Isds::Envelope> &envelopes, Isds::DmStatus *status, QString *errMsg)
{
	QByteArray response;
	enum Isds::Connection::ErrCode errCode = Isds::Connection::communicate(
	    session, MESSAGE_SERVICE,
	    Isds::Xml::soapRequestMessageList(rml, fromTime, toTime, orgUnitNum,
	        statusFilter, offset, limit),
	    response);
	if (errCode != Isds::Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	Isds::DmStatus dmStatus = Isds::Xml::toDmStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Isds::Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dmStatus.message();
	}
	iOk = dmStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dmStatus);
	}
	/* Cannot use dmStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Isds::Type::ERR_ISDS;
	}

	envelopes = Isds::Xml::readMessageList(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Isds::Type::ERR_XML;
	}

	return Isds::Type::ERR_SUCCESS;
}

enum  Isds::Type::Error Isds::getListOfReceivedMessages(Session &session,
    const QDateTime &fromTime, const QDateTime &toTime,
    long int recipientOrgUnitNum, Type::DmFiltStates statusFilter,
    unsigned long int offset, unsigned long int limit,
    QList<Envelope> &envelopes, DmStatus *status, QString *errMsg)
{
	return getListOfMessages(Xml::RML_RCVD, session, fromTime, toTime,
	    recipientOrgUnitNum, statusFilter, offset, limit, envelopes,
	    status, errMsg);
}

enum  Isds::Type::Error Isds::getListOfSentMessages(Session &session,
    const QDateTime &fromTime, const QDateTime &toTime,
    long int senderOrgUnitNum, Type::DmFiltStates statusFilter,
    unsigned long int offset, unsigned long int limit,
    QList<Envelope> &envelopes, DmStatus *status, QString *errMsg)
{
	return getListOfMessages(Xml::RML_SNT, session, fromTime, toTime,
	    senderOrgUnitNum, statusFilter, offset, limit, envelopes,
	    status, errMsg);
}

/*!
 * @brief Download signed data message or delivery info.
 *
 * @param[in]     rsmd Determines request type.
 * @param[in,out] session Communication session.
 * @param[in]     dmId Message identifier.
 * @param[out]    message Downloaded message.
 * @param[out]    status Status as reported by the ISDS.
 * @param[out]    errMsg Error message.
 * @return Error status.
 */
static
enum Isds::Type::Error getSignedMessage(
    enum Isds::Xml::RequestSignedMessageData rsmd, Isds::Session &session,
    qint64 dmId, Isds::Message &message, Isds::DmStatus *status,
    QString *errMsg)
{
	if (Q_UNLIKELY(dmId < 0)) {
		return Isds::Type::ERR_INVAL;
	}

	QByteArray response;
	enum Isds::Connection::ErrCode errCode = Isds::Connection::communicate(
	    session,
	    (rsmd == Isds::Xml::RSMD_DELINFO) ? MESSAGE_SERVICE : LOGIN_SERVICE,
	    Isds::Xml::soapRequestGetSignedMessage(rsmd, dmId), response);
	if (errCode != Isds::Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	Isds::DmStatus dmStatus = Isds::Xml::toDmStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Isds::Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dmStatus.message();
	}
	iOk = dmStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dmStatus);
	}
	/* Cannot use dmStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Isds::Type::ERR_ISDS;
	}

	/* Obtain CMS message. */
	response = Isds::Xml::cmsZfoFromSoap(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Isds::Type::ERR_XML;
	}

	/* Read message content. */
	message = Isds::toMessage(macroStdMove(response), &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Isds::Type::ERR_XML;
	}

	return Isds::Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::getSignedDeliveryInfo(Session &session,
    qint64 dmId, Message &message, DmStatus *status, QString *errMsg)
{
	return getSignedMessage(Xml::RSMD_DELINFO, session, dmId, message,
	    status, errMsg);
}

enum Isds::Type::Error Isds::getSignedReceivedMessage(Session &session,
    qint64 dmId, Message &message, DmStatus *status, QString *errMsg)
{
	return getSignedMessage(Xml::RSMD_RCVD_MSG, session, dmId, message,
	    status, errMsg);
}

enum Isds::Type::Error Isds::getSignedSentMessage(Session &session,
    qint64 dmId, Message &message, DmStatus *status, QString *errMsg)
{
	return getSignedMessage(Xml::RSMD_SNT_MSG, session, dmId, message,
	    status, errMsg);
}

enum Isds::Type::Error Isds::sendMessage(Session &session, Message &message,
    DmStatus *status, QString *errMsg)
{
	if (Q_UNLIKELY(message.isNull())) {
		return Type::ERR_INVAL;
	}

	QByteArray response;
	enum Connection::ErrCode errCode = Connection::ERR_NO_ERROR;
	{
		bool iOk = false;
		QByteArray request =
		    Xml::soapRequestCreateMessage(message, &iOk);
		if (Q_UNLIKELY(!iOk)) {
			return Type::ERR_SOAP;
		}
		errCode = Connection::communicate(session, LOGIN_SERVICE,
		    request, response);
	}
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DmStatus dmStatus = Xml::toDmStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dmStatus.message();
	}
	iOk = dmStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dmStatus);
	}
	/* Cannot use dmStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	qint64 dmId = Xml::readCreateMessageResponse(response);
	if (Q_UNLIKELY(dmId < 0)) {
		return Type::ERR_XML;
	}

	{
		Envelope env = message.envelope();
		env.setDmId(dmId);
		message.setEnvelope(macroStdMove(env));
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::authenticateMessage(Session &session,
    const QByteArray &rawData, bool &valid, DmStatus *status, QString *errMsg)
{
	if (Q_UNLIKELY(rawData.isEmpty())) {
		return Type::ERR_INVAL;
	}

	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(
	    session, LOGIN_SERVICE, Xml::soapRequestAuthenticateMessage(rawData),
	    response);
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DmStatus dmStatus = Xml::toDmStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dmStatus.message();
	}
	iOk = dmStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dmStatus);
	}
	/* Cannot use dmStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	valid = Xml::readAuthenticateMessageResponse(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::getMessageAuthorInfo(Session &session, qint64 dmId,
    enum Type::SenderType &userType, QString &authorName, DmStatus *status,
    QString *errMsg)
{
	if (Q_UNLIKELY(dmId < 0)) {
		return Type::ERR_INVAL;
	}

	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(
	    session, MESSAGE_SERVICE, Xml::soapRequestGetMessageAuthor(dmId),
	    response);
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DmStatus dmStatus = Xml::toDmStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dmStatus.message();
	}
	iOk = dmStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dmStatus);
	}
	/* Cannot use dmStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	if (!Xml::readMessageAuthor(response, userType, authorName)) {
		return Type::ERR_XML;
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::markMessageAsDownloaded(Session &session,
    qint64 dmId, DmStatus *status, QString *errMsg)
{
	if (Q_UNLIKELY(dmId < 0)) {
		return Type::ERR_INVAL;
	}

	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(
	    session, MESSAGE_SERVICE,
	    Xml::soapRequestMarkMessageAsDownloaded(dmId), response);
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DmStatus dmStatus = Xml::toDmStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dmStatus.message();
	}
	iOk = dmStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dmStatus);
	}
	/* Cannot use dmStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}

	return Type::ERR_SUCCESS;
}

enum Isds::Type::Error Isds::dummyOperation(Session &session, DmStatus *status,
    QString *errMsg)
{
	QByteArray response;
	enum Connection::ErrCode errCode = Connection::communicate(session,
	    LOGIN_SERVICE, Xml::soapRequestDummyOperation(), response);
	if (errCode != Connection::ERR_NO_ERROR) {
		if (errMsg != Q_NULLPTR) {
			*errMsg = session.ctx()->lastIsdsMsg();
		}
		return errCode2Error(errCode);
	}

	bool iOk = false;
	DmStatus dmStatus = Xml::toDmStatus(response, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_XML;
	}
	if (errMsg != Q_NULLPTR) {
		*errMsg = dmStatus.message();
	}
	iOk = dmStatus.ok();
	if (status != Q_NULLPTR) {
		*status = macroStdMove(dmStatus);
	}
	/* Cannot use dmStatus.ok(), content might be moved. */
	if (Q_UNLIKELY(!iOk)) {
		return Type::ERR_ISDS;
	}
	return Type::ERR_SUCCESS;
}
