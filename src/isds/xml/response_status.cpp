/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QString>
#include <QXmlStreamReader>

#include "src/isds/xml/response_status.h"

#ifdef Q_COMPILER_RVALUE_REFS
#  define macroStdMove(x) ::std::move(x)
#else /* Q_COMPILER_RVALUE_REFS */
#  define macroStdMove(x) (x)
#endif /* Q_COMPILER_RVALUE_REFS */

/*!
 * @brief Describes the dbStatus and dbStatus elements.
 */
struct ResponseStatusElements {
	QLatin1String statusName;
	QLatin1String codeName;
	QLatin1String messageName;
};

/*
 * <dbStatus>
 *   <dbStatusCode></dbStatusCode>
 *   <dbStatusMessage></dbStatusMessage>
 * </dbStatus>
 */
static
const ResponseStatusElements dbStatusElements = {
    QLatin1String("dbStatus"), QLatin1String("dbStatusCode"), QLatin1String("dbStatusMessage")
};

/*
 * <dmStatus>
 *   <dmStatusCode></dmStatusCode>
 *   <dmStatusMessage></dmStatusMessage>
 * </dmStatus>
 */
static
const ResponseStatusElements dmStatusElements = {
    QLatin1String("dmStatus"), QLatin1String("dmStatusCode"), QLatin1String("dmStatusMessage")
};

/*!
 * @brief Parse the status code and message from XML data.
 *
 * @param[in]  xmlData Response XML data.
 * @param[in]  elements Element names.
 * @param[out] code Status code string.
 * @param[out] message Status message.
 * @return False on XML error or if status element not found.
 */
static
bool readXmlStatus(const QByteArray &xmlData,
    const ResponseStatusElements &elements, QString &code, QString &message)
{
	QXmlStreamReader xml(xmlData);
	bool statusFound = false;

	while (!xml.atEnd() && !xml.hasError()) {
		QXmlStreamReader::TokenType tokenType = xml.readNext();
		if (tokenType == QXmlStreamReader::StartDocument) {
			continue;
		}
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
		QStringView tokenName = xml.name();
#else /* < Qt-6.0 */
		QStringRef tokenName = xml.name();
#endif /* >= Qt-6.0 */
		if ((tokenType == QXmlStreamReader::StartElement) &&
		    (tokenName == elements.statusName)) {
			statusFound = true;
			/*
			 * Parse status part of XML response
			 * (<p:dbStatus> or <q:dmStatus>).
			 *
			 * While not read status end element.
			 */
			while (!((tokenType == QXmlStreamReader::EndElement) &&
			         (tokenName == elements.statusName))) {
				if (tokenType == QXmlStreamReader::StartElement) {
					if (tokenName == elements.codeName) {
						xml.readNext();
						code = xml.text().toString();
					} else if (tokenName == elements.messageName) {
						xml.readNext();
						message = xml.text().toString();
					}
				}
				tokenType = xml.readNext();
				tokenName = xml.name();
			}
		}
	}

	return (!xml.hasError()) && statusFound;
}

Isds::DbStatus Isds::Xml::toDbStatus(const QByteArray &xmlData, bool *ok)
{
	QString code;
	QString message;
	DbStatus dbStatus;

	bool iOk = readXmlStatus(xmlData, dbStatusElements, code, message);
	if (ok != Q_NULLPTR) {
		*ok = iOk;
	}
	if (Q_UNLIKELY(!iOk)) {
		return dbStatus;
	}

	dbStatus.setCode(macroStdMove(code));
	dbStatus.setMessage(macroStdMove(message));
	return dbStatus;
}

bool Isds::Xml::dbStatusOk(const QByteArray &xmlData, QString &message)
{
	bool iOk = false;
	DbStatus dbStatus = toDbStatus(xmlData, &iOk);
	message = dbStatus.message();
	return iOk && dbStatus.ok();
}

Isds::DmStatus Isds::Xml::toDmStatus(const QByteArray &xmlData, bool *ok)
{
	QString code;
	QString message;
	DmStatus dmStatus;

	bool iOk = readXmlStatus(xmlData, dmStatusElements, code, message);
	if (ok != Q_NULLPTR) {
		*ok = iOk;
	}
	if (Q_UNLIKELY(!iOk)) {
		return dmStatus;
	}

	dmStatus.setCode(macroStdMove(code));
	dmStatus.setMessage(macroStdMove(message));
	return dmStatus;
}

bool Isds::Xml::dmStatusOk(const QByteArray &xmlData, QString &message)
{
	bool iOk = false;
	DmStatus dmStatus = toDmStatus(xmlData, &iOk);
	message = dmStatus.message();
	return iOk && dmStatus.ok();
}
