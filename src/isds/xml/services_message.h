/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

/*
 * Here you can find functions for reading and writing SOAP requests and
 * responses for servcies described in the document
 * pril_2/WS_manipulace_s_datovymi_zpravami.pdf .
 *
 * It doesn't matter whether the responses are still encapsulated in the SOAP
 * envelope. The code is able to handle both situations.
 */

class QByteArray; /* Forward declaration. */

namespace Isds {

	namespace Xml {

		/*!
		 * @brief Reads SignedMessageDownloadResponse or
		 *     GetSignedDeliveryInfoResponse content and returns
		 *     the content of the dmSignature element which should be
		 *     a CMS message.
		 *
		 * @param[in] xmlData XML data.
		 * @param[out] ok Set to false if dmSignature content not found.
		 * @return Null value if content could not be read.
		 *     Binary data (i.e. non-base64 encoded) else.
		 */
		QByteArray cmsZfoFromSoap(const QByteArray &xmlData,
		    bool *ok = Q_NULLPTR);

	}

}
