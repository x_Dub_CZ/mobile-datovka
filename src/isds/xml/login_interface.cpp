/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/isds/isds_const.h" /* ISDS_XML_NS */
#include "src/isds/xml/login_interface.h"
#include "src/isds/xml/soap.h"

/*!
 * @brief Converts OTP method into string description.
 *
 * @param[in] otpMethod OTP method type.
 * @return OTP type string.
 */
static
const char *otpMethodCStr(enum Isds::Type::OtpMethod otpMethod)
{
	static const char *nullStr = "";
	static const char *hotpStr = "HOTP";
	static const char *totpStr = "TOTP";

	switch (otpMethod) {
	case Isds::Type::OM_HMAC:
		return hotpStr;
		break;
	case Isds::Type::OM_TIME:
		return totpStr;
		break;
	default:
		Q_ASSERT(0);
		return nullStr;
		break;
	}
}

QByteArray Isds::Xml::soapRequestChangePasswordOTP(
    enum Type::OtpMethod otpMethod, const QString &oldPwd,
    const QString &newPwd)
{
	QByteArray xmlData(
	    "<ChangePasswordOTP xmlns=\"http://isds.czechpoint.cz/v20/asws\">"
	    "<dbOldPassword>");
	xmlData.append(oldPwd.toUtf8());
	xmlData.append("</dbOldPassword><dbNewPassword>");
	xmlData.append(newPwd.toUtf8());
	xmlData.append("</dbNewPassword><dbOTPType>");
	xmlData.append(otpMethodCStr(otpMethod));
	xmlData.append("</dbOTPType></ChangePasswordOTP>");
	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestChangeISDSPassword(const QString &oldPwd,
    const QString &newPwd)
{
	QByteArray xmlData(
	    "<ChangeISDSPassword xmlns=\"" ISDS_XML_NS "\"><dbOldPassword>");
	xmlData.append(oldPwd.toUtf8());
	xmlData.append("</dbOldPassword><dbNewPassword>");
	xmlData.append(newPwd.toUtf8());
	xmlData.append("</dbNewPassword></ChangeISDSPassword>");
	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestSendSmsCode(void)
{
	QByteArray xmlData(
	    "<SendSMSCode xmlns=\"http://isds.czechpoint.cz/v20/asws\">"
	    "</SendSMSCode>");
	return coatWithSoapEnvelope(xmlData);
}
