/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDomDocument>
#include <QDomElement>
#include <QString>

class QByteArray; /* Forward declaration. */

namespace Isds {

	namespace Xml {

		/*!
		 * @brief Converts a XML document into a DOM document.
		 *
		 * @note Reads the document data with namespace processing
		 *     enabled.
		 *
		 * @param[in]  xmlData XML data.
		 * @param[out] ok Set to false on a XML error.
		 * @return DOM document on success.
		 */
		QDomDocument toDomDocument(const QByteArray &xmlData,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Finds and returns an element node in a DOM document
		 *     with given name.
		 *
		 * @note DOM document must have namespace processing enabled.
		 *
		 * @param[in] dom DOM document.
		 * @param[in] tagName Local name.
		 * @return Non-null node if found.
		 */
		QDomElement findSingleDomElement(const QDomDocument &dom,
		    const QString &tagName);

		/*!
		 * @brief Returns the string value from the node content
		 *     (child node).
		 *
		 * @note DOM document must have namespace processing enabled.
		 *
		 * @param[in]  elem DOM element node whose text child should be read.
		 * @param[out] ok Set to false on error.
		 * @return Content of the child text node.
		 */
		QString readSingleChildStringValue(const QDomElement &elem,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Return element node attribute value.
		 *
		 * @param[in]  elem DOM element node whose attributes should be accessed.
		 * @param[in]  attribute Attribute name.
		 * @param[in]  errorMissing Treat messing attribute as error.
		 * @param[out] ok Set to false on error.
		 * @return Attribute value, null on error.
		 */
		QString readAttributeValue(const QDomElement &elem,
		    const QString &attribute, bool errorMissing = false,
		    bool *ok = Q_NULLPTR);

	}

}
