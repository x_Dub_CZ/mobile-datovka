/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/isds/xml/soap.h"

QByteArray Isds::Xml::coatWithSoapEnvelope(const QByteArray &xmlData)
{
	QByteArray requestContent("<?xml version=\"1.0\" encoding=\"utf-8\"?>"
	    "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">"
	    "<SOAP-ENV:Body>");
	requestContent.append(xmlData);
	requestContent.append("</SOAP-ENV:Body></SOAP-ENV:Envelope>");
	return requestContent;
}

/*
 * https://techbase.kde.org/Development/Tutorials/QtDOM_Tutorial
 *
 * There is a long-lasting problem which, despite the statements mentioned
 * in the document, still has not been resolved properly
 * (at least not until Qt-5.14.2 which we use now).
 * Therefore using the 'Workaround for broken namespace handling' approach.
 */

QDomDocument Isds::Xml::buildEmptyDocument(void)
{
	static const QString target("xml");
	static const QString data("version=\"1.0\" encoding=\"utf-8\"");

	QDomDocument doc;
	doc.appendChild(doc.createProcessingInstruction(target, data));
	return doc;
}

QDomElement Isds::Xml::buildSoapEnvelope(QDomDocument &doc)
{
	static const QString xsdNs("xmlns:xsd");
	static const QString xsdNsUri("http://www.w3.org/2001/XMLSchema");
	static const QString soapNs("xmlns:SOAP-ENV");
	static const QString soapNsUri("http://schemas.xmlsoap.org/soap/envelope/");
	static const QString envelopeTag("SOAP-ENV:Envelope");
	static const QString bodyTag("SOAP-ENV:Body");

	QDomElement elem1 = doc.createElement(envelopeTag);
	elem1.setAttribute(soapNs, soapNsUri);
	elem1.setAttribute(xsdNs, xsdNsUri);
	doc.appendChild(elem1);
	QDomElement elem2 = doc.createElement(bodyTag);
	elem1.appendChild(elem2);

	return elem2;
}
