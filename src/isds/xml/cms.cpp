/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <cstdlib> /* size_t */

#include "src/crypto/crypto.h"
#include "src/isds/xml/cms.h"

QByteArray Isds::Xml::cmsContent(const QByteArray &cmsData)
{
	/* Decode CMS and obtain message XML data. Uses OpenSSL. */
	void *content = NULL;
	size_t len = 0;
	if (Q_UNLIKELY(extract_cms_data(cmsData.data(), cmsData.length(), &content, &len) != 0)) {
		return QByteArray();
	}
	if (Q_UNLIKELY(len == 0)) {
		::std::free(content); content = NULL;
		return QByteArray();
	}
	QByteArray data((char *)content, len);
	::std::free(content); content = NULL;

	return data;
}
