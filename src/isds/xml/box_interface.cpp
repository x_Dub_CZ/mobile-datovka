/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QByteArray>
#include <QDomNode>
#include <QString>
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
#  include <QStringView>
#else /* < Qt-6.0 */
#  include <QStringRef>
#endif /* >= Qt-6.0 */

#include <QXmlStreamReader>
#include <utility> /* ::std::move */

#include "src/datovka_shared/isds/type_conversion.h"
#include "src/isds/conversion/isds_time_conversion.h"
#include "src/isds/conversion/isds_type_conversion.h"
#include "src/isds/isds_const.h" /* ISDS_XML_NS */
#include "src/isds/xml/helper.h"
#include "src/isds/xml/box_interface.h"
#include "src/isds/xml/soap.h"

#ifdef Q_COMPILER_RVALUE_REFS
#  define macroStdMove(x) ::std::move(x)
#else /* Q_COMPILER_RVALUE_REFS */
#  define macroStdMove(x) (x)
#endif /* Q_COMPILER_RVALUE_REFS */

QDateTime Isds::Xml::readPasswordInfoExpiration(const QByteArray &xmlData,
    bool *ok)
{
	if (Q_UNLIKELY(xmlData.isEmpty())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return QDateTime();
	}

	QXmlStreamReader xml(xmlData);
	while (!xml.atEnd() && !xml.hasError()) {
		QXmlStreamReader::TokenType tokenType = xml.readNext();
		if (tokenType == QXmlStreamReader::StartDocument) {
			continue;
		}
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
		QStringView tokenName = xml.name();
#else /* < Qt-6.0 */
		QStringRef tokenName = xml.name();
#endif /* >= Qt-6.0 */
		if ((tokenType == QXmlStreamReader::StartElement) &&
		    (tokenName == QLatin1String("pswExpDate"))) {
			xml.readNext();
			if (ok != Q_NULLPTR) {
				*ok = true;
			}
			return isoDateTimeStrToUtcDateTime(
			    xml.text().toString());
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return QDateTime();
}

/*!
 * @brief Gather available address data from supplied element node.
 *
 * @note Unknown or missing entries are ignored.
 *
 * @param[in,out] address Address whose content should be modified.
 * @param[in]     elem DOM element node whose children should be scanned for data.
 * @return False on failure, true on success.
 */
static
bool readAddressExt2Content(Isds::AddressExt2 &address, const QDomElement &elem)
{
	if (Q_UNLIKELY(elem.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	bool iOk = false;
	QString value;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		const QString localName = elem.localName();
		if (localName == QLatin1String("adCode")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				address.setCode(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("adCity")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				address.setCity(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("adDistrict")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				address.setDistrict(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("adStreet")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				address.setStreet(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("adNumberInStreet")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				address.setNumberInStreet(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("adNumberInMunicipality")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				address.setNumberInMunicipality(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("adZipCode")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				address.setZipCode(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("adState")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				address.setState(macroStdMove(value));
			} else {
				goto fail;
			}
		} else {
			/*
			 * There may be other entries in the supplied nodes
			 * but we shall ignore them.
			 */
		}
	}

	return true;

fail:
	return false;
}

/*!
 * @brief Gather available birth info data from supplied element node.
 *
 * @note Unknown or missing entries are ignored.
 *
 * @param[in,out] birthInfo Birth info whose content should be modified.
 * @param[in]     elem DOM element node whose children should be scanned for data.
 * @return False on failure, true on success.
 */
static
bool readBirthInfoContent(Isds::BirthInfo &birthInfo, const QDomElement &elem)
{
	if (Q_UNLIKELY(elem.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	bool iOk = false;
	QString value;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		const QString localName = elem.localName();
		if (localName == QLatin1String("biDate")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				birthInfo.setDate(Isds::isoDateStrToDate(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("biCity")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				birthInfo.setCity(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("biCounty")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				birthInfo.setCounty(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("biState")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				birthInfo.setState(macroStdMove(value));
			} else {
				goto fail;
			}
		} else {
			/*
			 * There may be other entries in the supplied nodes
			 * but we shall ignore them.
			 */
		}
	}

	return true;

fail:
	return false;
}

/*!
 * @brief Gather available person name data from supplied element node.
 *
 * @note Unknown or missing entries are ignored.
 *
 * @param[in,out] personName Person name whose content should be modified.
 * @param[in]     elem DOM element node whose children should be scanned for data.
 * @return False on failure, true on success.
 */
static
bool readPersonName2Content(Isds::PersonName2 &personName,
    const QDomElement &elem)
{
	if (Q_UNLIKELY(elem.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	bool iOk = false;
	QString value;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		const QString localName = elem.localName();
		if (localName == QLatin1String("pnGivenNames")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				personName.setGivenNames(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("pnLastName")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				personName.setLastName(macroStdMove(value));
			} else {
				goto fail;
			}
		}  else {
			/*
			 * There may be other entries in the supplied nodes
			 * but we shall ignore them.
			 */
		}
	}

	return true;

fail:
	return false;
}

/*!
 * @brief Gather available owner info data from supplied element node.
 *
 * @note Unknown or missing entries are ignored.
 *
 * @param[in,out] info Owner info whose content should be modified.
 * @param[in]     elem DOM element node whose children should be scanned for data.
 * @return False on failure, true on success.
 */
static
bool readOwnerInfoExt2Content(Isds::DbOwnerInfoExt2 &info, const QDomElement &elem)
{
	if (Q_UNLIKELY(elem.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	bool iOk = false;
	QString value;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		const QString localName = elem.localName();
		if (localName == QLatin1String("dbID")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setDbID(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("aifoIsds")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setAifoIsds(Isds::str2BoolType(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dbType")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setDbType(Isds::str2DbType(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("ic")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setIc(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("firmName")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setFirmName(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("nationality")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setNationality(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dbIdOVM")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setDbIdOVM(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dbState")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					long int val = value.toLong(&iOk);
					if (iOk) {
						enum Isds::Type::DbState state =
						    Isds::long2DbState(val, &iOk);
						if (iOk) {
							info.setDbState(state);
						}
					}
				}
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dbOpenAddressing")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setDbOpenAddressing(Isds::str2BoolType(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dbUpperID")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setDbUpperID(macroStdMove(value));
			} else {
				goto fail;
			}
		} else {
			/*
			 * There may be other entries in the supplied nodes
			 * but we shall ignore them.
			 */
		}
	}

	{
		Isds::AddressExt2 address;
		if (readAddressExt2Content(address, elem)) {
			info.setAddress(macroStdMove(address));
		} else {
			goto fail;
		}
	}
	{
		Isds::BirthInfo birthInfo;
		if (readBirthInfoContent(birthInfo, elem)) {
			info.setBirthInfo(macroStdMove(birthInfo));
		} else {
			goto fail;
		}
	}
	{
		Isds::PersonName2 personName;
		if (readPersonName2Content(personName, elem)) {
			info.setPersonName(macroStdMove(personName));
		} else {
			goto fail;
		}
	}

	return true;

fail:
	return false;
}

/*!
 * @brief Gather available user info data from supplied element node.
 *
 * @note Unknown or missing entries are ignored.
 *
 * @param[in,out] info User info whose content should be modified.
 * @param[in]     elem DOM element node whose children should be scanned for data.
 * @return False on failure, true on success.
 */
static
bool readUserInfoExt2Content(Isds::DbUserInfoExt2 &info, const QDomElement &elem)
{
	if (Q_UNLIKELY(elem.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	bool iOk = false;
	QString value;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		const QString localName = elem.localName();
		if (localName == QLatin1String("aifoIsds")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setAifoIsds(Isds::str2BoolType(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("biDate")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setBiDate(Isds::isoDateStrToDate(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("isdsID")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setIsdsID(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("userType")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setUserType(
				    Isds::str2UserType(macroStdMove(value)));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("userPrivils")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setUserPrivils(
				    Isds::long2Privileges(value.toLongLong()));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("ic")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setIc(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("firmName")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setFirmName(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("caStreet")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setCaStreet(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("caCity")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setCaCity(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("caZipCode")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setCaZipCode(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("caState")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setCaState(macroStdMove(value));
			} else {
				goto fail;
			}
		} else {
			/*
			 * There may be other entries in the supplied nodes
			 * but we shall ignore them.
			 */
		}
	}

	{
		Isds::AddressExt2 address;
		if (readAddressExt2Content(address, elem)) {
			info.setAddress(macroStdMove(address));
		} else {
			goto fail;
		}
	}
	{
		Isds::PersonName2 personName;
		if (readPersonName2Content(personName, elem)) {
			info.setPersonName(macroStdMove(personName));
		} else {
			goto fail;
		}
	}

	return true;

fail:
	return false;
}

Isds::DbOwnerInfoExt2 Isds::Xml::readOwnerInfoExt2(const QDomDocument &dom,
    bool *ok)
{
	bool iOk = false;
	DbOwnerInfoExt2 info;
	const QDomElement elem = findSingleDomElement(dom,
	    QLatin1String("dbOwnerInfo"));

	if (Q_UNLIKELY(elem.isNull())) {
		Q_ASSERT(0);
		goto fail;
	}

	iOk = readOwnerInfoExt2Content(info, elem);
	if (Q_UNLIKELY(!iOk)) {
		Q_ASSERT(0);
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return info;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return DbOwnerInfoExt2();
}

Isds::DbOwnerInfoExt2 Isds::Xml::toOwnerInfo(const QByteArray &xmlData, bool *ok)
{
	if (Q_UNLIKELY(xmlData.isEmpty())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return DbOwnerInfoExt2();
	}

	bool iOk = false;
	DbOwnerInfoExt2 dbOwnerInfo;
	QDomDocument dom = toDomDocument(xmlData, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}

	dbOwnerInfo = readOwnerInfoExt2(dom, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return dbOwnerInfo;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return DbOwnerInfoExt2();
}

Isds::DbUserInfoExt2 Isds::Xml::readUserInfoExt2(const QDomDocument &dom,
    bool *ok)
{
	bool iOk = false;
	DbUserInfoExt2 info;
	const QDomElement elem = findSingleDomElement(dom,
	    QLatin1String("dbUserInfo"));

	if (Q_UNLIKELY(elem.isNull())) {
		Q_ASSERT(0);
		goto fail;
	}

	iOk = readUserInfoExt2Content(info, elem);
	if (Q_UNLIKELY(!iOk)) {
		Q_ASSERT(0);
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return info;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return DbUserInfoExt2();
}

Isds::DbUserInfoExt2 Isds::Xml::toUserInfoExt2(const QByteArray &xmlData,
    bool *ok)
{
	if (Q_UNLIKELY(xmlData.isEmpty())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return DbUserInfoExt2();
	}

	bool iOk = false;
	DbUserInfoExt2 dbUserInfo;
	QDomDocument dom = toDomDocument(xmlData, &iOk);

	dbUserInfo = readUserInfoExt2(dom, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return dbUserInfo;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return DbUserInfoExt2();
}

/*!
 * @brief Extract highlighting tags and compute start/end pairs.
 *
 * @param[in]  inText Text to be scanned for start/end tags.
 * @param[out] outText Resulting text which does not contain the highlighting tags.
 * @param[out] matches Pairs of start/end indexes pointing into \a outText.
 * @return True on success, false on failure.
 */
static
bool sanitiseHighlighting(const QString &inText,
    QString &outText, QList< QPair<int, int> > &matches)
{
	static const QString startTag("|$*HL_START*$|"), endTag("|$*HL_END*$|");

	int start = -1;
	int end = -1;
	QString workText = inText;
	QList< QPair<int, int> > workMatches;
	do {
		start = workText.indexOf(startTag, 0, Qt::CaseSensitive);
		if (start >= 0) {
			workText.remove(start, startTag.size());
		}
		end = workText.indexOf(endTag, 0, Qt::CaseSensitive);
		if (end >= 0) {
			workText.remove(end, endTag.size());
		}
		if ((start >= 0) && (end >= 0)) {
			/* End points at last character. */
			workMatches.append(QPair<int, int>(start, end - 1));
		} else if ((start >= 0) || (end >= 0)) {
			Q_ASSERT(0);
			return false;
		}
	} while (start >= 0);

	outText = workText;
	matches = workMatches;
	return true;
}

/*!
 * @brief Read full-text result content from DOM element node.
 *
 * @param[in]  elem DOM element node.
 * @param[out] ok Set to false on error.
 * @return Data result content, null value on error.
 */
static
Isds::DbResult2 readResultContent(const QDomElement &elem, bool *ok)
{
	if (Q_UNLIKELY(elem.isNull())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return Isds::DbResult2();
	}

	bool iOk = false;
	QString value;
	Isds::DbResult2 result;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		const QString localName = elem.localName();
		if (localName == QLatin1String("dbID")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				result.setDbID(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dbType")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				result.setDbType(Isds::str2DbType(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dbName")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				result.setDbName(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dbAddress")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				result.setDbAddress(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dbBiDate")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				result.setDbBiDate(Isds::isoDateStrToDate(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dbICO")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				result.setIc(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dbIdOVM")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				result.setDbIdOVM(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dbSendOptions")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				bool act = false;
				bool pub = false;
				bool com = false;
				if (Isds::str2dbSendOptions(value, act, pub, com)) {
					result.setActive(act);
					result.setPublicSending(pub);
					result.setCommercialSending(com);
				} else {
					goto fail;
				}
			} else {
				goto fail;
			}
		} else {
			/* There shouldn't be other content. */
			Q_ASSERT(0);
			goto fail;
		}
	}

	{
		QString name;
		QList< QPair<int, int> > matches;
		if (sanitiseHighlighting(result.dbName(), name, matches)) {
			result.setDbName(macroStdMove(name));
			result.setNameMatches(macroStdMove(matches));
		}
	}
	{
		QString address;
		QList< QPair<int, int> > matches;
		if (sanitiseHighlighting(result.dbAddress(), address, matches)) {
			result.setDbAddress(macroStdMove(address));
			result.setAddressMatches(macroStdMove(matches));
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return result;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return Isds::DbResult2();
}

/*!
 * @brief Read full-text result list content from DOM element node.
 *
 * @param[in]  elem DOM element node.
 * @param[out] ok Set to false on error.
 * @return Result list, empty list on error.
 */
static
QList<Isds::DbResult2> readResultList(const QDomElement &elem, bool *ok)
{
	if (Q_UNLIKELY(elem.isNull())) {
		/* No attachment list found, treat this as an error. */
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return QList<Isds::DbResult2>();
	}

	bool iOk = false;
	QList<Isds::DbResult2> results;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		if (elem.localName() == QLatin1String("dbResult")) {
			Isds::DbResult2 result = readResultContent(elem, &iOk);
			if (iOk) {
				results.append(result);
			} else {
				goto fail;
			}
		} else {
			/* There shouldn't be other content. */
			Q_ASSERT(0);
			goto fail;
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return results;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return QList<Isds::DbResult2>();
}

/*!
 * @brief Read full-text result list content and search frame status from DOM
 *     element node.
 *
 * @param[in]  dom DOM document.
 * @param[out] totalCount Total number of results matching the query.
 * @param[out] currentCount Size of this page.
 * @param[out] position Index of this page.
 * @param[out] lastPage Set to true if there is no successive page.
 * @param[out] ok Set to false on any error.
 * @return Result list, empty list on error.
 */
static
QList<Isds::DbResult2> readFulltextResults(const QDomDocument &dom,
    long int &totalCount, long int &currentCount, long int &position,
    bool &lastPage, bool *ok)
{
	if (Q_UNLIKELY(dom.isNull())) {
		Q_ASSERT(0);
		return QList<Isds::DbResult2>();
	}

	bool iOk = false;
	QString value;
	QList<Isds::DbResult2> boxes;
	bool haveTotal = false;
	bool haveCurrent = false;
	bool havePosition = false;
	bool haveLast = false;
	bool haveBoxes = false;

	const QDomElement elem = Isds::Xml::findSingleDomElement(dom,
	    QLatin1String("ISDSSearch3Response"));

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}

		const QDomElement elem = node.toElement();
		const QString localName = elem.localName();

		if (localName == QLatin1String("totalCount")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					long int val = value.toLong(&iOk);
					if (iOk) {
						totalCount = val;
						haveTotal = true;
					}
				}
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("currentCount")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					long int val = value.toLong(&iOk);
					if (iOk) {
						currentCount = val;
						haveCurrent = true;
					}
				}
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("position")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					long int val = value.toLong(&iOk);
					if (iOk) {
						position = val;
						havePosition = true;
					}
				}
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("lastPage")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				enum Isds::Type::NilBool val =
				    Isds::numStr2BoolType(value);
				if (val != Isds::Type::BOOL_NULL) {
					lastPage = (val == Isds::Type::BOOL_TRUE);
					haveLast = true;
				}
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("dbResults")) {
			QList<Isds::DbResult2> bs = readResultList(elem, &iOk);
			if (iOk) {
				boxes = macroStdMove(bs);
				haveBoxes = true;
			} else {
				goto fail;
			}
		}  else {
			/*
			 * There may be other entries in the supplied nodes
			 * but we shall ignore them.
			 */
		}
	}

	if (Q_UNLIKELY(!(haveTotal && haveCurrent && havePosition && haveLast && haveBoxes))) {
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return boxes;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return QList<Isds::DbResult2>();
}

QList<Isds::DbResult2> Isds::Xml::toDbResult2(const QByteArray &xmlData,
    long int &totalCount, long int &currentCount, long int &position,
    bool &lastPage, bool *ok)
{
	if (Q_UNLIKELY(xmlData.isEmpty())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return QList<DbResult2>();
	}

	bool iOk = false;
	QDomDocument dom = toDomDocument(xmlData, &iOk);
	QList<DbResult2> boxes;

	boxes = readFulltextResults(dom, totalCount, currentCount, position,
	    lastPage, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}

	return boxes;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return QList<DbResult2>();
}


/*!
 * @brief Gather available long term storage info data from supplied element node.
 *
 * @note Unknown or missing entries are ignored.
 *
 * @param[in,out] dtInfo Long term storage info whose content should be modified.
 * @param[in]     elem DOM element node whose children should be scanned for data.
 * @return False on failure, true on success.
 */
static
bool readDtInfoContent(Isds::DTInfoOutput &dtInfo, const QDomElement &elem)
{
	if (Q_UNLIKELY(elem.isNull())) {
		Q_ASSERT(0);
		return false;
	}

	bool iOk = false;
	QString value;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		const QString localName = elem.localName();
		if (localName == QLatin1String("ActDTType")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					long int val = value.toLong(&iOk);
					if (iOk) {
						enum Isds::Type::DTType type =
						    Isds::long2DTType(val, &iOk);
						if (iOk) {
							dtInfo.setActDTType(type);
						}
					}
				}
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("ActDTCapacity")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					long int val = value.toLong(&iOk);
					if (iOk) {
						dtInfo.setActDTCapacity(val);
					}
				}
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("ActDTFrom")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				dtInfo.setActDTFrom(Isds::isoDateStrToDate(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("ActDTTo")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				dtInfo.setActDTTo(Isds::isoDateStrToDate(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("ActDTCapUsed")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					long int val = value.toLong(&iOk);
					if (iOk) {
						dtInfo.setActDTCapUsed(val);
					}
				}
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("FutDTType")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					long int val = value.toLong(&iOk);
					if (iOk) {
						enum Isds::Type::DTType type =
						    Isds::long2DTType(val, &iOk);
						if (iOk) {
							dtInfo.setFutDTType(type);
						}
					}
				}
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("FutDTCapacity")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					long int val = value.toLong(&iOk);
					if (iOk) {
						dtInfo.setFutDTCapacity(val);
					}
				}
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("FutDTFrom")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				dtInfo.setFutDTFrom(Isds::isoDateStrToDate(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("FutDTTo")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				dtInfo.setFutDTTo(Isds::isoDateStrToDate(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("FutDTPaid")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				if (!value.isNull()) {
					long int val = value.toLong(&iOk);
					if (iOk) {
						enum Isds::Type::FutDTPaidState state =
						    Isds::long2DTPaidState(val, &iOk);
						if (iOk) {
							dtInfo.setFutDTPaid(state);
						}
					}
				}
			} else {
				goto fail;
			}
		} else {
			/*
			 * There may be other entries in the supplied nodes
			 * but we shall ignore them.
			 */
		}
	}

	return true;

fail:
	return false;
}

Isds::DTInfoOutput Isds::Xml::readDTInfo(const QDomDocument &dom,
    bool *ok)
{
	bool iOk = false;
	DTInfoOutput info;
	const QDomElement elem = findSingleDomElement(dom,
	    QLatin1String("DTInfoResponse"));

	if (Q_UNLIKELY(elem.isNull())) {
		Q_ASSERT(0);
		goto fail;
	}

	iOk = readDtInfoContent(info, elem);
	if (Q_UNLIKELY(!iOk)) {
		Q_ASSERT(0);
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return info;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return DTInfoOutput();
}

Isds::DTInfoOutput Isds::Xml::toDTInfo(const QByteArray &xmlData, bool *ok)
{
	if (Q_UNLIKELY(xmlData.isEmpty())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return DTInfoOutput();
	}

	bool iOk = false;
	DTInfoOutput dtInfo;
	QDomDocument dom = toDomDocument(xmlData, &iOk);

	dtInfo = readDTInfo(dom, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return dtInfo;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return DTInfoOutput();
}

bool Isds::Xml::readPdzSendInfo(const QByteArray &xmlData, bool *ok)
{
	if (Q_UNLIKELY(xmlData.isEmpty())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return false;
	}

	QXmlStreamReader xml(xmlData);
	while (!xml.atEnd() && !xml.hasError()) {
		QXmlStreamReader::TokenType tokenType = xml.readNext();
		if (tokenType == QXmlStreamReader::StartDocument) {
			continue;
		}
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
		QStringView tokenName = xml.name();
#else /* < Qt-6.0 */
		QStringRef tokenName = xml.name();
#endif /* >= Qt-6.0 */
		if ((tokenType == QXmlStreamReader::StartElement) &&
		    (tokenName == QLatin1String("PDZsiResult"))) {
			xml.readNext();
			if (ok != Q_NULLPTR) {
				*ok = true;
			}
			return Isds::str2BoolType(xml.text().toString());
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return false;
}

/*!
 * @brief Read pdz info content from DOM element node.
 *
 * @param[in]  elem DOM element node.
 * @param[out] ok Set to false on error.
 * @return Data result content, null value on error.
 */
static
Isds::PDZInfoRec readPdzInfoContent(const QDomElement &elem, bool *ok)
{
	if (Q_UNLIKELY(elem.isNull())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return Isds::PDZInfoRec();
	}

	bool iOk = false;
	QString value;
	Isds::PDZInfoRec info;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		const QString localName = elem.localName();
		if (localName == QLatin1String("PDZType")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setPdzType(Isds::str2pdzPaymentType(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("PDZRecip")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setPdzRecip(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("PDZPayer")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setPdzPayer(macroStdMove(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("PDZExpire")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setPdzExpire(
				    Isds::isoDateTimeStrToUtcDateTime(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("PDZCnt")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setPdzCnt(value.toLongLong());
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("ODZIdent")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				info.setOdzIdent(macroStdMove(value));
			} else {
				goto fail;
			}
		} else {
			/* There shouldn't be other content. */
			Q_ASSERT(0);
			goto fail;
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return info;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return Isds::PDZInfoRec();
}

/*!
 * @brief Read pdz info list content from DOM element node.
 *
 * @param[in]  elem DOM element node.
 * @param[out] ok Set to false on error.
 * @return Result list, empty list on error.
 */
static
QList<Isds::PDZInfoRec> readPdzInfoList(const QDomElement &elem, bool *ok)
{
	if (Q_UNLIKELY(elem.isNull())) {
		/* No attachment list found, treat this as an error. */
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return QList<Isds::PDZInfoRec>();
	}

	bool iOk = false;
	QList<Isds::PDZInfoRec> infos;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		if (elem.localName() == QLatin1String("dbPDZRecord")) {
			Isds::PDZInfoRec info = readPdzInfoContent(elem, &iOk);
			if (iOk) {
				infos.append(info);
			} else {
				goto fail;
			}
		} else {
			/* There shouldn't be other content. */
			Q_ASSERT(0);
			goto fail;
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return infos;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return QList<Isds::PDZInfoRec>();
}

/*!
 * @brief Read pdz info result list content and search frame status from DOM
 *        element node.
 *
 * @param[in]  dom DOM document.
 * @param[out] ok Set to false on any error.
 * @return Result list, empty list on error.
 */
static
QList<Isds::PDZInfoRec> readPdzInfoResults(const QDomDocument &dom, bool *ok)
{
	if (Q_UNLIKELY(dom.isNull())) {
		Q_ASSERT(0);
		return QList<Isds::PDZInfoRec>();
	}

	bool iOk = false;
	QString value;
	QList<Isds::PDZInfoRec> infos;

	const QDomElement elem = Isds::Xml::findSingleDomElement(dom,
	    QLatin1String("PDZInfoResponse"));

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}

		const QDomElement elem = node.toElement();
		const QString localName = elem.localName();

		if (localName == QLatin1String("dbPDZRecords")) {
			QList<Isds::PDZInfoRec> pi = readPdzInfoList(elem, &iOk);
			if (iOk) {
				infos = macroStdMove(pi);
			} else {
				goto fail;
			}
		}  else {
			/*
			 * There may be other entries in the supplied nodes
			 * but we shall ignore them.
			 */
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return infos;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return QList<Isds::PDZInfoRec>();
}

QList<Isds::PDZInfoRec> Isds::Xml::toPdzInfoRecs(const QByteArray &xmlData,
    bool *ok)
{
	if (Q_UNLIKELY(xmlData.isEmpty())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return QList<PDZInfoRec>();
	}

	bool iOk = false;
	QDomDocument dom = toDomDocument(xmlData, &iOk);
	QList<PDZInfoRec> infos;

	infos = readPdzInfoResults(dom, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}

	return infos;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return QList<PDZInfoRec>();
}

/*!
 * @brief Read credit event content from DOM element node.
 *
 * @param[in]  elem DOM element node.
 * @param[out] ok Set to false on error.
 * @return Data result content, null value on error.
 */
static
Isds::CreditEvent readCreditEventContent(const QDomElement &elem, bool *ok)
{
	if (Q_UNLIKELY(elem.isNull())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return Isds::CreditEvent();
	}

	bool iOk = false;
	QString value;
	Isds::CreditEvent event;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		const QString localName = elem.localName();
		if (localName == QLatin1String("ciEventTime")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				event.setTime(
				    Isds::isoDateTimeStrToUtcDateTime(value));
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("ciCreditChange")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				event.setCreditChange(value.toLongLong());
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("ciCreditAfter")) {
			value = Isds::Xml::readSingleChildStringValue(elem, &iOk);
			if (iOk) {
				event.setCreditAfter(value.toLongLong());
			} else {
				goto fail;
			}
		} else {
			/*
			 * There may be other entries in the supplied nodes
			 * but we shall ignore them.
			 */
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return event;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return Isds::CreditEvent();
}

/*!
 * @brief Read data box credit event list content from DOM element node.
 *
 * @param[in]  elem DOM element node.
 * @param[out] ok Set to false on error.
 * @return Result list, empty list on error.
 */
static
QList<Isds::CreditEvent> readCreditEventList(const QDomElement &elem, bool *ok)
{
	if (Q_UNLIKELY(elem.isNull())) {
		/* No attachment list found, treat this as an error. */
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return QList<Isds::CreditEvent>();
	}

	bool iOk = false;
	QList<Isds::CreditEvent> history;

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}
		const QDomElement elem = node.toElement();

		if (elem.localName() == QLatin1String("ciRecord")) {
			Isds::CreditEvent event = readCreditEventContent(elem,
			    &iOk);
			if (iOk) {
				history.append(event);
			} else {
				goto fail;
			}
		} else {
			/* There shouldn't be other content. */
			Q_ASSERT(0);
			goto fail;
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return history;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return QList<Isds::CreditEvent>();
}

/*!
 * @brief Read data box credit info result list content and search frame
 *        status from DOM element node.
 *
 * @param[in]  dom DOM document.
 * @param[out] currentCredit Current credit in CZK.
 * @param[out] notifEmail Notification email address
 * @param[out] ok Set to false on any error.
 * @return Result list, empty list on error.
 */
static
QList<Isds::CreditEvent> readDataBoxCreditInfo(const QDomDocument &dom,
    qint64 &currentCredit, QString &notifEmail, bool *ok)
{
	if (Q_UNLIKELY(dom.isNull())) {
		Q_ASSERT(0);
		return QList<Isds::CreditEvent>();
	}

	bool iOk = false;
	QString value;
	QList<Isds::CreditEvent> history;

	const QDomElement elem = Isds::Xml::findSingleDomElement(dom,
	    QLatin1String("DataBoxCreditInfoResponse"));

	const QDomNodeList childNodes = elem.childNodes();
	for (int i = 0; i < childNodes.size(); ++i) {
		const QDomNode &node = childNodes.at(i);
		if (Q_UNLIKELY(node.nodeType() != QDomNode::ElementNode)) {
			continue;
		}

		const QDomElement elem = node.toElement();
		const QString localName = elem.localName();

		if (localName == QLatin1String("currentCredit")) {
			value = Isds::Xml::readSingleChildStringValue(elem,
			    &iOk);
			if (iOk) {
				currentCredit = value.toLongLong();
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("notifEmail")) {
			value = Isds::Xml::readSingleChildStringValue(elem,
			    &iOk);
			if (iOk) {
				notifEmail = ::std::move(value);
			} else {
				goto fail;
			}
		} else if (localName == QLatin1String("ciRecords")) {
			QList<Isds::CreditEvent> cel =
			    readCreditEventList(elem, &iOk);
			if (iOk) {
				history = macroStdMove(cel);
			} else {
				goto fail;
			}
		}  else {
			/*
			 * There may be other entries in the supplied nodes
			 * but we shall ignore them.
			 */
		}
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}
	return history;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return QList<Isds::CreditEvent>();
}

QList<Isds::CreditEvent> Isds::Xml::toDataBoxCreditInfo(
    const QByteArray &xmlData, qint64 &currentCredit, QString &notifEmail,
    bool *ok)
{
	if (Q_UNLIKELY(xmlData.isEmpty())) {
		if (ok != Q_NULLPTR) {
			*ok = false;
		}
		return QList<CreditEvent>();
	}

	bool iOk = false;
	QDomDocument dom = toDomDocument(xmlData, &iOk);
	QList<CreditEvent> history;

	history = readDataBoxCreditInfo(dom, currentCredit, notifEmail, &iOk);
	if (Q_UNLIKELY(!iOk)) {
		goto fail;
	}

	if (ok != Q_NULLPTR) {
		*ok = true;
	}

	return history;

fail:
	if (ok != Q_NULLPTR) {
		*ok = false;
	}
	return QList<CreditEvent>();
}

QByteArray Isds::Xml::soapRequestGetPasswordInfo(void)
{
	QByteArray xmlData("<GetPasswordInfo xmlns=\"" ISDS_XML_NS "\">"
	    "<dbDummy/></GetPasswordInfo>");
	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestGetOwnerInfoFromLogin2(void)
{
	QByteArray xmlData("<GetOwnerInfoFromLogin2 xmlns=\"" ISDS_XML_NS "\">"
	    "<dbDummy/></GetOwnerInfoFromLogin2>");
	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestGetUserInfoFromLogin2(void)
{
	QByteArray xmlData("<GetUserInfoFromLogin2 xmlns=\"" ISDS_XML_NS "\">"
	    "<dbDummy/></GetUserInfoFromLogin2>");
	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestFindDataBox2(
    const DbOwnerInfoExt2 &dbOwnerInfo)
{
	QByteArray xmlData("<FindDataBox2 xmlns=\"" ISDS_XML_NS "\">"
	    "<dbOwnerInfo><dbID>");
	xmlData.append(dbOwnerInfo.dbID().toUtf8());
	xmlData.append("</dbID><dbType>");
	xmlData.append(dbType2Str(dbOwnerInfo.dbType()).toUtf8());
	xmlData.append("</dbType>"
	    "<ic/><pnFirstName/><pnMiddleName/><pnLastName/>"
	    "<pnLastNameAtBirth/><firmName/><adCity/><adStreet/>"
	    "<adNumberInStreet/><adNumberInMunicipality/>"
	    "<adZipCode/><adState/><nationality/><email/>"
	    "<telNumber/><identifier/><registryCode/>"
	    "<dbState>0</dbState>"
	    "<dbEffectiveOVM>false</dbEffectiveOVM>"
	    "<dbOpenAddressing>false</dbOpenAddressing>"
	    "</dbOwnerInfo></FindDataBox2>");
	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestIsdsSearch3(const QString &query,
    enum Type::FulltextSearchType searchType, enum Type::DbType boxType,
    long int page, long int pageSize, enum Type::NilBool highlighting)
{
	QByteArray xmlData(
	    "<ISDSSearch3 xmlns=\"" ISDS_XML_NS "\"><searchText>");
	xmlData.append(query.toUtf8());
	xmlData.append("</searchText><searchType>");
	xmlData.append(fulltextSearchType2Str(searchType).toUtf8());
	xmlData.append("</searchType><searchScope>");
	xmlData.append(fulltextDbType2Str(boxType).toUtf8());
	xmlData.append("</searchScope><page>");
	xmlData.append(QString::number(page).toUtf8());
	xmlData.append("</page><pageSize>");
	xmlData.append(QString::number(pageSize).toUtf8());
	xmlData.append("</pageSize>");
	if (highlighting != Type::BOOL_NULL) {
		xmlData.append(QString("<highlighting>%1</highlighting>")
		    .arg(boolType2NumStr(highlighting)).toUtf8());
	} else {
		xmlData.append("<highlighting/>");
	}
	xmlData.append("</ISDSSearch3>");
	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestDTInfo(const QString &dbID)
{
	QByteArray xmlData("<DTInfo xmlns=\"" ISDS_XML_NS "\"><dbId>");
	xmlData.append(dbID.toUtf8());
	xmlData.append("</dbId></DTInfo>");
	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestPdzInfo(const QString &dbID)
{
	QByteArray xmlData("<PDZInfo xmlns=\"" ISDS_XML_NS "\"><PDZSender>");
	xmlData.append(dbID.toUtf8());
	xmlData.append("</PDZSender></PDZInfo>");
	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestPdzSendInfo(const QString &dbID,
    enum Type::PdzMessageType type)
{
	QByteArray xmlData("<PDZSendInfo xmlns=\"" ISDS_XML_NS "\"><dbId>");
	xmlData.append(dbID.toUtf8());
	xmlData.append("</dbId><PDZType>");
	xmlData.append(pdzMessageType2Str(type).toUtf8());
	xmlData.append("</PDZType></PDZSendInfo>");
	return coatWithSoapEnvelope(xmlData);
}

QByteArray Isds::Xml::soapRequestDataBoxCerditInfo(const QString &dbID,
    const QDate &fromDate, const QDate &toDate)
{
	QByteArray xmlData("<DataBoxCreditInfo xmlns=\"" ISDS_XML_NS "\"><dbID>");
	xmlData.append(dbID.toUtf8());
	xmlData.append("</dbID>");
	if (!fromDate.isNull() && fromDate.isValid()) {
		xmlData.append("<ciFromDate>");
		xmlData.append(fromDate.toString().toUtf8());
		xmlData.append("</ciFromDate>");
	} else {
		xmlData.append("<ciFromDate/>");
	}
	if (!toDate.isNull() && toDate.isValid()) {
		xmlData.append("<ciTodate>");
		xmlData.append(toDate.toString().toUtf8());
		xmlData.append("</ciTodate>");
	} else {
		xmlData.append("<ciTodate/>");
	}
	xmlData.append("</DataBoxCreditInfo>");
	return coatWithSoapEnvelope(xmlData);
}
