/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS */

#include <QString>

namespace Isds {
	class Session; /* Forward declaration. */
}
class QByteArray; /* Forward declaration. */

namespace Isds {

	/*!
	 * @brief Encapsulates connection to ISDS.
	 */
	class Connection {
		Q_DECLARE_TR_FUNCTIONS(Connection)

	public:
		/*!
		 * @brief Error codes.
		 */
		enum ErrCode {
			ERR_NO_ERROR = 0, /*!< No error, reply is OK. */
			ERR_NO_CONNECTION, /*!< No active connection to internet. */
			ERR_BAD_REQUEST, /*!< Bad request error. */
			ERR_REPLY, /*!< An reply error. */
			ERR_SERVER_UNAVAILABLE, /*!< ISDS server is out of service. */
			ERR_TIMEOUT, /*!< Connection timed out. */
			ERR_UNAUTHORIZED, /*!< Authorization error. */
			ERR_UNAUTHORIZED_HOTP, /*!< HOTP authorization error. */
			ERR_UNAUTHORIZED_TOTP, /*!< TOTP authorization error. */
			ERR_UNAUTHORIZED_TOTP_SMS, /*!< TOTP SMS sending error. */
			ERR_UNSPECIFIED /*!< Unspecified error. */
		};

		/*!
		 * @brief Set network manager and send request to server.
		 *
		 * @param[in]  ctx ISDS account session.
		 * @param[in]  soapEndPoint SOAP end point path.
		 * @param[in]  requestData Data to be sent.
		 * @param[out] replyData Data from the reply (may be empty).
		 * @return Error code.
		 */
		static
		enum ErrCode communicate(Isds::Session &session,
		    const QString &soapEndPoint, const QByteArray &requestData,
		    QByteArray &replyData);

	private:
		/*!
		 * @brief Localised error description of the error code value.
		 *
		 * @param[in] errCode Error code.
		 * @return Error description text.
		 */
		static
		QString errorDescription(enum ErrCode errCode);
	};

}
