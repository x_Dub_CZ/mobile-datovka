/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> // Q_DECLARE_TR_FUNCTIONS
#include <QString>

#include "src/datovka_shared/isds/types.h"

/*!
 * @brief Provides conversion functions for ISDS types.
 */
class IsdsConversion {
	Q_DECLARE_TR_FUNCTIONS(IsdsConversion)

private:
	/*!
	 * @brief Private constructor.
	 */
	IsdsConversion(void);

public:
	/*!
	 * @brief Returns localised user type description.
	 *
	 * @param[in] type User type.
	 * @return Localised user type description.
	 */
	static
	QString userTypeToDescr(enum Isds::Type::UserType type);

	/*!
	 * @brief Returns localised sender type description.
	 *
	 * @param[in] type Sender type as given by GetMessageAuthor service.
	 * @return Localised sender type description.
	 */
	static
	QString senderTypeToDescr(enum Isds::Type::SenderType type);

	/*!
	 * @brief Returns localised description of data box state.
	 *
	 * @param[in] state Data-box state code.
	 * @return Localised data box state description.
	 */
	static
	QString dbStateToText(int state);

	/*!
	 * @brief Returns localised message type description.
	 *
	 * @param[in] val Message type char.
	 * @return Localised message type description.
	 */
	static
	QString dmTypeToText(const QString &val);

	/*!
	 * @brief Returns localised message status description.
	 *
	 * @param[in] status Message status code.
	 * @return Localised message status description.
	 */
	static
	QString dmMessageStatusToText(int status);

	/*!
	 * @brief Convert MEP resolution code from isds to enum.
	 *
	 * @param[in] status Isds MEP resolution code.
	 * @return MEP resolution code.
	 */
	static
	enum Isds::Type::MepResolution isdsMepStatusToMepResolution(int status);
};
