/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/datovka_shared/isds/type_conversion.h"
#include "src/isds/conversion/isds_type_conversion.h"

static const QString strNull;
static const QString strTrue("true"), strFalse("false");

enum Isds::Type::NilBool Isds::str2BoolType(const QString &s)
{
	if (s.isEmpty()) {
		return Type::BOOL_NULL;
	} else if (s == strTrue) {
		return Type::BOOL_TRUE;
	} else if (s == strFalse) {
		return Type::BOOL_FALSE;
	} else {
		Q_ASSERT(0);
		return Type::BOOL_FALSE;
	}
}

const QString &Isds::boolType2Str(enum Type::NilBool b)
{
	switch (b) {
	case Type::BOOL_NULL: return strNull; break;
	case Type::BOOL_FALSE: return strFalse; break;
	case Type::BOOL_TRUE: return strTrue; break;
	default:
		Q_ASSERT(0);
		return strNull;
		break;
	}
}

static const QString str1("1"), str0("0");

enum Isds::Type::NilBool Isds::numStr2BoolType(const QString &s)
{
	if (s.isEmpty()) {
		return Type::BOOL_NULL;
	} else if (s == str1) {
		return Type::BOOL_TRUE;
	} else if (s == str0) {
		return Type::BOOL_FALSE;
	} else {
		Q_ASSERT(0);
		return Type::BOOL_FALSE;
	}
}

const QString &Isds::boolType2NumStr(enum Type::NilBool b)
{
	switch (b) {
	case Type::BOOL_NULL: return strNull; break;
	case Type::BOOL_FALSE: return str0; break;
	case Type::BOOL_TRUE: return str1; break;
	default:
		Q_ASSERT(0);
		return strNull;
		break;
	}
}

static const QString strAll("ALL");

enum Isds::Type::DbType Isds::str2FulltextDbType(const QString &s)
{
	if (s == strAll) {
		return Type::BT_SYSTEM;
	} else {
		return str2DbType(s);
	}
}

const QString &Isds::fulltextDbType2Str(enum Type::DbType bt)
{
	if (bt == Type::BT_SYSTEM) {
		return strAll;
	} else {
		return dbType2Str(bt);
	}
}

/* Sharing "ALL" with previous function. */
static const QString strDz("DZ"), strPdz("PDZ"), strNone("NONE"),
    strDisabled("DISABLED");

bool Isds::str2dbSendOptions(const QString &s, bool &act, bool &pub, bool &com)
{
	if (s == strAll) {
		act = true;
		pub = true;
		com = true;
	} else if (s == strDz) {
		act = true;
		pub = true;
		com = false;
	} else if (s == strPdz) {
		act = true;
		pub = false;
		com = true;
	} else if (s == strNone) {
		act = true;
		pub = false;
		com = false;
	} else if (s == strDisabled) {
		act = true;
		pub = false;
		com = false;
	} else {
		Q_ASSERT(0);
		return false;
	}

	return true;
}

const QString &Isds::dbSendOptions2str(bool act, bool pub, bool com)
{
	if (!act) {
		return strDisabled;
	} else if (pub && com) { /* act && pub && com */
		return strAll;
	} else if (pub) { /* act && pub && !com */
		return strDz;
	} else if (com) { /* act !pub && com */
		return strPdz;
	} else { /* act !pub && !com */
		return strNone;
	}
}

static const QString strPdzNormal("Normal"), strPdzInit("Init");

const QString &Isds::pdzMessageType2Str(enum Type::PdzMessageType type)
{
	switch (type) {
	case Type::PMT_NORMAL: return strPdzNormal; break;
	case Type::PMT_INIT: return strPdzInit; break;
	default:
		Q_ASSERT(0);
		return strNull;
		break;
	}
}

static const QString strK("K"), strE("E"), strG("G"), strO("O"),
    strZ("Z"), strD("D");

enum Isds::Type::PdzPaymentType Isds::str2pdzPaymentType(const QString &s)
{
	if (s.isNull()) {
		return Type::PPT_UNKNOWN;
	} else if (s == strK) {
		return  Type::PPT_K;
	} else if (s == strE) {
		return Type::PPT_E;
	} else if (s == strG) {
		return Type::PPT_G;
	} else if (s == strO) {
		return Type::PPT_O;
	} else if (s == strZ) {
		return Type::PPT_Z;
	} else if (s == strD) {
		return Type::PPT_D;
	} else {
		Q_ASSERT(0);
		return Type::PPT_UNKNOWN;
	}
}
