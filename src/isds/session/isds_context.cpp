/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <utility> /* ::std::swap */

#include <QSslCertificate>
#include <QSslKey>
#include <QString>
#include <QUrl>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/isds/session/isds_context.h"

/* Null objects - for convenience. */
static const AcntId nullAcntId;
static const QList<QNetworkCookie> nullCookies;
static const QSslCertificate nullSslCertificate;
static const QSslKey nullSslKey;
static const QString nullString;
static const QUrl nullUrl;

/*!
 * @brief PIMPL Context class.
 */
class Isds::ContextPrivate {

public:
	ContextPrivate(void)
	    : m_lim(AcntData::LIM_UNKNOWN), m_acntId(), m_url(), m_password(),
	    m_sslCertificate(), m_sslKey(), m_cookies(), m_lastIsdsMsg(),
	    m_loginState(LS_NOT_LOGGED_IN)
	{ }

	ContextPrivate &operator=(const ContextPrivate &other) Q_DECL_NOTHROW
	{
		m_lim = other.m_lim;
		m_acntId = other.m_acntId;
		m_url = other.m_url;
		m_password = other.m_password;
		m_sslCertificate = other.m_sslCertificate;
		m_sslKey = other.m_sslKey;
		m_cookies = other.m_cookies;
		m_lastIsdsMsg = other.m_lastIsdsMsg;
		m_loginState = other.m_loginState;

		return *this;
	}

	bool operator==(const ContextPrivate &other) const
	{
		return (m_lim == other.m_lim) &&
		    (m_acntId == other.m_acntId) &&
		    (m_url == other.m_url) &&
		    (m_password == other.m_password) &&
		    (m_sslCertificate == other.m_sslCertificate) &&
		    (m_sslKey == other.m_sslKey) &&
		    (m_cookies == other.m_cookies) &&
		    (m_lastIsdsMsg == other.m_lastIsdsMsg) &&
		    (m_loginState == other.m_loginState);
	}

	enum AcntData::LoginMethod m_lim;
	AcntId m_acntId;
	QUrl m_url;
	QString m_password;
	QSslCertificate m_sslCertificate;
	QSslKey m_sslKey;
	QList<QNetworkCookie> m_cookies;
	QString m_lastIsdsMsg;
	enum Isds::LoginState m_loginState;
};

Isds::Context::Context(void)
    : d_ptr(Q_NULLPTR)
{
}

Isds::Context::Context(const Context &other)
    : d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) ContextPrivate) : Q_NULLPTR)
{
	Q_D(Context);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::Context::Context(Context &&other) Q_DECL_NOEXCEPT
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    : d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#else /* < Qt-5.12 */
    : d_ptr(other.d_ptr.take())
#endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

Isds::Context::~Context(void)
{
}

/*!
 * @brief Ensures private context presence.
 *
 * @note Returns if private context could not be allocated.
 */
#define ensureContextPrivate(_x_) \
	do { \
		if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) { \
			ContextPrivate *p = new (::std::nothrow) ContextPrivate; \
			if (Q_UNLIKELY(p == Q_NULLPTR)) { \
				Q_ASSERT(0); \
				return _x_; \
			} \
			d_ptr.reset(p); \
		} \
	} while (0)

Isds::Context &Isds::Context::operator=(const Context &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureContextPrivate(*this);
	Q_D(Context);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
Isds::Context &Isds::Context::operator=(Context &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool Isds::Context::operator==(const Context &other) const
{
	Q_D(const Context);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool Isds::Context::operator!=(const Context &other) const
{
	return !operator==(other);
}

bool Isds::Context::isNull(void) const
{
	Q_D(const Context);
	return d == Q_NULLPTR;
}

enum AcntData::LoginMethod Isds::Context::loginMethod(void) const
{
	Q_D(const Context);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return AcntData::LIM_UNKNOWN;
	}
	return d->m_lim;
}

void Isds::Context::setLoginMethod(enum AcntData::LoginMethod lim)
{
	ensureContextPrivate();
	Q_D(Context);
	d->m_lim = lim;
}

const AcntId &Isds::Context::acntId(void) const
{
	Q_D(const Context);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullAcntId;
	}
	return d->m_acntId;
}

void Isds::Context::setAcntId(const AcntId &aid)
{
	ensureContextPrivate();
	Q_D(Context);
	d->m_acntId = aid;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::Context::setAcntId(AcntId &&aid)
{
	ensureContextPrivate();
	Q_D(Context);
	d->m_acntId = ::std::move(aid);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QUrl &Isds::Context::url(void) const
{
	Q_D(const Context);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullUrl;
	}
	return d->m_url;
}

void Isds::Context::setUrl(const QUrl &url)
{
	ensureContextPrivate();
	Q_D(Context);
	d->m_url = url;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::Context::setUrl(QUrl &&url)
{
	ensureContextPrivate();
	Q_D(Context);
	d->m_url = ::std::move(url);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::Context::password(void) const
{
	Q_D(const Context);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_password;
}

void Isds::Context::setPassword(const QString &pwd)
{
	ensureContextPrivate();
	Q_D(Context);
	d->m_password = pwd;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::Context::setPassword(QString &&pwd)
{
	ensureContextPrivate();
	Q_D(Context);
	d->m_password = ::std::move(pwd);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QSslCertificate &Isds::Context::sslCertificate(void) const
{
	Q_D(const Context);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullSslCertificate;
	}
	return d->m_sslCertificate;
}

void Isds::Context::setSslCertificate(const QSslCertificate &sc)
{
	ensureContextPrivate();
	Q_D(Context);
	d->m_sslCertificate = sc;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::Context::setSslCertificate(QSslCertificate &&sc)
{
	ensureContextPrivate();
	Q_D(Context);
	d->m_sslCertificate = ::std::move(sc);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QSslKey &Isds::Context::sslKey(void) const
{
	Q_D(const Context);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullSslKey;
	}
	return d->m_sslKey;
}

void Isds::Context::setSslKey(const QSslKey &sk)
{
	ensureContextPrivate();
	Q_D(Context);
	d->m_sslKey = sk;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::Context::setSslKey(QSslKey &&sk)
{
	ensureContextPrivate();
	Q_D(Context);
	d->m_sslKey = ::std::move(sk);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QList<QNetworkCookie> &Isds::Context::cookies(void) const
{
	Q_D(const Context);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullCookies;
	}
	return d->m_cookies;
}

void Isds::Context::setCookies(const QList<QNetworkCookie> &cl)
{
	ensureContextPrivate();
	Q_D(Context);
	d->m_cookies = cl;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::Context::setCookies(QList<QNetworkCookie> &&cl)
{
	ensureContextPrivate();
	Q_D(Context);
	d->m_cookies = ::std::move(cl);
}
#endif /* Q_COMPILER_RVALUE_REFS */

const QString &Isds::Context::lastIsdsMsg(void) const
{
	Q_D(const Context);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}
	return d->m_lastIsdsMsg;
}

void Isds::Context::setLastIsdsMsg(const QString &lim)
{
	ensureContextPrivate();
	Q_D(Context);
	d->m_lastIsdsMsg = lim;
}

#ifdef Q_COMPILER_RVALUE_REFS
void Isds::Context::setLastIsdsMsg(QString &&lim)
{
	ensureContextPrivate();
	Q_D(Context);
	d->m_lastIsdsMsg = ::std::move(lim);
}
#endif /* Q_COMPILER_RVALUE_REFS */

enum Isds::LoginState Isds::Context::loginState(void) const
{
	Q_D(const Context);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return LS_NOT_LOGGED_IN;
	}
	return d->m_loginState;
}

void Isds::Context::setLoginState(enum LoginState lst)
{
	ensureContextPrivate();
	Q_D(Context);
	d->m_loginState = lst;
}

void Isds::swap(Context &first, Context &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}
