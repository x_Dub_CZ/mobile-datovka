/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QMap>

#include "src/datovka_shared/identifiers/account_id.h"

class AccountsMap; /* Forward declaration. */
namespace Isds {
	class Session; /* Forward declaration. */
}
class QString; /* Forward declaration. */

/*!
 * @brief Container holding account sessions.
 */
class Sessions {
public:
	/*!
	 * @brief Constructor.
	 */
	Sessions(void);

	/*!
	 * @brief Destructor.
	 */
	~Sessions(void);

	/*!
	 * @brief Returns all keys from session container.
	 */
	QList<AcntId> keys(void) const;

	/*!
	 * @brief Returns true if a session exists.
	 *
	 * @param[in] acntId Account identifier.
	 * @return True if a session for the given account exists in the container.
	 *
	 */
	bool holdsSession(const AcntId &acntId) const;

	/*!
	 * @brief Store session pointer into container.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] session Session pointer.
	 * @return True if session pointer has been stored.
	 */
	bool insertSession(const AcntId &acntId, Isds::Session *session);

	/*!
	 * @brief Remove and delete session.
	 *
	 * @param[in] acntId Account identifier.
	 */
	void removeSession(const AcntId &acntId);

	/*!
	 * @brief Returns pointer to associated session.
	 *
	 * @param[in] acntId Account identifier.
	 * @return Pointer to associated session. Returns Q_NULLPTR if session
	 *     does not exist.
	 */
	Isds::Session *session(const AcntId &acntId) const;

private:
	QMap<AcntId, Isds::Session *> m_sessions; /*!< Holds sessions. */
};
