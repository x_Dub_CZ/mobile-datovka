/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QtGlobal> /* QT_VERSION_CHECK */

#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

class QString; /* Forward declaration. */

namespace Isds {

	class ResponseStatusPrivate;
	/*!
	 * @brief Generalises the interface of the <dbStatus> and <dmStatus>
	 *     section of the web service responses. These sections, apart from
	 *     the element names, technically contain the same content.
	 */
	class ResponseStatus {
		Q_DECLARE_PRIVATE(ResponseStatus)

	protected:
		ResponseStatus(void);
		ResponseStatus(const ResponseStatus &other);
#ifdef Q_COMPILER_RVALUE_REFS
		ResponseStatus(ResponseStatus &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
	public:
		~ResponseStatus(void);

		ResponseStatus &operator=(const ResponseStatus &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		ResponseStatus &operator=(ResponseStatus &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const ResponseStatus &other) const;
		bool operator!=(const ResponseStatus &other) const;

		friend void swap(ResponseStatus &first, ResponseStatus &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

	protected:
		/* Status code. */
		const QString &code(void) const;
		void setCode(const QString &c);
#ifdef Q_COMPILER_RVALUE_REFS
		void setCode(QString &&c);
#endif /* Q_COMPILER_RVALUE_REFS */
		/* Status description message. */
		const QString &message(void) const;
		void setMessage(const QString &m);
#ifdef Q_COMPILER_RVALUE_REFS
		void setMessage(QString &&m);
#endif /* Q_COMPILER_RVALUE_REFS */

		/* For convenience, returns true if code is "0000". */
		bool ok(void) const;

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<ResponseStatusPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<ResponseStatusPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	void swap(ResponseStatus &first, ResponseStatus &second) Q_DECL_NOTHROW;

	/*!
	 * @brief Encapsulates the <dbStatus> entry of the data-box-related
	 *    web services.
	 */
	class DbStatus : public ResponseStatus {
	public:
		/* dbStatusCode */
		using ResponseStatus::code;
		using ResponseStatus::setCode;
		/* dbStatusMessage */
		using ResponseStatus::message;
		using ResponseStatus::setMessage;

		using ResponseStatus::ok;
	};

	/*!
	 * @brief Encapsulates the <dmStatus> entry of the data-message-related
	 *    web services.
	 */
	class DmStatus : public ResponseStatus {
	public:
		/* dmStatusCode */
		using ResponseStatus::code;
		using ResponseStatus::setCode;
		/* dmStatusMessage */
		using ResponseStatus::message;
		using ResponseStatus::setMessage;

		using ResponseStatus::ok;
	};

}
