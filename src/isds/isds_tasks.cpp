/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QFileInfo>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/box_interface2.h"
#include "src/datovka_shared/isds/message_interface.h"
#include "src/datovka_shared/isds/type_conversion.h"
#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/utility/strings.h"
#include "src/dialogues/dialogues.h"
#include "src/global.h"
#include "src/isds/conversion/isds_type_conversion.h" /* dbSendOptions2str() */
#include "src/isds/isds_tasks.h"
#include "src/models/accountmodel.h"
#include "src/models/databoxmodel.h"
#include "src/net/db_wrapper.h"
#include "src/settings/accounts.h"
#include "src/settings/prefs_specific.h"
#include "src/setwrapper.h"
#include "src/sqlite/zfo_db.h"
#include "src/worker/task_change_password.h"
#include "src/worker/task_credit_info.h"
#include "src/worker/task_download_account_info.h"
#include "src/worker/task_download_delivery_info.h"
#include "src/worker/task_download_dt_info.h"
#include "src/worker/task_download_message.h"
#include "src/worker/task_download_message_list.h"
#include "src/worker/task_find_databox.h"
#include "src/worker/task_find_databox_fulltext.h"
#include "src/worker/task_import_zfo.h"
#include "src/worker/task_pdz_info.h"
#include "src/worker/task_recipient_info.h"
#include "src/worker/task_send_message.h"
#include "src/worker/task_send_sms.h"

#ifdef Q_COMPILER_RVALUE_REFS
#  define macroStdMove(x) ::std::move(x)
#else /* Q_COMPILER_RVALUE_REFS */
#  define macroStdMove(x) (x)
#endif /* Q_COMPILER_RVALUE_REFS */

bool Isds::Tasks::changePassword(const AcntId &acntId, const QString &oldPwd,
    const QString &newPwd, const QString &otpCode)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	bool success = false;
	QString isdsText = tr("Unknown error");
	QString errTxt = tr("Wrong username");

	/* These fields must be set */
	if (!acntId.isValid() || oldPwd.isEmpty() || newPwd.isEmpty()) {
		Dialogues::errorMessage(Dialogues::CRITICAL,
		    tr("Error"), errTxt, tr("Internal error"));
		return success;
	}

	/* Set OTP type and OTP code */
	enum Isds::Type::OtpMethod otpMethod = Isds::Type::OM_UNKNOWN;
	AcntData &acntData((*GlobInstcs::acntMapPtr)[acntId]);
	if (acntData.loginMethod() == AcntData::LIM_UNAME_PWD_TOTP) {
		otpMethod = Isds::Type::OM_TIME;
	} else if (acntData.loginMethod() == AcntData::LIM_UNAME_PWD_HOTP) {
		otpMethod = Isds::Type::OM_HMAC;
	}

	TaskChangePassword *task = new (::std::nothrow) TaskChangePassword(
	    acntId, otpMethod, oldPwd, newPwd, otpCode);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}
	task->setAutoDelete(false);
	GlobInstcs::workPoolPtr->runSingle(task);
	success = TaskChangePassword::DL_SUCCESS == task->m_result;
	isdsText = task->m_isdsText;
	delete task; task = Q_NULLPTR;

	/* Show result dialogue */
	if (!success) {
		Dialogues::errorMessage(Dialogues::CRITICAL,
		    tr("Change password error: %1").arg(acntId.username()),
		    tr("Failed to change password for username '%1'.").arg(acntId.username()),
		    tr("ISDS returns: %1").arg(isdsText));
		return success;
	}
	Dialogues::errorMessage(Dialogues::INFORMATION,
	    tr("Change password: %1").arg(acntId.username()),
	    tr("Password for username '%1' has changed.").arg(acntId.username()),
	    tr("ISDS returns: %1").arg(isdsText));

	/* Store new password to account settings */
	acntData.setPassword(newPwd);

	return success;
}

void Isds::Tasks::downloadMessage(const AcntId &acntId,
    enum Messages::MessageType messageType, qint64 msgId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	TaskDownloadMessage *task = new (::std::nothrow) TaskDownloadMessage(
	    acntId, msgId, messageType,
	    (GlobalSettingsQmlWrapper::zfoDbSizeMBs() > 0), acntId.testing());
	if (task != Q_NULLPTR) {
		task->setAutoDelete(true);
		GlobInstcs::workPoolPtr->assignLo(task);
	}
}

QString Isds::Tasks::findDatabox(const AcntId &acntId, const QString &dbID,
    enum Type::DbType dbType)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return QString();
	}

	TaskFindDatabox *task = new (::std::nothrow) TaskFindDatabox(
	    acntId, dbID, dbType);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QString();
	}
	task->setAutoDelete(false);
	GlobInstcs::workPoolPtr->runSingle(task);
	QString dbInfo = task->m_dbInfo;
	QList<Isds::DbOwnerInfoExt2> foundBoxes = task->m_foundBoxes;
	delete task; task = Q_NULLPTR;

	return dbInfo;
}

/* Maximal number of search results on one page: ISDS allows 100 */
#define ISDS_SEARCH2_ITEMS_PER_PAGE 100

bool Isds::Tasks::findDataboxFulltext(const AcntId &acntId,
    const QString &phrase, enum Type::FulltextSearchType searchType,
    enum Type::DbType searchScope, long int page,
    QList<Isds::DbResult2> &dbList, long int &totalCount,
    long int &currentCount, long int &position, bool &lastPage,
    QString &lastError)
{
	TaskFindDataboxFulltext *task =
	    new (::std::nothrow) TaskFindDataboxFulltext(acntId, phrase,
	        searchType, searchScope, page, ISDS_SEARCH2_ITEMS_PER_PAGE,
	        false);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}
	task->setAutoDelete(false);
	GlobInstcs::workPoolPtr->runSingle(task);
	bool success = TaskFindDataboxFulltext::DL_SUCCESS == task->m_result;
	dbList = task->m_dbList;
	totalCount = task->m_totalCount;
	currentCount = task->m_currentCount;
	position = task->m_position;
	lastPage = task->m_lastPage;
	lastError = task->m_lastError;
	delete task; task = Q_NULLPTR;

	return success;
}

QString Isds::Tasks::getAccountDbId(const AcntId &acntId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return QString();
	}

	/* Last param: false = account info won't store into database */
	TaskDownloadAccountInfo *task =
	    new (::std::nothrow) TaskDownloadAccountInfo(acntId, false);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QString();
	}
	task->setAutoDelete(false);
	GlobInstcs::workPoolPtr->runSingle(task);
	QString dbId(task->m_dbID);
	delete task; task = Q_NULLPTR;
	return dbId;
}

void Isds::Tasks::getAccountInfo(const AcntId &acntId)
{
	debugFuncCall();

	if (Q_UNLIKELY(GlobInstcs::workPoolPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	/* Last param: true = store account info into database */
	TaskDownloadAccountInfo *task =
	    new (::std::nothrow) TaskDownloadAccountInfo(acntId, true);
	if (Q_NULLPTR != task) {
		task->setAutoDelete(true);
		GlobInstcs::workPoolPtr->assignHi(task);
	}
}

void Isds::Tasks::downloadDTInfo(const AcntId &acntId, const QString &dbID)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR) ||
		dbID.isEmpty())) {
		Q_ASSERT(0);
		return;
	}

	TaskDTInfo *task =
	    new (::std::nothrow) TaskDTInfo(acntId, dbID);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
	}
	task->setAutoDelete(false);
	GlobInstcs::workPoolPtr->runSingle(task);
	DTInfoOutput dtInfo(task->m_dtInfo);
	delete task; task = Q_NULLPTR;

	/* Store data into db */
	if (!DbWrapper::insertDTInfoToDb(dbID, dtInfo)) {
		logErrorNL("%s", "DB: Cannot store long term storage info into database");
	}
}

void Isds::Tasks::getDeliveryInfo(const AcntId &acntId, qint64 msgId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	TaskDownloadDeliveryInfo *task =
	    new (::std::nothrow) TaskDownloadDeliveryInfo(acntId, msgId);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}
	task->setAutoDelete(true);
	GlobInstcs::workPoolPtr->assignHi(task);
	delete task; task = Q_NULLPTR;
}

QString Isds::Tasks::importZfoMessages(const QList<AcntId> &acntIdList,
    const QStringList &filePathList, bool authenticate)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return QString();
	}

	int zfoTotal = filePathList.count();
	int zfoNumber = 0;

	foreach (const QString &file, filePathList) {
		++zfoNumber;
		TaskImportZfo *task = new (::std::nothrow) TaskImportZfo(
		    acntIdList, file, authenticate, zfoNumber, zfoTotal);
		if (task != Q_NULLPTR) {
			task->setAutoDelete(true);
			GlobInstcs::workPoolPtr->assignLo(task);
		}
	}

	return tr("ZFO import is running... Wait until import will be finished.");
}

void Isds::Tasks::recipientInfo(const AcntId &acntId, const QString &dbID,
    bool senderOvm, bool canUseInitReply,
    const QList<Isds::PDZInfoRec> &pdzInfos)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	TaskRecipientInfo *task = new (::std::nothrow) TaskRecipientInfo(
	    acntId, dbID, senderOvm, canUseInitReply, pdzInfos);
	if (task != Q_NULLPTR) {
		task->setAutoDelete(true);
		GlobInstcs::workPoolPtr->assignHi(task);
	}
}

bool Isds::Tasks::sendGovRequest(const AcntId &acntId,
    const Isds::Message &msg, QSet<QString> &transactIds)
{
	debugFuncCall();

	/* List of unique identifiers. */
	QStringList taskIdentifiers;
	const QDateTime currentTime(QDateTime::currentDateTimeUtc());
	taskIdentifiers.append(acntId.username() + "_"
	    + currentTime.toString() + "_"
	    + Utility::generateRandomString(6));

#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
	transactIds = QSet<QString>(taskIdentifiers.begin(), taskIdentifiers.end());
#else /* < Qt-5.14.0 */
	transactIds = taskIdentifiers.toSet();
#endif /* >= Qt-5.14.0 */

	TaskSendMessage *task = new (::std::nothrow) TaskSendMessage(acntId,
	    msg, taskIdentifiers.first());
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}
	task->setAutoDelete(true);
	GlobInstcs::workPoolPtr->assignHi(task);

	return true;
}

void Isds::Tasks::sendMessage(const AcntId &acntId, qint64 dmID,
    const QString &dmAnnotation, const DataboxListModel *databoxModel,
    const FileListModel *attachModel,
    const QString &dmLegalTitleLaw, const QString &dmLegalTitleYear,
    const QString &dmLegalTitleSect, const QString &dmLegalTitlePar,
    const QString &dmLegalTitlePoint, const QString &dmToHands,
    const QString &dmRecipientRefNumber, const QString &dmRecipientIdent,
    const QString &dmSenderRefNumber, const QString &dmSenderIdent,
    bool dmOVM, bool dmPublishOwnID, bool dmAllowSubstDelivery,
    bool dmPersonalDelivery, QSet<QString> &transactIds)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::zfoDbPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(databoxModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access data box model.");
		Q_ASSERT(0);
		Dialogues::errorMessage(Dialogues::CRITICAL,
		    tr("Error"), tr("Cannot access data box model."),
		    tr("Internal error"));
		return;
	}

	if (Q_UNLIKELY(attachModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access attachment model.");
		Q_ASSERT(0);
		Dialogues::errorMessage(Dialogues::CRITICAL,
		    tr("Error"), tr("Cannot access attachment model."),
		    tr("Internal error"));
		return;
	}

	const QList<DataboxModelEntry> dbList = databoxModel->allEntries();

	/* Check mandatory fields for initiatory and reply commercial messages. */
	foreach (const DataboxModelEntry &db, dbList) {
		if (db.dmType() == Isds::Type::MT_I
		         && dmSenderRefNumber.isEmpty()) {
			/* Message is initiatory PDZ, dmSenderRefNumber must be filled */
			Dialogues::errorMessage(Dialogues::CRITICAL,
			    tr("Error sending message"),
			    tr("Pre-paid transfer charges for message reply have been enabled."),
			    tr("The sender reference number must be specified in the additional section in order to send the message."));
			return;
		} else if (db.dmType() == Isds::Type::MT_O
		        && dmRecipientRefNumber.isEmpty()) {
			/* Message is PDZ reply on initiatory message, dmRecipientRefNumber must be filled */
			Dialogues::errorMessage(Dialogues::CRITICAL,
			    tr("Error sending message"),
			    tr("Message uses the offered payment of transfer charges by the recipient."),
			    tr("The recipient reference number must be specified in the additional section in order to send the message."));
			return;
		}
	}

	Isds::Envelope envelope;
	/* Fill message envelope */
	envelope.setDmAnnotation(dmAnnotation);
	envelope.setDmLegalTitleLawStr(dmLegalTitleLaw);
	envelope.setDmLegalTitleYearStr(dmLegalTitleYear);
	envelope.setDmLegalTitleSect(dmLegalTitleSect);
	envelope.setDmLegalTitlePar(dmLegalTitlePar);
	envelope.setDmLegalTitlePoint(dmLegalTitlePoint);
	envelope.setDmToHands(dmToHands);
	envelope.setDmRecipientRefNumber(dmRecipientRefNumber);
	envelope.setDmRecipientIdent(dmRecipientIdent);
	envelope.setDmSenderRefNumber(dmSenderRefNumber);
	envelope.setDmSenderIdent(dmSenderIdent);
	envelope.setDmAllowSubstDelivery(dmAllowSubstDelivery ?
	   Isds::Type::BOOL_TRUE : Isds::Type::BOOL_FALSE);
	envelope.setDmPersonalDelivery(dmPersonalDelivery ?
	   Isds::Type::BOOL_TRUE : Isds::Type::BOOL_FALSE);
	envelope.setDmMessageStatus(Isds::Type::MS_POSTED);
	envelope.setDmOVM(dmOVM ? Isds::Type::BOOL_TRUE :
	    Isds::Type::BOOL_FALSE);
	envelope.setDmPublishOwnID(dmPublishOwnID ? Isds::Type::BOOL_TRUE :
	    Isds::Type::BOOL_FALSE);

	FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
	    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[acntId]));
	if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
		logErrorNL("Cannot access file database for username '%s'.",
		    acntId.username().toUtf8().constData());
		Dialogues::errorMessage(Dialogues::CRITICAL,
		    tr("Error"), tr("Cannot access file database."),
		    tr("Internal error"));
		return;
	}

	QList<Isds::Document> documents;
	Isds::Type::FileMetaType fileMetaType = Isds::Type::FMT_MAIN;
	/* Load file contents from storage or database to file structure */
	foreach (const FileListModel::Entry &file, attachModel->allEntries()) {

		Isds::Document document;
		document.setFileDescr(file.fileName());
		document.setFileMetaType(fileMetaType);

		/*
		 * Since 2011 Mime Type can be empty and MIME type will
		 * be filled up on the ISDS server. It allows sending files
		 * with special mime types without recognition by application.
		 */
		document.setMimeType(QStringLiteral(""));

		if (file.fileId() > 0) {
			// load file content from database, valid file ID
			document = fDb->getFileFromDb(file.fileId());
			document.setFileMetaType(fileMetaType);
		} else if (file.fileId() == Files::DB_ZFO_ID) {
			// load zfo content from zfo database
			document.setBase64Content(
			    GlobInstcs::zfoDbPtr->getZfoContentFromDb(dmID,
			        acntId.testing()));
			GlobInstcs::zfoDbPtr->updateZfoLastAccessTime(dmID,
			    acntId.testing());
		} else if (!file.filePath().isEmpty()) {
			// load file content from storage (path)
			QFile fin(file.filePath());
			if (!fin.open(QIODevice::ReadOnly)) {
				Dialogues::errorMessage(Dialogues::CRITICAL,
				    tr("Error"), tr("Cannot load file content from path:"),
				    file.filePath());
				return;
			}
			document.setBinaryContent(fin.readAll());
		} else {
			Dialogues::errorMessage(Dialogues::CRITICAL,
			    tr("Error"), tr("No file to send."),
			    tr("Internal error"));
			return;
		}
		documents.append(document);
		fileMetaType = Isds::Type::FMT_ENCLOSURE;
	}

	/* Set total attachment size in KB from attachment model. */
	envelope.setDmAttachmentSize(attachModel->dataSizeSum() / 1024);

	/* List of unique identifiers. */
	QStringList taskIdentifiers;
	const QDateTime currentTime(QDateTime::currentDateTimeUtc());

	/*
	 * Generate unique identifiers.
	 * These must be complete before creating first task.
	 */
	foreach (const DataboxModelEntry &db, dbList) {
		taskIdentifiers.append(acntId.username() + "_"
		    + db.dbID() + "_"
		    + currentTime.toString() + "_"
		    + Utility::generateRandomString(6));
	}

#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
	transactIds = QSet<QString>(taskIdentifiers.begin(), taskIdentifiers.end());
#else /* < Qt-5.14.0 */
	transactIds = taskIdentifiers.toSet();
#endif /* >= Qt-5.14.0 */

	Isds::Message message;
	message.setDocuments(macroStdMove(documents));

	/* Send message to all recipients. */
	for (int i = 0; i < dbList.size(); ++i) {
		const DataboxModelEntry &db(dbList.at(i));
		envelope.setDbIDRecipient(db.dbID());
		envelope.setDmRecipient(db.dbName());
		envelope.setDmRecipientAddress(db.dbAddress());
		envelope.setDmType(Isds::Envelope::dmType2Char(db.dmType()));
		message.setEnvelope(envelope);
		TaskSendMessage *task = new (::std::nothrow) TaskSendMessage(
		    acntId, message, taskIdentifiers.at(i));
		if (task != Q_NULLPTR) {
			task->setAutoDelete(true);
			GlobInstcs::workPoolPtr->assignHi(task);
		}
	}
}

bool Isds::Tasks::sendSMS(const AcntId &acntId, const QString &oldPwd)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	TaskSendSMS *task = new (::std::nothrow) TaskSendSMS(acntId, oldPwd);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return false;
	}
	task->setAutoDelete(false);
	GlobInstcs::workPoolPtr->runSingle(task);
	bool success = TaskSendSMS::DL_SUCCESS == task->m_result;
	delete task; task = Q_NULLPTR;
	return success;
}

#define ANY_EXCEPT_VAULT \
	(Isds::Type::MFS_POSTED | Isds::Type::MFS_STAMPED | \
	 Isds::Type::MFS_INFECTED | Isds::Type::MFS_DELIVERED | \
	 Isds::Type::MFS_ACCEPTED_FICT | Isds::Type::MFS_ACCEPTED | \
	 Isds::Type::MFS_READ | Isds::Type::MFS_UNDELIVERABLE | \
	 Isds::Type::MFS_REMOVED)
/*!
 * @brief Maximum length of message list to be downloaded.
 */
#define MESSAGE_LIST_LIMIT 100000

void Isds::Tasks::syncSingleAccount(const AcntId &acntId,
    enum Messages::MessageType msgDirect)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	TaskDownloadMessageList *task =
	    new (::std::nothrow) TaskDownloadMessageList(acntId, msgDirect,
	    GlobalSettingsQmlWrapper::downloadOnlyNewMsgs() ?
	         ANY_EXCEPT_VAULT : Isds::Type::MFS_ANY,
	    1, MESSAGE_LIST_LIMIT, GlobInstcs::workPoolPtr,
	    GlobalSettingsQmlWrapper::downloadCompleteMsgs(),
	    (GlobalSettingsQmlWrapper::zfoDbSizeMBs() > 0), acntId.testing());
	if (task != Q_NULLPTR) {
		task->setAutoDelete(true);
		GlobInstcs::workPoolPtr->assignHi(task);
	}
}

void Isds::Tasks::downloadPdzInfo(const AcntId &acntId,
    const QString &dbID)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR) ||
		dbID.isEmpty())) {
		Q_ASSERT(0);
		return;
	}

	TaskPDZInfo *task =
	    new (::std::nothrow) TaskPDZInfo(acntId, dbID);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}
	task->setAutoDelete(true);
	GlobInstcs::workPoolPtr->assignHi(task);
}

QList<Isds::CreditEvent> Isds::Tasks::downloadCreditInfo(const AcntId &acntId,
    const QString &dbID, const QDate &fromDate, const QDate &toDate,
    qint64 &currentCredit, QString &notifEmail)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::workPoolPtr == Q_NULLPTR) ||
		dbID.isEmpty())) {
		Q_ASSERT(0);
		return QList<CreditEvent>();
	}

	TaskCreditInfo *task =
	    new (::std::nothrow) TaskCreditInfo(acntId, dbID, fromDate, toDate);
	if (Q_UNLIKELY(task == Q_NULLPTR)) {
		Q_ASSERT(0);
		return QList<CreditEvent>();
	}
	task->setAutoDelete(false);
	GlobInstcs::workPoolPtr->runSingle(task);
	currentCredit = task->m_currentCredit;
	notifEmail = task->m_notifEmail;
	QList<CreditEvent> creditEvents(task->m_creditEvents);
	delete task; task = Q_NULLPTR;

	return creditEvents;
}

