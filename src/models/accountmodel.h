/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAbstractListModel>
#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"

class AccountsMap; /* Forward declaration. */
class AcntData; /* Forward declaration. */
class QSettings; /* Forward declaration. */

/*!
 * @brief Account hierarchy.
 */
class AccountListModel : public QAbstractListModel {
	Q_OBJECT

public:
	/*!
	 * @brief Roles which this model supports.
	 */
	enum Roles {
		ROLE_ACCOUNT_NAME = Qt::UserRole,
		ROLE_USER_NAME,
		ROLE_TEST_ACCOUNT,
		ROLE_STORE_INTO_DB,
		ROLE_RECEIVED_UNREAD,
		ROLE_RECEIVED_TOTAL,
		ROLE_SENT_UNREAD,
		ROLE_SENT_TOTAL
	};
	Q_ENUM(Roles)

	/*!
	 * @brief Return add account result.
	 */
	enum AddAcntResult {
		AA_SUCCESS = 0, /*!< Operation was successful. */
		AA_EXISTS, /*!< Account already exists in the model. */
		AA_ERR /*!< Internal error. */
	};

	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Pointer to parent object.
	 */
	explicit AccountListModel(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Copy constructor.
	 *
	 * @note Needed for QVariant conversion.
	 *
	 * @param[in] model Model to be copied.
	 * @param[in] parent Pointer to parent object.
	 */
	explicit AccountListModel(const AccountListModel &model,
	    QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Set account map to be the source of data for the model.
	 *
	 * @param[in] accounts Pointer to account map.
	 */
	void setAccounts(AccountsMap *accounts);

	/*!
	 * @brief Return number of rows under the given parent.
	 *
	 * @param[in] parent Parent node index.
	 * @return Number of rows.
	 */
	virtual
	int rowCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the model's role names.
	 *
	 * @return Model's role names.
	 */
	virtual
	QHash<int, QByteArray> roleNames(void) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Return data stored in given location under given role.
	 *
	 * @param[in] index Index specifying the item.
	 * @param[in] role  Data role.
	 * @return Data from model.
	 */
	virtual
	QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns item flags for given index.
	 *
	 * @brief[in] index Index specifying the item.
	 * @return Item flags.
	 */
	virtual
	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Load data from supplied settings.
	 *
	 * @param[in] settings Settings structure to be read.
	 */
	void loadAccountsFromSettings(const QSettings &settings);

	/*!
	 * @brief Store data to settings structure.
	 *
	 * @param[in]  pinVal PIN value to encrypt passwords with.
	 * @param[out] settings Setting structure to store the model content
	 *     into.
	 */
	void saveAccountsToSettings(const QString &pinVal,
	    QSettings &settings) const;

	/*!
	 * @brief Add account.
	 *
	 * @patam[in]  acntSettings Settings data to be added into the model.
	 * @param[out] idx Index of newly added account if specified.
	 * @return Error state.
	 */
	enum AddAcntResult addAccount(const AcntData &acntData,
	    QModelIndex *idx = Q_NULLPTR);

	/*!
	 * @brief Delete account.
	 *
	 * @param[in] acntId Account identifier.
	 */
	void deleteAccount(const AcntId &acntId);

	/*!
	 * @brief Returns account identifier for given node.
	 *
	 * @param[in] index Data index.
	 * @return Account identifier of the account which the node belongs to
	 *     or invalid value on error.
	 */
	AcntId acntId(const QModelIndex &index) const;

	/*!
	 * @brief Returns node related to user name.
	 *
	 * @param[in] acntId Account identifier.
	 * @return Top node index or invalid index if no such name found.
	 */
	QModelIndex acntIndex(const AcntId &acntId) const;

	/*!
	 * @brief Updates counters that are displayed by the model.
	 *
	 * @note Negative values are ignored.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] recNew   Received new.
	 * @param[in] recTot   Received total.
	 * @param[in] sntNew   Sent new.
	 * @param[in] sntTot   Sent total.
	 */
	void updateCounters(const AcntId &acntId, int recNew, int recTot,
	    int sntNew, int sntTot);

	/*!
	 * @brief Converts QVariant (obtained from QML) into a pointer.
	 *
	 * @note Some weird stuff happens in QML when passing instances
	 *     directly as constant reference. Wrong constructors are called
	 *     and no data are passed.
	 * @note QML passes objects (which were created in QML) as QVariant
	 *     values holding pointers. You therefore may call invokable methods
	 *     with QVariant arguments from QML.
	 * @note If you use
	 *     qRegisterMetaType<Type *>("Type *") and
	 *     qRegisterMetaType<Type *>("const Type *")
	 *     then QML will be able to call invokable methods without explicit
	 *     conversion from QVariant arguments.
	 *     Q_DECLARE_METATYPE(Type *) is not needed.
	 *
	 * @param[in] variant QVariant holding the pointer.
	 * @return Pointer if it could be acquired, Q_NULLPTR else. This
	 *     function does not allocate a new instance.
	 */
	static
	AccountListModel *fromVariant(const QVariant &modelVariant);

private slots:
	/*!
	 * @brief This slot handles changes of account data.
	 *
	 * @param[in] acntId Account identifier.
	 */
	void handleAccountDataChange(const AcntId &acntId);

private:
	QList<AcntId> m_acntIds; /*!<
	                          * List of account identifiers,
	                          * defines the ordering of the model.
	                          */
	AccountsMap *m_accountsPtr; /*!< Pointer to account data container. */
};

/* QML passes its arguments via QVariant. */
Q_DECLARE_METATYPE(AccountListModel)
Q_DECLARE_METATYPE(AccountListModel::Roles)
