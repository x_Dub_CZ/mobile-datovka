/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAbstractListModel>
#include <QSqlQuery>

class MessageModelEntry {
public:
	MessageModelEntry(const MessageModelEntry &other);
	MessageModelEntry(qint64 dmId, const QString &userName, bool testing,
	    const QString &from, const QString &to,
	    const QString &annotation, const QString &dTime,
	    const QString &aTime, bool readLocally, bool attachmentsDownloaded,
	    bool recordsManagement, int msgType, int daysToDeletion);

	MessageModelEntry &operator=(const MessageModelEntry &other) Q_DECL_NOTHROW;

	qint64 dmId(void) const;
	QString userName(void) const;
	void setUserName(const QString &userName);
	bool testing(void) const;
	void setTesting(bool testing);
	QString from(void) const;
	void setFrom(const QString &from);
	QString to(void) const;
	void setTo(const QString &to);
	QString annotation(void) const;
	void setAnnotation(const QString &annotation);
	QString deliveryTime(void) const;
	void setDeliveryTime(const QString &dTime);
	QString acceptanceTime(void) const;
	void setAcceptanceTime(const QString &aTime);
	bool readLocally(void) const;
	void setReadLocally(bool readLocally);
	bool attachmentsDownloaded(void) const;
	void setAttachmentsDownloaded(bool attachmentsDownloaded);
	bool recordsManagement(void) const;
	void setSecordsManagement(bool recordsManagement);
	int msgType(void) const;
	void setMsgType(int msgType);
	int daysToDeletion(void) const;
	void setDaysToDeletion(int daysToDeletion);

private:
	qint64 m_dmId; /*!< Message identifier. */
	QString m_userName; /*!< Account user name of messages. */
	bool m_testing; /*!< True if account is testing. */
	QString m_from; /*!< From for received messages. */
	QString m_to; /*!< To for sent messages. */
	QString m_annotation; /*!< Message subject. */
	QString m_deliveryTime; /*!< Delivery time. */
	QString m_acceptanceTime; /*!< Acceptance time. */
	bool m_readLocally; /*!< Message has been viewed. */
	bool m_attachmentsDownloaded; /*!< Attachments downloaded. */
	bool m_recordsManagement; /*!< Message is uploaded in records management. */
	int m_msgType; /*!< Message orientation /sent/received/. */
	int m_daysToDeletion; /*!< Number of left days until isds deletion. */
};

class MessageListModel : public QAbstractListModel {
	Q_OBJECT

public:
	/*!
	 * @brief Roles which this model supports.
	 */
	enum Roles {
		ROLE_MSG_ID = Qt::UserRole,
		ROLE_USERNAME,
		ROLE_TESTING,
		ROLE_FROM,
		ROLE_TO,
		ROLE_ANNOTATION,
		ROLE_DELIVERY_TIME,
		ROLE_ACCEPTANCE_TIME,
		ROLE_READ_LOCALLY,
		ROLE_ATTACHMENTS_DOWNLOADED,
		ROLE_RECORDS_MANAGEMENT,
		ROLE_MSG_TYPE,
		ROLE_MSG_DAYS_TO_ISDS_DELETION
	};
	Q_ENUM(Roles)

	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Pointer to parent object.
	 */
	explicit MessageListModel(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Copy constructor.
	 *
	 * @note Needed for QVariant conversion.
	 *
	 * @param[in] model Model to be copied.
	 * @param[in] parent Pointer to parent object.
	 */
	explicit MessageListModel(const MessageListModel &model,
	    QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Return number of rows under the given parent.
	 *
	 * @param[in] parent Parent node index.
	 * @return Number of rows.
	 */
	virtual
	int rowCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the model's role names.
	 *
	 * @return Model's role names.
	 */
	virtual
	QHash<int, QByteArray> roleNames(void) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Return data stored in given location under given role.
	 *
	 * @param[in] index Index specifying the item.
	 * @param[in] role  Data role.
	 * @return Data from model.
	 */
	virtual
	QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns item flags for given index.
	 *
	 * @brief[in] index Index specifying the item.
	 * @return Item flags.
	 */
	virtual
	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Delete message.
	 *
	 * @param[in] dmId Message identifier.
	 */
	void deleteMessage(qint64 dmId);

	/*!
	 * @brief Sets the content of the model according to the supplied query.
	 *
	 * @param[in] userName Account user name.
	 * @param[in] testing True if account is testing.
	 * @param[in,out] query SQL query result.
	 * @param[in] isAppend True if we can append other queries into model.
	 *                   False means that model will clear before append.
	 * @return Number of items in the model.
	 */
	int setQuery(const QString &userName, bool testing, QSqlQuery &query,
	     bool isAppend);

	/*!
	 * @brief Returns index of message with given id.
	 *
	 * @param[in] dmId Message identifier.
	 * @return Invalid index if not found.
	 */
	QModelIndex messageIndex(qint64 dmId) const;

	/*!
	 * @brief Clears the model.
	 */
	void clearAll(void);

	/*!
	 * @brief Removes a message from the model.
	 *
	 * @param[in] dmId Message identifier.
	 */
	void removeMessage(qint64 dmId);

	/*!
	 * @brief Override message as being read.
	 *
	 * @note Emits dataChanged signal.
	 *
	 * @param[in] dmId      Message id.
	 * @param[in] forceRead Set whether to force read state.
	 * @return True on success.
	 */
	bool overrideRead(qint64 dmId, bool forceRead);

	/*!
	 * @brief Override messages as being read.
	 *
	 * @note Emits dataChanged signal.
	 *
	 * @param[in] forceRead Set whether to force read state.
	 * @return True on success.
	 */
	bool overrideReadAll(bool forceRead);

	/*!
	 * @brief Override message as having its attachments having downloaded.
	 *
	 * note Emits dataChanged signal.
	 *
	 * @param[in] dmId            Message id.
	 * @param[in] forceDownloaded Set whether to force attachments
	 *                            downloaded state.
	 * @return True on success.
	 */
	bool overrideDownloaded(qint64 dmId, bool forceDownloaded);

	/*!
	 * @brief Converts QVariant (obtained from QML) into a pointer.
	 *
	 * @note Some weird stuff happens in QML when passing instances
	 *     directly as constant reference. Wrong constructors are called
	 *     and no data are passed.
	 * @note QML passes objects (which were created in QML) as QVariant
	 *     values holding pointers. You therefore may call invokable methods
	 *     with QVariant arguments from QML.
	 * @note If you use
	 *     qRegisterMetaType<Type *>("Type *") and
	 *     qRegisterMetaType<Type *>("const Type *")
	 *     then QML will be able to call invokable methods without explicit
	 *     conversion from QVariant arguments.
	 *     Q_DECLARE_METATYPE(Type *) is not needed.
	 *
	 * @param[in] variant QVariant holding the pointer.
	 * @return Pointer if it could be acquired, Q_NULLPTR else. This
	 *     function does not allocate a new instance.
	 */
	static
	MessageListModel *fromVariant(const QVariant &modelVariant);

	/*!
	 * @brief Update records management icon after message upload.
	 *
	 * @note Emits dataChanged signal.
	 *
	 * @param[in] dmId       Message id.
	 * @param[in] isUploadRm Set whether to force records management
	 *                       upload state.
	 * @return True on success.
	 */
	bool updateRmStatus(qint64 dmId, bool isUploadRm);

private:

	QList<MessageModelEntry> m_messages; /*!< List of messages stored. */
};

/* QML passes its arguments via QVariant. */
Q_DECLARE_METATYPE(MessageListModel)
Q_DECLARE_METATYPE(MessageListModel::Roles)
