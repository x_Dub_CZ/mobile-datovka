/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAbstractListModel>
#include <QObject>
#include <QSqlQuery>

#include "src/datovka_shared/isds/types.h"

class DataboxModelEntry : public QObject {
	Q_OBJECT
	Q_PROPERTY(QString dbID READ dbID WRITE setDbID NOTIFY dbIDChanged)
	Q_PROPERTY(QString dbType READ dbType WRITE setDbType NOTIFY dbTypeChanged)
	Q_PROPERTY(QString dbName READ dbName WRITE setDbName NOTIFY dbNameChanged)
	Q_PROPERTY(QString dbAddress READ dbAddress WRITE setDbAddress NOTIFY dbAddressChanged)
	Q_PROPERTY(QString dbIC READ dbIC WRITE setDbIC NOTIFY dbICChanged)
	Q_PROPERTY(QString dbSendOptions READ dbSendOptions WRITE setDbSendOptions NOTIFY dbSendOptionsChanged)

public:
	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit DataboxModelEntry(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Copy constructor.
	 *
	 * @note Needed for QVariant conversion.
	 *
	 * @param[in] dme Entry to be copied.
	 */
	DataboxModelEntry(const DataboxModelEntry &dme);

	/*!
	 * @brief Constructor.
	 */
	DataboxModelEntry(const QString &dbID, const QString &dbType,
	    const QString &dbName, const QString &dbAddress, const QString &dbIC,
	    const QString &dbSendOptions, enum Isds::Type::NilBool pdz,
	    enum Isds::Type::DmType dmType,
	    const QList<enum Isds::Type::DmType> &dmTypes);

	/*!
	 * @brief Assignment operator.
	 *
	 * @param[in] entry Source entry.
	 * @return Reference to itself.
	 */
	DataboxModelEntry &operator=(const DataboxModelEntry &entry);

	QString dbID(void) const;
	void setDbID(const QString &dbID);

	QString dbType(void) const;
	void setDbType(const QString &dbType);

	QString dbName(void) const;
	void setDbName(const QString &dbName);

	QString dbAddress(void) const;
	void setDbAddress(const QString &dbAddress);

	QString dbIC(void) const;
	void setDbIC(const QString &dbIC);

	QString dbSendOptions(void) const;
	void setDbSendOptions(const QString &dbSendOptions);

	enum Isds::Type::NilBool pdz(void) const;
	void setPdz(enum Isds::Type::NilBool pdz);

	enum Isds::Type::DmType dmType(void) const;
	void setDmType(enum Isds::Type::DmType dmType);

	QList<enum Isds::Type::DmType> dmTypes(void) const;
	void setDmTypes(const QList<enum Isds::Type::DmType> &dmTypes);

	/*!
	 * @brief Converts QVariant (obtained from QML) into a pointer.
	 *
	 * @note Some weird stuff happens in QML when passing instances
	 *     directly as constant reference. Wrong constructors are called
	 *     and no data are passed.
	 * @note QML passes objects (which were created in QML) as QVariant
	 *     values holding pointers. You therefore may call invokable methods
	 *     with QVariant arguments from QML.
	 * @note If you use
	 *     qRegisterMetaType<Type *>("Type *") and
	 *     qRegisterMetaType<Type *>("const Type *")
	 *     then QML will be able to call invokable methods without explicit
	 *     conversion from QVariant arguments.
	 *     Q_DECLARE_METATYPE(Type *) is not needed.
	 *
	 * @param[in] variant QVariant holding the pointer.
	 * @return Pointer if it could be acquired, Q_NULLPTR else. This
	 *     function does not allocate a new instance.
	 */
	static
	DataboxModelEntry *fromVariant(const QVariant &entryVariant);

signals:
	void dbIDChanged(const QString &newDbID);
	void dbTypeChanged(const QString &newDbType);
	void dbNameChanged(const QString *newDbName);
	void dbAddressChanged(const QString &newDbAddress);
	void dbICChanged(const QString &newDbIC);
	void dbSendOptionsChanged(const QString &newDbSendOptions);

private:
	QString m_dbID; /*!< Databox identifier. */
	QString m_dbType; /*!< Databox type. */
	QString m_dbName; /*!< Databox owner name. */
	QString m_dbAddress; /*!< Databox owner address. */
	QString m_dbIC; /*!< Databox owner IC. */
	QString m_dbSendOptions; /*!< Flag send option (see IDSD documentation). */
	enum Isds::Type::NilBool m_pdz; /*!< True if message is commercial. */
	enum Isds::Type::DmType m_dmType; /*!< Commercial message type. */
	QList<enum Isds::Type::DmType> m_dmTypes; /*!< Commercial message type list. */
};

/* QML passes its arguments via QVariant. */
Q_DECLARE_METATYPE(DataboxModelEntry)

class DataboxListModel : public QAbstractListModel {
	Q_OBJECT

public:
	/*!
	 * @brief Roles which this model supports.
	 */
	enum Roles {
		ROLE_DB_ID = Qt::UserRole,
		ROLE_DB_TYPE,
		ROLE_DB_NAME,
		ROLE_DB_ADDRESS,
		ROLE_DB_IC,
		ROLE_DB_SEND_OPTION,
		ROLE_PDZ,
		ROLE_PDZ_DMTYPE,
		ROLE_PDZ_DMTYPE_LIST,
		ROLE_DB_SELECTED /* TODO -- See whether ItemSelectionModel can be used instead. */
	};
	Q_ENUM(Roles)

	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Pointer to parent object.
	 */
	explicit DataboxListModel(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Copy constructor.
	 *
	 * @note Needed for QVariant conversion.
	 *
	 * @param[in] model Model to be copied.
	 * @param[in] parent Pointer to parent object.
	 */
	explicit DataboxListModel(const DataboxListModel &model,
	    QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Return number of rows under the given parent.
	 *
	 * @param[in] parent Parent node index.
	 * @return Number of rows.
	 */
	virtual
	int rowCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the model's role names.
	 *
	 * @return Model's role names.
	 */
	virtual
	QHash<int, QByteArray> roleNames(void) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Return data stored in given location under given role.
	 *
	 * @param[in] index Index specifying the item.
	 * @param[in] role Data role.
	 * @return Data from model.
	 */
	virtual
	QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Return list of all entries.
	 *
	 * @return List of all entries.
	 */
	QList<DataboxModelEntry> allEntries(void) const;

	/*!
	 * @brief Add data-box entry into model.
	 *
	 * @param[in] entry Data-box entry data.
	 * @return True if data were added.
	 */
	bool addEntry(const DataboxModelEntry &entry);

	/*!
	 * @brief Add data-box entry into model.
	 *
	 * @param[in] entry Data-box entry data.
	 * @return True if data were added.
	 */
	Q_INVOKABLE
	bool addEntry(const DataboxModelEntry *entry);

	/*!
	 * @brief Remove model entry.
	 *
	 * @param[in] boxId Data box identifier of the removed box entry.
	 * @return True if entry was really removed.
	 */
	Q_INVOKABLE
	bool removeEntry(const QString &boxId);

	/*!
	 * @brief Return entry at given row.
	 *
	 * @note http://doc.qt.io/qt-5/qtqml-cppintegration-data.html#data-ownership
	 *     Use this method only inside QML code.
	 *
	 * @param[in] boxId Data box identifier.
	 * @return Newly allocated model entry.
	 */
	Q_INVOKABLE
	DataboxModelEntry *entry(const QString &boxId) const;

	/*!
	 * @brief Assign selection state to box entry.
	 *
	 * @param[in] boxId Data box identifier.
	 * @param[in] selected Selection state.
	 * @return True if selection state was successfully set.
	 */
	Q_INVOKABLE
	bool selectEntry(const QString &boxId, bool selected);

	/*!
	 * @brief Assign selection state for box entries.
	 *
	 * @param[in] boxIds Data box identifiers.
	 * @param[in] selected Selection state.
	 * @return true if at least one selection state was successfully set.
	 */
	Q_INVOKABLE
	bool selectEntries(const QStringList &boxIds, bool selected);

	/*!
	 * @brief Return data box identifiers held within the model.
	 *
	 * @return List of data box identifiers.
	 */
	Q_INVOKABLE
	QStringList boxIds(void) const;

	/*!
	 * @brief Sets the content of the model according to the supplied query.
	 *
	 * @param[in,out] query SQL query result.
	 * @param[in] dbId Account databox ID.
	 * @param[in] isAppend True if we can append other queries into model.
	 *                   False means that model will clear before append.
	 * @return Number of items in the model.
	 */
	int setQuery(QSqlQuery &query, const QString &dbId, bool isAppend);

	/*!
	 * @brief Set PDZ payment type.
	 *
	 * @param[in] boxId Data box identifier of entry.
	 * @param[in] dmType PDZ payment type.
	 */
	Q_INVOKABLE
	void setPdzDmType(const QString &boxId, int dmType);

	/*!
	 * @brief Returns item flags for given index.
	 *
	 * @brief[in] index Index specifying the item.
	 * @return Item flags.
	 */
	virtual
	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Clears the model.
	 */
	void clearAll(void);

	/*!
	 * @brief Converts QVariant (obtained from QML) into a pointer.
	 *
	 * @note Some weird stuff happens in QML when passing instances
	 *     directly as constant reference. Wrong constructors are called
	 *     and no data are passed.
	 * @note QML passes objects (which were created in QML) as QVariant
	 *     values holding pointers. You therefore may call invokable methods
	 *     with QVariant arguments from QML.
	 * @note If you use
	 *     qRegisterMetaType<Type *>("Type *") and
	 *     qRegisterMetaType<Type *>("const Type *")
	 *     then QML will be able to call invokable methods without explicit
	 *     conversion from QVariant arguments.
	 *     Q_DECLARE_METATYPE(Type *) is not needed.
	 *
	 * @param[in] variant QVariant holding the pointer.
	 * @return Pointer if it could be acquired, Q_NULLPTR else. This
	 *     function does not allocate a new instance.
	 */
	static
	DataboxListModel *fromVariant(const QVariant &modelVariant);

private:
	/*!
	 * @brief Convenience structure.
	 */
	class InternalEntry {
	public:
		InternalEntry(void)
		    : entry(), selected(false)
		{
		}

		InternalEntry(const DataboxModelEntry &e, bool s)
		    : entry(e), selected(s)
		{
		}

		DataboxModelEntry entry; /*!< Sata box entry. */
		bool selected; /*!< Selection information. */
	};

	QList<QString> m_boxIds; /*!< List of box identifiers (map keys). */
	QMap<QString, InternalEntry> m_entries; /*!< Data box entries. */
};

/* QML passes its arguments via QVariant. */
Q_DECLARE_METATYPE(DataboxListModel)
Q_DECLARE_METATYPE(DataboxListModel::Roles)
