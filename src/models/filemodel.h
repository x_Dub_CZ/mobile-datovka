/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAbstractListModel>
#include <QSqlQuery>

class QmlAcntId; /* Forward declaration. */

class FileListModel : public QAbstractListModel {
	Q_OBJECT

public:
	class Entry {
	public:
		Entry(const Entry &other);
		Entry(int fileId, const QString &fileName,
		    const QByteArray &data, qint64 fileSize,
		    const QString &filePath);

		Entry &operator=(const Entry &other) Q_DECL_NOTHROW;

		int fileId(void) const;
		const QString &fileName(void) const;
		void setFileName(const QString &fileName);
		QString fileSizeStr(void) const;
		const QByteArray &binaryContent(void) const;
		void setBinaryContent(const QByteArray &data);
		qint64 fileSize(void) const;
		void setFileSize(qint64 fileSize);
		const QString &filePath(void) const;
		void setFilePath(const QString &filePath);

	private:
		int m_fileId; /*!< File database identifier. */
		QString m_fileName; /*!< File name. */
		QByteArray m_data; /*!< Binary file content. */
		qint64 m_fileSize; /*!< Binary size. */
		QString m_filePath; /*!< File path. */
	};

	/*!
	 * @brief Roles which this model supports.
	 */
	enum Roles {
		ROLE_FILE_ID = Qt::UserRole,
		ROLE_FILE_NAME,
		ROLE_FILE_SIZE_STR,
		ROLE_BINARY_DATA,
		ROLE_FILE_SIZE_BYTES,
		ROLE_FILE_PATH
	};
	Q_ENUM(Roles)

	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Pointer to parent object.
	 */
	explicit FileListModel(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Copy constructor.
	 *
	 * @note Needed for QVariant conversion.
	 *
	 * @param[in] model Model to be copied.
	 * @param[in] parent Pointer to parent object.
	 */
	explicit FileListModel(const FileListModel &model,
	    QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Return number of rows under the given parent.
	 *
	 * @param[in] parent Parent node index.
	 * @return Number of rows.
	 */
	virtual
	int rowCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the model's role names.
	 *
	 * @return Model's role names.
	 */
	virtual
	QHash<int, QByteArray> roleNames(void) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Return data stored in given location under given role.
	 *
	 * @param[in] index Index specifying the item.
	 * @param[in] role  Data role.
	 * @return Data from model.
	 */
	virtual
	QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Return list of all entries.
	 *
	 * @return List of all entries.
	 */
	QList<FileListModel::Entry> allEntries(void) const;

	/*!
	 * @brief Return file path from attachment model.
	 *
	 * @param[in] row Row specifying the item.
	 * @return File path string.
	 */
	Q_INVOKABLE
	QString filePathFromRow(int row) const;

	/*!
	 * @brief Compute the sum of sizes of all data.
	 *
	 * @return Real file size in bytes.
	 */
	Q_INVOKABLE
	qint64 dataSizeSum(void) const;

	/*!
	 * @brief Returns item flags for given index.
	 *
	 * @brief[in] index Index specifying the item.
	 * @return Item flags.
	 */
	virtual
	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Set the content of the model according to the supplied query.
	 *
	 * @param[in,out] query SQL query result.
	 */
	void setQuery(QSqlQuery &query);

	/*!
	 * @brief Appends entry line to model.
	 *
	 * @param[in] entry Entry to append to model data.
	 */
	void appendFileEntry(const Entry &entry);

	/*!
	 * @brief Appends file from path to model.
	 *
	 * @param[in] fileId File id.
	 * @param[in] fileName File name.
	 * @param[in] filePath File path.
	 * @param[in] fileSizeBytes Real file size in bytes.
	 */
	Q_INVOKABLE
	void appendFileFromPath(int fileId, const QString &fileName,
	    const QString &filePath, qint64 fileSizeBytes);

	/*!
	 * @brief Clears the model.
	 */
	Q_INVOKABLE
	void clearAll(void);

	/*!
	 * @brief Converts QVariant (obtained from QML) into a pointer.
	 *
	 * @note Some weird stuff happens in QML when passing instances
	 *     directly as constant reference. Wrong constructors are called
	 *     and no data are passed.
	 * @note QML passes objects (which were created in QML) as QVariant
	 *     values holding pointers. You therefore may call invokable methods
	 *     with QVariant arguments from QML.
	 * @note If you use
	 *     qRegisterMetaType<Type *>("Type *") and
	 *     qRegisterMetaType<Type *>("const Type *")
	 *     then QML will be able to call invokable methods without explicit
	 *     conversion from QVariant arguments.
	 *     Q_DECLARE_METATYPE(Type *) is not needed.
	 *
	 * @param[in] variant QVariant holding the pointer.
	 * @return Pointer if it could be acquired, Q_NULLPTR else. This
	 *     function does not allocate a new instance.
	 */
	static
	FileListModel *fromVariant(const QVariant &modelVariant);

	/*!
	 * @brief Set content from database.
	 *
	 * @note QML has no notion about qint64, therefore the id is passed
	 *     via a string.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgIdStr String containing message identifier.
	 * @return True on success.
	 */
	Q_INVOKABLE
	bool setFromDb(const QmlAcntId *qAcntId, const QString &msgIdStr);

	/*!
	 * @brief Remove file from attachment model.
	 *
	 * @param[in] row Row specifying the item.
	 */
	Q_INVOKABLE
	void removeItem(int row);

private:
	QList<Entry> m_files; /*!< List of attachment entries. */
};

/* QML passes its arguments via QVariant. */
Q_DECLARE_METATYPE(FileListModel)
Q_DECLARE_METATYPE(FileListModel::Roles)
