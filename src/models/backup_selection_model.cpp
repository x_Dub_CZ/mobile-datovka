/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QQmlEngine> /* qmlRegisterType */

#include "src/models/backup_selection_model.h"

void BackupRestoreSelectionModel::declareQML(void)
{
	qmlRegisterType<BackupRestoreSelectionModel>("cz.nic.mobileDatovka.models", 1, 0, "BackupRestoreSelectionModel");
	qRegisterMetaType<BackupRestoreSelectionModel>("BackupRestoreSelectionModel");
	qRegisterMetaType<BackupRestoreSelectionModel::Roles>("BackupRestoreSelectionModel::Roles");

	qRegisterMetaType<BackupRestoreSelectionModel *>("BackupRestoreSelectionModel *");
	qRegisterMetaType<BackupRestoreSelectionModel *>("const BackupRestoreSelectionModel *");
}

BackupRestoreSelectionModel::BackupRestoreSelectionModel(QObject *parent)
    : QAbstractListModel(parent),
    m_entries(),
    m_numSelected(0)
{
}

BackupRestoreSelectionModel::BackupRestoreSelectionModel(
    const BackupRestoreSelectionModel &model,
    QObject *parent)
    : QAbstractListModel(parent),
    m_entries(model.m_entries),
    m_numSelected(model.m_numSelected)
{
}

int BackupRestoreSelectionModel::rowCount(const QModelIndex &parent) const
{
	return !parent.isValid() ? m_entries.size() : 0;
}

QHash<int, QByteArray> BackupRestoreSelectionModel::roleNames(void) const
{
	static QHash<int, QByteArray> roles;
	if (roles.isEmpty()) {
		roles[ROLE_ACCOUNT_NAME] = "rAcntName";
		roles[ROLE_USERNAME] = "rUserName";
		roles[ROLE_TESTING] = "rTestAccount";
		roles[ROLE_BOX_ID] = "rBoxId";
		roles[ROLE_SELECTED] = "rSelected";
		roles[ROLE_SUBDIR] = "rSubdir";
	}
	return roles;
}

QVariant BackupRestoreSelectionModel::data(const QModelIndex &index,
    int role) const
{
	if (Q_UNLIKELY((index.row() < 0) || (index.row() >= m_entries.size()))) {
		return QVariant();
	}

	const Entry &entry(m_entries.at(index.row()));

	switch (role) {
	case ROLE_ACCOUNT_NAME:
		return entry.accountName;
		break;
	case ROLE_USERNAME:
		return entry.acntId.username();
		break;
	case ROLE_TESTING:
		return entry.acntId.testing();
		break;
	case ROLE_BOX_ID:
		return entry.boxId;
		break;
	case ROLE_SELECTED:
		return entry.selected;
		break;
	case ROLE_SUBDIR:
		return entry.subdir;
		break;
	default:
		break;
	}

	return QVariant();
}

Qt::ItemFlags BackupRestoreSelectionModel::flags(const QModelIndex &index) const
{
	return QAbstractListModel::flags(index);
}

void BackupRestoreSelectionModel::appendData(bool selected,
    const QString &acntName, const AcntId &acntId, const QString &dbId,
    const QString &subdir)
{
	Entry entry(selected, acntName, acntId, dbId, subdir);

	beginInsertRows(QModelIndex(), rowCount(), rowCount());

	m_entries.append(entry);
	if (selected) {
		++m_numSelected;
	}

	endInsertRows();
}

void BackupRestoreSelectionModel::setSelected(int row, bool select)
{
	if (Q_UNLIKELY((row < 0) || (row >= rowCount()))) {
		Q_ASSERT(0);
		return;
	}

	if (m_entries[row].selected != select) {
		m_entries[row].selected = select;
		if (select) {
			++m_numSelected;
		} else {
			--m_numSelected;
		}
		QModelIndex changedIdx(index(row, 0, QModelIndex()));
		emit dataChanged(changedIdx, changedIdx);
	}
}

int BackupRestoreSelectionModel::numSelected(void) const
{
	return m_numSelected;
}

void BackupRestoreSelectionModel::setAllSelected(bool select)
{
	int top = -1;
	int bottom = -1;

	for (int row = 0; row < m_entries.size(); ++row) {
		Entry &e(m_entries[row]);
		if (e.selected != select) {
			if (top < 0) {
				top = row;
			}
			bottom = row;
			m_entries[row].selected = select;
		}
	}
	m_numSelected = select ? rowCount() : 0;

	if (top >= 0) {
		if (Q_UNLIKELY((bottom < 0) || bottom >= m_entries.size())) {
			Q_ASSERT(0);
		}

		emit dataChanged(index(top, 0), index(bottom, 0));
	}
}

void BackupRestoreSelectionModel::clearAll(void)
{
	beginResetModel();
	m_entries.clear();
	m_numSelected = 0;
	endResetModel();
}

QList<AcntId> BackupRestoreSelectionModel::selectedAccountIds(void) const
{
	QList<AcntId> ids;
	for (int row = 0; row < m_entries.size(); ++row) {
		if (m_entries[row].selected) {
			ids.append(m_entries[row].acntId);
		}
	}
	return ids;
}

BackupRestoreSelectionModel::Entry BackupRestoreSelectionModel::accountEntry(
    const AcntId &acntId) const
{
	for (int row = 0; row < m_entries.size(); ++row) {
		if (m_entries[row].acntId == acntId) {
			return m_entries[row];
		}
	}
	return Entry();
}

BackupRestoreSelectionModel *BackupRestoreSelectionModel::fromVariant(
    const QVariant &modelVariant)
{
	if (!modelVariant.canConvert<QObject *>()) {
		return Q_NULLPTR;
	}
	QObject *obj = qvariant_cast<QObject *>(modelVariant);
	return qobject_cast<BackupRestoreSelectionModel *>(obj);
}
