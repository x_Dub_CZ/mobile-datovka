/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QQmlEngine> /* qmlRegisterType */
#include <QSqlRecord>

#include "src/datovka_shared/io/records_management_db.h"
#include "src/global.h"
#include "src/models/messagemodel.h"
#include "src/sqlite/dbs.h"

MessageModelEntry::MessageModelEntry(const MessageModelEntry &other)
    : m_dmId(other.m_dmId),
    m_userName(other.m_userName),
    m_testing(other.m_testing),
    m_from(other.m_from),
    m_to(other.m_to),
    m_annotation(other.m_annotation),
    m_deliveryTime(other.m_deliveryTime),
    m_acceptanceTime(other.m_acceptanceTime),
    m_readLocally(other.m_readLocally),
    m_attachmentsDownloaded(other.m_attachmentsDownloaded),
    m_recordsManagement(other.m_recordsManagement),
    m_msgType(other.m_msgType),
    m_daysToDeletion(other.m_daysToDeletion)
{
}

MessageModelEntry::MessageModelEntry(qint64 dmId, const QString &userName,
    bool testing, const QString &from, const QString &to,
    const QString &annotation, const QString &dTime, const QString &aTime,
    bool readLocally, bool attachmentsDownloaded, bool recordsManagement,
    int msgType, int daysToDeletion)
    : m_dmId(dmId),
    m_userName(userName),
    m_testing(testing),
    m_from(from),
    m_to(to),
    m_annotation(annotation),
    m_deliveryTime(dTime),
    m_acceptanceTime(aTime),
    m_readLocally(readLocally),
    m_attachmentsDownloaded(attachmentsDownloaded),
    m_recordsManagement(recordsManagement),
    m_msgType(msgType),
    m_daysToDeletion(daysToDeletion)
{
}

MessageModelEntry &MessageModelEntry::operator=(
const MessageModelEntry &other) Q_DECL_NOTHROW
{
	m_dmId = other.m_dmId;
	m_userName = other.m_userName;
	m_testing = other.m_testing;
	m_from = other.m_from;
	m_to = other.m_to;
	m_annotation = other.m_annotation;
	m_deliveryTime = other.m_deliveryTime;
	m_acceptanceTime = other.m_acceptanceTime;
	m_readLocally = other.m_readLocally;
	m_attachmentsDownloaded = other.m_attachmentsDownloaded;
	m_recordsManagement = other.m_recordsManagement;
	m_msgType = other.m_msgType;
	m_daysToDeletion = other.m_daysToDeletion;

	return *this;
}

qint64 MessageModelEntry::dmId(void) const
{
	return m_dmId;
}

QString MessageModelEntry::userName(void) const
{
	return m_userName;
}

void MessageModelEntry::setUserName(const QString &userName)
{
	m_userName = userName;
}

bool MessageModelEntry::testing(void) const
{
	return m_testing;
}

void MessageModelEntry::setTesting(bool testing)
{
	m_testing = testing;
}

QString MessageModelEntry::from(void) const
{
	return m_from;
}

void MessageModelEntry::setFrom(const QString &from)
{
	m_from = from;
}

QString MessageModelEntry::to(void) const
{
	return m_to;
}

void MessageModelEntry::setTo(const QString &to)
{
	m_to = to;
}

QString MessageModelEntry::annotation(void) const
{
	return m_annotation;
}

void MessageModelEntry::setAnnotation(const QString &annotation)
{
	m_annotation = annotation;
}

QString MessageModelEntry::deliveryTime(void) const
{
	return m_deliveryTime;
}

void MessageModelEntry::setDeliveryTime(const QString &dTime)
{
	m_deliveryTime = dTime;
}

QString MessageModelEntry::acceptanceTime(void) const
{
	return m_acceptanceTime;
}

void MessageModelEntry::setAcceptanceTime(const QString &aTime)
{
	m_acceptanceTime = aTime;
}

bool MessageModelEntry::readLocally(void) const
{
	return m_readLocally;
}

void MessageModelEntry::setReadLocally(bool readLocally)
{
	m_readLocally = readLocally;
}

bool MessageModelEntry::attachmentsDownloaded(void) const
{
	return m_attachmentsDownloaded;
}

void MessageModelEntry::setAttachmentsDownloaded(bool attachmentsDownloaded)
{
	m_attachmentsDownloaded = attachmentsDownloaded;
}

bool MessageModelEntry::recordsManagement(void) const
{
	return m_recordsManagement;
}

void MessageModelEntry::setSecordsManagement(bool recordsManagement)
{
	m_recordsManagement = recordsManagement;
}

int MessageModelEntry::msgType(void) const
{
	return m_msgType;
}

void MessageModelEntry::setMsgType(int msgType)
{
	m_msgType = msgType;
}

int MessageModelEntry::daysToDeletion(void) const
{
	return m_daysToDeletion;
}

void MessageModelEntry::setDaysToDeletion(int daysToDeletion)
{
	m_daysToDeletion = daysToDeletion;
}

void MessageListModel::declareQML(void)
{
	qmlRegisterType<MessageListModel>("cz.nic.mobileDatovka.models", 1, 0, "MessageListModel");
	qRegisterMetaType<MessageListModel>("MessageListModel");
	qRegisterMetaType<MessageListModel::Roles>("MessageListModel::Roles");

	qRegisterMetaType<MessageListModel *>("MessageListModel *");
	qRegisterMetaType<MessageListModel *>("const MessageListModel *");
}

MessageListModel::MessageListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

MessageListModel::MessageListModel(const MessageListModel &model,
    QObject *parent)
    : QAbstractListModel(parent),
    m_messages(model.m_messages)
{
}

int MessageListModel::rowCount(const QModelIndex &parent) const
{
	return !parent.isValid() ? m_messages.size() : 0;
}

QHash<int, QByteArray> MessageListModel::roleNames(void) const
{
	static QHash<int, QByteArray> roles;
	if (roles.isEmpty()) {
		roles[ROLE_MSG_ID] = "rMsgId";
		roles[ROLE_USERNAME] = "rUserName";
		roles[ROLE_TESTING] = "rTesting";
		roles[ROLE_FROM] = "rFrom";
		roles[ROLE_TO] = "rTo";
		roles[ROLE_ANNOTATION] = "rAnnotation";
		roles[ROLE_DELIVERY_TIME] = "rDelivTime";
		roles[ROLE_ACCEPTANCE_TIME] = "rAcceptTime";
		roles[ROLE_READ_LOCALLY] = "rReadLocally";
		roles[ROLE_ATTACHMENTS_DOWNLOADED] = "rAttachmentsDownloaded";
		roles[ROLE_RECORDS_MANAGEMENT] = "rRecordsManagement";
		roles[ROLE_MSG_TYPE] = "rMsgType";
		roles[ROLE_MSG_DAYS_TO_ISDS_DELETION] = "rMsgDaysToDeletion";
	}
	return roles;
}

QVariant MessageListModel::data(const QModelIndex &index, int role) const
{
	if ((index.row() < 0) || (index.row() >= m_messages.size())) {
		return QVariant();
	}

	const MessageModelEntry &message(m_messages.at(index.row()));

	switch (role) {
	case ROLE_MSG_ID:
		return message.dmId();
		break;
	case ROLE_USERNAME:
		return message.userName();
		break;
	case ROLE_TESTING:
		return message.testing();
		break;
	case ROLE_FROM:
		return message.from();
		break;
	case ROLE_TO:
		return message.to();
		break;
	case ROLE_ANNOTATION:
		return message.annotation();
		break;
	case ROLE_DELIVERY_TIME:
		return message.deliveryTime();
		break;
	case ROLE_ACCEPTANCE_TIME:
		return message.acceptanceTime();
		break;
	case ROLE_READ_LOCALLY:
		return message.readLocally();
		break;
	case ROLE_ATTACHMENTS_DOWNLOADED:
		return message.attachmentsDownloaded();
		break;
	case ROLE_RECORDS_MANAGEMENT:
		return message.recordsManagement();
		break;
	case ROLE_MSG_TYPE:
		return message.msgType();
		break;
	case ROLE_MSG_DAYS_TO_ISDS_DELETION:
		return message.daysToDeletion();
		break;
	default:
		/* Do nothing. */
		break;
	}

	return QVariant();
}

Qt::ItemFlags MessageListModel::flags(const QModelIndex &index) const
{
	return QAbstractListModel::flags(index);
}

void MessageListModel::deleteMessage(qint64 dmId)
{
	QModelIndex msgIdx(messageIndex(dmId));

	if (!msgIdx.isValid()) {
		return;
	}

	int row = msgIdx.row();

	beginRemoveRows(QModelIndex(), row, row);

	m_messages.removeAt(row);

	endRemoveRows();
}

int MessageListModel::setQuery(const QString &userName,
    bool testing, QSqlQuery &query, bool isAppend)
{
	int msgCnt = 0;

	if (query.record().count() != 9) {
		return msgCnt;
	}

	beginResetModel();

	if (!isAppend) {
		m_messages.clear();
	}

	QStringList rmPathList;

	query.first();
	while (query.isActive() && query.isValid()) {

		rmPathList.clear();
		if (Q_NULLPTR != GlobInstcs::recMgmtDbPtr) {
			rmPathList = GlobInstcs::recMgmtDbPtr->
			    storedMsgLocations(query.value(0).toLongLong());
		}

		m_messages.append(MessageModelEntry(
		    query.value(0).toLongLong(),
		    userName,
		    testing,
		    query.value(1).toString(),
		    query.value(2).toString(),
		    query.value(3).toString(),
		    dateTimeStrFromDbFormat(query.value(4).toString(),
			DATETIME_QML_FORMAT),
		    dateTimeStrFromDbFormat(query.value(5).toString(),
			DATETIME_QML_FORMAT),
		    query.value(6).toBool(),
		    query.value(7).toBool(),
		    !rmPathList.isEmpty(),
		    query.value(8).toInt(),
		    remainsDaysToMsgDeletion(query.value(5).toString())
		    ));
		msgCnt++;
		query.next();
	}

	endResetModel();

	return msgCnt;
}

QModelIndex MessageListModel::messageIndex(qint64 dmId) const
{
	for (int row = 0; row < m_messages.size(); ++row) {
		if (m_messages.at(row).dmId() == dmId) {
			return index(row, 0, QModelIndex());
		}
	}
	return QModelIndex();
}

void MessageListModel::clearAll(void)
{
	beginResetModel();
	m_messages.clear();
	endResetModel();
}

void MessageListModel::removeMessage(qint64 dmId)
{
	QModelIndex idx(messageIndex(dmId));
	if (!idx.isValid()) {
		return;
	}

	int row = idx.row();
	Q_ASSERT((row >= 0) && (row < m_messages.size()));

	beginRemoveRows(QModelIndex(), row, row);
	m_messages.removeAt(row);
	endRemoveRows();
}

bool MessageListModel::overrideRead(qint64 dmId, bool forceRead)
{
	QModelIndex msgIdx(messageIndex(dmId));

	if (!msgIdx.isValid()) {
		return false;
	}

	int row = msgIdx.row();
	Q_ASSERT((row >= 0) && (row < m_messages.size()));

	m_messages[row].setReadLocally(forceRead);

	emit dataChanged(msgIdx, msgIdx);

	return true;
}

bool MessageListModel::overrideReadAll(bool forceRead)
{
	for (int row = 0; row < m_messages.size(); ++row) {
		m_messages[row].setReadLocally(forceRead);
		emit dataChanged(index(row), index(row));
	}
	return true;
}

bool MessageListModel::overrideDownloaded(qint64 dmId, bool forceDownloaded)
{
	QModelIndex msgIdx(messageIndex(dmId));

	if (!msgIdx.isValid()) {
		return false;
	}

	int row = msgIdx.row();
	Q_ASSERT((row >= 0) && (row < m_messages.size()));

	m_messages[row].setAttachmentsDownloaded(forceDownloaded);

	emit dataChanged(msgIdx, msgIdx);

	return true;
}

MessageListModel *MessageListModel::fromVariant(const QVariant &modelVariant)
{
	if (!modelVariant.canConvert<QObject *>()) {
		return Q_NULLPTR;
	}
	QObject *obj = qvariant_cast<QObject *>(modelVariant);
	return qobject_cast<MessageListModel *>(obj);
}

bool MessageListModel::updateRmStatus(qint64 dmId, bool isUploadRm)
{
	QModelIndex msgIdx(messageIndex(dmId));

	if (!msgIdx.isValid()) {
		return false;
	}

	int row = msgIdx.row();
	Q_ASSERT((row >= 0) && (row < m_messages.size()));

	m_messages[row].setSecordsManagement(isUploadRm);

	emit dataChanged(msgIdx, msgIdx);

	return true;
}
