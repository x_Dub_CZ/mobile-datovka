/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QAbstractListModel>
#include <QString>

namespace Gov {
	class Service; /* Forward declaration. */
}

/*!
 * @brief Holds information about available gov services.
 */
class GovServiceListModel : public QAbstractListModel {
	Q_OBJECT

public:
	/*!
	 * @brief Model entry.
	 */
	class Entry {
	public:
		Entry(const Entry &other);
		Entry(const QString &srvcInternId, const QString &srvcFullName,
		    const QString &instName, const QString &srvcBoxId);

		Entry &operator=(const Entry &other) Q_DECL_NOTHROW;

		const QString &srvcInternId(void) const;
		const QString &srvcFullName(void) const;
		const QString &instName(void) const;
		const QString &srvcBoxId(void) const;

	private:
		QString m_srvcInternId; /*!< Short unique internal gov service identifier. */
		QString m_srvcFullName; /*!< Gov service full name. */
		QString m_instName; /*!< Gov institute name. */
		QString m_srvcBoxId; /*!< Gov institute databox ID */
	};

	/*!
	 * @brief Roles which this model supports.
	 */
	enum Roles {
		ROLE_GOV_SRVC_INTERN_ID = Qt::UserRole,
		ROLE_GOV_SRVC_FULL_NAME,
		ROLE_GOV_SRVC_INST_NAME,
		ROLE_GOV_SRVC_BOXID
	};
	Q_ENUM(Roles)

	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Pointer to parent object.
	 */
	explicit GovServiceListModel(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Copy constructor.
	 *
	 * @note Needed for QVariant conversion.
	 *
	 * @param[in] model Model to be copied.
	 * @param[in] parent Pointer to parent object.
	 */
	explicit GovServiceListModel(const GovServiceListModel &model,
	    QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Return number of rows under the given parent.
	 *
	 * @param[in] parent Parent node index.
	 * @return Number of rows.
	 */
	virtual
	int rowCount(const QModelIndex &parent = QModelIndex()) const
	    Q_DECL_OVERRIDE;

	/*!
	 * @brief Returns the model's role names.
	 *
	 * @return Model's role names.
	 */
	virtual
	QHash<int, QByteArray> roleNames(void) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Return data stored in given location under given role.
	 *
	 * @param[in] index Index specifying the item.
	 * @param[in] role  Data role.
	 * @return Data from model.
	 */
	virtual
	QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Return list of all entries.
	 *
	 * @return List of all entries.
	 */
	const QList<Entry> &allEntries(void) const;

	/*!
	 * @brief Returns item flags for given index.
	 *
	 * @brief[in] index Index specifying the item.
	 * @return Item flags.
	 */
	virtual
	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Appends service to model.
	 *
	 * @param[in] gg E-gov service data.
	 */
	void appendService(const Gov::Service *gs);

	/*!
	 * @brief Clears the model.
	 */
	Q_INVOKABLE
	void clearAll(void);

	/*!
	 * @brief Converts QVariant (obtained from QML) into a pointer.
	 *
	 * @note Some weird stuff happens in QML when passing instances
	 *     directly as constant reference. Wrong constructors are called
	 *     and no data are passed.
	 * @note QML passes objects (which were created in QML) as QVariant
	 *     values holding pointers. You therefore may call invokable methods
	 *     with QVariant arguments from QML.
	 * @note If you use
	 *     qRegisterMetaType<Type *>("Type *") and
	 *     qRegisterMetaType<Type *>("const Type *")
	 *     then QML will be able to call invokable methods without explicit
	 *     conversion from QVariant arguments.
	 *     Q_DECLARE_METATYPE(Type *) is not needed.
	 *
	 * @param[in] variant QVariant holding the pointer.
	 * @return Pointer if it could be acquired, Q_NULLPTR else. This
	 *     function does not allocate a new instance.
	 */
	static
	GovServiceListModel *fromVariant(const QVariant &modelVariant);

private:
	QList<Entry> m_services; /*!< List of service entries. */
};

/* QML passes its arguments via QVariant. */
Q_DECLARE_METATYPE(GovServiceListModel)
Q_DECLARE_METATYPE(GovServiceListModel::Roles)
