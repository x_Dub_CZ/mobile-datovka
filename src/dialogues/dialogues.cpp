/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#if !defined(Q_OS_IOS)
//#define USE_QML_DIALOGUES 1
#endif /* !defined(Q_OS_IOS) */

#include <QQmlEngine> /* qmlRegisterType */
#include <QWindow>

#include "src/dialogues/dialogues.h"
#include "src/dialogues/qml_input_dialogue.h"
#include "src/dialogues/qml_message_dialogue.h"
#include "src/dialogues/widget_input_dialogue.h"
#include "src/dialogues/widget_message_dialogue.h"

#if !defined(USE_QML_DIALOGUES)
/*!
 * @brief Converts echo mode from helper class definition.
 *
 * @param[in] echoMode Echo mode as defined in Dialogues.
 * @return Echo mode as defined in QLineEdit.
 */
static
QLineEdit::EchoMode toLineEditEchoMode(enum Dialogues::EchoMode echoMode)
{
	switch (echoMode) {
	case Dialogues::EM_NORMAL:
		return QLineEdit::Normal;
		break;
	case Dialogues::EM_PWD:
		return QLineEdit::Password;
		break;
	case Dialogues::EM_NOECHO:
		return QLineEdit::NoEcho;
		break;
	case Dialogues::EM_PWD_ECHOONEDIT:
		return QLineEdit::PasswordEchoOnEdit;
		break;
	default:
		Q_ASSERT(0);
		return QLineEdit::Normal;
		break;
	}
}
#endif /* !defined(USE_QML_DIALOGUES) */

/*!
 * @brief Converts dialogue icon from helper class definition.
 *
 * @param[in] icon Icon as used in Dialogues.
 * @return Icon as used in WidgetMessageDialogue.
 */
static
enum QMessageBox::Icon toMessageBoxIcon(enum Dialogues::Icon icon)
{
	switch (icon) {
	case Dialogues::NO_ICON:
		return QMessageBox::NoIcon;
		break;
	case Dialogues::QUESTION:
		return QMessageBox::Question;
		break;
	case Dialogues::INFORMATION:
		return QMessageBox::Information;
		break;
	case Dialogues::WARNING:
		return QMessageBox::Warning;
		break;
	case Dialogues::CRITICAL:
		return QMessageBox::Critical;
		break;
	default:
		Q_ASSERT(0);
		return QMessageBox::NoIcon;
		break;
	}
}

/*!
 * @brief Converts dialogue button from helper class definition.
 *
 * @param[in] button Button as used in Dialogues.
 * @return Button as used in WidgetMessageDialogue.
 */
static
enum QMessageBox::StandardButton toMessageBoxButton(
    enum Dialogues::Button button)
{
	switch (button) {
	case Dialogues::OK:
		return QMessageBox::Ok;
		break;
	case Dialogues::CANCEL:
		return QMessageBox::Cancel;
		break;
	case Dialogues::YES:
		return QMessageBox::Yes;
		break;
	case Dialogues::NO:
		return QMessageBox::No;
		break;
	case Dialogues::NO_BUTTON:
		return QMessageBox::NoButton;
		break;
	default:
		Q_ASSERT(0);
		return QMessageBox::NoButton;
		break;
	}
}

/*!
 * @brief Concerts return value from message dialogue.
 *
 * @param[in] retVal Return value as returned by message dialogue.
 * @return Converted value.
 */
static
int toDialogueRetVal(int retVal)
{
	switch (retVal) {
	case -1:
		return -1;
		break;
	case QMessageBox::Ok:
		return Dialogues::OK;
		break;
	case QMessageBox::Cancel:
		return Dialogues::CANCEL;
		break;
	case QMessageBox::Yes:
		return Dialogues::YES;
		break;
	case QMessageBox::No:
		return Dialogues::NO;
		break;
	case QMessageBox::NoButton:
		return Dialogues::NO_BUTTON;
		break;
	default:
		Q_ASSERT(0);
		return retVal;
		break;
	}
}

/*!
 * @brief Converts dialogue button from helper class definition.
 *
 * @param[in] button Button as used in Dialogues.
 * @return Button as used in WidgetMessageDialogue.
 */
static
QMessageBox::StandardButtons toMessageBoxButtons(Dialogues::Buttons buttons)
{
	QMessageBox::StandardButtons outButtons = QMessageBox::NoButton;

	outButtons |= (buttons & Dialogues::OK) ? QMessageBox::Ok : QMessageBox::NoButton;
	outButtons |= (buttons & Dialogues::CANCEL) ? QMessageBox::Cancel : QMessageBox::NoButton;
	outButtons |= (buttons & Dialogues::YES) ? QMessageBox::Yes : QMessageBox::NoButton;
	outButtons |= (buttons & Dialogues::NO) ? QMessageBox::No : QMessageBox::NoButton;

	return outButtons;
}

void Dialogues::declareQML(void)
{
	qmlRegisterUncreatableType<Dialogues>("cz.nic.mobileDatovka.dialogues", 1, 0, "Dialogues", "Access to enums & flags only.");
	qRegisterMetaType<Dialogues::EchoMode>("Dialogues::EchoMode");
	qRegisterMetaType<Dialogues::Icon>("Dialogues::Icon");
	qRegisterMetaType<Dialogues::Button>("Dialogues::Button");
}

QString Dialogues::getText(QObject *parent, const QString &title,
    const QString &message, enum EchoMode echoMode, const QString &text,
    const QString &placeholderText, bool *ok,
    Qt::InputMethodHints inputMethodHints)
{
#if defined(USE_QML_DIALOGUES)
	return QmlInputDialogue::getText(qobject_cast<QWindow *>(parent),
	    title, message, echoMode, text, placeholderText, ok,
	    inputMethodHints);
#else /* !defined(USE_QML_DIALOGUES) */
	Q_UNUSED(placeholderText);
	return WidgetInputDialogue::getText(qobject_cast<QWidget *>(parent),
	    title, message, toLineEditEchoMode(echoMode), text, ok,
	    Qt::Dialog, inputMethodHints);
#endif /* defined(USE_QML_DIALOGUES) */
}

int Dialogues::message(enum Icon icon, const QString &title,
    const QString &text, const QString &infoText,
    Buttons buttons, enum Button dfltButton,
    const QList< QPair<QString, int> > &customButtons, int *selected)
{
	return toDialogueRetVal(
	    WidgetMessageDialogue::message(toMessageBoxIcon(icon), title,
	        text, infoText, toMessageBoxButtons(buttons),
	        toMessageBoxButton(dfltButton), customButtons, selected));
}

void Dialogues::errorMessage(enum Icon icon, const QString &title,
    const QString &text, const QString &infoText)
{
#if defined(USE_QML_DIALOGUES)
	QmlMessageDialogue::errorMessage(Q_NULLPTR, icon, title, text,
	    infoText);
#else /* !defined(USE_QML_DIALOGUES) */
	WidgetMessageDialogue::errorMessage(toMessageBoxIcon(icon), title,
	    text, infoText);
#endif /* defined(USE_QML_DIALOGUES) */
}
