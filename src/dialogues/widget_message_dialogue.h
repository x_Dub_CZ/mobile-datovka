/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QMessageBox>
#include <QString>

/*!
 * @brief Encapsulated widget-based message dialogue.
 */
class WidgetMessageDialogue : public QMessageBox {
	Q_OBJECT

private:
	/*!
	 * @brief Private constructor.
	 */
	WidgetMessageDialogue(Icon icon, const QString &title,
	    const QString &text, StandardButtons buttons = NoButton,
	    QWidget *parent = Q_NULLPTR,
	    Qt::WindowFlags f = Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);

public:
	/*!
	 * @brief Show message and await response.
	 *
	 * @param[in] icon Icon identifying importance of the message.
	 * @param[in] title Title of the dialogue window.
	 * @param[in] text Main message of the dialogue.
	 * @param[in] infoText Additional informative text.
	 * @param[in] buttons Standard buttons to be displayed.
	 * @param[in] dfltButton Identifies default action button.
	 * @param[in] customButtons List of pairs of custom button strings and values.
	 * @param[out] selected Value of selected custom button.
	 * @return Dialogue code or standard button value, -1 if custom button
	 *     was selected and custom button was selected then \a selected is
	 *     set to value of custom button.
	 */
	static
	int message(enum QMessageBox::Icon icon, const QString &title,
	    const QString &text, const QString &infoText,
	    QMessageBox::StandardButtons buttons = NoButton,
	    enum QMessageBox::StandardButton dfltButton = QMessageBox::NoButton,
	    const QList< QPair<QString, int> > &customButtons = QList< QPair<QString, int> >(),
	    int *selected = Q_NULLPTR);

	/*!
	 * @brief Show error message that can only be acknowledged.
	 *
	 * @param[in] icon Icon identifying importance of the message.
	 * @param[in] title Title of the dialogue window.
	 * @param[in] text Main message of the dialogue.
	 * @param[in] infoText Additional informative text.
	 */
	static
	void errorMessage(enum QMessageBox::Icon icon, const QString &title,
	    const QString &text, const QString &infoText);
};
