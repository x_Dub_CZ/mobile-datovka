/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/dialogues/qml_dialogue_helper.h"
#include "src/dialogues/qml_message_dialogue.h"

QmlMessageDialogue::QmlMessageDialogue(QObject *parent, QWindow *window)
    : QObject(parent),
    m_alreadyReturned(false),
    m_loop(this)
{
	Q_UNUSED(window)
}

void QmlMessageDialogue::errorMessage(QWindow *parent,
    enum Dialogues::Icon icon, const QString &title, const QString &text,
    const QString &infoText)
{
	QmlMessageDialogue dialogue(parent, QmlDlgHelper::topLevelWindow());

	/* Connects signals and slots that interrupt the event loop. */
	connect(QmlDlgHelper::dlgEmitter, SIGNAL(closed(int, int)),
	    &dialogue, SLOT(dlgClosed(int, int)));

	/* Cause QML window to pop up. */
	emit QmlDlgHelper::dlgEmitter->dlgMessage(icon, title, text, infoText,
	    Dialogues::OK);

	if (!dialogue.m_alreadyReturned) {
		dialogue.m_loop.exec();
	}
}

void QmlMessageDialogue::dlgClosed(int button, int customButton)
{
	m_alreadyReturned = true;
	m_loop.quit();
}
