/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QClipboard>
#include <QDialogButtonBox>
#include <QGuiApplication>
#include <QPlainTextEdit>
#include <QPushButton>

#include "src/datovka_shared/log/log.h"
#include "src/dialogues/widget_input_dialogue.h"

WidgetInputDialogue::WidgetInputDialogue(QWidget *parent, Qt::WindowFlags flags)
    : QInputDialog(parent, flags),
    m_timer(this),
    m_pasteButton(Q_NULLPTR),
    m_textField(Q_NULLPTR)
{
}

WidgetInputDialogue::~WidgetInputDialogue(void)
{
	m_timer.stop();

	if ((m_pasteButton !=  NULL) && (m_textField != NULL)) {
		m_pasteButton->disconnect(SIGNAL(clicked()), m_textField,
		    SLOT(paste()));
		m_timer.disconnect(SIGNAL(timeout()), this,
		    SLOT(activatePasteButton()));
	}
}

QString WidgetInputDialogue::getText(QWidget *parent, const QString &title,
    const QString &label, QLineEdit::EchoMode mode, const QString &text,
    bool *ok, Qt::WindowFlags flags, Qt::InputMethodHints inputMethodHints)
{
#if defined Q_OS_ANDROID
//#if 1
	WidgetInputDialogue dialogue(parent, flags);

	dialogue.setWindowTitle(title);
	dialogue.setLabelText(label);
	dialogue.setTextValue(text);
	dialogue.setTextEchoMode(mode);
	dialogue.setInputMethodHints(inputMethodHints);

	dialogue.setVisible(true); /* Constructs the dialogue. */
	dialogue.setVisible(false);
	dialogue.extendDialoguePaste();

	int ret = dialogue.exec();
	if (ok) {
		*ok = !!ret;
	}
	if (ret) {
		return dialogue.textValue();
	} else {
		return QString();
	}
#else /* !defined Q_OS_ANDROID */
	return QInputDialog::getText(parent, title, label, mode, text, ok,
	    flags, inputMethodHints);
#endif /* defined Q_OS_ANDROID */
}

void WidgetInputDialogue::activatePasteButton(void)
{
	if (m_pasteButton == Q_NULLPTR) {
		return;
	}

	QClipboard *clipboard = QGuiApplication::clipboard();
	if (clipboard == Q_NULLPTR) {
		return;
	}

	/* Activate button only when clipboard contains text. */
	//m_pasteButton->setVisible(!clipboard->text().isEmpty());
	m_pasteButton->setEnabled(!clipboard->text().isEmpty());
}

void WidgetInputDialogue::extendDialoguePaste(void)
{
	QList<QDialogButtonBox *> buttonBoxes(
	    findChildren<QDialogButtonBox *>());
	QDialogButtonBox *buttonBox = Q_NULLPTR;

	m_textField = findEditArea();
	if (m_textField == Q_NULLPTR) {
		/* No suitable text field found. */
		logWarningNL("%s", "Could not find editable area.");
		return;
	}

	/* There should be only one button box. */
	if (buttonBoxes.size() == 1) {
		buttonBox = buttonBoxes.at(0);
	}

	if (buttonBox == Q_NULLPTR) {
		logWarningNL("%s", "Could not locate button box.");
		m_textField = Q_NULLPTR;
		return;
	}

	m_pasteButton = buttonBox->addButton(
	    tr("Paste"), QDialogButtonBox::ActionRole);

	if (m_pasteButton == Q_NULLPTR) {
		logWarningNL("%s", "Could not create paste button.");
		m_textField = Q_NULLPTR;
		return;
	}
	m_pasteButton->setEnabled(false);
	//m_pasteButton->setVisible(false);

	connect(m_pasteButton, SIGNAL(clicked()), m_textField, SLOT(paste()));
	connect(&m_timer, SIGNAL(timeout()), this, SLOT(activatePasteButton()));

	m_timer.setInterval(500);
	m_timer.start();

	logInfoNL("%s", "Created and connected paste button.");
}

QObject *WidgetInputDialogue::findEditArea(void) const
{
	QLineEdit *lineEdit = Q_NULLPTR;
	QPlainTextEdit *plainTextEdit = Q_NULLPTR;
	{
		QList<QLineEdit *> lineEdits(findChildren<QLineEdit *>());
		if (lineEdits.size() == 1) {
			lineEdit = lineEdits.at(0);
		}

		QList<QPlainTextEdit *> plainTextEdits(
		    findChildren<QPlainTextEdit *>());
		if (plainTextEdits.size() == 1) {
			plainTextEdit = plainTextEdits.at(0);
		}
	}

	if ((lineEdit != Q_NULLPTR) && (plainTextEdit == Q_NULLPTR)) {
		return lineEdit;
	} else if ((lineEdit == Q_NULLPTR) && (plainTextEdit != Q_NULLPTR)) {
		return plainTextEdit;
	}

	return Q_NULLPTR;
}
