/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QQmlEngine> /* qmlRegisterType */

#include "src/qml_isds/message_interface.h"

void QmlIsdsEnvelope::declareQML(void)
{
	qmlRegisterType<QmlIsdsEnvelope>("cz.nic.mobileDatovka.qmlIsds", 1, 0, "IsdsEnvelope");
	qRegisterMetaType<QmlIsdsEnvelope>("QmlIsdsEnvelope");
}

QmlIsdsEnvelope::QmlIsdsEnvelope(QObject *parent)
    : QObject(parent),
    Isds::Envelope()
{
}

QmlIsdsEnvelope::QmlIsdsEnvelope(const Isds::Envelope &other)
    : QObject(Q_NULLPTR),
    Isds::Envelope(other)
{
}

#ifdef Q_COMPILER_RVALUE_REFS
QmlIsdsEnvelope::QmlIsdsEnvelope(Isds::Envelope &&other) Q_DECL_NOEXCEPT
    : QObject(Q_NULLPTR),
    Isds::Envelope(::std::move(other))
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

QmlIsdsEnvelope::QmlIsdsEnvelope(const QmlIsdsEnvelope &other)
    : QObject(Q_NULLPTR),
    Isds::Envelope(other)
{
}

void QmlIsdsEnvelope::setDmId(qint64 id)
{
	Isds::Envelope::setDmId(id);
	emit dmIdChanged(Isds::Envelope::dmId());
	emit dmIDChanged(Isds::Envelope::dmID());
}

bool QmlIsdsEnvelope::setDmSenderOrgUnitNumStr(const QString &soun)
{
	bool ret = Isds::Envelope::setDmSenderOrgUnitNumStr(soun);
	if (ret) {
		emit dmSenderOrgUnitNumStrChanged(Isds::Envelope::dmSenderOrgUnitNumStr());
		emit dmSenderOrgUnitNumChanged(Isds::Envelope::dmSenderOrgUnitNum());
	}
	return ret;
}

bool QmlIsdsEnvelope::setDmRecipientOrgUnitNumStr(const QString &roun)
{
	bool ret = Isds::Envelope::setDmRecipientOrgUnitNumStr(roun);
	if (ret) {
		emit dmRecipientOrgUnitNumStrChanged(Isds::Envelope::dmRecipientOrgUnitNumStr());
		emit dmRecipientOrgUnitNumChanged(Isds::Envelope::dmRecipientOrgUnitNum());
	}
	return ret;
}

bool QmlIsdsEnvelope::setDmLegalTitleLawStr(const QString &l)
{
	bool ret = Isds::Envelope::setDmLegalTitleLawStr(l);
	if (ret) {
		emit dmLegalTitleLawStrChanged(Isds::Envelope::dmLegalTitleLawStr());
		emit dmLegalTitleLawChanged(Isds::Envelope::dmLegalTitleLaw());
	}
	return ret;
}

bool QmlIsdsEnvelope::setDmLegalTitleYearStr(const QString &y)
{
	bool ret = Isds::Envelope::setDmLegalTitleYearStr(y);
	if (ret) {
		emit dmLegalTitleYearStrChanged(Isds::Envelope::dmLegalTitleYearStr());
		emit dmLegalTitleYearChanged(Isds::Envelope::dmLegalTitleYear());
	}
	return ret;
}

void QmlIsdsEnvelope::setDmID(const QString &id)
{
	Isds::Envelope::setDmID(id);
	emit dmIdChanged(Isds::Envelope::dmId());
	emit dmIDChanged(Isds::Envelope::dmID());
}

void QmlIsdsEnvelope::setDbIDSender(const QString &sbi)
{
	Isds::Envelope::setDbIDSender(sbi);
	emit dbIDSenderChanged(Isds::Envelope::dbIDSender());
}

void QmlIsdsEnvelope::setDmSender(const QString &sn)
{
	Isds::Envelope::setDmSender(sn);
	emit dmSenderChanged(Isds::Envelope::dmSender());
}

void QmlIsdsEnvelope::setDmSenderAddress(const QString &sa)
{
	Isds::Envelope::setDmSenderAddress(sa);
	emit dmSenderAddressChanged(Isds::Envelope::dmSenderAddress());
}

void QmlIsdsEnvelope::setDmSenderType(enum Isds::Type::DbType st)
{
	Isds::Envelope::setDmSenderType(st);
	emit dmSenderTypeChanged(Isds::Envelope::dmSenderType());
}

void QmlIsdsEnvelope::setDmRecipient(const QString &rn)
{
	Isds::Envelope::setDmRecipient(rn);
	emit dmRecipientChanged(Isds::Envelope::dmRecipient());
}

void QmlIsdsEnvelope::setDmRecipientAddress(const QString &ra)
{
	Isds::Envelope::setDmRecipientAddress(ra);
	emit dmRecipientAddressChanged(Isds::Envelope::dmRecipientAddress());
}

void QmlIsdsEnvelope::setDmAmbiguousRecipient(enum Isds::Type::NilBool ar)
{
	Isds::Envelope::setDmAmbiguousRecipient(ar);
	emit dmAmbiguousRecipientChanged(Isds::Envelope::dmAmbiguousRecipient());
}

void QmlIsdsEnvelope::setDmMessageStatus(enum Isds::Type::DmState s)
{
	Isds::Envelope::setDmMessageStatus(s);
	emit dmMessageStatusChanged(Isds::Envelope::dmMessageStatus());
}

void QmlIsdsEnvelope::setDmAttachmentSize(qint64 as)
{
	Isds::Envelope::setDmAttachmentSize(as);
	emit dmAttachmentSizeChanged(Isds::Envelope::dmAttachmentSize());
}

void QmlIsdsEnvelope::setDmDeliveryTime(const QDateTime &dt)
{
	Isds::Envelope::setDmDeliveryTime(dt);
	emit dmDeliveryTimeChanged(Isds::Envelope::dmDeliveryTime());
}

void QmlIsdsEnvelope::setDmAcceptanceTime(const QDateTime &at)
{
	Isds::Envelope::setDmAcceptanceTime(at);
	emit dmAcceptanceTimeChanged(Isds::Envelope::dmAcceptanceTime());
}

void QmlIsdsEnvelope::setDmQTimestamp(const QByteArray &ts)
{
	Isds::Envelope::setDmQTimestamp(ts);
	emit dmQTimestampChanged(Isds::Envelope::dmQTimestamp());
}

void QmlIsdsEnvelope::setDmSenderOrgUnit(const QString &sou)
{
	Isds::Envelope::setDmSenderOrgUnit(sou);
	emit dmSenderOrgUnitChanged(Isds::Envelope::dmSenderOrgUnit());
}

void QmlIsdsEnvelope::setDmSenderOrgUnitNum(qint64 soun)
{
	Isds::Envelope::setDmSenderOrgUnitNum(soun);
	emit dmSenderOrgUnitNumStrChanged(Isds::Envelope::dmSenderOrgUnitNumStr());
	emit dmSenderOrgUnitNumChanged(Isds::Envelope::dmSenderOrgUnitNum());
}

void QmlIsdsEnvelope::setDbIDRecipient(const QString &rbi)
{
	Isds::Envelope::setDbIDRecipient(rbi);
	emit dbIDRecipientChanged(Isds::Envelope::dbIDRecipient());
}

void QmlIsdsEnvelope::setDmRecipientOrgUnit(const QString &rou)
{
	Isds::Envelope::setDmRecipientOrgUnit(rou);
	emit dmRecipientOrgUnitChanged(Isds::Envelope::dmRecipientOrgUnit());
}

void QmlIsdsEnvelope::setDmRecipientOrgUnitNum(qint64 roun)
{
	Isds::Envelope::setDmRecipientOrgUnitNum(roun);
	emit dmRecipientOrgUnitNumStrChanged(Isds::Envelope::dmRecipientOrgUnitNumStr());
	emit dmRecipientOrgUnitNumChanged(Isds::Envelope::dmRecipientOrgUnitNum());
}

void QmlIsdsEnvelope::setDmToHands(const QString &th)
{
	Isds::Envelope::setDmToHands(th);
	emit dmToHandsChanged(Isds::Envelope::dmToHands());
}

void QmlIsdsEnvelope::setDmAnnotation(const QString &a)
{
	Isds::Envelope::setDmAnnotation(a);
	emit dmAnnotationChanged(Isds::Envelope::dmAnnotation());
}

void QmlIsdsEnvelope::setDmRecipientRefNumber(const QString &rrn)
{
	Isds::Envelope::setDmRecipientRefNumber(rrn);
	emit dmRecipientRefNumberChanged(Isds::Envelope::dmRecipientRefNumber());
}

void QmlIsdsEnvelope::setDmSenderRefNumber(const QString &srn)
{
	Isds::Envelope::setDmSenderRefNumber(srn);
	emit dmSenderRefNumberChanged(Isds::Envelope::dmSenderRefNumber());
}

void QmlIsdsEnvelope::setDmRecipientIdent(const QString &ri)
{
	Isds::Envelope::setDmRecipientIdent(ri);
	emit dmRecipientIdentChanged(Isds::Envelope::dmRecipientIdent());
}

void QmlIsdsEnvelope::setDmSenderIdent(const QString &si)
{
	Isds::Envelope::setDmSenderIdent(si);
	emit dmSenderIdentChanged(Isds::Envelope::dmSenderIdent());
}

void QmlIsdsEnvelope::setDmLegalTitleLaw(qint64 l)
{
	Isds::Envelope::setDmLegalTitleLaw(l);
	emit dmLegalTitleLawStrChanged(Isds::Envelope::dmLegalTitleLawStr());
	emit dmLegalTitleLawChanged(Isds::Envelope::dmLegalTitleLaw());
}

void QmlIsdsEnvelope::setDmLegalTitleYear(qint64 y)
{
	Isds::Envelope::setDmLegalTitleYear(y);
	emit dmLegalTitleYearStrChanged(Isds::Envelope::dmLegalTitleYearStr());
	emit dmLegalTitleYearChanged(Isds::Envelope::dmLegalTitleYear());
}

void QmlIsdsEnvelope::setDmLegalTitleSect(const QString &s)
{
	Isds::Envelope::setDmLegalTitleSect(s);
	emit dmLegalTitleSectChanged(Isds::Envelope::dmLegalTitleSect());
}

void QmlIsdsEnvelope::setDmLegalTitlePar(const QString &p)
{
	Isds::Envelope::setDmLegalTitlePar(p);
	emit dmLegalTitleParChanged(Isds::Envelope::dmLegalTitlePar());
}

void QmlIsdsEnvelope::setDmLegalTitlePoint(const QString &p)
{
	Isds::Envelope::setDmLegalTitlePoint(p);
	emit dmLegalTitlePointChanged(Isds::Envelope::dmLegalTitlePoint());
}

void QmlIsdsEnvelope::setDmPersonalDelivery(enum Isds::Type::NilBool pd)
{
	Isds::Envelope::setDmPersonalDelivery(pd);
	emit dmPersonalDeliveryChanged(Isds::Envelope::dmPersonalDelivery());
}

void QmlIsdsEnvelope::setDmAllowSubstDelivery(enum Isds::Type::NilBool sd)
{
	Isds::Envelope::setDmAllowSubstDelivery(sd);
	emit dmAllowSubstDeliveryChanged(Isds::Envelope::dmAllowSubstDelivery());
}

void QmlIsdsEnvelope::setDmType(QChar t)
{
	Isds::Envelope::setDmType(t);
	emit dmTypeChanged(Isds::Envelope::dmType());
}
