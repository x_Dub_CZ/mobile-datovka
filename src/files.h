/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>

#include "src/qml_interaction/message_info.h"

class FileListModel; /* Forward declaration. */
class QmlAcntId; /* Forward declaration. */

/*
 * Class Files provides interface between QML and file database.
 * Class is initialised in the main function (main.cpp)
 */
class Files : public QObject {
    Q_OBJECT

public:
	/* Defines file type to be saved or sent via email */
	enum MsgAttachFlag {
		NO_FILES = 0x00,
		MSG_ZFO = 0x01, /* Complete message in zfo format. */
		MSG_ATTACHS = 0x02 /* Attachments of the data message. */
	};
	Q_ENUM(MsgAttachFlag)
	/*
	 * Flags inside QML:
	 * https://forum.qt.io/topic/10060/q_enums-q_declare_metatype-and-qml/2
	 */
	Q_DECLARE_FLAGS(MsgAttachFlags, MsgAttachFlag)
	Q_FLAG(MsgAttachFlags)

	/* Defines missing file id vaule and zfo id */
	enum FileIdType {
		NO_FILE_ID = -1,
		DB_ZFO_ID = -2
	};
	Q_ENUM(FileIdType)

	/*!
	 * @brief Declare various properties to the QML system.
	 */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit Files(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Delete files from all databases where file lifetime expired.
	 *
	 * @param[in] days Lifetime in days.
	 */
	void deleteExpiredFilesFromDbs(int days);

	/*!
	 * @brief Get attachment file icon from file name.
	 *
	 * @param[in] fileName File name.
	 * @return File icon resources string.
	 */
	Q_INVOKABLE static
	QString getAttachmentFileIcon(const QString &fileName);

	/*!
	 * @brief Get attachment file size in bytes.
	 *
	 * @param[in] filePath Path to file.
	 * @return File size in bytes.
	 */
	Q_INVOKABLE static
	qint64 getAttachmentSizeInBytes(const QString &filePath);

	/*!
	 * @brief Obtain attachment from database.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] fileId Attachment file identifier.
	 * @return Attachment content.
	 */
	Q_INVOKABLE static
	QByteArray getFileRawContentFromDb(const QmlAcntId *qAcntId, int fileId);

	/*!
	 * @brief Open attachment from database.
	 *
	 * @param[in] qAcntId Account identifier.nt.
	 * @param[in] fileId Attachment file identifier.
	 */
	Q_INVOKABLE static
	void openAttachmentFromDb(const QmlAcntId *qAcntId, int fileId);

	/*!
	 * @brief Open attachment in default application.
	 *
	 * @param[in] fileName File name.
	 * @param[in] binaryData Raw file content.
	 */
	Q_INVOKABLE static
	void openAttachment(const QString &fileName,
	    const QByteArray &binaryData);

	/*!
	 * @brief Send message attachments or complete zfo message
	 *     from database with email application.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgId Message id.
	 * @param[in] attachFlags Specifies which attachments to send.
	 */
	Q_INVOKABLE
	void sendMsgFilesWithEmail(const QmlAcntId *qAcntId,
	    qint64 msgId, MsgAttachFlags attachFlags);

	/*!
	 * @brief Delete file database.
	 *
	 * @param[in] qAcntId Account identifier.
	 */
	Q_INVOKABLE
	void deleteFileDb(const QmlAcntId *qAcntId);

	/*!
	 * @brief Vacuum all file databases.
	 */
	Q_INVOKABLE
	void vacuumFileDbs(void);

	/*!
	 * @brief Delete message attachments from databases.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgId Message id.
	 * @return true if success.
	 */
	Q_INVOKABLE
	bool deleteAttachmentsFromDb(const QmlAcntId *qAcntId, qint64 msgId);

	/*!
	 * @brief Checks whether file is readable.
	 *
	 * @param[in] filePath Path to file.
	 * @return True if file exists and is readable.
	 */
	static
	bool fileReadable(const QString &filePath);

	/*!
	 * @brief Tests whether attachment is ZFO file.
	 *
	 * @param[in] fileName File name.
	 * @return True if file has zfo suffix.
	 */
	Q_INVOKABLE static
	bool isZfoFile(const QString &fileName);

	/*!
	 * @brief Returns raw file content.
	 *
	 * @param[in] filePath Path to file.
	 * @return File content, QByteArray() on error.
	 */
	Q_INVOKABLE static
	QByteArray rawFileContent(const QString &filePath);

	/*!
	 * @brief Parse content of ZFO file.
	 *
	 * @note QML handles deallocation of returned objects. For more retail
	 *     see section 'Data Ownership'
	 *     of 'Data Type Conversion Between QML and C++'
	 *     at http://doc.qt.io/qt-5/qtqml-cppintegration-data.html .
	 *
	 * @param[in,out] attachModel Attachment model to be set.
	 * @param[in] rawZfoData Raw ZFO data.
	 * @return Pointer to newly allocated structure containing message
	 *     information.
	 */
	Q_INVOKABLE static
	MsgInfo *zfoData(FileListModel *attachModel,
	    const QByteArray &rawZfoData);

	/*!
	 * @brief Sets attachment model.
	 *
	 * @param[out] attachModel Attachment model to be set.
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgId Message identifier.
	 * @return True if success.
	 */
	static
	bool setAttachmentModel(FileListModel &attachModel,
	    const QmlAcntId *qAcntId, qint64 msgId);

	/*!
	 * @brief Send attachment from ZFO with email application.
	 *
	 * @note QML does not know qint64 therefore we use a QString as
	 *     message id.
	 *
	 * @param[in] attachModel Attachment model.
	 * @param[in] msgIdStr String with message id.
	 * @param[in] subject Email subject.
	 * @param[in] body Email body.
	 */
	Q_INVOKABLE static
	void sendAttachmentEmailZfo(const FileListModel *attachModel,
	    const QString &msgIdStr, QString subject, QString body);

	/*!
	 * @brief Save message attachments or complete zfo message from
	 *        database to external storage.
	 *
	 * @param[in] qAcntId Account identifier.
	 * @param[in] msgIdStr String with message id.
	 * @param[in] attachFlags Specifies which attachments to save.
	 */
	Q_INVOKABLE static
	void saveMsgFilesToDisk(const QmlAcntId *qAcntId,
	    const QString &msgIdStr, MsgAttachFlags attachFlags);

	/*!
	 * @brief Save attachments from ZFO to disk.
	 *
	 * @param[in] attachModel Attachment model.
	 * @param[in] msgIdStr String with message id.
	 */
	Q_INVOKABLE static
	void saveAttachmentsToDiskZfo(const FileListModel *attachModel,
	    const QString &msgIdStr);

	/*!
	 * @brief Delete temporary file from storage.
	 *
	 * @note This function is used on iOS when external ZFO file is opened.
	 *       iOS moves target file to Datovka private folder. Here, after
	 *       reading of file content, Datovka must this file removed itself.
	 *
	 * @param[filePath] Path to file.
	 */
	Q_INVOKABLE
	void deleteTmpFileFromStorage(const QString &filePath);

	/*!
	 * @brief Create final PDF file into send location.
	 *
	 * @param[in] text Text message.
	 * @return PDF file path.
	 */
	Q_INVOKABLE
	QString appendPDF(const QString &text);

	/*!
	 * @brief Create PDF file into tmp location and open preview.
	 *
	 * @param[in] text Text message.
	 */
	Q_INVOKABLE
	void viewPDF(const QString &text);

signals:
	/*!
	 * @brief Set new statusbar text and active busy indicator to QML.
	 *
	 * @param[in] txt Text message for statusbar.
	 * @param[in] busy True means the statusbar busy indicator is active
	 *                   and shown, false = disabled and hidden
	 */
	void statusBarTextChanged(QString txt, bool busy);

private:
	/*!
	 * @brief Generates a notification dialogue about the result of the file
	 *     saving.
	 *
	 * @param[in] destPath Path to which the attachment was saved,
	 *                     failure notification is generated when path is empty.
	 */
	static
	void fileSavingNotification(const QString &destPath);

	/*!
	 * @brief Open attachment from path in default application.
	 *
	 * @param[in] filePath File path.
	 */
	Q_INVOKABLE static
	void openAttachmentFromPath(const QString &filePath);

	/*!
	 * @brief Parse xml data of zfo file.
	 *
	 * @todo This function must be reworked as it is called multiple times
	 *     on a single message to obtain various data.
	 *
	 * @param[out] type ZFO type.
	 * @param[out] idStr Message identifier number held in a string.
	 * @param[out] annotation Annotation string.
	 * @param[out] msgDescrHtml Message text.
	 * @param[out] attachModel Attachment model to be set.
	 * @param[out] emailBody Email body.
	 * @param[in] xmlData Xml file data.
	 * @return True if success.
	 */
	static
	bool parseXmlData(enum MsgInfo::ZfoType *type, QString *idStr,
	    QString *annotation, QString *msgDescrHtml,
	    FileListModel *attachModel, QString *emailBody, QByteArray xmlData);

	/*!
	 * @brief Parse and show xml data of zfo file.
	 *
	 * @param[in] type ZFO file type.
	 * @param[out] idStr Message identifier number held in a string.
	 * @param[out] annotation Annotation string.
	 * @param[out] msgDescrHtml Message text.
	 * @param[out] attachModel Attachment model to be set.
	 * @param[out] emailBody Email body.
	 * @param[in] xmlData Xml file data.
	 * @return True if success.
	 */
	static
	bool parseAndShowXmlData(enum MsgInfo::ZfoType type, QString *idStr,
	    QString *annotation, QString *msgDescrHtml,
	    FileListModel *attachModel,  QString *emailBody,
	    QByteArray &xmlData);
};

Q_DECLARE_OPERATORS_FOR_FLAGS(Files::MsgAttachFlags)

/* Declare FileIdType to QML. */
Q_DECLARE_METATYPE(Files::FileIdType)
Q_DECLARE_METATYPE(Files::MsgAttachFlag)
Q_DECLARE_METATYPE(Files::MsgAttachFlags)
