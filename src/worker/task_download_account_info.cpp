/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDateTime>
#include <QThread>

#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/isds/services/box_interface.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"
#include "src/net/db_wrapper.h"
#include "src/worker/emitter.h"
#include "src/worker/task_download_account_info.h"

TaskDownloadAccountInfo::TaskDownloadAccountInfo(const AcntId &acntId,
    bool storeToDb)
    : m_result(DL_ERR),
    m_dbID(),
    m_acntId(acntId),
    m_storeToDb(storeToDb),
    m_erroTxt()
{
}

void TaskDownloadAccountInfo::run(void)
{
	/* ### Worker task begin. ### */

	logDebugLv0NL("%s", "--------------ACCOUNT INFO TASK---------------");
	logDebugLv0NL("Starting in thread '%p'", QThread::currentThreadId());

	m_result = downloadAccountInfo(m_acntId, m_storeToDb, m_dbID, m_erroTxt);

	if (m_storeToDb) {
		if (GlobInstcs::msgProcEmitterPtr != Q_NULLPTR) {
			emit GlobInstcs::msgProcEmitterPtr->downloadAccountInfoFinishedSignal(
			    m_acntId,
			    TaskDownloadAccountInfo::DL_SUCCESS == m_result,
			    m_erroTxt);
		}
	}

	logDebugLv0NL("Finished in thread '%p'", QThread::currentThreadId());
	logDebugLv0NL("%s", "-----------------------------------------------");

	/* ### Worker task end. ### */
}

/*!
 * @brief Translate communication error onto task error.
 */
static
enum TaskDownloadAccountInfo::Result error2result(enum Isds::Type::Error error)
{
	switch (error) {
	case Isds::Type::ERR_SUCCESS:
		return TaskDownloadAccountInfo::DL_SUCCESS;
		break;
	case Isds::Type::ERR_ISDS:
		return TaskDownloadAccountInfo::DL_ISDS_ERROR;
		break;
	case Isds::Type::ERR_SOAP:
	case Isds::Type::ERR_XML:
		return TaskDownloadAccountInfo::DL_XML_ERROR;
		break;
	default:
		return TaskDownloadAccountInfo::DL_ERR;
		break;
	}
}

enum TaskDownloadAccountInfo::Result TaskDownloadAccountInfo::downloadAccountInfo(
    const AcntId &acntId, bool storeToDb, QString &dbID, QString &errText)
{
	if (Q_UNLIKELY((!acntId.isValid()) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (!GlobInstcs::isdsSessionsPtr->holdsSession(acntId)))) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	Isds::Session *session = GlobInstcs::isdsSessionsPtr->session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	logDebugLv1NL("Sending from account '%s'",
	    acntId.username().toUtf8().constData());

	/* Send SOAP request to download account owner info */
	Isds::DbOwnerInfoExt2 dbOwnerInfo;
	enum Isds::Type::Error error = Isds::getOwnerInfo2(
	    *session, dbOwnerInfo, Q_NULLPTR, &errText);
	if (Q_UNLIKELY(error != Isds::Type::ERR_SUCCESS)) {
		return error2result(error);
	}

	dbID = dbOwnerInfo.dbID();

	if (!storeToDb) {
		return DL_SUCCESS;
	}

	/* Store data into db */
	if (!DbWrapper::insertAccountInfoToDb(acntId, dbOwnerInfo)) {
		logErrorNL("%s", "DB: Cannot store owner info into database");
		return DL_DB_INS_ERR;
	}

	logDebugLv1NL("%s", "Owner info has been downloaded");

	/* Send SOAP request to download user info */
	Isds::DbUserInfoExt2 dbUserInfo;
	error = Isds::getUserInfo2(*session, dbUserInfo, Q_NULLPTR, &errText);
	if (Q_UNLIKELY(error != Isds::Type::ERR_SUCCESS)) {
		return error2result(error);
	}

	/* Store data into db */
	if (!DbWrapper::insertUserInfoToDb(acntId, dbUserInfo)) {
		logErrorNL("%s", "DB: Cannot store user info into database");
		return DL_DB_INS_ERR;
	}

	logDebugLv1NL("%s", "User info has been downloaded");

	/* Send SOAP request download password expiration. */
	QDateTime pwdExp;
	error = Isds::getPasswordInfo(*session, pwdExp, Q_NULLPTR, &errText);
	if (Q_UNLIKELY(error != Isds::Type::ERR_SUCCESS)) {
		return error2result(error);
	}

	/* Store data into db */
	if (!DbWrapper::insertPwdExpirationToDb(acntId, pwdExp)) {
		logErrorNL("DB: Cannot store password info into database: '%s'",
		    errText.toUtf8().constData());
		return DL_DB_INS_ERR;
	}

	logDebugLv1NL("%s", "Password info has been downloaded");

	/* Send SOAP request to download long term storage info */
	Isds::DTInfoOutput dtInfo;
	error = Isds::dtInfo(*session, dbID, dtInfo, Q_NULLPTR, &errText);
	if (Q_UNLIKELY(error != Isds::Type::ERR_SUCCESS)) {
		return error2result(error);
	}

	/* Store data into db */
	if (!DbWrapper::insertDTInfoToDb(dbID, dtInfo)) {
		logErrorNL("DB: Cannot store long term storage info into database: '%s'",
		    errText.toUtf8().constData());
		return DL_DB_INS_ERR;
	}

	logDebugLv1NL("%s", "Long term storage info has been downloaded");

	return DL_SUCCESS;
}
