/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QDate>
#include <QList>
#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/box_interface.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing download credit information.
 */
class TaskCreditInfo : public Task {
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		DL_SUCCESS, /*!< Operation was successful. */
		DL_ISDS_ERROR, /*!< Error communicating with ISDS. */
		DL_XML_ERROR, /*!< Error parsing XML. */
		DL_DB_INS_ERR, /*!< Error inserting into database. */
		DL_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] dbId Data box identifier.
	 * @param[in] fromDate From date.
	 * @param[in] toDate To date.
	 */
	explicit TaskCreditInfo(const AcntId &acntId, const QString &dbId,
	    const QDate &fromDate, const QDate &toDate);

	/*!
	 * @brief Performs action.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	enum Result m_result; /*!< Return state. */
	QString m_lastError; /*!< Last ISDS error message. */
	qint64 m_currentCredit; /*!< Remaining credit in CZK. */
	QString m_notifEmail; /*!< Notification email address. */
	QList<Isds::CreditEvent> m_creditEvents; /*!< Credit event list. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskCreditInfo(const TaskCreditInfo &);
	TaskCreditInfo &operator=(const TaskCreditInfo &);

	/*!
	 * @brief Download credit event information from ISDS.
	 *
	 * @param[in]  acntId Account identifier.
	 * @param[in]  dbId Data box identifier.
 	 * @param[in]  fromDate From date.
	 * @param[in]  toDate To date.
	 * @param[out] currentCredit Remaining credit in CZK.
	 * @param[out] notifEmail Notification email address.
	 * @param[out] creditEvents Credit event list.
	 * @param[out] lastError Last ISDS error message.
	 * @return Error state.
	 */
	static
	enum Result creditInfo(const AcntId &acntId, const QString &dbId,
	    const QDate &fromDate, const QDate &toDate, qint64 &currentCredit,
	    QString &notifEmail, QList<Isds::CreditEvent> &creditEvents,
	    QString &lastError);

	const AcntId m_acntId; /*!< Account identifier. */
	const QString m_dbId; /*!< Data box identifier. */
	const QDate m_fromDate; /*!< From date. */
	const QDate m_toDate; /*!< To date. */
};
