/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS */
#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/box_interface.h"
#include "src/models/databoxmodel.h"
#include "src/worker/task.h"

/*!
 * @brief Task download info about the recipient and information about PDZ
 *     payment methods.
 */
class TaskRecipientInfo : public Task {
public:
	/*!
	 * @brief Return recipient data box state describing what happened.
	 */
	enum Result {
		TR_SUCCESS, /*!< Operation was successful. */
		TR_DB_NO_ACTIVE, /*!< Recipient data box is not active. */
		TR_DB_NOT_EXISTS, /*!< Recipient data box does not exists. */
		TR_DB_SEARCH_ERROR, /*!< Recipient data box search error. */
		TR_DB_UNKN_OVM_UNKN_PDZ, /*!< No information about OVM or PDZ. */
		TR_DB_UNKN_PUBLIC_NO_PDZ, /*!< No information about OVM, no PDZ. */
		TR_DB_NO_PUBLIC_UNKN_PDZ, /*!< No OVM, no information about PDZ. */
		TR_DB_NO_PUBLIC_NO_PDZ, /*!< No OVM, no PDZ. */
		TR_ERR /*!< Other internal error. */
	};

	/*!
	 * @brief Get recipient info and PDZ payment methods.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] dbID Recipient data box id.
	 * @param[in] senderOvm True if sender is OVM.
	 * @param[in] canUseInitReply True if can use reply
	 *                            on initiatory message.
	 * @param[in] pdzInfos List of PDZ info.
	 */
	explicit TaskRecipientInfo(const AcntId &acntId, const QString &dbID,
	    bool senderOvm, bool canUseInitReply,
	    const QList<Isds::PDZInfoRec> &pdzInfos);

	/*!
	 * @brief Performs action.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	enum Result m_result; /*!< Return state. */
	QString m_lastError; /*!< Last ISDS error message. */
	DataboxModelEntry m_dbEntry; /*!< Recipient data box info. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskRecipientInfo(const TaskRecipientInfo &);
	TaskRecipientInfo &operator=(const TaskRecipientInfo &);

	/*!
	 * @brief Get recipient info and PDZ payment methods.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] dbID Recipient data box id.
	 * @param[in] senderOvm True if sender is OVM.
	 * @param[in] canUseInitReply True if can use reply
	 *                            on initiatory message.
	 * @param[in] pdzInfos List of PDZ info.
	 * @param[out] dbEntry Recipient data box info.
	 * @param[out] lastError Last ISDS error message.
	 * @return Error state.
	 */
	static
	enum Result recipientInfo(const AcntId &acntId,
	    const QString &dbID, bool senderOvm, bool canUseInitReply,
	    const QList<Isds::PDZInfoRec> &pdzInfos,
	    DataboxModelEntry &dbEntry, QString &lastError);

	const AcntId m_acntId; /*!< Account identifier. */
	const QString m_dbId; /*!< Data box identifier. */
	bool m_senderOvm; /*!< True if sender is OVM. */
	bool m_canUseInitReply; /*!< True if can use reply on initiatory message. */
	QList<Isds::PDZInfoRec> m_pdzInfos; /*!< PDZ info list. */
};
