/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QThread>

#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/io/filesystem.h"
#include "src/isds/services/message_interface.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"
#include "src/messages.h"
#include "src/net/db_wrapper.h"
#include "src/worker/emitter.h"
#include "src/worker/task_download_message.h"

TaskDownloadMessage::TaskDownloadMessage(const AcntId &acntId, qint64 msgId,
    enum Messages::MessageType msgDirect, bool saveZfo, bool isTestAccount)
    : m_result(DL_ERR),
    m_acntId(acntId),
    m_msgId(msgId),
    m_msgDirect(msgDirect),
    m_saveZfo(saveZfo),
    m_isTestAccount(isTestAccount),
    m_lastError()
{
}

void TaskDownloadMessage::run(void)
{
	if (Q_UNLIKELY((Messages::TYPE_RECEIVED != m_msgDirect) &&
	        (Messages::TYPE_SENT != m_msgDirect))) {
		Q_ASSERT(0);
		return;
	}

	/* ### Worker task begin. ### */

	logDebugLv0NL("%s", "-------------DOWNLOAD MESSAGE TASK-------------");
	logDebugLv0NL("Starting in thread '%p'", QThread::currentThreadId());

	m_result = downloadMessage(m_acntId, m_msgId, m_msgDirect, m_saveZfo,
	    m_isTestAccount, m_lastError);

	if (GlobInstcs::msgProcEmitterPtr != Q_NULLPTR) {
		emit GlobInstcs::msgProcEmitterPtr->downloadMessageFinishedSignal(
		    m_acntId, m_msgId,
		    TaskDownloadMessage::DL_SUCCESS == m_result,
		    m_lastError);
	}

	logDebugLv0NL("Finished in thread '%p'", QThread::currentThreadId());
	logDebugLv0NL("%s", "-----------------------------------------------");

	/* ### Worker task end. ### */
}

/*!
 * @brief Translate communication error onto task error.
 */
static
enum TaskDownloadMessage::Result error2result(enum Isds::Type::Error error)
{
	switch (error) {
	case Isds::Type::ERR_SUCCESS:
		return TaskDownloadMessage::DL_SUCCESS;
		break;
	case Isds::Type::ERR_ISDS:
		return TaskDownloadMessage::DL_ISDS_ERROR;
		break;
	case Isds::Type::ERR_SOAP:
	case Isds::Type::ERR_XML:
		return TaskDownloadMessage::DL_XML_ERROR;
		break;
	default:
		return TaskDownloadMessage::DL_ERR;
		break;
	}
}

enum TaskDownloadMessage::Result TaskDownloadMessage::downloadMessage(
    const AcntId &acntId, qint64 msgId, enum Messages::MessageType msgDirect,
    bool saveZfo, bool isTestAccount, QString &lastError)
{
	if (Q_UNLIKELY((!acntId.isValid()) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (!GlobInstcs::isdsSessionsPtr->holdsSession(acntId)))) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	Isds::Session *session = GlobInstcs::isdsSessionsPtr->session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	logDebugLv1NL("Sending from account '%s'",
	    acntId.username().toUtf8().constData());

	enum Isds::Type::Error error = Isds::Type::ERR_ERROR;
	Isds::Message message;
	if (Messages::TYPE_RECEIVED == msgDirect) {
		error = Isds::getSignedReceivedMessage(*session, msgId, message,
		    Q_NULLPTR, &lastError);
	} else {
		error = Isds::getSignedSentMessage(*session, msgId, message,
		    Q_NULLPTR, &lastError);
	}
	if (Q_UNLIKELY(error != Isds::Type::ERR_SUCCESS)) {
		return error2result(error);
	}

	if (Q_UNLIKELY(message.isNull())) {
		logErrorNL("SOAP: %s", lastError.toUtf8().constData());
		return DL_XML_ERROR;
	}

	if (message.documents().isEmpty()) {
		logErrorNL("SOAP: %s", "Message attachment missing!");
		return DL_ERR;
	}

	enum MessageDb::MessageType msgType = MessageDb::TYPE_RECEIVED;
	switch (msgDirect) {
	case Messages::TYPE_RECEIVED:
		msgType = MessageDb::TYPE_RECEIVED;
		break;
	case Messages::TYPE_SENT:
		msgType = MessageDb::TYPE_SENT;
		break;
	default:
		/*
		 * Any other translates to sent, but forces the application to
		 * crash in debugging mode.
		 */
		Q_ASSERT(0);
		msgType = MessageDb::TYPE_SENT;
		break;
	}

	/* Store data into db */
	if (!DbWrapper::insertCompleteMessageToDb(acntId, message, msgType,
	        lastError)) {
		logErrorNL("DB: Insert error: %s",
		    lastError.toUtf8().constData());
		return DL_DB_INS_ERR;
	}

	/*
	 * Following functions don't have to check return values
	 * because they are complementary actions for message download
	 * (not required for success complete message download).
	 */

	/* Send SOAP request to download message author info */
	enum Isds::Type::SenderType userType;
	QString authorName;
	error = Isds::getMessageAuthorInfo(*session, msgId,
	    userType, authorName, Q_NULLPTR, &lastError);
	if (Q_UNLIKELY(error == Isds::Type::ERR_SUCCESS)) {
		DbWrapper::updateAuthorInfo(acntId, userType, authorName,
		    msgId, lastError);
	}

	/* Send SOAP request mark message as read (only for received message) */
	if (Messages::TYPE_RECEIVED == msgDirect) {
		Isds::markMessageAsDownloaded(*session, msgId, Q_NULLPTR,
		    &lastError);
	}

	/* Save message ZFO to zfo database */
	if (saveZfo) {
		const QByteArray &zfoData = message.raw();
		DbWrapper::insertZfoToDb(msgId, isTestAccount, zfoData.size(),
		    zfoData);
	}

	/* Clear message content. */
	message = Isds::Message();

	Isds::getSignedDeliveryInfo(*session, msgId, message,
	    Q_NULLPTR, &lastError);
	/* Store events from delivery info into database. */
	if (!message.envelope().dmEvents().isEmpty()) {
		DbWrapper::insertMesasgeDeliveryInfoToDb(acntId,
		    message.envelope().dmEvents(), msgId, lastError);
	}

	logDebugLv1NL("%s", "Complete message has been downloaded");

	return DL_SUCCESS;
}
