/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS */
#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/worker/pool.h"
#include "src/messages.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing download message list.
 */
class TaskDownloadMessageList : public Task {
	Q_DECLARE_TR_FUNCTIONS(TaskDownloadMessageList)

public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		DL_SUCCESS, /*!< Operation was successful. */
		DL_ISDS_ERROR, /*!< Error communicating with ISDS. */
		DL_XML_ERROR, /*!< Error parsing XML. */
		DL_DB_INS_ERR, /*!< Error inserting into database. */
		DL_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] msgDirect Received or sent message.
	 * @param[in] dmStatusFilter Download message status filter.
	 * @param[in] dmOffset Message download offset.
	 * @param[in] dmLimit Message list length limit.
	 * @param[in] workPool Pointer to worker pool.
	 * @param[in] downloadCompleteMsgs Download complete messages.
	 * @param[in] saveZfo Save ZFO to local storage.
	 * @param[in] isTestAccount True if account is ISDS testing.
	 */
	explicit TaskDownloadMessageList(const AcntId &acntId,
	    enum Messages::MessageType msgDirect,
	    Isds::Type::DmFiltStates dmStatusFilter, unsigned long int dmOffset,
	    unsigned long int dmLimit, WorkerPool *workPool,
	    bool downloadCompleteMsgs, bool saveZfo, bool isTestAccount);

	/*!
	 * @brief Performs actual message list download.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	/*!
	 * @brief Download message list from ISDS for given account.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] msgDirect Received or sent message.
	 * @param[in] dmStatusFilter Download message status filter.
	 * @param[in] dmOffset Message download offset.
	 * @param[in] dmLimit Message list length limit.
	 * @param[in] workPool Pointer to worker pool.
	 * @param[in] downloadCompleteMsgs Download complete messages.
	 * @param[in] saveZfo Save ZFO to local storage.
	 * @param[in] isTestAccount True if account is ISDS testing.
	 * @param[out] msgIds Message ID list (for download of complete messages).
	 * @param[out] statusBarText Text about new messages for status bar.
	 * @param[out] lastError Last error description.
	 * @return Error state.
	 */
	static
	enum Result downloadMessageList(const AcntId &acntId,
	    enum Messages::MessageType msgDirect,
	    Isds::Type::DmFiltStates dmStatusFilter, unsigned long int dmOffset,
	    unsigned long int dmLimit, WorkerPool *workPool,
	    bool downloadCompleteMsgs, bool saveZfo, bool isTestAccount,
	    QString &statusBarText, QString &lastError);

	enum Result m_result; /*!< Return state. */
	QString m_statusBarText; /*!< Return text about new messages for status bar. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskDownloadMessageList(const TaskDownloadMessageList &);
	TaskDownloadMessageList &operator=(const TaskDownloadMessageList &);

	const AcntId m_acntId; /*!< Account id. */
	enum Messages::MessageType m_msgDirect; /*!< Sent or received list. */
	Isds::Type::DmFiltStates m_dmStatusFilter; /*!< Defines type of messages to be downloaded. */
	unsigned long int m_dmOffset; /*!< Message download offset. */
	unsigned long int m_dmLimit; /*!< List length limit. */
	WorkerPool *m_workPool; /*!< Pointer to worker pool. */
	bool m_downloadCompleteMsgs; /*!< Download complete messages. */
	bool m_saveZfo; /*!< Save ZFO to local storage. */
	bool m_isTestAccount; /*!< True if account is ISDS testing. */
	QString m_lastError; /*!< Last error description. */
};
