/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QThread>

#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/isds/services/login_interface.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"
#include "src/settings/accounts.h"
#include "src/worker/task_send_sms.h"

TaskSendSMS::TaskSendSMS(const AcntId &acntId, const QString &oldPwd)
    : m_result(DL_ERR),
    m_acntId(acntId),
    m_oldPwd(oldPwd)
{
}

void TaskSendSMS::run(void)
{
	/* ### Worker task begin. ### */

	logDebugLv0NL("%s", "-----------------SEND SMS TASK-----------------");
	logDebugLv0NL("Starting in thread '%p'", QThread::currentThreadId());

	m_result = sendSMS(m_acntId, m_oldPwd);

	logDebugLv0NL("Finished in thread '%p'", QThread::currentThreadId());
	logDebugLv0NL("%s", "-----------------------------------------------");

	/* ### Worker task end. ### */
}

/*!
 * @brief Translate communication error onto task error.
 */
static
enum TaskSendSMS::Result error2result(enum Isds::Type::Error error)
{
	switch (error) {
	case Isds::Type::ERR_SUCCESS:
		return TaskSendSMS::DL_SUCCESS;
		break;
	case Isds::Type::ERR_ISDS:
		return TaskSendSMS::DL_ISDS_ERROR;
		break;
	case Isds::Type::ERR_SOAP:
	case Isds::Type::ERR_XML:
		return TaskSendSMS::DL_XML_ERROR;
		break;
	default:
		return TaskSendSMS::DL_ERR;
		break;
	}
}

enum TaskSendSMS::Result TaskSendSMS::sendSMS(const AcntId &acntId,
    const QString &oldPwd)
{
	if (Q_UNLIKELY(!acntId.isValid())) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	AcntData acntData((*GlobInstcs::acntMapPtr)[acntId]);
	Isds::Session *session = Isds::Session::createSession(acntData);
	if (session == Q_NULLPTR) {
		logErrorNL("Cannot create session for username %s",
		    acntId.username().toUtf8().constData());
		return DL_ERR;
	}

	logDebugLv1NL("Sending SMS for account '%s'",
	    acntId.username().toUtf8().constData());

	/* Send SOAP request */
	QString errTxt;
	session->ctx()->setPassword(oldPwd);
	enum Isds::Type::Error error = Isds::sendSMSCode(*session,
	     Q_NULLPTR, &errTxt);
	delete session;

	if (Q_UNLIKELY(error != Isds::Type::ERR_SUCCESS)) {
		return error2result(error);
	}

	logDebugLv1NL("%s", "SMS has been sent");

	return DL_SUCCESS;
}
