/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing ZFO file import.
 */
class TaskImportZfo : public Task {
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		IMP_SUCCESS, /*!< Import was successful. */
		IMP_ZFO_ERR, /*!< Load ZFO file error. */
		IMP_ZFO_FORMAT_ERR, /*!< ZFO format is wrong. Not valid message. */
		IMP_ZFO_DATA_ERR, /*!< Data couldn't be read or do not match supplied type. */
		IMP_NO_ACCOUNT_ERR, /*!< Relevant account does not exist for the message. */
		IMP_ISDS_ERR, /*!< Error communicating with ISDS. */
		IMP_AUTH_ERR, /*!< Data couldn't be authenticated. */
		IMP_DB_INS_ERR, /*!< Error inserting into database. */
		IMP_DB_EXISTS, /*!< Message already exists. */
		IMP_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntIdList List of account ids.
	 * @param[in] fileName Full path to ZFO file.
	 * @param[in] authenticate True if you want to authenticate message
	 *                         before importing in ISDS.
	 * @param[in] zfoNumber ZFO number in import process.
	 * @param[in] zfoTotal Overall number of ZFO for import.
	 */
	explicit TaskImportZfo(
	    const QList<AcntId> &acntIdList, const QString &fileName,
	    bool authenticate, int zfoNumber, int zfoTotal);

	/*!
	 * @brief Performs action.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	/*!
	 * @brief Import ZFO file.
	 *
	 * @param[in] acntIdList List of account ids.
	 * @param[in] fileName Full path to ZFO file.
	 * @param[in] authenticate True if you want to authenticate message
	 *                         before importing in ISDS.
	 * @param[out] errText Error/state description.
	 */
	static
	enum Result importZfo(
	    const QList<AcntId> &acntIdList, const QString &fileName,
	    bool authenticate, QString &errText);

	enum Result m_result; /*!< Import outcome. */
	QString m_errText; /*!< Result/error description */

private:
	/*!
	 * @brief Returns html error import color text.
	 *
	 * @param[in] text Original error description.
	 * @return color error text string.
	 */
	static
	QString setErrorTextAndColor(const QString &text);

	/*!
	 * Disable copy and assignment.
	 */
	TaskImportZfo(const TaskImportZfo &);
	TaskImportZfo &operator=(const TaskImportZfo &);

	const QList<AcntId> m_acntIdList; /*!< List of account ids. */
	const QString m_fileName; /*!< Full file path to imported file. */
	bool m_auth; /*!< True if authentication before importing. */
	int m_zfoNumber; /*!< ZFO number in import process. */
	int m_zfoTotal; /*!< Overall number of ZFO for import. */
};
