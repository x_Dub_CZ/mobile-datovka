/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QThread>

#include "src/datovka_shared/isds/type_conversion.h"
#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/isds/services/box_interface.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"
#include "src/worker/emitter.h"
#include "src/worker/task_recipient_info.h"

TaskRecipientInfo::TaskRecipientInfo(const AcntId &acntId, const QString &dbID,
    bool senderOvm, bool canUseInitReply,
    const QList<Isds::PDZInfoRec> &pdzInfos)
    : m_result(TR_ERR),
    m_lastError(),
    m_dbEntry(),
    m_acntId(acntId),
    m_dbId(dbID),
    m_senderOvm(senderOvm),
    m_canUseInitReply(canUseInitReply),
    m_pdzInfos(pdzInfos)
{
	Q_ASSERT(m_acntId.isValid());
	Q_ASSERT(!m_dbId.isEmpty());
}

void TaskRecipientInfo::run(void)
{
	logDebugLv0NL("%s", "--------------RECIPIENT INFO TASK--------------");
	logDebugLv0NL("Starting in thread '%p'", QThread::currentThreadId());

	/* ### Worker task begin. ### */

	m_result = recipientInfo(m_acntId, m_dbId, m_senderOvm,
	    m_canUseInitReply, m_pdzInfos, m_dbEntry, m_lastError);

	if (GlobInstcs::msgProcEmitterPtr != Q_NULLPTR) {
		emit GlobInstcs::msgProcEmitterPtr->
		    recipientInfoFinishedSignal(m_dbEntry, m_result);
	}

	logDebugLv0NL("Finished in thread '%p'", QThread::currentThreadId());
	logDebugLv0NL("%s", "-----------------------------------------------");

	/* ### Worker task end. ### */
}

/*!
 * @brief Set possible PDZ payment methods based on PDZinfos of data box.
 *
 * @param[in] pdzInfos List of available PDZ payment methods.
 * @param[in] canSendPdz True if can receive a commercial messages.
 * @param[in] canSendInitiatory True if can receive initiatory commercial messages.
 * @param[in] canUseInitReply True if can use prepaid reply commercial messages.
 * @return List of possible PDZ payment methods.
 */
static
QList<Isds::Type::DmType> setPossiblePdzPaymentMethods(
    const QList<Isds::PDZInfoRec> &pdzInfos, bool canSendPdz,
    bool canSendInitiatory, bool canUseInitReply)
{
	QList<Isds::Type::DmType> dmPaymentTypes;

	/* Warning: Do not change order of append and prepend of items. */
	/* Priority: O->G->K->E */
	foreach (const Isds::PDZInfoRec &info, pdzInfos) {
		if (info.pdzType() == Isds::Type::PPT_G) {
			/*
			 * The payment type G may be present only once.
			 * If it is active, it will always be used.
			 * Cannot be changed to another payment type
			 * but can be used type I.
			 */
			dmPaymentTypes.clear();
			dmPaymentTypes.append(Isds::Type::MT_G);
			if (canSendInitiatory) {
				dmPaymentTypes.append(Isds::Type::MT_I);
			}
			break;
		} else if (info.pdzType() == Isds::Type::PPT_K) {
			/* Can be selected also I when K is available. */
			if (canSendInitiatory) {
				dmPaymentTypes.append(Isds::Type::MT_I);
			}
			dmPaymentTypes.prepend(Isds::Type::MT_K);
		} else if (info.pdzType() == Isds::Type::PPT_E) {
			dmPaymentTypes.append(Isds::Type::MT_E);
		} else if (info.pdzType() == Isds::Type::PPT_O) {
			/* Not used now. */
		}
	}

	/* Use PDZSendInfo results if PDZInfo is empty or unknown. */
	if (pdzInfos.isEmpty()) {
		if (canSendPdz == Isds::Type::BOOL_TRUE) {
			dmPaymentTypes.append(Isds::Type::MT_K);
			dmPaymentTypes.append(Isds::Type::MT_E);
		}
		if (canSendInitiatory) {
			dmPaymentTypes.append(Isds::Type::MT_I);
		}
	}

	/* Add prepaid reply payment method if it is available. */
	if (canUseInitReply) {
		dmPaymentTypes.prepend(Isds::Type::MT_O);
	}

	return dmPaymentTypes;
}

enum TaskRecipientInfo::Result TaskRecipientInfo::recipientInfo(
    const AcntId &acntId, const QString &dbID, bool senderOvm,
    bool canUseInitReply, const QList<Isds::PDZInfoRec> &pdzInfos,
    DataboxModelEntry &dbEntry, QString &lastError)
{
	if (Q_UNLIKELY((!acntId.isValid()) || (dbID.isEmpty()) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (!GlobInstcs::isdsSessionsPtr->holdsSession(acntId)))) {
		Q_ASSERT(0);
		return TR_ERR;
	}

	Isds::Session *session = GlobInstcs::isdsSessionsPtr->session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		Q_ASSERT(0);
		return TR_ERR;
	}

	dbEntry.setDbID(dbID);

	logDebugLv1NL("Sending from account '%s'",
	    acntId.username().toUtf8().constData());

	/* Search for box information according to supplied identifier. */
	bool lastPage;
	QList<Isds::DbResult2> foundBoxes;
	long int totalCount, currentCount, position;
	enum Isds::Type::Error error = Isds::isdsSearch3(*session, dbID,
	    Isds::Type::FST_BOX_ID, Isds::Type::BT_NULL, 0, 1,
	    Isds::Type::BOOL_FALSE, foundBoxes, totalCount, currentCount,
	    position, lastPage, Q_NULLPTR, &lastError);

	QString recipDbType;
	QString recipIc;
	QString recipName;
	QString recipAddress;
	enum Isds::Type::NilBool recipDbEffectiveOVM = Isds::Type::BOOL_NULL;
	enum Result result = TR_SUCCESS;

	if (foundBoxes.size() == 1) {

		const Isds::DbResult2 &entry(foundBoxes.first());

		/* Recipient data box is not active. */
		if (!entry.active()) {
			return TR_DB_NO_ACTIVE;
		}

		/* Set current recipient name and address, data box type. */
		recipIc = entry.ic();
		recipName = entry.dbName();
		recipAddress = entry.dbAddress();
		recipDbType= dbType2Str(foundBoxes.at(0).dbType());

		/* Test if recipient data box is OVM or sub-OVM */
		recipDbEffectiveOVM =
		    ((Isds::Type::BT_NULL < entry.dbType()) && (entry.dbType() < Isds::Type::BT_PO))
		        ? Isds::Type::BOOL_TRUE : Isds::Type::BOOL_FALSE;

	} else if (foundBoxes.isEmpty()) {

		if (error == Isds::Type::ERR_SUCCESS) {
			/* No data box found. */
			return TR_DB_NOT_EXISTS;
		} else {
			/*
			 * Search error -- maybe because of insufficient
			 * privileges.
			 * Try downloading PDZ sending information instead
			 * of exiting here.
			 */
		}

	} else {
		Q_ASSERT(0);
		return TR_ERR;
	}

	const bool sendPublic = senderOvm ||
	    (recipDbEffectiveOVM == Isds::Type::BOOL_TRUE);
	enum Isds::Type::NilBool canSendNormalPDZ =
	    sendPublic ? Isds::Type::BOOL_FALSE : Isds::Type::BOOL_NULL;
	bool canSendInitiatoryPDZ = false;
	QList<Isds::Type::DmType> dmTypes;

	/* Sender is non-OVM, recipient is non-OVM so get PDZ send info. */
	if (!sendPublic) {

		bool canSendNormal = sendPublic;
		enum Isds::Type::Error error = Isds::pdzSendInfo(*session, dbID,
		    Isds::Type::PMT_NORMAL, canSendNormal, Q_NULLPTR, &lastError);
		if (error == Isds::Type::ERR_SUCCESS) {
			canSendNormalPDZ = canSendNormal ?
			    Isds::Type::BOOL_TRUE : Isds::Type::BOOL_FALSE;
			if (canSendNormal) {
				Isds::pdzSendInfo(*session, dbID, Isds::Type::PMT_INIT,
				    canSendInitiatoryPDZ, Q_NULLPTR, &lastError);
			}
		}

		dmTypes = setPossiblePdzPaymentMethods(pdzInfos,
		    canSendNormal, canSendInitiatoryPDZ, canUseInitReply);
	}

	if (senderOvm) {
		/* I'm always sending public messages because I'm OVM. */
	} else if ((recipDbEffectiveOVM == Isds::Type::BOOL_NULL) &&
	    (canSendNormalPDZ == Isds::Type::BOOL_NULL)) {
		/*
		 * I'm not OVM and I could not download any information about
		 * the recipient.
		 */
		result = TR_DB_UNKN_OVM_UNKN_PDZ;

	} else if ((recipDbEffectiveOVM == Isds::Type::BOOL_NULL) &&
	    (canSendNormalPDZ == Isds::Type::BOOL_FALSE)) {
		/*
		 * Don't know whether I can send public and I cannot send PDZs.
		 */
		result = TR_DB_UNKN_PUBLIC_NO_PDZ;

	} else if ((recipDbEffectiveOVM == Isds::Type::BOOL_FALSE) &&
	    (canSendNormalPDZ == Isds::Type::BOOL_NULL)) {
		/*
		 * Cannot send public message and don't know whether I can send
		 * PDZs.
		 */
		result = TR_DB_NO_PUBLIC_UNKN_PDZ;

	} else if ((recipDbEffectiveOVM == Isds::Type::BOOL_FALSE) &&
	    (canSendNormalPDZ == Isds::Type::BOOL_FALSE)) {
		/* I cannot send a public nor a commercial message. */
		result = TR_DB_NO_PUBLIC_NO_PDZ;
	}

	dbEntry.setDbName(recipName);
	dbEntry.setDbAddress(recipAddress);
	dbEntry.setDbType(recipDbType);
	dbEntry.setDbIC(recipIc);
	dbEntry.setPdz(canSendNormalPDZ);
	if (!dmTypes.isEmpty()) {
		dbEntry.setDmType(dmTypes.at(0));
	} else {
		dbEntry.setDmType(Isds::Type::MT_UNKNOWN);
	}
	dbEntry.setDmTypes(dmTypes);

	return result;
}
