/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QThread>

#include "src/datovka_shared/isds/error.h"
#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/isds/services/box_interface.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"
#include "src/worker/emitter.h"
#include "src/worker/task_credit_info.h"

TaskCreditInfo::TaskCreditInfo(const AcntId &acntId, const QString &dbId,
    const QDate &fromDate, const QDate &toDate)
    : m_result(DL_ERR),
    m_lastError(),
    m_currentCredit(0),
    m_notifEmail(),
    m_creditEvents(),
    m_acntId(acntId),
    m_dbId(dbId),
    m_fromDate(fromDate),
    m_toDate(toDate)
{
	Q_ASSERT(m_acntId.isValid());
	Q_ASSERT(!m_dbId.isEmpty());
}

void TaskCreditInfo::run(void)
{
	logDebugLv0NL("%s", "----------DOWNLOAD CREDIT INFO TASK------------");
	logDebugLv0NL("Starting in thread '%p'", QThread::currentThreadId());

	/* ### Worker task begin. ### */

	m_result = creditInfo(m_acntId, m_dbId, m_fromDate, m_toDate,
	    m_currentCredit, m_notifEmail, m_creditEvents, m_lastError);

	logDebugLv0NL("Finished in thread '%p'", QThread::currentThreadId());
	logDebugLv0NL("%s", "-----------------------------------------------");

	/* ### Worker task end. ### */
}

/*!
 * @brief Translate communication error onto task error.
 */
static
enum TaskCreditInfo::Result error2result(enum Isds::Type::Error error)
{
	switch (error) {
	case Isds::Type::ERR_SUCCESS:
		return TaskCreditInfo::DL_SUCCESS;
		break;
	case Isds::Type::ERR_ISDS:
		return TaskCreditInfo::DL_ISDS_ERROR;
		break;
	case Isds::Type::ERR_SOAP:
	case Isds::Type::ERR_XML:
		return TaskCreditInfo::DL_XML_ERROR;
		break;
	default:
		return TaskCreditInfo::DL_ERR;
		break;
	}
}

enum TaskCreditInfo::Result TaskCreditInfo::creditInfo(const AcntId &acntId,
    const QString &dbId, const QDate &fromDate, const QDate &toDate,
    qint64 &currentCredit, QString &notifEmail,
    QList<Isds::CreditEvent> &creditEvents, QString &lastError)
{
	if (Q_UNLIKELY((!acntId.isValid()) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (!GlobInstcs::isdsSessionsPtr->holdsSession(acntId)))) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	Isds::Session *session = GlobInstcs::isdsSessionsPtr->session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	logDebugLv1NL("Sending from account '%s'",
	    acntId.username().toUtf8().constData());

	enum Isds::Type::Error error = Isds::dataBoxCreditInfo(*session, dbId,
	    fromDate, toDate, currentCredit, notifEmail, creditEvents,
	    Q_NULLPTR, &lastError);
	if (Q_UNLIKELY(error != Isds::Type::ERR_SUCCESS)) {
		return error2result(error);
	}

	return DL_SUCCESS;
}
