/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/types.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing change password.
 */
class TaskChangePassword : public Task {
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		DL_SUCCESS, /*!< Operation was successful. */
		DL_ISDS_ERROR, /*!< Error communicating with ISDS. */
		DL_XML_ERROR, /*!< Error parsing XML. */
		DL_DB_INS_ERR, /*!< Error inserting into database. */
		DL_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] otpMethod OTP login method (TOTP/HOTP).
	 * @param[in] oldPwd Old password.
	 * @param[in] newPwd New password.
	 * @param[in] otpCode Otp code.
	 */
	explicit TaskChangePassword(const AcntId &acntId,
	    enum Isds::Type::OtpMethod otpMethod, const QString &oldPwd,
	    const QString &newPwd, const QString &otpCode);

	/*!
	 * @brief Performs actual change password.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	/*!
	 * @brief Change password.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] oldPwd Old password.
	 * @param[in] newPwd New password.
	 * @param[out] lastError Last error description.
	 * @return Error state.
	 */
	static
	enum Result changePassword(const AcntId &acntId,
	    const QString &oldPwd, const QString &newPwd, QString &lastError);

	/*!
	 * @brief Change password otp.
	 *
	 * @param[in] acntId Account id.
	 * @param[in] oldPwd Old password.
	 * @param[in] newPwd New password.
	 * @param[in] otpCode Otp code.
	 * @param[in] otpMethod OTP login method (TOTP/HOTP).
	 * @param[out] lastError Last error description.
	 * @return Error state.
	 */
	static
	enum Result changePasswordOtp(const AcntId &acntId,
	    const QString &oldPwd, const QString &newPwd,
	    const QString &otpCode, enum Isds::Type::OtpMethod otpMethod,
	    QString &lastError);

	enum Result m_result; /*!< Return state. */
	QString m_isdsText; /*!< Return ISDS result text. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskChangePassword(const TaskChangePassword &);
	TaskChangePassword &operator=(const TaskChangePassword &);

	const AcntId m_acntId; /*!< Account id. */
	enum Isds::Type::OtpMethod m_otpMethod; /*!< OTP login method (TOTP/HOTP). */
	QString m_oldPwd; /*!< Old password. */
	QString m_newPwd; /*!< New password. */
	QString m_otpCode; /*!< Otp code. */
};
