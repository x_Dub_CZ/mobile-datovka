/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/isds/box_interface.h"
#include "src/worker/task.h"

/*!
 * @brief Task describing download PDZ information.
 */
class TaskPDZInfo : public Task {
public:
	/*!
	 * @brief Return state describing what happened.
	 */
	enum Result {
		DL_SUCCESS, /*!< Operation was successful. */
		DL_ISDS_ERROR, /*!< Error communicating with ISDS. */
		DL_XML_ERROR, /*!< Error parsing XML. */
		DL_DB_INS_ERR, /*!< Error inserting into database. */
		DL_ERR /*!< Other error. */
	};

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] dbId Data box identifier.
	 */
	explicit TaskPDZInfo(const AcntId &acntId, const QString &dbId);

	/*!
	 * @brief Performs action.
	 */
	virtual
	void run(void) Q_DECL_OVERRIDE;

	enum Result m_result; /*!< Return state. */
	QString m_lastError; /*!< Last ISDS error message. */
	QList<Isds::PDZInfoRec> m_pdzInfoRecs; /*!< PDZ info record list. */
	qint64 m_currentCredit; /*!< Current credit in CZK. */

private:
	/*!
	 * Disable copy and assignment.
	 */
	TaskPDZInfo(const TaskPDZInfo &);
	TaskPDZInfo &operator=(const TaskPDZInfo &);

	/*!
	 * @brief Download PDZ information from ISDS.
	 *
	 * @param[in]  acntId Account identifier.
	 * @param[in]  dbId Data box identifier.
	 * @param[out] pdzInfoRecs List of PDZ info.
	 * @param[out] currentCredit Current credit in CZK.
	 * @param[out] lastError Last ISDS error message.
	 * @return Error state.
	 */
	static
	enum Result pdzInfo(const AcntId &acntId, const QString &dbId,
	    QList<Isds::PDZInfoRec> &pdzInfoRecs, qint64 &currentCredit,
	    QString &lastError);

	const AcntId m_acntId; /*!< Account identifier. */
	const QString m_dbId; /*!< Data box identifier. */
};
