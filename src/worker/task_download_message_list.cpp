/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QThread>

#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/worker/pool.h" /* List with whole messages. */
#include "src/global.h"
#include "src/isds/io/connection.h"
#include "src/isds/services/message_interface.h"
#include "src/isds/session/isds_session.h"
#include "src/isds/session/isds_sessions.h"
#include "src/net/db_wrapper.h"
#include "src/worker/emitter.h"
#include "src/worker/task_download_message.h"
#include "src/worker/task_download_message_list.h"
#include "src/settings/account.h"
#include "src/settings/accounts.h"
#include "src/settings/prefs_specific.h"
#include "src/sqlite/message_db_container.h"

TaskDownloadMessageList::TaskDownloadMessageList(const AcntId &acntId,
    enum Messages::MessageType msgDirect,
    Isds::Type::DmFiltStates dmStatusFilter, unsigned long int dmOffset,
    unsigned long int dmLimit, WorkerPool *workPool, bool downloadCompleteMsgs,
    bool saveZfo, bool isTestAccount)
    : m_result(DL_ERR),
    m_statusBarText(),
    m_acntId(acntId),
    m_msgDirect(msgDirect),
    m_dmStatusFilter(dmStatusFilter),
    m_dmOffset(dmOffset),
    m_dmLimit(dmLimit),
    m_workPool(workPool),
    m_downloadCompleteMsgs(downloadCompleteMsgs),
    m_saveZfo(saveZfo),
    m_isTestAccount(isTestAccount),
    m_lastError()
{
}

void TaskDownloadMessageList::run(void)
{
	if (Q_UNLIKELY((Messages::TYPE_RECEIVED != m_msgDirect) &&
	        (Messages::TYPE_SENT != m_msgDirect))) {
		Q_ASSERT(0);
		return;
	}

	/* ### Worker task begin. ### */

	if ((Messages::TYPE_RECEIVED == m_msgDirect)) {
		logDebugLv0NL("%s", "---------RECEIVED MESSAGE LIST TASK------------");
	} else {
		logDebugLv0NL("%s", "------------SENT MESSAGE LIST TASK-------------");
	}
	logDebugLv0NL("Starting in thread '%p'", QThread::currentThreadId());

	m_result = downloadMessageList(m_acntId, m_msgDirect, m_dmStatusFilter,
	    m_dmOffset, m_dmLimit, m_workPool, m_downloadCompleteMsgs,
	    m_saveZfo, m_isTestAccount, m_statusBarText, m_lastError);

	if (GlobInstcs::msgProcEmitterPtr != Q_NULLPTR) {
		emit GlobInstcs::msgProcEmitterPtr->downloadMessageListFinishedSignal(
		    m_acntId,
		    TaskDownloadMessageList::DL_SUCCESS == m_result,
		    m_statusBarText, m_lastError,
		    m_msgDirect == Messages::TYPE_RECEIVED);
	}

	logDebugLv0NL("Finished in thread '%p'", QThread::currentThreadId());
	logDebugLv0NL("%s", "-----------------------------------------------");

	/* ### Worker task end. ### */
}

/*!
 * @brief Translate communication error onto task error.
 */
static
enum TaskDownloadMessageList::Result error2result(enum Isds::Type::Error error)
{
	switch (error) {
	case Isds::Type::ERR_SUCCESS:
		return TaskDownloadMessageList::DL_SUCCESS;
		break;
	case Isds::Type::ERR_ISDS:
		return TaskDownloadMessageList::DL_ISDS_ERROR;
		break;
	case Isds::Type::ERR_SOAP:
	case Isds::Type::ERR_XML:
		return TaskDownloadMessageList::DL_XML_ERROR;
		break;
	default:
		return TaskDownloadMessageList::DL_ERR;
		break;
	}
}

enum TaskDownloadMessageList::Result TaskDownloadMessageList::downloadMessageList(
    const AcntId &acntId, enum Messages::MessageType msgDirect,
    Isds::Type::DmFiltStates dmStatusFilter, unsigned long int dmOffset,
    unsigned long int dmLimit, WorkerPool *workPool, bool downloadCompleteMsgs,
    bool saveZfo, bool isTestAccount, QString &statusBarText, QString &lastError)
{
	if (Q_UNLIKELY((!acntId.isValid()) ||
	        (GlobInstcs::isdsSessionsPtr == Q_NULLPTR) ||
	        (GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (!GlobInstcs::isdsSessionsPtr->holdsSession(acntId)))) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	Isds::Session *session = GlobInstcs::isdsSessionsPtr->session(acntId);
	if (Q_UNLIKELY(session == Q_NULLPTR)) {
		Q_ASSERT(0);
		return DL_ERR;
	}

	logDebugLv1NL("Sending from account '%s'",
	    acntId.username().toUtf8().constData());

	enum Isds::Type::Error error = Isds::Type::ERR_ERROR;
	QList<Isds::Envelope> envelopes;
	if (Messages::TYPE_RECEIVED == msgDirect) {
		error = Isds::getListOfReceivedMessages(*session, QDateTime(),
		    QDateTime(), -1, dmStatusFilter, dmOffset, dmLimit,
		    envelopes, Q_NULLPTR, &lastError);
	} else {
		error = Isds::getListOfSentMessages(*session, QDateTime(),
		    QDateTime(), -1, dmStatusFilter, dmOffset, dmLimit,
		    envelopes, Q_NULLPTR, &lastError);
	}
	if (Q_UNLIKELY(error != Isds::Type::ERR_SUCCESS)) {
		return error2result(error);
	}

	if (envelopes.isEmpty()) {
		statusBarText = tr("%1: No new messages.").arg(acntId.username());
		return DL_SUCCESS;
	}

	/* Store data into db */
	QList<qint64> listOfNewMsgIds;
	QList<qint64> messageChangedStatusList;
	if (!DbWrapper::insertMessageListToDb(acntId,
	    (Messages::TYPE_RECEIVED == msgDirect) ? MessageDb::TYPE_RECEIVED
	        : MessageDb::TYPE_SENT,
	    envelopes, messageChangedStatusList, lastError,
	    listOfNewMsgIds)) {
		logErrorNL("DB: Insert error: %s",
		    lastError.toUtf8().constData());
		return DL_DB_INS_ERR;
	}

	/* Store info text about new messages (for showing in status bar) */
	statusBarText = lastError;

	/*
	 * For sent message where message status was changed
	 * we should update delivery info as well.
	 */
	if (Messages::TYPE_SENT == msgDirect) {

		foreach (qint64 msgId, messageChangedStatusList) {

			/*
			 * Following functions don't have to check return values
			 * because they are complementary actions for message
			 * list download (delivery info is not important).
			 */
			Isds::Message message;

			Isds::getSignedDeliveryInfo(*session, msgId, message,
			    Q_NULLPTR, Q_NULLPTR);
			/* Store events from delivery info into database. */
			if (!message.envelope().dmEvents().isEmpty()) {
				DbWrapper::insertMesasgeDeliveryInfoToDb(acntId,
				    message.envelope().dmEvents(), msgId,
				    lastError);
			}
		}
	}

	/* Download new complete messages if this setting is enabled.
	 * (create and insert download compete message tasks to worker pool) */
	if (downloadCompleteMsgs) {
		foreach (qint64 msgId, listOfNewMsgIds) {
			TaskDownloadMessage *task;
			task = new (::std::nothrow) TaskDownloadMessage(acntId,
			    msgId, msgDirect, saveZfo, isTestAccount);
			task->setAutoDelete(true);
			workPool->assignLo(task, WorkerPool::APPEND);
		}
	}

	logDebugLv1NL("%s", "Message list has been downloaded");

	return DL_SUCCESS;
}
