/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include "src/datovka_shared/records_management/models/upload_hierarchy_proxy_model.h"

/*!
 * @brief Enables filtering from QML according to metadata.
 */
class UploadHierarchyQmlProxyModel : public RecMgmt::UploadHierarchyProxyModel {
	Q_OBJECT

public:
	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit UploadHierarchyQmlProxyModel(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Copy constructor.
	 *
	 * @note Needed for QVariant conversion.
	 * @note This is a dummy function. Calling this constructor causes an
	 *     assertion failure.
	 *
	 * @param[in] other Model to be copied.
	 * @param[in] parent Pointer to parent object.
	 */
	explicit UploadHierarchyQmlProxyModel(
	    const UploadHierarchyQmlProxyModel &other,
	    QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Set source model.
	 *
	 * @param[in] sourceModel Source model to be used.
	 */
	Q_INVOKABLE virtual
	void setSourceModel(QAbstractItemModel *sourceModel) Q_DECL_OVERRIDE;

	/*!
	 * @brief Map proxy model row onto source model row.
	 *
	 * @note Calls mapToSource() but cannot be called mapToSource() because
	 *     it would hide an overloaded virtual method.
	 *
	 * @param[in] proxyRow Proxy model row.
	 * @return Corresponding source model row.
	 */
	Q_INVOKABLE
	int mapToSourceContent(int proxyRow) const;

	/*!
	 * @brief Set the role where the key is used for filtering.
	 *
	 * @param[in] role Role number, -1 for all roles.
	 */
	Q_INVOKABLE
	void setFilterRole(int role);

	/*!
	 * @brief Sorts the model content.
	 *
	 * @note Calls sort() but cannot be called sort() because
	 *     it would hide an overloaded virtual method.
	 */
	Q_INVOKABLE
	void sortContent(void);

public slots:
	/*!
	 * @brief Sets regular expression according to expression core.
	 *
	 * @param[in] patternCore Regular expression core.
	 */
	Q_INVOKABLE
	void setFilterRegExpStr(const QString &patternCore);
};

/* QML passes its arguments via QVariant. */
Q_DECLARE_METATYPE(UploadHierarchyQmlProxyModel)
