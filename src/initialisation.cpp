/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QtGlobal> /* QT_VERSION, qVersion() */
#include <QTimeZone>

#include "src/datovka_shared/log/log.h"
#include "src/initialisation.h"

void logAppVersion(void)
{
	logInfoNL("Application version %s.", VERSION);
}

void logQtVersion(void)
{
	logInfoNL("Compile-time Qt version 0x%x (%s).", QT_VERSION, QT_VERSION_STR);
	logInfoNL("Run-time Qt version %s.", qVersion());
}

void logTimeZone(void)
{
	const QTimeZone sysZone(
#if (QT_VERSION >= QT_VERSION_CHECK(5, 5, 0))
	    QTimeZone::systemTimeZone()
#else /* < Qt-5.5 */
	    QTimeZone::systemTimeZoneId()
#endif /* >= Qt-5.5 */
	);
	int offset = sysZone.offsetFromUtc(QDateTime::currentDateTime());
	logInfoNL("Local time zone is %s (UTC%+ds).", sysZone.id().constData(),
	    offset);
}
