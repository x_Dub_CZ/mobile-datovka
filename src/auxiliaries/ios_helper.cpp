/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDir>
#include <QFileInfo>
#include <QMessageBox>
#include <QQmlEngine>

#include "src/auxiliaries/ios_helper.h"
#include "src/io/filesystem.h"
#include "src/datovka_shared/log/log.h"

#ifdef Q_OS_IOS
#include "ios/src/icloud_io.h"
#define ICLOUD_DATOVKA_CONTAINER_NAME "Datovka"
#define SEND_FILE_PATH_PREFIX "Documents/Datovka"
#endif /* Q_OS_IOS */

void IosHelper::declareQML(void)
{
	qmlRegisterUncreatableType<IosHelper>("cz.nic.mobileDatovka.iOsHelper", 1, 0, "IosImportAction", "Access to enums & flags only.");
	qRegisterMetaType<IosHelper::IosImportAction>("IosHelper::IosImportAction");
}

IosHelper::IosHelper(QObject *parent)
    : QObject(parent),
    m_importAction(IosImportAction::IMPORT_NONE)
{
}

void IosHelper::storeFilesToCloud(
    const QList<IosHelper::FileICloud> &iCloudFiles)
{
	debugFuncCall();

#ifdef Q_OS_IOS

	if (!ICloudIo::isCloudOn()) {
		QMessageBox::critical(Q_NULLPTR, tr("iCloud error"),
		    tr("Unable to access iCloud account. Open the settings and check your iCloud settings."),
		    QMessageBox::Ok);
		return;
	}

	/* Upload files to iCloud */
	bool success = true;
	QStringList errorUploads;
	IosHelper::FileICloud iCloudFile;

	foreach (iCloudFile, iCloudFiles) {

		ICloudIo::ICloudResult retCode =
		    ICloudIo::moveFileToCloud(iCloudFile.srcFilePath,
		    iCloudFile.destiCloudPath);

		QFileInfo fi(iCloudFile.srcFilePath);

		switch (retCode) {
		case ICloudIo::ICLOUD_NOT_ON:
			success = false;
			errorUploads.append(tr("Unable to access iCloud!"));
			goto finish;
			break;
		case ICloudIo::ICLOUD_TARGET_SAVE_DIR_ERROR:
			success = false;
			errorUploads.append(tr("Cannot create subdirectory '%1' in iCloud.").arg(iCloudFile.destiCloudPath));
			goto finish;
			break;
		case ICloudIo::ICLOUD_FILE_EXISTS:
			success = false;
			errorUploads.append(tr("File '%1' already exists in iCloud.").arg(fi.fileName()));
			break;
		case ICloudIo::ICLOUD_FILE_UPLOAD_ERROR:
			success = false;
			errorUploads.append(tr("File '%1' upload failed.").arg(fi.fileName()));
			break;
		case ICloudIo::ICLOUD_FILE_UPLOAD_SUCCESS:
			errorUploads.append(tr("File '%1' has been stored into iCloud.").arg(fi.fileName()));
			break;
		default:
			break;
		}
	}

finish:

	/* Delete files and source directory. */
	QFileInfo fi(iCloudFile.srcFilePath);
	QDir dir(fi.absolutePath());
	dir.removeRecursively();

	/* Show final notification */
	if (success) {
		QMessageBox::information(Q_NULLPTR, tr("Saved to iCloud"),
		    tr("Files have been stored into iCloud.") + "\n\n" +
		    tr("Path: '%1'").arg(QString(ICLOUD_DATOVKA_CONTAINER_NAME)
		        + "/" + iCloudFile.destiCloudPath),
		    QMessageBox::Ok);
	} else {
		QString txt;
		foreach (const QString &error, errorUploads) {
			txt += "\n" + error;
		}
		QMessageBox::warning(Q_NULLPTR,
		    tr("iCloud Problem"),
		    tr("Files have not been saved!") + "\n" + txt,
		    QMessageBox::Ok);
	}

#else /* !Q_OS_IOS */
	Q_UNUSED(iCloudFiles);
#endif /* Q_OS_IOS */
}

QString IosHelper::getShortSendFilePath(const QString &sandBoxFilePath)
{
#ifdef Q_OS_IOS

	// Get short local send file path
	QString pattern(SEND_FILE_PATH_PREFIX);
	int pos = sandBoxFilePath.indexOf(pattern) + pattern.length();
	return sandBoxFilePath.mid(pos);

#else
	Q_UNUSED(sandBoxFilePath);
	return QString();
#endif
}

void IosHelper::clearRestoreFolder(const QString &folder)
{
	debugFuncCall();

	QDir dir(folder);
	dir.removeRecursively();
}

void IosHelper::clearBackupTransferDirs(void)
{
	debugFuncCall();

	QDir dir(appBackupDirPath());
	dir.removeRecursively();
	dir.setPath(appTransferDirPath());
	dir.removeRecursively();
}

void IosHelper::clearSendAndTmpDirs(void)
{
	debugFuncCall();

	QDir dir(appSendDirPath());
	dir.removeRecursively();
	dir.setPath(appTmpDirPath());
	dir.removeRecursively();
}

void IosHelper::openDocumentPickerControllerForImport(enum IosImportAction action,
    const QStringList &allowedUtis)
{
	debugFuncCall();

#ifdef Q_OS_IOS

	if (IosImportAction::IMPORT_NONE != m_importAction) {
		// Another document picker import acitvity is running.
		return;
	}

	m_importAction = action;

	if (!ICloudIo::openDocumentPickerControllerForImport(allowedUtis)) {
		m_importAction = IosImportAction::IMPORT_NONE;
	}

#else /* !Q_OS_IOS */
	Q_UNUSED(action);
	Q_UNUSED(allowedUtis);
#endif /* Q_OS_IOS */
}

void IosHelper::importFilesToAppInbox(const QList<QUrl> &selectedFileUrls)
{
	if (selectedFileUrls.isEmpty()) {
		m_importAction = IosImportAction::IMPORT_NONE;
		return;
	}

	QStringList filePaths;
	switch (m_importAction) {
	case IosImportAction::IMPORT_SEND:
		foreach (const QUrl &fileUrl, selectedFileUrls) {
			filePaths.append(moveFileToTargetPath(fileUrl,
			    appSendDirPath()));
		}
		emit sendFilesSelectedSig(filePaths);
		break;
	case IosImportAction::IMPORT_ZFOS:
		foreach (const QUrl &fileUrl, selectedFileUrls) {
			filePaths.append(moveFileToTargetPath(fileUrl,
			    appTmpDirPath()));
		}
		emit zfoFilesSelectedSig(filePaths);
		break;
	case IosImportAction::IMPORT_CERT:
		foreach (const QUrl &fileUrl, selectedFileUrls) {
			filePaths.append(moveFileToTargetPath(fileUrl,
			    appCertDirPath()));
		}
		emit certFilesSelectedSig(filePaths);
		break;
	case IosImportAction::IMPORT_JSON:
		foreach (const QUrl &fileUrl, selectedFileUrls) {
			filePaths.append(moveFileToTargetPath(fileUrl,
			    appRestoreDirPath()));
		}
		emit jsonFilesSelectedSig(filePaths);
		break;
	case IosImportAction::IMPORT_ZFO_DIR:
		/*
		 * TODO - Import ZFOs from selected directory on iOS is not used
		 * now because the UIDocumentPickerViewController still has not
		 * support for this.
		 */
		break;
	default: break;
	}

	m_importAction = IosImportAction::IMPORT_NONE;
}

void IosHelper::storeFilesToDeviceStorage(const QStringList &srcFilePaths)
{
	debugFuncCall();

#ifdef Q_OS_IOS

	if (Q_UNLIKELY(srcFilePaths.isEmpty())) {
		return;
	}

	ICloudIo::openDocumentPickerControllerForExport(srcFilePaths);

#else /* !Q_OS_IOS */
	Q_UNUSED(srcFilePaths);
#endif /* Q_OS_IOS */
}

bool IosHelper::isIos(void)
{
#ifdef Q_OS_IOS
	return true;
#else
	return false;
#endif
}

QString IosHelper::moveFileToTargetPath(const QUrl &sourceFileUrl,
	const QString &targetPath)
{
#ifdef Q_OS_IOS

	if (Q_UNLIKELY(!sourceFileUrl.isValid() || targetPath.isEmpty())) {
		return QString();
	}

	QUrl targetFileUrl(ICloudIo::moveFile(sourceFileUrl, targetPath));
	if (targetFileUrl.isValid()) {
		targetFileUrl.setScheme(QString());
		return QUrl::fromPercentEncoding(targetFileUrl.toString().toUtf8());
	}

#else /* !Q_OS_IOS */
	Q_UNUSED(sourceFileUrl);
	Q_UNUSED(targetPath);
#endif

	return QString();
}

QString IosHelper::getCertFileLocation(void)
{
	return appCertDirPath();
}
