/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QMap>
#include <QPair>
#include <QString>
#include <QVector>

#include "src/sqlite/zfo_db_tables.h"

namespace MsgZfoTbl {
	const QString tabName("message_zfos");

	const QVector< QPair<QString, enum EntryType> > knownAttrs = {
	{"dmID",  DB_INTEGER}, /* NOT NULL*/
	{"testEnv", DB_BOOLEAN}, /* NOT NULL */
	{"lastAccessTime", DB_INTEGER}, /* NOT NULL*/
	{"size", DB_INTEGER}, /* NOT NULL*/
	{"data", DB_TEXT} /* NOT NULL*/
	/*
	 * CHECK (testEnv IN (0, 1)),
	 * UNIQUE (dmID, testEnv)
	 */
	};

	const QMap<QString, QString> colConstraints = {
	    {"dmID", "NOT NULL"},
	    {"testEnv", "NOT NULL"},
	    {"lastAccessTime", "NOT NULL"},
	    {"size", "NOT NULL"},
	    {"data", "NOT NULL"}
	};

	const QString &tblConstraint(
	    ",\n"
	    "        CHECK (testEnv IN (0, 1)),\n"
	    "        UNIQUE (dmID, testEnv)"
	);

	const QMap<QString, SQLiteTbl::AttrProp> attrProps = {
	{"dmID",             {DB_INTEGER, ""}},
	{"testEnv",          {DB_BOOLEAN, ""}},
	{"lastAccessTime",   {DB_INTEGER, ""}},
	{"size",             {DB_INTEGER, ""}},
	{"data",             {DB_TEXT, ""}}
	};
} /* namespace MsgZfoTbl */
SQLiteTbl msgZfoTbl(MsgZfoTbl::tabName, MsgZfoTbl::knownAttrs,
    MsgZfoTbl::attrProps, MsgZfoTbl::colConstraints, MsgZfoTbl::tblConstraint);

namespace ZfoSizeCntTbl {
	const QString tabName("zfo_size_cnt");

	const QVector< QPair<QString, enum EntryType> > knownAttrs = {
	    {"id", DB_INTEGER},
	    {"totalSize", DB_INTEGER}
	};

	const QMap<QString, QString> colConstraints = {
	    {"id", "NOT NULL"},
	    {"totalSize", "NOT NULL"}
	};

	const QString &tblConstraint(
	    ",\n"
	    "        PRIMARY KEY (id),\n"
	    "        CHECK (id = 0)"
	);

	const QMap<QString, SQLiteTbl::AttrProp> attrProps = {
	    {"id", {DB_INTEGER, ""}},
	    {"totalSize", {DB_INTEGER, ""}}
	};
} /* namespace ZfoSizeCntTbl */
SQLiteTbl zfoSizeCntTbl(ZfoSizeCntTbl::tabName, ZfoSizeCntTbl::knownAttrs,
    ZfoSizeCntTbl::attrProps, ZfoSizeCntTbl::colConstraints,
    ZfoSizeCntTbl::tblConstraint);
