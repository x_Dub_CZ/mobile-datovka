/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QString>

#include "src/datovka_shared/io/sqlite/db_single.h"

/* Zfo db filename */
#define ZFO_DB_NAME "zfo.db"

/*!
 * @brief Encapsulates zfo database.
 */
class ZfoDb : public SQLiteDbSingle {

public:
	/* Use parent class constructor. */
	using SQLiteDbSingle::SQLiteDbSingle;

	/* Make some inherited methods public. */
	using SQLiteDbSingle::closeDb;

	/*!
	 * @brief Get Zfo content.
	 *
	 * @param[in] msgId String with message id.
	 * @param[in] isTestAccount True if account is in the ISDS testing environment.
	 * @return Zfo content in base64.
	 */
	QByteArray getZfoContentFromDb(qint64 msgId, bool isTestAccount);

	/*!
	 * @brief Get Zfo size in bytes.
	 *
	 * @param[in] msgId String with message id.
	 * @param[in] isTestAccount True if account is in the ISDS testing environment.
	 * @return Size of zfo file in bytes.
	 */
	int getZfoSizeFromDb(qint64 msgId, bool isTestAccount);

	/*!
	 * @brief Insert or update zfo file info to db.
	 *
	 * @param[in] msgId message ID.
	 * @param[in] isTestAccount True if account is ISDS testing.
	 * @param[in] zfoSize Real size of zfo.
	 * @param[in] zfoBase64Data Zfo data in base64.
	 * @param[in] sizeLimit Zfo database size limit in bytes.
	 * @param[in] transaction True to create a transaction.
	 * @return true if success.
	 */
	bool insertZfoToDb(qint64 msgId, bool isTestAccount, int zfoSize,
	    const QByteArray &zfoBase64Data, unsigned int sizeLimit,
	    bool transaction);

	/*!
	 * @brief Test if is zfo database size exceeded.
	 *
	 * @param[in] currentLimit Current database size limit.
	 * @return True if db size is exceeded.
	 */
	bool isDbSizeExceeded(unsigned int currentLimit);

	/*!
	 * @brief Open database file.
	 *
	 * @param[in] fileName File name.
	 * @param[in] storeToDisk Whether to create db in memory.
	 * @return True on success.
	 */
	bool openDb(const QString &fileName, bool storeToDisk);

	/*!
	 * @brief Release db - delete oldest zfo files and vacuum db.
	 *
	 * @param[in] releaseSpace How many bytes should be released.
	 * @param[in] transaction True to create a transaction.
	 * @return True true if success.
	 */
	bool releaseDb(unsigned int releaseSpace, bool transaction);

	/*!
	 * @brief Update Zfo last access time.
	 *
	 * @param[in] msgId String with message id.
	 * @param[in] isTestAccount True if account is in the ISDS test environment.
	 */
	void updateZfoLastAccessTime(qint64 msgId, bool isTestAccount);

protected:
	/*!
	 * @brief Returns list of tables.
	 *
	 * @return List of pointers to tables.
	 */
	virtual
	QList<class SQLiteTbl *> listOfTables(void) const Q_DECL_OVERRIDE;

	/*!
	 * @brief Inserts ZFO size row if it does not exist.
	 *
	 * @return True on success.
	 */
	virtual
	bool assureConsistency(void) Q_DECL_OVERRIDE;
};
