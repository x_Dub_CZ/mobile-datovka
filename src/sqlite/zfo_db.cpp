/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <tuple> /* ::std::tuple */
#include <QDir>
#include <QFile>
#include <QMutexLocker>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QVariant>

#include "src/datovka_shared/compat_qt/variant.h" /* nullVariantWhenIsNull */
#include "src/datovka_shared/log/log.h"
#include "src/io/filesystem.h"
#include "src/sqlite/dbs.h"
#include "src/sqlite/zfo_db.h"
#include "src/sqlite/zfo_db_tables.h"

bool ZfoDb::openDb(const QString &fileName, bool storeToDisk)
{
	QString dirName(dfltDbAndConfigLoc());

	if (Q_UNLIKELY(storeToDisk && (dirName.isEmpty() || fileName.isEmpty()))) {
		Q_ASSERT(0);
		return false;
	}

	SQLiteDb::OpenFlags flags = SQLiteDb::CREATE_MISSING;
	flags |= storeToDisk ? SQLiteDb::NO_OPTIONS : SQLiteDb::FORCE_IN_MEMORY;

	return SQLiteDb::openDb(
	    dirName + QDir::separator() + QDir::toNativeSeparators(fileName),
	    flags);
}

QByteArray ZfoDb::getZfoContentFromDb(qint64 msgId, bool isTestAccount)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "SELECT data FROM message_zfos WHERE "
	    "dmID = :msgId AND testEnv = :isTestAccount";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":msgId", msgId);
	query.bindValue(":isTestAccount", isTestAccount);

	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		return query.value(0).toByteArray();
	}
fail:
	return QByteArray();
}

int ZfoDb::getZfoSizeFromDb(qint64 msgId, bool isTestAccount)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "SELECT size FROM message_zfos WHERE "
	    "dmID = :msgId AND testEnv = :isTestAccount";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":msgId", msgId);
	query.bindValue(":isTestAccount", isTestAccount);

	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		return query.value(0).toInt();
	}
fail:
	return 0;
}

bool ZfoDb::insertZfoToDb(qint64 msgId, bool isTestAccount, int zfoSize,
    const QByteArray &zfoData, unsigned int sizeLimit, bool transaction)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;
	qint64 lastAccessTime = -1;

	if (transaction) {
		transaction = beginTransaction();
		if (Q_UNLIKELY(!transaction)) {
			logErrorNL("%s", "Cannot begin transaction.");
			goto fail;
		}
	}

	queryStr = "SELECT lastAccessTime FROM message_zfos WHERE "
	    "dmID = :msgId AND testEnv = :isTestAccount";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":msgId", msgId);
	query.bindValue(":isTestAccount", isTestAccount);
	if (query.exec() && query.isActive()) {
		query.first();
		if (query.isValid()) {
			lastAccessTime = query.value(0).toLongLong();
		}
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	/* ZFO is in the database -> update and return. */
	if (-1 != lastAccessTime) {
		queryStr = "UPDATE message_zfos SET dmID = :msgId, "
		    "testEnv = :isTestAccount, lastAccessTime = :downloadTime, "
		    "size = :zfoSize, data = :zfoData "
		    "WHERE dmID = :msgId AND testEnv = :isTestAccount";
		if (Q_UNLIKELY(!query.prepare(queryStr))) {
			logErrorNL("Cannot prepare SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			goto fail;
		}
		query.bindValue(":msgId", msgId);
		query.bindValue(":isTestAccount", isTestAccount);
		query.bindValue(":downloadTime", QDateTime::currentSecsSinceEpoch());
		query.bindValue(":zfoSize", zfoSize);
		query.bindValue(":zfoData", nullVariantWhenIsNull(zfoData));
		if (Q_UNLIKELY(!query.exec())) {
			logErrorNL("Cannot execute SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			goto fail;
		}

		goto finish;
	}

	/* ZFO is not in the database -> insert and release db size if needed. */

	// if database is exceeded, release some oldest zfo files (based on size)
	if (isDbSizeExceeded(sizeLimit - zfoSize)) {
		releaseDb(zfoSize, false);
	}

	queryStr = "INSERT INTO message_zfos ("
	    "dmID, testEnv, lastAccessTime, size, data) VALUES ("
	    ":msgId, :isTestAccount, :downloadTime, :zfoSize, :zfoData)";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":msgId", msgId);
	query.bindValue(":isTestAccount", isTestAccount);
	query.bindValue(":downloadTime", QDateTime::currentSecsSinceEpoch());
	query.bindValue(":zfoSize", zfoSize);
	query.bindValue(":zfoData", nullVariantWhenIsNull(zfoData));
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	// update db size counter
	queryStr = "UPDATE zfo_size_cnt SET totalSize = totalSize + :zfoSize";

	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":zfoSize", zfoSize);
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

finish:
	if (transaction) {
		commitTransaction();
	}
	return true;

fail:
	if (transaction) {
		rollbackTransaction();
	}
	return false;
}

bool ZfoDb::isDbSizeExceeded(unsigned int currentLimit)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "SELECT totalSize FROM zfo_size_cnt WHERE id = :id";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	query.bindValue(":id", 0);
	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		return (currentLimit < query.value(0).toUInt());
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
	}

	return false;
}

void ZfoDb::updateZfoLastAccessTime(qint64 msgId, bool isTestAccount)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "UPDATE message_zfos SET "
	    "lastAccessTime = :newTime "
	    "WHERE dmID = :msgId AND testEnv = :isTestAccount";

	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return;
	}

	query.bindValue(":msgId", msgId);
	query.bindValue(":isTestAccount", isTestAccount);
	query.bindValue(":newTime", QDateTime::currentSecsSinceEpoch());

	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
	}
}

/*!
 * @brief Delete zfo from database.
 *
 * @param[in] query SQL query to work with.
 * @param[in] msgId String with message id.
 * @param[in] isTestAccount True if account is in the ISDS testing environment.
 * @param[in] zfoSize Zfo size.
 */
static
void deleteZfo(QSqlQuery &query, qint64 msgId, bool isTestAccount, int zfoSize)
{
	QString queryStr = "DELETE FROM message_zfos WHERE "
	    "dmID = :msgId AND testEnv = :isTestAccount";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":msgId", msgId);
	query.bindValue(":isTestAccount", isTestAccount);
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	queryStr = "UPDATE zfo_size_cnt SET totalSize = totalSize - :zfoSize";

	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	query.bindValue(":zfoSize", zfoSize);

	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

fail:
	return;
}

bool ZfoDb::releaseDb(unsigned int releaseSpace, bool transaction)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;
	unsigned int releasedBytes = 0;

	typedef ::std::tuple<qint64, bool, int> zfoEntry;
	QList<zfoEntry> releasedZfoEntries;

	if (transaction) {
		transaction = beginTransaction();
		if (Q_UNLIKELY(!transaction)) {
			logErrorNL("%s", "Cannot begin transaction.");
			goto fail;
		}
	}

	queryStr = "SELECT dmId, testEnv, size, lastAccessTime "
	    "FROM message_zfos ORDER BY lastAccessTime ASC";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	if (query.exec() && query.isActive()) {
		query.first();
		while (query.isValid()) {
			if (releaseSpace > releasedBytes) {
				releasedBytes += query.value(2).toLongLong();
				releasedZfoEntries.append(
				    zfoEntry(query.value(0).toLongLong(),
				        query.value(1).toBool(),
				        query.value(2).toInt()));
			} else {
				break;
			}
			query.next();
		}
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	foreach (const zfoEntry &entry, releasedZfoEntries) {
		deleteZfo(query, ::std::get<0>(entry), ::std::get<1>(entry),
		    ::std::get<2>(entry));
	}

	if (transaction) {
		commitTransaction();
	}

	/*
	 * Not using VACUUM here. Is is expensive and cannot be called from
	 * within a transaction.
	 */

	return true;

fail:
	if (transaction) {
		rollbackTransaction();
	}
	return false;
}

QList<class SQLiteTbl *> ZfoDb::listOfTables(void) const
{
	QList<class SQLiteTbl *> tables;
	tables.append(&msgZfoTbl);
	tables.append(&zfoSizeCntTbl);
	return tables;
}

bool ZfoDb::assureConsistency(void)
{
	logInfoNL("Assuring zfo_size_cnt content in database '%s'.",
	    fileName().toUtf8().constData());

	/* Insert ZFO size count row into table if it does not exist. */
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "SELECT count(*) FROM zfo_size_cnt";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s.",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	if (query.exec() && query.isActive() && query.first() && query.isValid()) {
		if (0 == query.value(0).toInt()) {
			queryStr = "INSERT INTO zfo_size_cnt (id,totalSize) "
			    "VALUES (0,0)";
			if (Q_UNLIKELY(!query.prepare(queryStr))) {
				logErrorNL("Cannot prepare SQL query: %s.",
				    query.lastError().text().toUtf8().constData());
				goto fail;
			}
			return query.exec();
		}
		return true;
	} else {
		logErrorNL("Cannot execute SQL query and/or read SQL data: %s.",
		    query.lastError().text().toUtf8().constData());
	}

fail:
	return false;
}
