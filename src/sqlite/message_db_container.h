/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QMap>
#include <QString>

#include "src/sqlite/message_db.h"

/*!
 * @brief Database container.
 */
class MsgDbContainer : private QMap<QString, MessageDb *> {
public:
	explicit MsgDbContainer(const QString &connectionName);
	~MsgDbContainer(void);

	/*!
	 * @brief Access/create+open message database set related to item.
	 *
	 * @param[in] locDir Directory where to search for the file.
	 * @param[in] userName ISDS user name, usually the login.
	 * @param[in] storeLocally True if it should be stored in local storage.
	 * @return Pointer to database, zero pointer on error.
	 */
	MessageDb *accessMessageDb(const QString &locDir,
	    const QString &userName, bool storeLocally);

	/*!
	 * @brief Delete all files related to dbset.
	 *
	 * @param db Deleted database.
	 * @return True on success.
	 */
	bool deleteDb(MessageDb *db);

	/*!
	 * @brief Creates the database name from supplied information.
	 *
	 * @param[in] userName ISDS user name.
	 * @return Database name.
	 */
	static
	QString constructDbFileName(const QString &userName);

	const QString m_connectionName;
};
