/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDir>
#include <QFile>

#include "src/sqlite/file_db_container.h"

FileDbContainer::FileDbContainer(const QString &connectionName)
    : QMap<QString, FileDb *>(),
    m_connectionName(connectionName)
{
}

FileDbContainer::~FileDbContainer(void)
{
	QMap<QString, FileDb *>::iterator i;

	for (i = this->begin(); i != this->end(); ++i) {
		delete i.value();
	}
}

FileDb *FileDbContainer::accessFileDb(const QString &locDir,
    const QString &userName, bool storeLocally)
{
	FileDb *db = Q_NULLPTR;
	bool open_ret;

	QString dbFileName = constructDbFileName(userName);

	/* Already opened. */
	if (this->find(dbFileName) != this->end()) {
		return (*this)[dbFileName];
	}

	db = new (::std::nothrow) FileDb(dbFileName);
	if (Q_UNLIKELY(Q_NULLPTR == db)) {
		Q_ASSERT(0);
		return Q_NULLPTR;
	}

	QString location = locDir + QDir::separator() +
	    QDir::toNativeSeparators(dbFileName);

	open_ret = db->openDb(location, storeLocally);
	if (Q_UNLIKELY(!open_ret)) {
		delete db;
		return Q_NULLPTR;
	}

	this->insert(dbFileName, db);

	return db;
}

bool FileDbContainer::deleteDb(FileDb *db)
{
	if (Q_UNLIKELY(Q_NULLPTR == db)) {
		Q_ASSERT(0);
		return false;
	}

	/* Find entry. */
	QMap<QString, FileDb *>::iterator it = this->begin();
	while ((it != this->end()) && (it.value() != db)) {
		++it;
	}
	/* Must exist. */
	if (Q_UNLIKELY(this->end() == it)) {
		Q_ASSERT(0);
		return false;
	}

	/* Remove from container. */
	this->erase(it);

	/* Get file name. */
	QString fileName = db->fileName();

	/* Close database. */
	delete db;

	if (fileName == SQLiteDb::memoryLocation) {
		return true;
	}

	/* Delete file. */
	if (Q_UNLIKELY(!QFile::remove(fileName))) {
		return false;
	}

	return true;
}

QString FileDbContainer::constructDbFileName(const QString &userName)
{
	return userName + "_file.db";
}
