/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/datovka_shared/io/db_tables.h"
#include "src/datovka_shared/io/records_management_db_tables.h"
#include "src/sqlite/account_db_tables.h"
#include "src/sqlite/file_db_tables.h"
#include "src/sqlite/message_db_tables.h"
#include "src/sqlite/zfo_db_tables.h"

void SQLiteTbls::localiseTableDescriptions(void)
{
	/* account_db */
	accntinfTbl.reloadLocalisedDescription();
	userinfTbl.reloadLocalisedDescription();
	dtinfTbl.reloadLocalisedDescription();
	/* message_db */
	msgsTbl.reloadLocalisedDescription();
	evntsTbl.reloadLocalisedDescription();
	/* file_db */
	flsTbl.reloadLocalisedDescription();
	/* zfo_db */
	msgZfoTbl.reloadLocalisedDescription();
	zfoSizeCntTbl.reloadLocalisedDescription();
	/* records_management_db */
	srvcInfTbl.reloadLocalisedDescription();
	strdFlsMsgsTbl.reloadLocalisedDescription();
}
