/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QList>
#include <QString>

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/io/sqlite/db.h"
#include "src/datovka_shared/isds/message_interface.h"
#include "src/messages.h"
#include "src/models/databoxmodel.h"

class MessageListModel; /* Forward declaration. */

/*!
 * @brief Encapsulates message database.
 */
class MessageDb : public SQLiteDb {

public:
	/*!
	 * @brief Used to distinguish between sent and received messages in db.
	 *
	 * @note This value cannot be changed without breaking backward
	 *     compatibility.
	 */
	enum MessageType {
		TYPE_RECEIVED = 1, /*!< One is received. */
		TYPE_SENT = 2 /*!< Two is sent. */
	};

	explicit MessageDb(const QString &connectionName);

	/*!
	 * @brief Copy db.
	 *
	 * @param[in] newFileName New file path.
	 * @return True on success.
	 *
	 * @note The copy is continued to be used. Original is closed.
	 */
	bool copyDb(const QString &newFileName);

	/*!
	 * @brief Open a new empty database file.
	 *
	 * @param[in] newFileName New file path.
	 * @return True on success.
	 */
	bool reopenDb(const QString &newFileName);

	/*!
	 * @brief Move db.
	 *
	 * @param[in] newFileName New file path.
	 * @return True on success.
	 */
	bool moveDb(const QString &newFileName);

	/*!
	 * @brief Delete message from db.
	 *
	 * @param[in] msgId Message ID.
	 * @param[in] transaction True to create a transaction.
	 * @return True on success.
	 */
	bool deleteMessageFromDb(qint64 msgId, bool transaction);

	/*!
	 * @brief Get list of databoxes from db.
	 *
	 * @param[in,out] dbModel Databox model to append databox info.
	 * @param[in] dbId Account databox ID.
	 */
	void getContactsFromDb(DataboxListModel *dbModel, const QString &dbId);

	/*!
	 * @brief Get count of new messages.
	 *
	 * @param[in] messageType Message type.
	 * @return count.
	 */
	int getNewMessageCountFromDb(enum MessageType messageType);

	/*!
	 * @brief Get message database size in bytes.
	 *
	 * @return Size in bytes.
	 */
	int getDbSizeInBytes(void);

	/*!
	 * @brief Get list of message IDs.
	 *
	 * @return Set of message IDs.
	 */
	QSet<qint64> getAllMessageIDsFromDb(void);

	/*!
	 * @brief Get list of messages from db which are older than number of days.
	 *
	 * @param[in] days Message age in days.
	 * @return List of expired message IDs.
	 */
	QList<qint64> getExpireMessageListFromDb(int days);

	/*!
	 * @brief Get count of received/sent messages.
	 *
	 * @param[in] messageType Message type.
	 * @return count.
	 */
	int getMessageCountFromDb(enum MessageType messageType);

	/*!
	 * @brief Return message envelope data.
	 *
	 * @param[in] dmId Message identifier.
	 * @return Message envelope data structure.
	 */
	Isds::Envelope getMessageEnvelopeFromDb(qint64 dmId);

	/*!
	 * @brief Set message list model from db for QML.
	 *
	 * @param[in,out] msgModel Model to append data to.
	 * @param[in] acntId Account identifier.
	 * @param[in] messageType Received or sent.
	 */
	void setMessageListModelFromDb(MessageListModel *msgModel,
	    const AcntId &acntId, enum MessageType messageType);

	/*!
	 * @brief Check if message exists in the database.
	 *
	 * @param[in] msgId message ID.
	 * @param[out] hasFiles message has downloaded attachments.
	 * @return return dmMessageStatus or -1 if not exist.
	 */
	short getMessageStatusFromDb(qint64 msgId, bool &hasFiles) const;

	/*!
	 * @brief Return HTML formatted message description.
	 *
	 * @param[in] dmId Message identifier.
	 * @return HTML formatted string containing message information.
	 *     Empty string is returned on error.
	 */
	QString getMessageDetailHtmlFromDb(qint64 dmId) const;

	/*!
	 * @brief Return some message data for email body.
	 *
	 * @param[in] dmId Message identifier.
	 * @param[out] body Email text.
	 * @param[out] subject Message subject.
	 * @return True on success.
	 */
	bool getMessageEmailDataFromDb(qint64 dmId, QString &body,
	    QString &subject);

	/*!
	 * @brief Insert or update events.
	 *
	 * @param[in] msgId Message ID.
	 * @param[in] events Event list.
	 * @param[in] transaction True to create a transaction.
	 * @return True on success.
	 */
	bool setMessageEvents(qint64 msgId, const QList<Isds::Event> &events,
	    bool transaction);

	/*!
	 * @brief Insert or update message envelope into messages table.
	 *
	 * @param[in] msgId Message Id.
	 * @param[in] messageType Received or sent.
	 * @param[in] envelope Contains message envelope data.
	 * @return True on success.
	 */
	bool insertOrUpdateMessageEnvelopeInDb(qint64 msgId,
	    enum MessageType messageType, const Isds::Envelope &envelope);

	/*!
	 * @brief Set message in database as localy read.
	 *
	 * @param[in] msgId Message ID.
	 * @param[in] read True = locally read; False = unread.
	 * @return True on success.
	 */
	bool markMessageLocallyRead(qint64 msgId, bool read);

	/*!
	 * @brief Set all messages in database as localy read.
	 *
	 * @param[in] messageType Received or sent.
	 * @param[in] read True = locally read; False = unread.
	 * @return True on success.
	 */
	bool markMessagesLocallyRead(enum MessageType messageType, bool read);

	/*!
	 * @brief Open database file.
	 *
	 * @param[in] fileName File name.
	 * @param[in] dbInMemory Whether to create db in memory.
	 * @return True on success.
	 */
	bool openDb(const QString &fileName, bool storeToDisk);

	/*!
	 * @brief Search and append messages into message model.
	 *
	 * @param[in] msgModel Model to append data to.
	 * @param[in] acntId Account identifier.
	 * @param[in] phrase Search phrase.
	 * @param[in] messageType Type of the sought messages.
	 * @param[in] msgIds List of message IDs where attachment name
	 *                   contains sought phrase.
	 * @return Number of results.
	 */
	int searchMessagesAndSetModelFromDb(MessageListModel *msgModel,
	    const AcntId &acntId, const QString &phrase,
	    enum MessageType messageType, const QList<qint64> &msgIds);

	/*!
	 * @brief Set flag if message attachment is present.
	 *
	 * @param[in] qint64 Message ID.
	 * @param[in] downloaded True if files were downloaded.
	 * @return True on success.
	 */
	bool setAttachmentDownloaded(qint64 msgId, bool downloaded);

	/*!
	 * @brief Set attachment flag for all mesasges in the database.
	 *
	 * @param[in] downloaded True if files were downloaded.
	 * @return True on success.
	 */
	bool setAttachmentsDownloaded(bool downloaded);

	/*!
	 * @brief Update author data.
	 *
	 * @param[in] msgId Message ID.
	 * @param[in] data Author data.
	 * @return True on success.
	 */
	bool updateMessageAuthorInfoInDb(qint64 msgId, const QString &data);

	/*!
	 * @brief Update message envelope in messages table.
	 *
	 * @param[in] envelope Contains message envelope data.
	 * @return True on success.
	 */
	bool updateMessageEnvelopeInDb(const Isds::Envelope &envelope);

protected:
	/*!
	 * @brief Returns list of tables.
	 *
	 * @return List of pointers to tables.
	 */
	virtual
	QList<class SQLiteTbl *> listOfTables(void) const Q_DECL_OVERRIDE;

private:
	static
	const QVector<QString> msgPrintedAttribs;
	static
	const QVector<QString> msgStatus;
	static
	const QVector<QString> fileItemIds;
};
