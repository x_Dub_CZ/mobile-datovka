/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDir>
#include <QMutexLocker>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>

#include "src/common.h"
#include "src/datovka_shared/compat_qt/variant.h" /* nullVariantWhenIsNull */
#include "src/datovka_shared/isds/box_interface.h"
#include "src/datovka_shared/isds/type_conversion.h"
#include "src/datovka_shared/log/log.h"
#include "src/io/filesystem.h"
#include "src/isds/conversion/isds_conversion.h"
#include "src/sqlite/account_db.h"
#include "src/sqlite/account_db_tables.h"
#include "src/sqlite/dbs.h"

#ifdef Q_COMPILER_RVALUE_REFS
#  define macroStdMove(x) ::std::move(x)
#else /* Q_COMPILER_RVALUE_REFS */
#  define macroStdMove(x) (x)
#endif /* Q_COMPILER_RVALUE_REFS */

const QVector<QString> AccountDb::dsPrintedAttribs = {"dbID",
    "dbType", "dbEffectiveOVM", "dbOpenAddressing", "dbState"};

const QVector<QString> AccountDb::ownerPrintedAttribs = {"firmName",
    "ic", "pnFirstName", "pnMiddleName", "pnLastName", "biDate", "adStreet",
    "adNumberInStreet", "adCity", "adZipCode", "adState",
    "nationality"};

const QVector<QString> AccountDb::userPrintedAttribs = {"firmName", "ic",
    "pnFirstName", "pnMiddleName", "pnLastName",
    "adStreet", "adNumberInStreet", "adCity", "adZipCode", "adState",
    "biDate", "caStreet", "caCity", "caZipCode", "caState",
    "userType", "userPrivils", "_pwdExpirDate"};

enum Isds::Type::DbType AccountDb::dbType(const QString &userName,
    enum Isds::Type::DbType defaultValue) const
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;

	if (Q_UNLIKELY(!m_db.isOpen())) {
		logErrorNL("%s", "Account database seems not to be open.");
		goto fail;
	}
	queryStr = "SELECT dbType FROM account_info WHERE key = :key";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":key", userName);
	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		return Isds::strVariant2DbType(query.value(0));
	} else {
		logErrorNL("Cannot execute SQL query and/or read SQL data: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
fail:
	return defaultValue;
}

QString AccountDb::dbId(const QString &userName,
    const QString &defaultValue) const
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;

	if (Q_UNLIKELY(!m_db.isOpen())) {
		logErrorNL("%s", "Account database seems not to be open.");
		goto fail;
	}
	queryStr = "SELECT dbID FROM account_info WHERE key = :key";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":key", userName);
	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		return query.value(0).toString();
	} else {
		logErrorNL("Cannot execute SQL query and/or read SQL data: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
fail:
	return defaultValue;
}

bool AccountDb::updatePwdExpirInDb(const QString &userName, const QDateTime &date)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;

	if (Q_UNLIKELY(!m_db.isOpen())) {
		logErrorNL("%s", "Account database seems not to be open.");
		goto fail;
	}

	queryStr = "UPDATE user_info "
	    "SET _pwdExpirDate = :expDate WHERE key = :key";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":key", userName);
	query.bindValue(":expDate", nullVariantWhenIsNull(dateTimeToDbFormatStr(date)));
	if (query.exec()) {
		return true;
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
fail:
	return false;
}

bool AccountDb::deleteAccountInfoFromDb(const QString &userName) const
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;

	if (Q_UNLIKELY(!m_db.isOpen())) {
		logErrorNL("%s", "Account database seems not to be open.");
		goto fail;
	}
	queryStr = "DELETE FROM account_info WHERE key = :key";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":key", userName);
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	return true;
fail:
	return false;
}

bool AccountDb::deleteUserInfoFromDb(const QString &userName) const
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;

	if (Q_UNLIKELY(!m_db.isOpen())) {
		logErrorNL("%s", "Account database seems not to be open.");
		goto fail;
	}
	queryStr = "DELETE FROM user_info WHERE key = :key";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":key", userName);
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	return true;
fail:
	return false;
}

bool AccountDb::deleteDTInfoFromDb(const QString &dbID) const
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;

	if (Q_UNLIKELY(!m_db.isOpen())) {
		logErrorNL("%s", "Account database seems not to be open.");
		goto fail;
	}
	queryStr = "DELETE FROM dt_info WHERE dbID = :dbID";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":dbID", dbID);
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	return true;
fail:
	return false;
}

QString AccountDb::accountDetailHtml(const QString &userName) const
{
	QString html;
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;

	html = divStart;

	html += "<h3>" + QObject::tr("Databox info") + "</h3>";

	queryStr = "SELECT ";
	for (int i = 0; i < (dsPrintedAttribs.size() - 1); ++i) {
		queryStr += dsPrintedAttribs[i] + ", ";
	}
	queryStr += dsPrintedAttribs.last();
	queryStr += " FROM account_info WHERE key = :userName";

	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":userName", userName);

	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		for (int i = 0; i < dsPrintedAttribs.size(); ++i) {
			if (!query.value(i).toString().isEmpty()) {
				if (dsPrintedAttribs[i] == "dbEffectiveOVM" ||
				    dsPrintedAttribs[i] == "dbOpenAddressing") {
					html += strongInfoLine(
					    accntinfTbl.attrProps[dsPrintedAttribs[i]].desc,
					    (query.value(i).toBool()) ?
					        QObject::tr("Yes")
					        : QObject::tr("No"));
				} else if (dsPrintedAttribs[i] == "dbState") {
					html += strongInfoLine(
					    accntinfTbl.attrProps[dsPrintedAttribs[i]].desc,
					    IsdsConversion::dbStateToText(query.value(i).toInt()));
				} else {
					html += strongInfoLine(
					    accntinfTbl.attrProps[dsPrintedAttribs[i]].desc,
					    query.value(i).toString());
				}
			}
		}
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	html += "<h3>" + QObject::tr("Owner info") + "</h3>";

	queryStr = "SELECT ";
	for (int i = 0; i < (ownerPrintedAttribs.size() - 1); ++i) {
		queryStr += ownerPrintedAttribs[i] + ", ";
	}
	queryStr += ownerPrintedAttribs.last();
	queryStr += " FROM account_info WHERE key = :userName";

	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":userName", userName);

	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		for (int i = 0; i < ownerPrintedAttribs.size(); ++i) {
			if (!query.value(i).toString().isEmpty()) {
				html += strongInfoLine(
				    accntinfTbl.attrProps[ownerPrintedAttribs[i]].desc,
				    query.value(i).toString());
			}
		}
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	html += "<h3>" + QObject::tr("User info") + "</h3>";

	queryStr = "SELECT ";
	for (int i = 0; i < (userPrintedAttribs.size() - 1); ++i) {
		queryStr += userPrintedAttribs[i] + ", ";
	}
	queryStr += userPrintedAttribs.last();
	queryStr += " FROM user_info WHERE key = :userName";

	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":userName", userName);

	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		for (int i = 0; i < userPrintedAttribs.size(); ++i) {
			if (!query.value(i).toString().isEmpty()) {
				if (userPrintedAttribs[i] == "userPrivils") {
					html += strongInfoLine(
					    userinfTbl.attrProps[userPrintedAttribs[i]].desc,
					    (query.value(i).toInt() == 255) ?
					        QObject::tr("Full control")
					        : QObject::tr("Restricted control"));
				} else if (userPrintedAttribs[i] == "userType") {
					html += strongInfoLine(
					    userinfTbl.attrProps[userPrintedAttribs[i]].desc,
					    IsdsConversion::userTypeToDescr(
					        Isds::str2UserType(
					            query.value(i).toString())));
				} else if (userPrintedAttribs[i] == "_pwdExpirDate") {
					html += strongInfoLine(
					    userinfTbl.attrProps[userPrintedAttribs[i]].desc,
					    dateTimeStrFromDbFormat(
					    query.value(i).toString(),
					    DATETIME_QML_FORMAT));
				} else {
					html += strongInfoLine(
					    userinfTbl.attrProps[userPrintedAttribs[i]].desc,
					    query.value(i).toString());
				}
			}
		}
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	html += divEnd;

	return html;

fail:
	return QString();
}

bool AccountDb::insertAccountInfoIntoDb(const QString &userName,
    const Isds::DbOwnerInfoExt2 &dbOwnerInfo)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;
	bool update = true;

	if (Q_UNLIKELY(!m_db.isOpen())) {
		logErrorNL("%s", "Account database seems not to be open.");
		goto fail;
	}

	queryStr = "SELECT count(*) "
	    "FROM account_info WHERE key = :key";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":key", userName);
	if (query.exec() && query.isActive()) {
		query.first();
		if (query.isValid()) {
			update = (query.value(0).toInt() != 0);
		}
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	if (update) {
		queryStr = "UPDATE account_info "
		    "SET dbID = :dbID, dbType = :dbType, ic = :ic, "
		    "pnFirstName = :pnFirstName, pnMiddleName = :pnMiddleName, "
		    "pnLastName = :pnLastName, "
		    "pnLastNameAtBirth = :pnLastNameAtBirth, "
		    "firmName = :firmName, biDate = :biDate, "
		    "biCity = :biCity, biCounty = :biCounty, "
		    "biState = :biState, "
		    "adCity = :adCity, adStreet = :adStreet, "
		    "adNumberInStreet = :adNumberInStreet, "
		    "adNumberInMunicipality = :adNumberInMunicipality, "
		    "adZipCode = :adZipCode, "
		    "adState = :adState, nationality = :nationality, "
		    "identifier = :identifier, registryCode = :registryCode, "
		    "dbState = :dbState, dbEffectiveOVM = :dbEffectiveOVM, "
		    "dbOpenAddressing = :dbOpenAddressing WHERE key = :key";
	} else {
		queryStr = "INSERT INTO account_info ("
		    "key, dbID, dbType, ic, pnFirstName, pnMiddleName, "
		    "pnLastName, pnLastNameAtBirth, firmName, biDate, biCity, "
		    "biCounty, biState, adCity, adStreet, adNumberInStreet, "
		    "adNumberInMunicipality, adZipCode, adState, nationality, "
		    "identifier, registryCode, dbState, dbEffectiveOVM, "
		    "dbOpenAddressing"
		    ") VALUES ("
		    ":key, :dbID, :dbType, :ic, :pnFirstName, :pnMiddleName, "
		    ":pnLastName, :pnLastNameAtBirth, :firmName, :biDate, :biCity, "
		    ":biCounty, :biState, :adCity, :adStreet, :adNumberInStreet, "
		    ":adNumberInMunicipality, :adZipCode, :adState, :nationality, "
		    ":identifier, :registryCode, :dbState, :dbEffectiveOVM, "
		    ":dbOpenAddressing"
		    ")";
	}

	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	query.bindValue(":key", nullVariantWhenIsNull(userName));
	query.bindValue(":dbID", nullVariantWhenIsNull(dbOwnerInfo.dbID()));
	query.bindValue(":dbType", nullVariantWhenIsNull(Isds::dbType2StrVariant(dbOwnerInfo.dbType())));
	query.bindValue(":ic", nullVariantWhenIsNull(dbOwnerInfo.ic()));
	query.bindValue(":pnFirstName", nullVariantWhenIsNull(dbOwnerInfo.personName().givenNames()));
	query.bindValue(":pnLastName", nullVariantWhenIsNull(dbOwnerInfo.personName().lastName()));
	query.bindValue(":firmName", nullVariantWhenIsNull(dbOwnerInfo.firmName()));
	query.bindValue(":biDate", nullVariantWhenIsNull(qDateToDbFormat(dbOwnerInfo.birthInfo().date())));
	query.bindValue(":biCity", nullVariantWhenIsNull(dbOwnerInfo.birthInfo().city()));
	query.bindValue(":biCounty", nullVariantWhenIsNull(dbOwnerInfo.birthInfo().county()));
	query.bindValue(":biState", nullVariantWhenIsNull(dbOwnerInfo.birthInfo().state()));
	query.bindValue(":adCity", nullVariantWhenIsNull(dbOwnerInfo.address().city()));
	query.bindValue(":adStreet", nullVariantWhenIsNull(dbOwnerInfo.address().street()));
	query.bindValue(":adNumberInStreet",
	    nullVariantWhenIsNull(dbOwnerInfo.address().numberInStreet()));
	query.bindValue(":adNumberInMunicipality",
	    nullVariantWhenIsNull(dbOwnerInfo.address().numberInMunicipality()));
	query.bindValue(":adZipCode", nullVariantWhenIsNull(dbOwnerInfo.address().zipCode()));
	query.bindValue(":adState", nullVariantWhenIsNull(dbOwnerInfo.address().state()));
	query.bindValue(":nationality", nullVariantWhenIsNull(dbOwnerInfo.nationality()));
	query.bindValue(":dbState",
	    Isds::dbState2Variant(dbOwnerInfo.dbState()));
	query.bindValue(":dbOpenAddressing",
	    Isds::nilBool2Variant(dbOwnerInfo.dbOpenAddressing()));

	/*
	 * These items were removed from new owner info structure.
	 */
	query.bindValue(":pnMiddleName", nullVariantWhenIsNull(QString()));
	query.bindValue(":pnLastNameAtBirth", nullVariantWhenIsNull(QString()));
	query.bindValue(":identifier", nullVariantWhenIsNull(QString()));
	query.bindValue(":registryCode", nullVariantWhenIsNull(QString()));
	query.bindValue(":dbEffectiveOVM",
	    Isds::nilBool2Variant(Isds::Type::BOOL_NULL));

	if (query.exec()) {
		return true;
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
fail:
	return false;
}

bool AccountDb::insertUserInfoIntoDb(const QString &userName,
    const Isds::DbUserInfoExt2 &dbUserInfo)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;
	bool update = true;

	if (Q_UNLIKELY(!m_db.isOpen())) {
		logErrorNL("%s", "Account database seems not to be open.");
		goto fail;
	}

	queryStr = "SELECT count(*) "
	    "FROM user_info WHERE key = :key";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":key", userName);
	if (query.exec() && query.isActive()) {
		query.first();
		if (query.isValid()) {
			update = (query.value(0).toInt() != 0);
		}
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	if (update) {
		queryStr = "UPDATE user_info "
		    "SET userType = :userType, userPrivils = :userPrivils, "
		    "pnFirstName = :pnFirstName, pnMiddleName = :pnMiddleName, "
		    "pnLastName = :pnLastName, "
		    "pnLastNameAtBirth = :pnLastNameAtBirth, "
		    "adCity = :adCity, adStreet = :adStreet, "
		    "adNumberInStreet = :adNumberInStreet, "
		    "adNumberInMunicipality = :adNumberInMunicipality, "
		    "adZipCode = :adZipCode, adState = :adState, "
		    "biDate = :biDate, ic = :ic, "
		    "firmName = :firmName, caStreet = :caStreet, "
		    "caCity = :caCity, caZipCode = :caZipCode, "
		    "caState = :caState WHERE key = :key";
	} else {
		queryStr = "INSERT INTO user_info ("
		    "key, userType, userPrivils, pnFirstName, pnMiddleName, "
		    "pnLastName, pnLastNameAtBirth, adCity, adStreet, "
		    "adNumberInStreet, adNumberInMunicipality, adZipCode, "
		    "adState, biDate, ic, firmName, caStreet, caCity, "
		    "caZipCode, caState"
		    ") VALUES ("
		    ":key, :userType, :userPrivils, :pnFirstName, "
		    ":pnMiddleName, :pnLastName, :pnLastNameAtBirth, :adCity, "
		    ":adStreet, :adNumberInStreet, :adNumberInMunicipality, "
		    ":adZipCode, :adState, :biDate, :ic, :firmName, :caStreet, "
		    ":caCity, :caZipCode, :caState)";
	}

	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	query.bindValue(":key", nullVariantWhenIsNull(userName));
	query.bindValue(":userType", nullVariantWhenIsNull(Isds::userType2Str(dbUserInfo.userType())));
	query.bindValue(":userPrivils",
	    Isds::privileges2Variant(dbUserInfo.userPrivils()));
	query.bindValue(":ic", nullVariantWhenIsNull(dbUserInfo.ic()));
	query.bindValue(":pnFirstName", nullVariantWhenIsNull(dbUserInfo.personName().givenNames()));
	query.bindValue(":pnLastName", nullVariantWhenIsNull(dbUserInfo.personName().lastName()));
	query.bindValue(":firmName", nullVariantWhenIsNull(dbUserInfo.firmName()));
	query.bindValue(":biDate", nullVariantWhenIsNull(qDateToDbFormat(dbUserInfo.biDate())));
	query.bindValue(":adCity", nullVariantWhenIsNull(dbUserInfo.address().city()));
	query.bindValue(":adStreet", nullVariantWhenIsNull(dbUserInfo.address().street()));
	query.bindValue(":adNumberInStreet",
	    nullVariantWhenIsNull(dbUserInfo.address().numberInStreet()));
	query.bindValue(":adNumberInMunicipality",
	    nullVariantWhenIsNull(dbUserInfo.address().numberInMunicipality()));
	query.bindValue(":adZipCode", nullVariantWhenIsNull(dbUserInfo.address().zipCode()));
	query.bindValue(":adState", nullVariantWhenIsNull(dbUserInfo.address().state()));
	query.bindValue(":caStreet", nullVariantWhenIsNull(dbUserInfo.caStreet()));
	query.bindValue(":caCity", nullVariantWhenIsNull(dbUserInfo.caCity()));
	query.bindValue(":caZipCode", nullVariantWhenIsNull(dbUserInfo.caZipCode()));
	query.bindValue(":caState", nullVariantWhenIsNull(dbUserInfo.caState()));

	/*
	 * These items were removed from new user info structure.
	 */
	query.bindValue(":pnMiddleName", nullVariantWhenIsNull(QString()));
	query.bindValue(":pnLastNameAtBirth", nullVariantWhenIsNull(QString()));

	if (query.exec()) {
		return true;
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
fail:
	return false;
}

bool AccountDb::insertDTInfoIntoDb(const QString &dbID,
    const Isds::DTInfoOutput &dtInfo)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;
	bool update = true;

	if (Q_UNLIKELY(!m_db.isOpen())) {
		logErrorNL("%s", "Account database seems not to be open.");
		goto fail;
	}

	queryStr = "SELECT count(*) "
	    "FROM dt_info WHERE dbID = :dbID";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":dbID", dbID);
	if (query.exec() && query.isActive()) {
		query.first();
		if (query.isValid()) {
			update = (query.value(0).toInt() != 0);
		}
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	if (update) {
		queryStr = "UPDATE dt_info "
		    "SET actDTType = :actDTType,"
		    "actDTCapacity = :actDTCapacity, "
		    "actDTFrom = :actDTFrom,"
		    "actDTTo = :actDTTo, "
		    "actDTCapUsed = :actDTCapUsed, "
		    "futDTType = :futDTType, "
		    "futDTCapacity = :futDTCapacity,"
		    "futDTFrom = :futDTFrom, "
		    "futDTTo = :futDTTo, "
		    "futDTPaid = :futDTPaid "
		    "WHERE dbID = :dbID";
	} else {
		queryStr = "INSERT INTO dt_info ("
		    "dbID, actDTType, actDTCapacity, actDTFrom, actDTTo, "
		    "actDTCapUsed, futDTType, futDTCapacity, futDTFrom, "
		    "futDTTo, futDTPaid"
		    ") VALUES ("
		    ":dbID, :actDTType, :actDTCapacity, :actDTFrom, "
		    ":actDTTo, :actDTCapUsed, :futDTType, :futDTCapacity, "
		    ":futDTFrom, :futDTTo, :futDTPaid)";
	}

	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	query.bindValue(":dbID", nullVariantWhenIsNull(dbID));
	query.bindValue(":actDTType", Isds::dtType2Variant(dtInfo.actDTType()));
	query.bindValue(":actDTCapacity",
	    Isds::nonNegativeLong2Variant(dtInfo.actDTCapacity()));
	query.bindValue(":actDTFrom", nullVariantWhenIsNull(qDateToDbFormat(dtInfo.actDTFrom())));
	query.bindValue(":actDTTo", nullVariantWhenIsNull(qDateToDbFormat(dtInfo.actDTTo())));
	query.bindValue(":actDTCapUsed",
	    Isds::nonNegativeLong2Variant(dtInfo.actDTCapUsed()));
	query.bindValue(":futDTType",
	    Isds::dtType2Variant(dtInfo.futDTType()));
	query.bindValue(":futDTCapacity",
	    Isds::nonNegativeLong2Variant(dtInfo.futDTCapacity()));
	query.bindValue(":futDTFrom", nullVariantWhenIsNull(qDateToDbFormat(dtInfo.futDTFrom())));
	query.bindValue(":futDTTo", nullVariantWhenIsNull(qDateToDbFormat(dtInfo.futDTTo())));
	query.bindValue(":futDTPaid",
	    Isds::dtPaidState2Variant(dtInfo.futDTPaid()));

	if (query.exec()) {
		return true;
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
fail:
	return false;
}

Isds::DbOwnerInfoExt2 AccountDb::getOwnerInfo(const QString &key) const
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;
	Isds::AddressExt2 address;
	Isds::BirthInfo biInfo;
	Isds::DbOwnerInfoExt2 dbOwnerInfo;
	Isds::PersonName2 personName;

	if (Q_UNLIKELY(!m_db.isOpen())) {
		logErrorNL("%s", "Account database seems not to be open.");
		goto fail;
	}

	queryStr = "SELECT "
	    "dbID, dbType, ic, pnFirstName, pnMiddleName, pnLastName, "
	    "pnLastNameAtBirth, firmName, biDate, biCity, biCounty, biState, "
	    "adCity, adStreet, adNumberInStreet, adNumberInMunicipality, "
	    "adZipCode, adState, nationality, identifier, registryCode, "
	    "dbState, dbEffectiveOVM, dbOpenAddressing "
	    "FROM account_info WHERE key = :key";

	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s.",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":key", key);
	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		dbOwnerInfo.setDbID(query.value(0).toString());
		dbOwnerInfo.setDbType(Isds::strVariant2DbType(query.value(1)));
		dbOwnerInfo.setIc(query.value(2).toString());
		personName.setGivenNames(query.value(3).toString());
		personName.setLastName(query.value(5).toString());
		dbOwnerInfo.setPersonName(macroStdMove(personName));
		dbOwnerInfo.setFirmName(query.value(7).toString());
		biInfo.setDate(dateFromDbFormat(query.value(8).toString()));
		biInfo.setCity(query.value(9).toString());
		biInfo.setCounty(query.value(10).toString());
		biInfo.setState(query.value(11).toString());
		dbOwnerInfo.setBirthInfo(macroStdMove(biInfo));
		address.setCity(query.value(12).toString());
		address.setStreet(query.value(13).toString());
		address.setNumberInStreet(query.value(14).toString());
		address.setNumberInMunicipality(query.value(15).toString());
		address.setZipCode(query.value(16).toString());
		address.setState(query.value(17).toString());
		dbOwnerInfo.setAddress(macroStdMove(address));
		dbOwnerInfo.setNationality(query.value(18).toString());
		dbOwnerInfo.setDbState(Isds::variant2DbState(query.value(21)));
		dbOwnerInfo.setDbOpenAddressing(
		    Isds::variant2NilBool(query.value(23)));
		return dbOwnerInfo;
	} else {
		logErrorNL("Cannot execute SQL query and/or read SQL data: "
		    "%s.", query.lastError().text().toUtf8().constData());
	}
fail:
	return Isds::DbOwnerInfoExt2();
}

Isds::DTInfoOutput AccountDb::getDTInfo(const QString &dbID) const
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;
	Isds::DTInfoOutput dtInfo;

	if (Q_UNLIKELY(!m_db.isOpen())) {
		logErrorNL("%s", "Account database seems not to be open.");
		goto fail;
	}

	queryStr = "SELECT "
	    "actDTType, actDTCapacity, actDTFrom, actDTTo, actDTCapUsed, "
	    "futDTType, futDTCapacity, futDTFrom, futDTTo, futDTPaid "
	    "FROM dt_info WHERE dbID = :dbID";

	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s.",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":dbID", dbID);
	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		dtInfo.setActDTType(
		    Isds::variant2DTType(query.value(0)));
		dtInfo.setActDTCapacity(
		    Isds::variant2nonNegativeLong(query.value(1)));
		dtInfo.setActDTFrom(dateFromDbFormat(query.value(2).toString()));
		dtInfo.setActDTTo(dateFromDbFormat(query.value(3).toString()));
		dtInfo.setActDTCapUsed(
		    Isds::variant2nonNegativeLong(query.value(4)));
		dtInfo.setFutDTType(
		    Isds::variant2DTType(query.value(5)));
		dtInfo.setFutDTCapacity(
		    Isds::variant2nonNegativeLong(query.value(6)));
		dtInfo.setFutDTFrom(dateFromDbFormat(query.value(7).toString()));
		dtInfo.setFutDTTo(dateFromDbFormat(query.value(8).toString()));
		dtInfo.setFutDTPaid(
		    Isds::variant2DTPaidState(query.value(9)));
		return dtInfo;
	} else {
		logErrorNL("Cannot execute SQL query and/or read SQL data: "
		    "%s.", query.lastError().text().toUtf8().constData());
	}
fail:
	return Isds::DTInfoOutput();
}

QStringList AccountDb::getPasswordExpirationList(
    const QStringList &nonPwdUserNames, int days) const
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QStringList expirPwdList;

	QString queryStr = "SELECT key, _pwdExpirDate FROM user_info";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return QStringList();
	}

	if (query.exec() && query.isActive()) {
		query.first();
		while (query.isValid()) {

			if (nonPwdUserNames.contains(query.value(0).toString())) {
				query.next();
				continue;
			}

			QString exprDateTime =
			    dateTimeStrFromDbFormat(query.value(1).toString(),
			    DATETIME_QML_FORMAT);
			if (!exprDateTime.isEmpty()) {

				const QDateTime dbDateTime(
				    dateTimeFromDbFormat(
				    query.value(1).toString()));
				if (!dbDateTime.isValid()) {
					query.next();
					continue;
				}

				const QDate dbDate = dbDateTime.date();
				if (Q_UNLIKELY(!dbDate.isValid())) {
					query.next();
					continue;
				}

				qint64 daysTo =
				    QDate::currentDate().daysTo(dbDate);
				if ((daysTo >= 0) && (daysTo < days)) {
					expirPwdList.append(QObject::tr("Password of username '%1' expires on %2.").arg(query.value(0).toString()).arg(exprDateTime));
				} else if ((daysTo < 0)) {
					expirPwdList.append(QObject::tr("Password of username '%1' expired on %2.").arg(query.value(0).toString()).arg(exprDateTime));
				}
			}
			query.next();
		}
	}

	return expirPwdList;
}

QList<class SQLiteTbl *> AccountDb::listOfTables(void) const
{
	QList<class SQLiteTbl *> tables;
	tables.append(&accntinfTbl);
	tables.append(&userinfTbl);
	tables.append(&dtinfTbl);
	return tables;
}
