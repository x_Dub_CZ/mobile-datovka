/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDir>
#include <QFile>
#include <QMutexLocker>
#include <QObject>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>

#include "src/auxiliaries/email_helper.h"
#include "src/common.h"
#include "src/datovka_shared/compat_qt/variant.h" /* nullVariantWhenIsNull */
#include "src/datovka_shared/io/records_management_db.h"
#include "src/datovka_shared/isds/type_conversion.h"
#include "src/datovka_shared/log/log.h"
#include "src/global.h"
#include "src/io/filesystem.h"
#include "src/isds/conversion/isds_conversion.h"
#include "src/models/messagemodel.h"
#include "src/sqlite/dbs.h"
#include "src/sqlite/message_db.h"
#include "src/sqlite/message_db_tables.h"

const QVector<QString> MessageDb::msgPrintedAttribs = {"dmSenderIdent",
    "dmSenderRefNumber", "dmRecipientIdent", "dmRecipientRefNumber",
    "dmToHands", "dmLegalTitleLaw", "dmLegalTitleYear", "dmLegalTitleSect",
    "dmLegalTitlePar", "dmLegalTitlePoint"};

const QVector<QString> MessageDb::msgStatus = {"dmDeliveryTime",
    "dmAcceptanceTime", "dmMessageStatus"};

const QVector<QString> MessageDb::fileItemIds = {"id", "message_id",
    "dmEncodedContent", "_dmFileDescr", "LENGTH(dmEncodedContent)"};

MessageDb::MessageDb(const QString &connectionName)
    : SQLiteDb(connectionName)
{
}

bool MessageDb::copyDb(const QString &newFileName)
{
	return SQLiteDb::copyDb(newFileName, SQLiteDb::CREATE_MISSING);
}

bool MessageDb::reopenDb(const QString &newFileName)
{
	return SQLiteDb::reopenDb(newFileName, SQLiteDb::CREATE_MISSING);
}

bool MessageDb::moveDb(const QString &newFileName)
{
	return SQLiteDb::moveDb(newFileName, SQLiteDb::CREATE_MISSING);
}

/*!
 * @brief Delete all events related to a message.
 *
 * @param[in] query SQL query to work with.
 * @param[in] msgId Message ID.
 * @return True on success.
 */
static
bool deleteEvents(QSqlQuery &query, qint64 msgId)
{
	QString queryStr = "DELETE FROM events WHERE dmID = :dmID";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	query.bindValue(":dmID", msgId);
	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	return true;
}

bool MessageDb::deleteMessageFromDb(qint64 msgId, bool transaction)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;

	if (transaction) {
		transaction = beginTransaction();
		if (Q_UNLIKELY(!transaction)) {
			logErrorNL("%s", "Cannot begin transaction.");
			goto fail;
		}
	}

	if (Q_UNLIKELY(!deleteEvents(query, msgId))) {
		goto fail;
	}

	queryStr = "DELETE FROM messages WHERE dmID = :dmID";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":dmID", msgId);

	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	if (transaction) {
		commitTransaction();
	}
	return true;

fail:
	if (transaction) {
		rollbackTransaction();
	}
	return false;
}

void MessageDb::getContactsFromDb(DataboxListModel *dbModel, const QString &dbId)
{
	if (Q_UNLIKELY(dbModel == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "SELECT "
	    "dbIDRecipient, dmRecipient, dmRecipientAddress FROM messages "
	    "WHERE (dmRecipientAddress IS NOT NULL) "
	    " UNION SELECT "
	    "dbIDSender, dmSender, dmSenderAddress FROM messages "
	    "WHERE (dmSenderAddress IS NOT NULL) "
	    "ORDER BY dmRecipient, dmSender DESC";

	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return;
	}

	if (query.exec() && query.isActive()) {
		dbModel->setQuery(query, dbId, false);
	}
}

int MessageDb::getDbSizeInBytes(void)
{
	QFileInfo fi(m_db.databaseName());
	return fi.size();
}

QSet<qint64> MessageDb::getAllMessageIDsFromDb(void)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QSet<qint64> msgIDList;

	QString queryStr = "SELECT dmID FROM messages";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return QSet<qint64>();
	}
	if (query.exec() && query.isActive()) {
		query.first();
		while (query.isValid()) {
			msgIDList.insert(query.value(0).toLongLong());
			query.next();
		}
	} else {
		return QSet<qint64>();
	}

	return msgIDList;
}

QList<qint64> MessageDb::getExpireMessageListFromDb(int days)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QList<qint64> msgIDList;

	/* count split date based on days */
	QDate now = QDate::currentDate();
	now = now.addDays(-1*days);

	QString queryStr = "SELECT dmID FROM messages WHERE dmDeliveryTime < :date";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return QList<qint64>();
	}
	query.bindValue(":date", now);

	if (query.exec() && query.isActive()) {
		query.first();
		while (query.isValid()) {
			msgIDList.append(query.value(0).toLongLong());
			query.next();
		}
	} else {
		return QList<qint64>();
	}

	return msgIDList;
}

short MessageDb::getMessageStatusFromDb(qint64 msgId, bool &hasFiles) const
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;
	int ret = -1;

	queryStr = "SELECT dmMessageStatus, _dmAttachDownloaded "
	    "FROM messages WHERE dmID = :dmID";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	query.bindValue(":dmID", msgId);

	if (query.exec() && query.isActive()) {
		query.first();
		if (query.isValid()) {
			ret = query.value(0).toInt();
			hasFiles = query.value(1).toBool();
		}
	}
	return ret;
}

void MessageDb::setMessageListModelFromDb(MessageListModel *msgModel,
    const AcntId &acntId, enum MessageType messageType)
{
	if (Q_UNLIKELY(msgModel == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "SELECT dmID, dmSender, dmRecipient, "
	    "dmAnnotation, dmDeliveryTime, dmAcceptanceTime, "
	    "_dmReadLocally, _dmAttachDownloaded, _dmMessageType FROM messages "
	    "WHERE _dmMessageType = :_dmMessageType ORDER BY dmID DESC";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return;
	}
	query.bindValue(":_dmMessageType", messageType);

	if (query.exec() && query.isActive()) {
		msgModel->setQuery(acntId.username(), acntId.testing(),
		    query, false);
	}
}

int MessageDb::getNewMessageCountFromDb(enum MessageType messageType)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "SELECT COUNT(*) FROM messages WHERE "
	   "_dmMessageType = :_dmMessageType AND _dmReadLocally = :_dmReadLocally";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return 0;
	}
	query.bindValue(":_dmMessageType", messageType);
	query.bindValue(":_dmReadLocally", false);

	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		return query.value(0).toInt();
	}
	return 0;
}

int MessageDb::getMessageCountFromDb(enum MessageType messageType)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "SELECT COUNT(*) FROM messages WHERE "
	   "_dmMessageType = :_dmMessageType";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return 0;
	}
	query.bindValue(":_dmMessageType", messageType);

	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		return query.value(0).toInt();
	}
	return 0;
}

Isds::Envelope MessageDb::getMessageEnvelopeFromDb(qint64 dmId)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;
	Isds::Envelope envelope;

	queryStr = "SELECT dmAnnotation, "
	    "dbIDSender, dmSender, dmSenderAddress, dmSenderType, "
	    "dmSenderOrgUnit, dmSenderOrgUnitNum, dmSenderRefNumber, "
	    "dmSenderIdent, dbIDRecipient, dmRecipient, dmRecipientAddress, "
	    "dmRecipientOrgUnit, dmRecipientOrgUnitNum, dmAmbiguousRecipient, "
	    "dmRecipientRefNumber, dmRecipientIdent, dmLegalTitleLaw, "
	    "dmLegalTitleYear, dmLegalTitleSect, dmLegalTitlePar, "
	    "dmLegalTitlePoint, dmToHands, dmPersonalDelivery, "
	    "dmAllowSubstDelivery, dmQTimestamp, dmDeliveryTime, "
	    "dmAcceptanceTime, dmMessageStatus, dmAttachmentSize, dmType "
	    "FROM messages WHERE dmID = :dmID";

	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		return Isds::Envelope();
	}

	query.bindValue(":dmID", dmId);
	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		envelope.setDmId(dmId);
		envelope.setDmID(QString::number(dmId));
		envelope.setDmAnnotation(query.value(0).toString());
		envelope.setDbIDSender(query.value(1).toString());
		envelope.setDmSender(query.value(2).toString());
		envelope.setDmSenderAddress(query.value(3).toString());
		envelope.setDmSenderType(
		    Isds::intVariant2DbType(query.value(4)));
		envelope.setDmSenderOrgUnit(query.value(5).toString());
		envelope.setDmSenderOrgUnitNumStr(query.value(6).toString());
		envelope.setDmSenderRefNumber(query.value(7).toString());
		envelope.setDmSenderIdent(query.value(8).toString());
		envelope.setDbIDRecipient(query.value(9).toString());
		envelope.setDmRecipient(query.value(10).toString());
		envelope.setDmRecipientAddress(query.value(11).toString());
		envelope.setDmRecipientOrgUnit(query.value(12).toString());
		envelope.setDmRecipientOrgUnitNumStr(
		    query.value(13).toString());
		envelope.setDmAmbiguousRecipient(
		    Isds::variant2NilBool(query.value(14)));
		envelope.setDmRecipientRefNumber(query.value(15).toString());
		envelope.setDmRecipientIdent(query.value(16).toString());
		envelope.setDmLegalTitleLawStr(query.value(17).toString());
		envelope.setDmLegalTitleYearStr(query.value(18).toString());
		envelope.setDmLegalTitleSect(query.value(19).toString());
		envelope.setDmLegalTitlePar(query.value(20).toString());
		envelope.setDmLegalTitlePoint(query.value(21).toString());
		envelope.setDmToHands(query.value(22).toString());
		envelope.setDmPersonalDelivery(
		     Isds::variant2NilBool(query.value(23)));
		envelope.setDmAllowSubstDelivery(
		    Isds::variant2NilBool(query.value(24)));
		envelope.setDmQTimestamp(query.value(25).toByteArray());
		envelope.setDmDeliveryTime(
		    dateTimeFromDbFormat(query.value(26).toString()));
		envelope.setDmAcceptanceTime(
		    dateTimeFromDbFormat(query.value(27).toString()));
		envelope.setDmMessageStatus(
		    Isds::variant2DmState(query.value(28)));
		envelope.setDmAttachmentSize(query.value(29).toLongLong());
		envelope.setDmType(Isds::variant2Char(query.value(30)));
	}

	return envelope;
}

bool MessageDb::getMessageEmailDataFromDb(qint64 dmId, QString &body,
    QString &subject)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;

	queryStr = "SELECT dmSender, dmRecipient, dmAcceptanceTime, "
	    "dmAnnotation FROM messages WHERE dmID = :dmID";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		return false;
	}
	query.bindValue(":dmID", dmId);
	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		body = generateEmailBodyText(dmId, query.value(0).toString(),
		    query.value(1).toString(), dateTimeStrFromDbFormat(
		    query.value(2).toString(), DATETIME_QML_FORMAT));
		subject = query.value(3).toString();
		return true;
	}
	return false;
}

QString MessageDb::getMessageDetailHtmlFromDb(qint64 dmId) const
{
	QString html;
	QStringList rmPathList;
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;

	html += divStart;
	html += "<h3>" + QObject::tr("General") + "</h3>";

	queryStr = "SELECT _dmMessageType, "
	    "dmAnnotation, dmAttachmentSize, "
	    "dbIDSender, dmSender, dmSenderAddress, _dmCustomData, "
	    "dbIDRecipient, dmRecipient, dmRecipientAddress, "
	    "dmPersonalDelivery, dmAllowSubstDelivery, dmType "
	    "FROM messages WHERE dmID = :dmID";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		goto fail;
	}
	query.bindValue(":dmID", dmId);
	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		html += strongInfoLine(QObject::tr("Subject"),
		    query.value(1).toString());
		QString size = query.value(2).toString();
		html += strongInfoLine(QObject::tr("Attachment size"),
		    (size == "0") ? "&lt;1 kB" : "~" + size + " kB");
		html += strongInfoLine(QObject::tr("Personal delivery"),
		    (query.value(10).toBool()) ? QObject::tr("Yes") : QObject::tr("No"));
		html += strongInfoLine(QObject::tr("Delivery by fiction"),
		    (query.value(11).toBool()) ? QObject::tr("Yes") : QObject::tr("No"));
		if (!query.value(12).toString().isEmpty()) {
			html += strongInfoLine(QObject::tr("Message type"),
			    IsdsConversion::dmTypeToText(query.value(12).toString()));
		}

		if (query.value(0).toInt() == TYPE_RECEIVED) {
			/* Information about message sender. */
			html += "<h3>" + QObject::tr("Sender") + "</h3>";
			html += strongInfoLine(QObject::tr("Databox ID"),
			    query.value(3).toString());
			html += strongInfoLine(QObject::tr("Name"),
			    query.value(4).toString());
			html += strongInfoLine(QObject::tr("Address"),
			    query.value(5).toString());
			if (!query.value(6).toString().isEmpty()) {
				html += strongInfoLine(QObject::tr("Message author"),
				    query.value(6).toString());
			}
		} else {
			/* Information about message recipient. */
			html += "<h3>" + QObject::tr("Recipient") + "</h3>";
			html += strongInfoLine(QObject::tr("Databox ID"),
			    query.value(7).toString());
			html += strongInfoLine(QObject::tr("Name"),
			    query.value(8).toString());
			html += strongInfoLine(QObject::tr("Address"),
			    query.value(9).toString());
		}
	} else {
		goto fail;
	}

	queryStr = "SELECT ";
	for (int i = 0; i < (msgPrintedAttribs.size() - 1); ++i) {
		queryStr += msgPrintedAttribs[i] + ", ";
	}
	queryStr += msgPrintedAttribs.last();
	queryStr += " FROM messages WHERE dmID = :dmID";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		goto fail;
	}
	query.bindValue(":dmID", dmId);
	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		for (int i = 0; i < msgPrintedAttribs.size(); ++i) {
			if (!query.value(i).toString().isEmpty()) {
				html += strongInfoLine(
				    msgsTbl.attrProps[msgPrintedAttribs[i]].desc,
				    query.value(i).toString());
			}
		}
	} else {
		goto fail;
	}

	html += "<h3>" + QObject::tr("Message state") + "</h3>";
	/* Message state */
	queryStr = "SELECT ";
	for (int i = 0; i < (msgStatus.size() - 1); ++i) {
		queryStr += msgStatus[i] + ", ";
	}
	queryStr += msgStatus.last();
	queryStr += " FROM messages WHERE "
	    "dmID = :dmID";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		goto fail;
	}
	query.bindValue(":dmID", dmId);
	if (query.exec() && query.isActive() &&
	    query.first() && query.isValid()) {
		html += strongInfoLine(
		    msgsTbl.attrProps[msgStatus[0]].desc,
		    dateTimeStrFromDbFormat(query.value(0).toString(),
			DATETIME_QML_FORMAT));
		html += strongInfoLine(
		    msgsTbl.attrProps[msgStatus[1]].desc,
		    dateTimeStrFromDbFormat(query.value(1).toString(),
			DATETIME_QML_FORMAT));
		html += strongInfoLine(
		    msgsTbl.attrProps[msgStatus[2]].desc,
		    QString::number(query.value(2).toInt()) + " -- " +
		    IsdsConversion::dmMessageStatusToText(query.value(2).toInt()));
	} else {
		goto fail;
	}

	queryStr = "SELECT dmEventTime, dmEventDescr"
	    " FROM events WHERE dmID = :dmID"
	    " ORDER BY dmEventTime ASC";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		goto fail;
	}
	query.bindValue(":dmID", dmId);
	if (query.exec() && query.isActive()) {
		query.first();
		if (query.isValid()) {
			html += "<h3>" + QObject::tr("Events") + "</h3>";
		}
		while (query.isValid()) {
			html += divStart +
			    strongInfoLine(
				dateTimeStrFromDbFormat(
				    query.value(0).toString(),
				    DATETIME_QML_FORMAT),
				query.value(1).toString()) +
			    divEnd;
			query.next();
		}
	} else {
		goto fail;
	}

	if (Q_NULLPTR != GlobInstcs::recMgmtDbPtr) {
		rmPathList = GlobInstcs::recMgmtDbPtr->storedMsgLocations(dmId);
	}

	if (!rmPathList.isEmpty()) {
		html += "<h3>" + QObject::tr("Records Management Location") + "</h3>";
		foreach (const QString &location, rmPathList) {
			html += strongInfoLine(QObject::tr("Location"), location);
		}
	}

	html += divEnd;

	return html;

fail:
	logErrorNL("%s", "Cannot prepare/execute SQL query.");
	return QString();
}

/*!
 * @brief Insert or update event.
 *
 * @param[in] query SQL query to work with.
 * @param[in] msgId Message ID.
 * @param[in] event Event structure.
 * @return True on success.
 */
static
bool insertEvent(QSqlQuery &query, qint64 msgId, const Isds::Event &event)
{
	QString queryStr = "INSERT INTO events (dmID, dmEventTime, dmEventDescr)"
	    " VALUES (:dmID, :dmEventTime, :dmEventDescr)";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}

	query.bindValue(":dmID", msgId);
	query.bindValue(":dmEventTime", nullVariantWhenIsNull(dateTimeToDbFormatStr(
	    event.time())));
	query.bindValue(":dmEventDescr", nullVariantWhenIsNull(event.descr()));

	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}

	return true;
}

bool MessageDb::setMessageEvents(qint64 msgId, const QList<Isds::Event> &events,
    bool transaction)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	if (transaction) {
		transaction = beginTransaction();
		if (Q_UNLIKELY(!transaction)) {
			logErrorNL("%s", "Cannot begin transaction.");
			goto fail;
		}
	}

	if (Q_UNLIKELY(!deleteEvents(query, msgId))) {
		goto fail;
	}

	foreach (const Isds::Event &event, events) {
		if (Q_UNLIKELY(!insertEvent(query, msgId, event))) {
			goto fail;
		}
	}

	if (transaction) {
		commitTransaction();
	}
	return true;

fail:
	if (transaction) {
		rollbackTransaction();
	}
	return false;
}

bool MessageDb::insertOrUpdateMessageEnvelopeInDb(qint64 msgId,
    enum MessageType messageType, const Isds::Envelope &envelope)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	qint64 dmID = -1;

	QString queryStr = "SELECT dmID FROM messages WHERE dmID = :dmID";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":dmID", msgId);
	if (query.exec() && query.isActive()) {
		query.first();
		if (query.isValid()) {
			dmID = query.value(0).toLongLong();
		}
	} else {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}

	// update message envelope
	if (-1 != dmID) {
		return updateMessageEnvelopeInDb(envelope);
	}

	queryStr = "INSERT INTO messages ("
	    "dmID, dbIDSender, dmSender, dmSenderAddress, dmSenderType, "
	    "dmRecipient, dmRecipientAddress, dmAmbiguousRecipient, "
	    "dmSenderOrgUnit, dmSenderOrgUnitNum, dbIDRecipient, "
	    "dmRecipientOrgUnit, dmRecipientOrgUnitNum, dmToHands, dmAnnotation, "
	    "dmRecipientRefNumber, dmSenderRefNumber, dmRecipientIdent, "
	    "dmSenderIdent, dmLegalTitleLaw, dmLegalTitleYear, "
	    "dmLegalTitleSect, dmLegalTitlePar, dmLegalTitlePoint, "
	    "dmPersonalDelivery, dmAllowSubstDelivery, dmQTimestamp, "
	    "dmDeliveryTime, dmAcceptanceTime, dmMessageStatus, "
	    "dmAttachmentSize, dmType, _dmMessageType, _dmDownloadDate, "
	    "_dmCustomData, _dmAttachDownloaded, _dmReadLocally"
	    ") VALUES ("
	    ":dmID, :dbIDSender, :dmSender, "
	    ":dmSenderAddress, :dmSenderType, :dmRecipient, "
	    ":dmRecipientAddress, :dmAmbiguousRecipient, :dmSenderOrgUnit, "
	    ":dmSenderOrgUnitNum, :dbIDRecipient, :dmRecipientOrgUnit, "
	    ":dmRecipientOrgUnitNum, :dmToHands, :dmAnnotation, "
	    ":dmRecipientRefNumber, :dmSenderRefNumber, :dmRecipientIdent, "
	    ":dmSenderIdent, :dmLegalTitleLaw, :dmLegalTitleYear, "
	    ":dmLegalTitleSect, :dmLegalTitlePar, :dmLegalTitlePoint,"
	    ":dmPersonalDelivery, :dmAllowSubstDelivery, :dmQTimestamp, "
	    ":dmDeliveryTime, :dmAcceptanceTime, :dmMessageStatus, "
	    ":dmAttachmentSize, :dmType, :_dmMessageType, :_dmDownloadDate, "
	    ":_dmCustomData, :_dmAttachDownloaded, :_dmReadLocally"
	    ")";

	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":dmID", msgId);
	query.bindValue(":dbIDSender", nullVariantWhenIsNull(envelope.dbIDSender()));
	query.bindValue(":dmSender", nullVariantWhenIsNull(envelope.dmSender()));
	query.bindValue(":dmSenderAddress", nullVariantWhenIsNull(envelope.dmSenderAddress()));
	query.bindValue(":dmSenderType", Isds::dbType2IntVariant(envelope.dmSenderType()));
	query.bindValue(":dmRecipient", envelope.dmRecipient());
	query.bindValue(":dmRecipientAddress", envelope.dmRecipientAddress());
	query.bindValue(":dmAmbiguousRecipient", Isds::nilBool2Variant(envelope.dmAmbiguousRecipient()));
	query.bindValue(":dmSenderOrgUnit", nullVariantWhenIsNull(envelope.dmSenderOrgUnit()));
	query.bindValue(":dmSenderOrgUnitNum", nullVariantWhenIsNull(envelope.dmSenderOrgUnitNumStr()));
	query.bindValue(":dbIDRecipient", nullVariantWhenIsNull(envelope.dbIDRecipient()));
	query.bindValue(":dmRecipientOrgUnit", nullVariantWhenIsNull(envelope.dmRecipientOrgUnit()));
	query.bindValue(":dmRecipientOrgUnitNum", nullVariantWhenIsNull(envelope.dmRecipientOrgUnitNumStr()));
	query.bindValue(":dmToHands", nullVariantWhenIsNull(envelope.dmToHands()));
	query.bindValue(":dmAnnotation", nullVariantWhenIsNull(envelope.dmAnnotation()));
	query.bindValue(":dmRecipientRefNumber", nullVariantWhenIsNull(envelope.dmRecipientRefNumber()));
	query.bindValue(":dmSenderRefNumber", nullVariantWhenIsNull(envelope.dmSenderRefNumber()));
	query.bindValue(":dmRecipientIdent", nullVariantWhenIsNull(envelope.dmRecipientIdent()));
	query.bindValue(":dmSenderIdent", nullVariantWhenIsNull(envelope.dmSenderIdent()));
	query.bindValue(":dmLegalTitleLaw", nullVariantWhenIsNull(envelope.dmLegalTitleLawStr()));
	query.bindValue(":dmLegalTitleYear", nullVariantWhenIsNull(envelope.dmLegalTitleYearStr()));
	query.bindValue(":dmLegalTitleSect", nullVariantWhenIsNull(envelope.dmLegalTitleSect()));
	query.bindValue(":dmLegalTitlePar", nullVariantWhenIsNull(envelope.dmLegalTitlePar()));
	query.bindValue(":dmLegalTitlePoint", nullVariantWhenIsNull(envelope.dmLegalTitlePoint()));
	query.bindValue(":dmPersonalDelivery", Isds::nilBool2Variant((envelope.dmPersonalDelivery())));
	query.bindValue(":dmAllowSubstDelivery", Isds::nilBool2Variant((envelope.dmAllowSubstDelivery())));

	/*
	 * We don't save message timestamp because we don't use it.
	 *
	 * query.bindValue(":dmQTimestamp", envelope.dmQTimestamp());
	*/
	query.bindValue(":dmQTimestamp", nullVariantWhenIsNull(QString()));

	query.bindValue(":dmDeliveryTime", nullVariantWhenIsNull(dateTimeToDbFormatStr(envelope.dmDeliveryTime())));
	query.bindValue(":dmAcceptanceTime", nullVariantWhenIsNull(dateTimeToDbFormatStr(envelope.dmAcceptanceTime())));
	query.bindValue(":dmMessageStatus", Isds::dmState2Variant(envelope.dmMessageStatus()));
	query.bindValue(":dmAttachmentSize", Isds::nonNegativeLong2Variant(envelope.dmAttachmentSize()));
	query.bindValue(":dmType", (!envelope.dmType().isNull()) ? envelope.dmType() : QVariant());
	query.bindValue(":_dmMessageType", messageType);
	query.bindValue(":_dmDownloadDate", nullVariantWhenIsNull(QDateTime::currentDateTime().toString(DATETIME_DB_FORMAT)));
	query.bindValue(":_dmCustomData", nullVariantWhenIsNull(QString()));
	query.bindValue(":_dmAttachDownloaded", false);
	query.bindValue(":_dmReadLocally", false);

	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	return true;
fail:
	return false;
}

bool MessageDb::markMessageLocallyRead(qint64 msgId, bool read)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr =
	    "UPDATE messages SET _dmReadLocally = :_dmReadLocally "
	    "WHERE dmID = :dmID";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	query.bindValue(":dmID", msgId);
	query.bindValue(":_dmReadLocally", read);

	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	return true;
}

bool MessageDb::markMessagesLocallyRead(enum MessageType messageType, bool read)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr =
	    "UPDATE messages SET _dmReadLocally = :_dmReadLocally "
	    "WHERE _dmMessageType = :_dmMessageType";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	query.bindValue(":_dmReadLocally", read);
	query.bindValue(":_dmMessageType", messageType);

	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	return true;
}

bool MessageDb::openDb(const QString &fileName, bool storeToDisk)
{
	SQLiteDb::OpenFlags flags = SQLiteDb::CREATE_MISSING;
	flags |= storeToDisk ? SQLiteDb::NO_OPTIONS : SQLiteDb::FORCE_IN_MEMORY;

	return SQLiteDb::openDb(fileName, flags);
}

int MessageDb::searchMessagesAndSetModelFromDb(MessageListModel *msgModel,
    const AcntId &acntId, const QString &phrase,
    enum MessageType messageType, const QList<qint64> &msgIds)
{
	if (Q_UNLIKELY(msgModel == Q_NULLPTR)) {
		Q_ASSERT(0);
		return 0;
	}

	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);
	QString queryStr;
	int resultCnt = 0;

	if (!msgIds.isEmpty()) {

		/*
		 * Append messages into model where attachment name
		 * contains search phrase.
		 */

		queryStr = "SELECT dmID, dmSender, dmRecipient, dmAnnotation, "
		    "dmDeliveryTime, dmAcceptanceTime, _dmReadLocally, "
		    "_dmAttachDownloaded, _dmMessageType "
		    "FROM messages WHERE _dmMessageType = :_dmMessageType "
		    "AND dmID IN (";
		for (int i = 0; i < msgIds.count(); ++i) {
			queryStr += QString::number(msgIds.at(i));
			if (i != (msgIds.count() - 1)) {
				queryStr += ", ";
			}
		}
		queryStr += ") ORDER BY dmID DESC";

		if (Q_UNLIKELY(!query.prepare(queryStr))) {
			logErrorNL("Cannot prepare SQL query: %s",
			    query.lastError().text().toUtf8().constData());
			return resultCnt;
		}
		query.bindValue(":_dmMessageType", messageType);

		if (query.exec() && query.isActive()) {
			resultCnt += msgModel->setQuery(acntId.username(),
			    acntId.testing(), query, true);
		}
	}

	/* Search in message envelopes and append resutls into model */
	queryStr = "SELECT DISTINCT dmID, dmSender, dmRecipient, dmAnnotation, ";
	queryStr += "dmDeliveryTime, dmAcceptanceTime, _dmReadLocally, ";
	queryStr += "_dmAttachDownloaded, _dmMessageType ";
	queryStr += "FROM messages WHERE ";
	queryStr += "_dmMessageType = :_dmMessageType AND ";
	queryStr += "(";
	queryStr += "dmID LIKE '%'||:_phrase||'%' OR ";
	queryStr += "dmSender LIKE '%'||:_phrase||'%' OR ";
	queryStr += "dmRecipient LIKE '%'||:_phrase||'%' OR ";
	queryStr += "dmAnnotation LIKE '%'||:_phrase||'%' OR ";
	queryStr += "dmRecipientRefNumber LIKE '%'||:_phrase||'%' OR ";
	queryStr += "dmSenderRefNumber LIKE '%'||:_phrase||'%' OR ";
	queryStr += "dmRecipientIdent LIKE '%'||:_phrase||'%' OR ";
	queryStr += "dmSenderIdent LIKE '%'||:_phrase||'%' OR ";
	queryStr += "dmToHands LIKE '%'||:_phrase||'%' OR ";
	queryStr += "dmDeliveryTime LIKE '%'||:_phrase||'%'";
	queryStr += ") ";
	queryStr += "ORDER BY dmID DESC";

	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return resultCnt;
	}
	query.bindValue(":_phrase", phrase);
	query.bindValue(":_dmMessageType", messageType);

	if (query.exec() && query.isActive()) {
		resultCnt += msgModel->setQuery(acntId.username(),
		    acntId.testing(), query, true);
	} else {
		/*
		 * SQL query can return empty search result. It is not error.
		 * Show log information like warning.
		*/
		logWarningNL(
		    "Cannot execute SQL query or empty search result: %s",
		    query.lastError().text().toUtf8().constData());
	}

	return resultCnt;
}

bool MessageDb::setAttachmentDownloaded(qint64 msgId, bool downloaded)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "UPDATE messages SET "
	    "_dmAttachDownloaded = :_dmAttachDownloaded "
	    "WHERE dmID = :dmID";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	query.bindValue(":dmID", msgId);
	query.bindValue(":_dmAttachDownloaded", downloaded);

	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	return true;
}

bool MessageDb::setAttachmentsDownloaded(bool downloaded)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "UPDATE messages SET "
	    "_dmAttachDownloaded = :_dmAttachDownloaded";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	query.bindValue(":_dmAttachDownloaded", downloaded);

	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	return true;
}

bool MessageDb::updateMessageAuthorInfoInDb(qint64 msgId, const QString &data)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr =
	    "UPDATE messages SET _dmCustomData = :_dmCustomData "
	    "WHERE dmID = :dmID";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	query.bindValue(":dmID", msgId);
	query.bindValue(":_dmCustomData", nullVariantWhenIsNull(data));

	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		return false;
	}
	return true;
}

bool MessageDb::updateMessageEnvelopeInDb(const Isds::Envelope &envelope)
{
	QMutexLocker locker(&m_lock);
	QSqlQuery query(m_db);

	QString queryStr = "UPDATE messages SET "
	    "dbIDSender = :dbIDSender, dmSender = :dmSender, "
	    "dmSenderAddress = :dmSenderAddress, "
	    "dmSenderType = :dmSenderType, "
	    "dmRecipient = :dmRecipient, "
	    "dmRecipientAddress = :dmRecipientAddress, "
	    "dmAmbiguousRecipient = :dmAmbiguousRecipient, "
	    "dmSenderOrgUnit = :dmSenderOrgUnit, "
	    "dmSenderOrgUnitNum = :dmSenderOrgUnitNum, "
	    "dbIDRecipient = :dbIDRecipient, "
	    "dmRecipientOrgUnit = :dmRecipientOrgUnit, "
	    "dmRecipientOrgUnitNum = :dmRecipientOrgUnitNum, "
	    "dmToHands = :dmToHands, dmAnnotation = :dmAnnotation, "
	    "dmRecipientRefNumber = :dmRecipientRefNumber, "
	    "dmSenderRefNumber = :dmSenderRefNumber, "
	    "dmRecipientIdent = :dmRecipientIdent, "
	    "dmSenderIdent = :dmSenderIdent, "
	    "dmLegalTitleLaw = :dmLegalTitleLaw, "
	    "dmLegalTitleYear = :dmLegalTitleYear, "
	    "dmLegalTitleSect = :dmLegalTitleSect, "
	    "dmLegalTitlePar = :dmLegalTitlePar, "
	    "dmLegalTitlePoint = :dmLegalTitlePoint, "
	    "dmPersonalDelivery = :dmPersonalDelivery, "
	    "dmAllowSubstDelivery = :dmAllowSubstDelivery, "
	    "dmQTimestamp = :dmQTimestamp, "
	    "dmDeliveryTime = :dmDeliveryTime, "
	    "dmAcceptanceTime = :dmAcceptanceTime, "
	    "dmMessageStatus = :dmMessageStatus, "
	    "dmAttachmentSize = :dmAttachmentSize, "
	    "dmType = :dmType "
	    "WHERE dmID = :dmID";
	if (Q_UNLIKELY(!query.prepare(queryStr))) {
		logErrorNL("Cannot prepare SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	query.bindValue(":dmID", envelope.dmId());
	query.bindValue(":dbIDSender", nullVariantWhenIsNull(envelope.dbIDSender()));
	query.bindValue(":dmSender", nullVariantWhenIsNull(envelope.dmSender()));
	query.bindValue(":dmSenderAddress", nullVariantWhenIsNull(envelope.dmSenderAddress()));
	query.bindValue(":dmSenderType", Isds::dbType2IntVariant(envelope.dmSenderType()));
	query.bindValue(":dmRecipient", nullVariantWhenIsNull(envelope.dmRecipient()));
	query.bindValue(":dmRecipientAddress", nullVariantWhenIsNull(envelope.dmRecipientAddress()));
	query.bindValue(":dmAmbiguousRecipient", Isds::nilBool2Variant(envelope.dmAmbiguousRecipient()));
	query.bindValue(":dmSenderOrgUnit", nullVariantWhenIsNull(envelope.dmSenderOrgUnit()));
	query.bindValue(":dmSenderOrgUnitNum", nullVariantWhenIsNull(envelope.dmSenderOrgUnitNumStr()));
	query.bindValue(":dbIDRecipient", nullVariantWhenIsNull(envelope.dbIDRecipient()));
	query.bindValue(":dmRecipientOrgUnit", nullVariantWhenIsNull(envelope.dmRecipientOrgUnit()));
	query.bindValue(":dmRecipientOrgUnitNum", nullVariantWhenIsNull(envelope.dmRecipientOrgUnitNumStr()));
	query.bindValue(":dmToHands", nullVariantWhenIsNull(envelope.dmToHands()));
	query.bindValue(":dmAnnotation", nullVariantWhenIsNull(envelope.dmAnnotation()));
	query.bindValue(":dmRecipientRefNumber", nullVariantWhenIsNull(envelope.dmRecipientRefNumber()));
	query.bindValue(":dmSenderRefNumber", nullVariantWhenIsNull(envelope.dmSenderRefNumber()));
	query.bindValue(":dmRecipientIdent", nullVariantWhenIsNull(envelope.dmRecipientIdent()));
	query.bindValue(":dmSenderIdent", nullVariantWhenIsNull(envelope.dmSenderIdent()));
	query.bindValue(":dmLegalTitleLaw", nullVariantWhenIsNull(envelope.dmLegalTitleLawStr()));
	query.bindValue(":dmLegalTitleYear", nullVariantWhenIsNull(envelope.dmLegalTitleYearStr()));
	query.bindValue(":dmLegalTitleSect", nullVariantWhenIsNull(envelope.dmLegalTitleSect()));
	query.bindValue(":dmLegalTitlePar", nullVariantWhenIsNull(envelope.dmLegalTitlePar()));
	query.bindValue(":dmLegalTitlePoint", nullVariantWhenIsNull(envelope.dmLegalTitlePoint()));
	query.bindValue(":dmPersonalDelivery", Isds::nilBool2Variant(envelope.dmPersonalDelivery()));
	query.bindValue(":dmAllowSubstDelivery", Isds::nilBool2Variant(envelope.dmAllowSubstDelivery()));

	/*
	 * We don't save message timestamp because we don't use it.
	 *
	 * query.bindValue(":dmQTimestamp", envelope.dmQTimestamp());
	*/
	query.bindValue(":dmQTimestamp", nullVariantWhenIsNull(QString()));

	query.bindValue(":dmDeliveryTime", nullVariantWhenIsNull(dateTimeToDbFormatStr(envelope.dmDeliveryTime())));
	query.bindValue(":dmAcceptanceTime", nullVariantWhenIsNull(dateTimeToDbFormatStr(envelope.dmAcceptanceTime())));
	query.bindValue(":dmMessageStatus", Isds::dmState2Variant(envelope.dmMessageStatus()));
	query.bindValue(":dmAttachmentSize", Isds::nonNegativeLong2Variant(envelope.dmAttachmentSize()));
	query.bindValue(":dmType", (!envelope.dmType().isNull()) ? envelope.dmType() : QVariant());

	if (Q_UNLIKELY(!query.exec())) {
		logErrorNL("Cannot execute SQL query: %s",
		    query.lastError().text().toUtf8().constData());
		goto fail;
	}
	return true;
fail:
	return false;
}

QList<class SQLiteTbl *> MessageDb::listOfTables(void) const
{
	QList<class SQLiteTbl *> tables;
	tables.append(&msgsTbl);
	tables.append(&evntsTbl);
	return tables;
}
