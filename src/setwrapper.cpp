/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QFileDialog>
#include <QSettings>

#include "src/datovka_shared/crypto/crypto_pin.h"
#include "src/datovka_shared/crypto/crypto_wrapped.h"
#include "src/datovka_shared/localisation/localisation.h"
#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/settings/prefs.h"
#include "src/datovka_shared/settings/records_management.h"
#include "src/dialogues/dialogues.h"
#include "src/font/font.h"
#include "src/global.h"
#include "src/models/accountmodel.h"
#include "src/io/filesystem.h"
#include "src/settings/account.h"
#include "src/settings/accounts.h"
#include "src/settings.h"
#include "src/setwrapper.h"

GlobalSettingsQmlWrapper::GlobalSettingsQmlWrapper(QObject *parent)
    : QObject(parent),
    m_cleanAppStart(true)
{
}

QString GlobalSettingsQmlWrapper::appVersion(void)
{
	return QStringLiteral(VERSION);
}

int GlobalSettingsQmlWrapper::attachmentLifeDays(void)
{
	qint64 val = 0;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	    GlobInstcs::prefsPtr->intVal("storage.database.attachments.keep_downloaded.days", val)) {
		return val;
	} else {
		Q_ASSERT(0);
		return val;
	}
}

void GlobalSettingsQmlWrapper::setAttachmentLifeDays(int attachmentLifeDays)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setIntVal(
		    "storage.database.attachments.keep_downloaded.days",
		    attachmentLifeDays);
	} else {
		Q_ASSERT(0);
	}
}

bool GlobalSettingsQmlWrapper::downloadCompleteMsgs(void)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		bool val = false;
		return GlobInstcs::prefsPtr->boolVal(
		    "action.sync_account.tie.action.download_message", val) && val;
	} else {
		Q_ASSERT(0);
		return false;
	}
}

void GlobalSettingsQmlWrapper::setDownloadCompleteMsgs(bool downloadCompleteMsgs)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setBoolVal(
		    "action.sync_account.tie.action.download_message",
		    downloadCompleteMsgs);
	} else {
		Q_ASSERT(0);
	}
}

bool GlobalSettingsQmlWrapper::downloadOnlyNewMsgs(void)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		bool val = false;
		return GlobInstcs::prefsPtr->boolVal(
		    "action.sync_account.restrict_to.new_messages.enabled", val) && val;
	} else {
		Q_ASSERT(0);
		return false;
	}
}

void GlobalSettingsQmlWrapper::setDownloadOnlyNewMsgs(bool downloadOnlyNewMsgs)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setBoolVal(
		    "action.sync_account.restrict_to.new_messages.enabled",
		    downloadOnlyNewMsgs);
	} else {
		Q_ASSERT(0);
	}
}

QString GlobalSettingsQmlWrapper::dbPath(void)
{
	QString val = dfltDbAndConfigLoc();

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	    GlobInstcs::prefsPtr->strVal("storage.database.path", val)) {
		return val;
	} else {
		Q_ASSERT(0);
		return val;
	}
}

void GlobalSettingsQmlWrapper::setDbPath(const QString &dbLocation)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setStrVal("storage.database.path",
		    !dbLocation.isEmpty() ? dbLocation : dfltDbAndConfigLoc());
	} else {
		Q_ASSERT(0);
	}
}

int GlobalSettingsQmlWrapper::fontSize(void)
{
	qint64 val = 16;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	    GlobInstcs::prefsPtr->intVal("font.size", val)) {
		return val;
	} else {
		Q_ASSERT(0);
		return val;
	}
}

void GlobalSettingsQmlWrapper::setFontSize(int fontSize)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setIntVal("font.size", fontSize);
	} else {
		Q_ASSERT(0);
	}
}

QString GlobalSettingsQmlWrapper::font(void)
{
	QString val = Font::fontSystem;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	    GlobInstcs::prefsPtr->strVal("font.type", val)) {
		return Font::fontCode(val);
	} else {
		Q_ASSERT(0);
		return Font::fontCode(val);
	}
}

void GlobalSettingsQmlWrapper::setFont(const QString &font)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setStrVal("font.type", Font::fontCode(font));
	} else {
		Q_ASSERT(0);
	}
}

QString GlobalSettingsQmlWrapper::language(void)
{
	QString val = Localisation::langSystem;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	    GlobInstcs::prefsPtr->strVal("translation.language", val)) {
		return val;
	} else {
		Q_ASSERT(0);
		return val;
	}
}

void GlobalSettingsQmlWrapper::setLanguage(const QString &language)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setStrVal("translation.language", language);
	} else {
		Q_ASSERT(0);
	}
}

int GlobalSettingsQmlWrapper::messageLifeDays(void)
{
	qint64 val = 0;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	    GlobInstcs::prefsPtr->intVal("storage.database.envelopes.keep_downloaded.days", val)) {
		return val;
	} else {
		Q_ASSERT(0);
		return val;
	}
}

void GlobalSettingsQmlWrapper::setMessageLifeDays(int messageLifeDays)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setIntVal(
		    "storage.database.envelopes.keep_downloaded.days",
		    messageLifeDays);
	} else {
		Q_ASSERT(0);
	}
}

int GlobalSettingsQmlWrapper::zfoDbSizeMBs(void)
{
	qint64 val = 200;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	    GlobInstcs::prefsPtr->intVal("storage.database.zfos.size.max.MB", val)) {
		return val;
	} else {
		Q_ASSERT(0);
		return val;
	}
}

void GlobalSettingsQmlWrapper::setZfoDbSizeMBs(int zfoDbSizeMBs)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setIntVal(
		    "storage.database.zfos.size.max.MB", zfoDbSizeMBs);
	} else {
		Q_ASSERT(0);
	}
}

bool GlobalSettingsQmlWrapper::showDefaultButton(void)
{
	return dbPath() != dfltDbAndConfigLoc();
}

QString GlobalSettingsQmlWrapper::pinValue(void)
{
	if (GlobInstcs::setPtr != Q_NULLPTR) {
		return GlobInstcs::setPtr->_pinVal;
	} else {
		Q_ASSERT(0);
		return QString();
	}
}

void GlobalSettingsQmlWrapper::updatePinSettings(const QString &pinValue)
{
	if (GlobInstcs::setPtr != Q_NULLPTR) {
		PinSettings::updatePinSettings(*GlobInstcs::setPtr, pinValue);
	} else {
		Q_ASSERT(0);
	}
}

QString GlobalSettingsQmlWrapper::rmUrl(void)
{
	if (GlobInstcs::recMgmtSetPtr != Q_NULLPTR) {
		return GlobInstcs::recMgmtSetPtr->url();
	} else {
		Q_ASSERT(0);
		return QString();
	}
}

void GlobalSettingsQmlWrapper::setRmUrl(const QString &rmUrl)
{
	if (Q_UNLIKELY(GlobInstcs::recMgmtSetPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	GlobInstcs::recMgmtSetPtr->setUrl(rmUrl);
}

QString GlobalSettingsQmlWrapper::rmToken(void)
{
	if (GlobInstcs::recMgmtSetPtr != Q_NULLPTR) {
		return GlobInstcs::recMgmtSetPtr->token();
	} else {
		Q_ASSERT(0);
		return QString();
	}
}

void GlobalSettingsQmlWrapper::setRmToken(const QString &rmToken)
{
	if (Q_UNLIKELY(GlobInstcs::recMgmtSetPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	GlobInstcs::recMgmtSetPtr->setToken(rmToken);
}

bool GlobalSettingsQmlWrapper::pinConfigured(void)
{
	return GlobInstcs::setPtr->pinConfigured();
}

bool GlobalSettingsQmlWrapper::pinValid(const QString &pinValue)
{
	return PinSettings::verifyPin(*GlobInstcs::setPtr, pinValue);
}

void GlobalSettingsQmlWrapper::verifyPin(const QString &pinValue)
{
	if (Q_UNLIKELY((GlobInstcs::setPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (!GlobInstcs::setPtr->pinConfigured()) {
		return;
	}

	bool verResult =
	    PinSettings::verifyPin(*GlobInstcs::setPtr, pinValue);
	if (verResult) {
		/* Decrypt all keys in account model. */
		GlobInstcs::acntMapPtr->decryptAllPwds(
		    GlobInstcs::setPtr->_pinVal);
		/* Decrypt records management token in global settings */
		if (GlobInstcs::recMgmtSetPtr != Q_NULLPTR) {
			GlobInstcs::recMgmtSetPtr->decryptToken(
			    GlobInstcs::setPtr->_pinVal);
		}
		if (m_cleanAppStart) {
			if (syncAfterCleanAppStart()) {
				emit runSyncAfterCleanAppStartSig();
			}
			m_cleanAppStart = false;
		}
	}

	emit sendPinReply(verResult);

	QString lastUpdStr(lastUpdateStr());
	if (verResult && !lastUpdStr.isEmpty()) {
		emit statusBarTextChanged(
		    tr("Last synchronisation: %1").arg(lastUpdStr), false);
	}

	if (verResult && (pwdExpirationDays() > 0)) {
		emit runGetPasswordExpirationInfoSig();
	}
}

bool GlobalSettingsQmlWrapper::useExplicitClipboardOperations(void)
{
//#if defined Q_OS_LINUX /* For testing purposes. */
#if defined Q_OS_ANDROID
	return true;
#else /* !defined Q_OS_ANDROID */
	return false;
#endif /* defined Q_OS_ANDROID */
}

QString GlobalSettingsQmlWrapper::changeDbPath(const QString &currentLocation,
    bool setDefaultLocation)
{

	QString newLocation(dfltDbAndConfigLoc());

	if (!setDefaultLocation) {
		/* TODO - Use new QML input dialogue instead of
		 * QFileDialog in the future
		 */
		newLocation = QFileDialog::getExistingDirectory(0,
		    tr("Select directory"), currentLocation,
		    QFileDialog::ShowDirsOnly
		    | QFileDialog::DontResolveSymlinks);
		if (newLocation.isEmpty()) {
			return QString();
		}
	} else {
		if (currentLocation == newLocation) {
			return QString();
		}
	}

	/* check if new location is writable */
	QFileInfo fi(newLocation);
	if (!fi.isWritable()) {
		Dialogues::errorMessage(Dialogues::CRITICAL,
		    tr("New location error"),
		    tr("It is not possible to store databases in the new location. Write permission denied."),
		    tr("Action will be cancelled."));
		return QString();
	}

	return newLocation;
}

int GlobalSettingsQmlWrapper::inactivityInterval(void)
{
	qint64 val = 0;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	    GlobInstcs::prefsPtr->intVal("ui.inactivity.lock.timeout.ms", val)) {
		return val / 1000;
	} else {
		Q_ASSERT(0);
		return -1;
	}
}

void GlobalSettingsQmlWrapper::setInactivityInterval(int secs)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setIntVal("ui.inactivity.lock.timeout.ms",
		    (secs > 0) ? (secs * 1000) : 0);
	} else {
		Q_ASSERT(0);
	}
}

QString GlobalSettingsQmlWrapper::lastUpdateStr(void)
{
	QDateTime val;
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->dateTimeVal(
		    "action.sync_account.last_invocation.datetime", val);
	} else {
		Q_ASSERT(0);
	}
	if (Q_UNLIKELY(!val.isValid())) {
		return QString();
	}
	val = val.toLocalTime();
	QDateTime currentDateTime(QDateTime::currentDateTime());

#define DATE_FMT QStringLiteral("yyyy-MM-dd")
	if (val.toString(DATE_FMT) == currentDateTime.toString(DATE_FMT)) {
		/* Only time. */
		return val.toString("HH:mm:ss");
	} else {
		/* Only date. */
		return val.toString("dd.MM.yyyy");
	}
#undef DATE_FMT
}

void GlobalSettingsQmlWrapper::setLastUpdateToNow(void)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setDateTimeVal(
		    "action.sync_account.last_invocation.datetime",
		    QDateTime::currentDateTimeUtc());
	} else {
		Q_ASSERT(0);
	}
}

void GlobalSettingsQmlWrapper::saveAllSettings(
    const AccountListModel *accountModel)
{
	if (Q_UNLIKELY(GlobInstcs::setPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(accountModel == Q_NULLPTR)) {
		logErrorNL("%s", "Missing account model to read settings from.");
		Q_ASSERT(0);
		return;
	}

	QSettings settings(Settings::settingsPath(), QSettings::IniFormat);
	settings.clear();
	GlobInstcs::setPtr->saveToSettings(settings);
	accountModel->saveAccountsToSettings(GlobInstcs::setPtr->_pinVal,
	    settings);
	settings.sync();
}

int GlobalSettingsQmlWrapper::debugVerbosityLevel(void)
{
	if (GlobInstcs::logPtr != Q_NULLPTR) {
		return GlobInstcs::logPtr->debugVerbosity();
	} else {
		Q_ASSERT(0);
		return 0;
	}
}

void GlobalSettingsQmlWrapper::setDebugVerbosityLevel(int dVL)
{
	if ((GlobInstcs::logPtr != Q_NULLPTR) &&
	    (GlobInstcs::setPtr != Q_NULLPTR)) {
		/* Set verbosity directly and store it to settings. */
		GlobInstcs::logPtr->setDebugVerbosity(dVL);
		GlobInstcs::prefsPtr->setIntVal("logging.verbosity.level.debug",
		    dVL);
	} else {
		Q_ASSERT(0);
	}
}

int GlobalSettingsQmlWrapper::logVerbosityLevel(void)
{
	if (GlobInstcs::logPtr != Q_NULLPTR) {
		return GlobInstcs::logPtr->logVerbosity();
	} else {
		Q_ASSERT(0);
		return 0;
	}
}

void GlobalSettingsQmlWrapper::setLogVerbosityLevel(int lVL)
{
	if ((GlobInstcs::logPtr != Q_NULLPTR) &&
	    (GlobInstcs::setPtr != Q_NULLPTR)) {
		GlobInstcs::logPtr->setLogVerbosity(lVL);
		GlobInstcs::prefsPtr->setIntVal("logging.verbosity.level.log",
		    lVL);
	} else {
		Q_ASSERT(0);
	}
}

bool GlobalSettingsQmlWrapper::cleanAppStart(void) const
{
	return m_cleanAppStart;
}

void GlobalSettingsQmlWrapper::setCleanAppStart(bool val)
{
	m_cleanAppStart = val;
}

bool GlobalSettingsQmlWrapper::syncAfterCleanAppStart(void)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		bool val = false;
		return GlobInstcs::prefsPtr->boolVal(
		    "page.account_list.on.start.tie.action.sync_all_accounts", val) && val;
	} else {
		Q_ASSERT(0);
		return false;
	}
}

void GlobalSettingsQmlWrapper::setSyncAfterCleanAppStart(bool syncAfterAppStart)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setBoolVal(
		    "page.account_list.on.start.tie.action.sync_all_accounts",
		    syncAfterAppStart);
	} else {
		Q_ASSERT(0);
	}
}

int GlobalSettingsQmlWrapper::pwdExpirationDays(void)
{
	qint64 val = 0;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	    GlobInstcs::prefsPtr->intVal("accounts.password.expiration.notify_ahead.days", val)) {
		return val;
	} else {
		Q_ASSERT(0);
		return val;
	}
}

void GlobalSettingsQmlWrapper::setPwdExpirationDays(int days)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setIntVal(
		    "accounts.password.expiration.notify_ahead.days", days);
	} else {
		Q_ASSERT(0);
	}
}

int GlobalSettingsQmlWrapper::msgDeletionNotifyAheadDays(void)
{
	qint64 val = 7;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	    GlobInstcs::prefsPtr->intVal("accounts.message.deletion.notify_ahead.days", val)) {
		return val;
	} else {
		Q_ASSERT(0);
		return val;
	}
}

int GlobalSettingsQmlWrapper::bannerVisibilityCount(void)
{
	qint64 val = 3;

	if ((GlobInstcs::prefsPtr != Q_NULLPTR) &&
	        GlobInstcs::prefsPtr->intVal(
	        "page.account_list.on.start.banner.mojeid2.visibility.count", val)) {
		return val;
	} else {
		Q_ASSERT(0);
		return val;
	}
}

void GlobalSettingsQmlWrapper::setBannerVisibilityCount(int cnt)
{
	if (GlobInstcs::prefsPtr != Q_NULLPTR) {
		GlobInstcs::prefsPtr->setIntVal(
		    "page.account_list.on.start.banner.mojeid2.visibility.count",
		    cnt);
	} else {
		Q_ASSERT(0);
	}
}
