/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>
#include <QString>

/*!
 * @provides interface between QML and file-system functionality.
 */
class InteractionFilesystem : public QObject {
	Q_OBJECT

public:
	/* Corresponds to some QStandardPaths::StandardLocation. */
	enum Location {
		DESKTOP_LOCATION,
		DOCUMENTS_LOCATION,
		DOWNLOAD_LOCATION,
		PICTURE_LOCATION,
		TEMP_LOCATION
	};
	Q_ENUM(Location)

	/* Don't forget to declare various properties to the QML system. */
	static
	void declareQML(void);

	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit InteractionFilesystem(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Copy constructor.
	 *
	 * @note Needed for QVariant conversion.
	 *
	 * @param[in] inter Object to be copied.
	 */
	InteractionFilesystem(const InteractionFilesystem &inter);

	/*!
	 * @brief Returns home location.
	 *
	 * @return Home location path.
	 */
	static Q_INVOKABLE
	QString locate(enum Location location);

	/*!
	 * @brief Check whether path is a readable directory.
	 *
	 * @param[in] path Absolute or relative path.
	 * @return Non-empty absolute path to directory if supplied path is a
	 *     directory. Returns empty string if directory does not exist or
	 *     is not accessible.
	 */
	static Q_INVOKABLE
	QString absoluteDirPath(const QString &path);

	/*!
	 * @brief Check whether path is a readable file.
	 *
	 * @param[in] path Absolute or relative path.
	 * @return Non-empty absolute path to directory without file name if
	 *     the supplied path is a file. Returns empty string if directory
	 *     does not exist or is not accessible.
	 */
	static Q_INVOKABLE
	QString absolutePath(const QString &path);
};

/* QML passes its arguments via QVariant. */
Q_DECLARE_METATYPE(InteractionFilesystem)
Q_DECLARE_METATYPE(InteractionFilesystem::Location)
