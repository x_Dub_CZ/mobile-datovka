/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QQmlEngine> /* qmlRegisterType */

#include "src/qml_interaction/message_info.h"

void MsgInfo::declareQML(void)
{
	qmlRegisterType<MsgInfo>("cz.nic.mobileDatovka.msgInfo", 1, 0, "MsgInfo");
	qRegisterMetaType<MsgInfo>("MsgInfo");
	qRegisterMetaType<MsgInfo::ZfoType>("MsgInfo::ZfoType");
}

MsgInfo::MsgInfo(QObject *parent)
    : QObject(parent),
    m_type(TYPE_UNKNOWN),
    m_idStr(),
    m_annotation(),
    m_descrHtml(),
    m_emailBody()
{
}

MsgInfo::MsgInfo(const MsgInfo &info)
    : QObject(),
    m_type(info.m_type),
    m_idStr(info.m_idStr),
    m_annotation(info.m_annotation),
    m_descrHtml(info.m_descrHtml),
    m_emailBody(info.m_emailBody)
{
}

MsgInfo::MsgInfo(enum ZfoType type, const QString &idStr,
    const QString &annotation, const QString &descrHtml,
    const QString &emailBody, QObject *parent)
    : QObject(parent),
    m_type(type),
    m_idStr(idStr),
    m_annotation(annotation),
    m_descrHtml(descrHtml),
    m_emailBody(emailBody)
{
}

enum MsgInfo::ZfoType MsgInfo::type(void) const
{
	return m_type;
}

void MsgInfo::setType(enum ZfoType type)
{
	m_type = type;
	emit typeChanged(m_type);
}

QString MsgInfo::idStr(void) const
{
	return m_idStr;
}

void MsgInfo::setIdStr(const QString &idStr)
{
	m_idStr = idStr;
	emit idStrChanged(m_idStr);
}

QString MsgInfo::annotation(void) const
{
	return m_annotation;
}

void MsgInfo::setAnnotation(const QString &annotation)
{
	m_annotation = annotation;
	emit annotationChanged(m_annotation);
}

QString MsgInfo::descrHtml(void) const
{
	return m_descrHtml;
}

void MsgInfo::setDescrHtml(const QString &descrHtml)
{
	m_descrHtml = descrHtml;
	emit descrHtmlChanged(m_descrHtml);
}

QString MsgInfo::emailBody(void) const
{
	return m_emailBody;
}

void MsgInfo::setEmailBody(const QString &emailBody)
{
	m_emailBody = emailBody;
	emit emailBodyChanged(m_emailBody);
}
