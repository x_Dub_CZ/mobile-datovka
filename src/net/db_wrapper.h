/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QCoreApplication> /* Q_DECLARE_TR_FUNCTIONS */

#include "src/datovka_shared/isds/message_interface.h"
#include "src/datovka_shared/isds/box_interface.h"
#include "src/datovka_shared/isds/box_interface2.h"
#include "src/sqlite/message_db_container.h"
#include "src/sqlite/file_db_container.h"

class AcntId; /* Forward declaration. */
class QDateTime; /* Forward declaration. */

/*
 * Class DbWrapper updates data from http response to database.
 * Class is initialised and used in the ISDS wrapper class (isds_wrapper.h)
 */
class DbWrapper {
	Q_DECLARE_TR_FUNCTIONS(DbWrapper)

public:
	/*!
	 * @brief Create account info text for QML.
	 *
	 * @param[in] dbOwnerInfo account info struct.
	 * @return account info string.
	 */
	static
	QString createAccountInfoStringForQml(
	    const Isds::DbOwnerInfoExt2 &dbOwnerInfo);

	/*!
	 * @brief Insert messagelist to db.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] messageType Message type.
	 * @param[in] envelopes List of message envelopes.
	 * @param[out] messageChangedStatusList List of message ids
	 *             where message status has changed.
	 * @param[out] txt Error description if something fails.
	 * @param[out] listOfNewMsgIds List of new message Ids for complete
	 *             message downloading.
	 * @return true if success.
	 */
	static
	bool insertMessageListToDb(const AcntId &acntId,
	    enum MessageDb::MessageType messageType,
	    const QList<Isds::Envelope> &envelopes,
	    QList<qint64> &messageChangedStatusList, QString &txt,
	    QList<qint64> &listOfNewMsgIds);

	/*!
	 * @brief Insert complete message to db.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] message Message data including message identifier.
	 * @param[in] messageType Message type.
	 * @param[out] txt Error description if something fails.
	 * @return true if success.
	 */
	static
	bool insertCompleteMessageToDb(const AcntId &acntId,
	    const Isds::Message &message,
	    enum MessageDb::MessageType messageType, QString &txt);

	/*!
	 * @brief Insert password expiration date to db.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] expirDate Password expiration date.
	 * @return true if success.
	 */
	static
	bool insertPwdExpirationToDb(const AcntId &acntId,
	    const QDateTime &expirDate);

	/*!
	 * @brief Insert account info to db.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] dbOwnerInfo Account info struct.
	 * @return true if success.
	 */
	static
	bool insertAccountInfoToDb(const AcntId &acntId,
	    const Isds::DbOwnerInfoExt2 &dbOwnerInfo);

	/*!
	 * @brief Insert long term storage info into db.
	 *
	 * @param[in] dbID Data box id.
	 * @param[in] dtInfo Long term storage info struct.
	 * @return True if success.
	 */
	static
	bool insertDTInfoToDb(const QString &dbID,
	    const Isds::DTInfoOutput &dtInfo);

	/*!
	 * @brief Insert user info to db.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] dbUserInfo User info struct.
	 * @return true if success.
	 */
	static
	bool insertUserInfoToDb(const AcntId &acntId,
	    const Isds::DbUserInfoExt2 &dbUserInfo);

	/*!
	 * @brief Update author info to db.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] userType Sender type.
	 * @param[in] authorName Author name string.
	 * @param[in] msgId Message ID.
	 * @param[out] txt Error description if something fails.
	 * @return true if success.
	 */
	static
	bool updateAuthorInfo(const AcntId &acntId,
	    enum Isds::Type::SenderType userType, const QString &authorName,
	    qint64 msgId, QString &txt);

	/*!
	 * @brief Insert or update delivery info to db.
	 *
	 * @param[in] acntId Account identifier.
	 * @param[in] events Event list.
	 * @param[in] msgId Message ID.
	 * @param[out] txt Error description if something fails.
	 * @return true if success.
	 */
	static
	bool insertMesasgeDeliveryInfoToDb(const AcntId &acntId,
	    const QList<Isds::Event> &events, qint64 msgId,
	    QString &txt);

	/*!
	 * @brief Insert or update zfo file info to db.
	 *
	 * @param[in] msgId message ID.
	 * @param[in] isTestAccount True if account is ISDS test enviroment.
	 * @param[in] zfoSize Real size of zfo in Bytes.
	 * @param[in] zfoData Zfo data.
	 * @return true if success.
	 */
	static
	bool insertZfoToDb(qint64 msgId, bool isTestAccount, int zfoSize,
	    const QByteArray &zfoData);

private:
	/*!
	 * @brief Private constructor.
	 */
	DbWrapper(void);
};
