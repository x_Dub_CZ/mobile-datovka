/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QEvent>

#include "src/global.h"
#include "src/locker.h"
#include "src/settings.h"

/*
 * Application behaviour on supported platforms:
 *
 * Android:
 * on close (not terminate): Active -(immediately)-> Inactive -(short time)-> Suspended
 * on attachment open: Active -(immediately)-> Inactive -(short time)-> Suspended
 *
 * iOS:
 * on close (not terminate): Active -(immediately)-> Inactive -(short time)-> Suspended
 * on attachment open: remains Active
 *
 * Don't know how to achieve hidden state.
 */

Locker::Locker(QObject *parent)
    : QObject(parent),
    m_inactivityInterval(0),
    m_inactivityTimer(),
    m_ignoreImmediateSuspension(false)
{
	/* Don't start time yet. */

	connect(&m_inactivityTimer, SIGNAL(timeout()),
	    this, SLOT(inactivityTimeOut()));
}

void Locker::ignoreNextSuspension(void)
{
	m_ignoreImmediateSuspension = true;
}

void Locker::setInactivityInterval(int secs)
{
	m_inactivityTimer.stop();
	if (secs > 0) {
		m_inactivityInterval = secs;
		m_inactivityTimer.setInterval(m_inactivityInterval * 1000);
		m_inactivityTimer.start();
	} else {
		m_inactivityInterval = 0;
	}
}

void Locker::processNewState(Qt::ApplicationState state)
{
	switch (state) {
	case Qt::ApplicationSuspended:
	case Qt::ApplicationHidden:
		if (!m_ignoreImmediateSuspension) {
			inactivityTimeOut();
		} else {
			/* Was ignored once. */
			m_ignoreImmediateSuspension = false;
		}
		return;
		break;
	case Qt::ApplicationInactive:
	case Qt::ApplicationActive:
		/* Let the timer handle the locking. */
		break;
	default:
		Q_ASSERT(0);
		return;
		break;
	}
}

void Locker::inactivityTimeOut(void)
{
	if (Q_UNLIKELY(GlobInstcs::setPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	/* Inactivity timer timed out and PIN value is set and recovered. */
	if (!GlobInstcs::setPtr->_pinVal.isEmpty()) {
		emit lockApp();
	}
}

bool Locker::eventFilter(QObject *watched, QEvent *event)
{
	switch (event->type()) {
	case QEvent::MouseMove:
	case QEvent::KeyPress:
	case QEvent::TouchBegin:
		if (m_inactivityInterval > 0) {
			m_inactivityTimer.stop(); /* Reset the timer. */
			m_inactivityTimer.start();
		}
		break;
	default:
		/* Do nothing. */
		break;
	}
	/* Standard event handling. */
	return QObject::eventFilter(watched, event);
}
