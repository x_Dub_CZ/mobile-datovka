/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QtGlobal> /* QT_VERSION_CHECK */

#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
#  include <memory> /* ::std::unique_ptr */
#else /* < Qt-5.12 */
#  include <QScopedPointer>
#endif /* >= Qt-5.12 */

#include "src/datovka_shared/isds/types.h"

class QString; /* Forward declaration. */

/*
 * Generic structures used to describe various aspects of the transmitted data.
 */

namespace Isds {

	class StatusPrivate;
	/*!
	 * @brief Response status.
	 */
	class Status {
		Q_DECLARE_PRIVATE(Status)

	public:
		Status(void);
		Status(const Status &other);
#ifdef Q_COMPILER_RVALUE_REFS
		Status(Status &&other) Q_DECL_NOEXCEPT;
#endif /* Q_COMPILER_RVALUE_REFS */
		~Status(void);

		Status &operator=(const Status &other) Q_DECL_NOTHROW;
#ifdef Q_COMPILER_RVALUE_REFS
		Status &operator=(Status &&other) Q_DECL_NOTHROW;
#endif /* Q_COMPILER_RVALUE_REFS */

		bool operator==(const Status &other) const;
		bool operator!=(const Status &other) const;

		friend void swap(Status &first, Status &second) Q_DECL_NOTHROW;

		bool isNull(void) const;

		/* Type. */
		enum Type::ResponseStatusType type(void) const;
		void setType(enum Type::ResponseStatusType t);
		/* Code. */
		const QString &code(void) const;
		void setCode(const QString &c);
#ifdef Q_COMPILER_RVALUE_REFS
		void setCode(QString &&c);
#endif /* Q_COMPILER_RVALUE_REFS */
		/* Message. */
		const QString &message(void) const;
		void setMessage(const QString &m);
#ifdef Q_COMPILER_RVALUE_REFS
		void setMessage(QString &&m);
#endif /* Q_COMPILER_RVALUE_REFS */
		/* Reference number. */
		const QString &refNum(void) const;
		void setRefNum(const QString &r);
#ifdef Q_COMPILER_RVALUE_REFS
		void setRefNum(QString &&r);
#endif /* Q_COMPILER_RVALUE_REFS */

	private:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
		::std::unique_ptr<StatusPrivate> d_ptr;
#else /* < Qt-5.12 */
		QScopedPointer<StatusPrivate> d_ptr;
#endif /* >= Qt-5.12 */
	};

	void swap(Status &first, Status &second) Q_DECL_NOTHROW;

}
