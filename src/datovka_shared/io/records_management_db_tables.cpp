/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QMap>
#include <QPair>
#include <QString>
#include <QVector>

#include "src/datovka_shared/io/records_management_db_tables.h"

namespace SrvcInfTbl {
	const QString tabName("service_info");

	const QVector< QPair<QString, enum EntryType> > knownAttrs = {
	{"url", DB_TEXT}, /* NOT NULL */
	{"name", DB_TEXT},
	{"token_name", DB_TEXT},
	{"logo_svg", DB_TEXT}
	/*
	 *  PRIMARY KEY (url)
	 */
	};

	const QMap<QString, QString> colConstraints = {
	    {"url", "NOT NULL"}
	};

	const QString tblConstraint(
	    ",\n"
	    "        PRIMARY KEY (url)"
	);

	const QMap<QString, SQLiteTbl::AttrProp> attrProps = {
	{"url",        {DB_TEXT, ""}},
	{"name",       {DB_TEXT, ""}},
	{"token_name", {DB_TEXT, ""}},
	{"logo_svg",   {DB_TEXT, ""}}
	};
} /* namespace SrvcInfTbl */
SQLiteTbl srvcInfTbl(SrvcInfTbl::tabName, SrvcInfTbl::knownAttrs,
    SrvcInfTbl::attrProps, SrvcInfTbl::colConstraints,
    SrvcInfTbl::tblConstraint);

namespace StrdFlsMsgsTbls {
	const QString tabName("stored_files_messages");

	const QVector< QPair<QString, enum EntryType> > knownAttrs = {
	{"dm_id", DB_INTEGER}, /* NOT NULL */
	{"separator", DB_TEXT}, /* NOT NULL */
	{"joined_locations", DB_TEXT} /* NOT NULL */
	/*
	 *  PRIMARY KEY (dm_id)
	 */
	};

	const QMap<QString, QString> colConstraints = {
	    {"dm_id", "NOT NULL"},
	    {"separator", "NOT NULL"},
	    {"joined_locations", "NOT NULL"}
	};

	const QString tblConstraint(
	    ",\n"
	    "        PRIMARY KEY (dm_id)"
	);

	const QMap<QString, SQLiteTbl::AttrProp> attrProps = {
	{"dm_id",            {DB_INTEGER, ""}},
	{"separator",        {DB_TEXT, ""}},
	{"joined_locations", {DB_TEXT, ""}}
	};
} /* namespace StrdFlsMsgsTbls */
SQLiteTbl strdFlsMsgsTbl(StrdFlsMsgsTbls::tabName, StrdFlsMsgsTbls::knownAttrs,
    StrdFlsMsgsTbls::attrProps, StrdFlsMsgsTbls::colConstraints,
    StrdFlsMsgsTbls::tblConstraint);
