/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

/*
 * QVariant changed slightly the behaviour in Qt-6:
 * https://runebook.dev/en/docs/qt/qtcore-changes-qt6
 *
 * Qt-5:
 *   QVariant(QString()).isNull() == true
 *   QVariant(QString()).isEmpty() == true
 *
 * Qt-6:
 *   QVariant(QString()).isNull() == false // !
 *   QVariant(QString()).isEmpty() == true
 *
 * We want to preserve the Qt-5 behaviour especially when storing null values
 * into the database.
 */

#define nullVariantWhenIsNull(val) \
	((!(val).isNull()) ? (val) : QVariant())
