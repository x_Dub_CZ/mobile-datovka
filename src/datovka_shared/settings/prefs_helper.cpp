/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QStringBuilder>
#include <QStringList>

#include "src/datovka_shared/settings/prefs_helper.h"

QString PrefsHelper::accountIdentifier(const QString &username, bool testing)
{
	return username % QStringLiteral("_") %
	    (testing ? QStringLiteral("1") : QStringLiteral("0"));
}

bool PrefsHelper::accountIdentifierData(const QString &accountId,
    QString &username, bool &testing)
{
	if (Q_UNLIKELY(accountId.isEmpty())) {
		Q_ASSERT(0);
		return false;
	}

	const QStringList split = accountId.split(QStringLiteral("_"));
	if (Q_UNLIKELY(split.size() != 2)) {
		Q_ASSERT(0);
		return false;
	}

	bool tmp = false;
	if (split[1] == QStringLiteral("0")) {
		/* tmp is already false. */
	} else if (split[1] == QStringLiteral("1")) {
		tmp = true;
	} else {
		Q_ASSERT(0);
		return false;
	}

	username = split[0];
	testing = tmp;
	return true;
}
