/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include "src/datovka_shared/settings/account.h"

/*!
 * @brief PIMPL AcntSettings class.
 */
class AcntSettingsPrivate {
	//Q_DISABLE_COPY(AcntSettingsPrivate)
public:
	AcntSettingsPrivate(void)
	    : m_accountName(), m_userName(),
	    m_loginMethod(AcntSettings::LIM_UNKNOWN), m_password(), m_pwdAlg(),
	    m_pwdSalt(), m_pwdIv(), m_pwdCode(), m_mepToken(),
	    m_isTestAccount(false), m_rememberPwd(false), m_dbDir(),
	    m_syncWithAll(false), m_p12File()
	{ }

	virtual
	~AcntSettingsPrivate(void)
	{ }

	AcntSettingsPrivate &operator=(const AcntSettingsPrivate &other) Q_DECL_NOTHROW
	{
		m_accountName = other.m_accountName;
		m_userName = other.m_userName;
		m_loginMethod = other.m_loginMethod;
		m_password = other.m_password;
		m_pwdAlg = other.m_pwdAlg;
		m_pwdSalt = other.m_pwdSalt;
		m_pwdIv = other.m_pwdIv;
		m_pwdCode = other.m_pwdCode;
		m_mepToken = other.m_mepToken;
		m_isTestAccount = other.m_isTestAccount;
		m_rememberPwd = other.m_rememberPwd;
		m_dbDir = other.m_dbDir;
		m_syncWithAll = other.m_syncWithAll;
		m_p12File = other.m_p12File;

		return *this;
	}

	bool operator==(const AcntSettingsPrivate &other) const
	{
		return (m_accountName == other.m_accountName) &&
		    (m_userName == other.m_userName) &&
		    (m_loginMethod == other.m_loginMethod) &&
		    (m_password == other.m_password) &&
		    (m_pwdAlg == other.m_pwdAlg) &&
		    (m_pwdSalt == other.m_pwdSalt) &&
		    (m_pwdIv == other.m_pwdIv) &&
		    (m_pwdCode == other.m_pwdCode) &&
		    (m_mepToken == other.m_mepToken) &&
		    (m_isTestAccount == other.m_isTestAccount) &&
		    (m_rememberPwd == other.m_rememberPwd) &&
		    (m_dbDir == other.m_dbDir) &&
		    (m_syncWithAll == other.m_syncWithAll) &&
		    (m_p12File == other.m_p12File);
	}

	QString m_accountName;
	QString m_userName;
	enum AcntSettings::LoginMethod m_loginMethod;
	QString m_password;
	QString m_pwdAlg;
	QByteArray m_pwdSalt;
	QByteArray m_pwdIv;
	QByteArray m_pwdCode;
	QString m_mepToken;
	bool m_isTestAccount;
	bool m_rememberPwd;
	QString m_dbDir;
	bool m_syncWithAll;
	QString m_p12File;
};
