/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QPainter>
#include <QPdfWriter>
#ifndef Q_OS_IOS
    #include <QPrinter>
#endif
#include <QTextDocument>

#include "src/datovka_shared/utility/pdf_printer.h"

bool Utility::printPDF(const QString &fileName, const QString &text)
{
	if (Q_UNLIKELY(text.isEmpty() || fileName.isEmpty())) {
		return false;
	}

#ifdef Q_OS_IOS
	QPdfWriter pdfwriter(fileName);
	pdfwriter.setPageSize(QPageSize(QPageSize::A4));
	pdfwriter.setPageMargins(QMargins(30,30,30,30));

	QPainter painter(&pdfwriter);
	painter.setFont(QFont("Times", 12));

	QRect r = painter.viewport();
	painter.drawText(r, Qt::AlignLeft|Qt::TextWordWrap, text);
#else
	QPrinter printer;
	printer.setFullPage(true);
	printer.setPageMargins(QMarginsF(0,0,0,0));
	printer.setOutputFileName(fileName);
	printer.setOutputFormat(QPrinter::PdfFormat);

	QTextDocument doc;
	doc.setPlainText(text);
	/*
	 * QPrinter::pageRect() is obsolete, using
	 * pageLayout().paintRectPixels(resolution()) instead.
	 * The unit of the returned rectangle is QPrinter::DevicePixel.
	 */
	doc.setPageSize(QSizeF(
#if (QT_VERSION >= QT_VERSION_CHECK(5, 3, 0))
	    printer.pageLayout().paintRectPixels(printer.resolution()).size()
#else /* < Qt-5.3 */
	    printer.pageRect().size()
#endif /* >= Qt-5.3 */
	    ));
	doc.setDocumentMargin(50.0);
	doc.print(&printer);
#endif /* Q_OS_IOS */

	return true;
}
