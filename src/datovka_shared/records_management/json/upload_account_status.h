/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#include <QDateTime>
#include <QString>

#include "src/datovka_shared/records_management/json/entry_error.h"

namespace RecMgmt {

	/*!
	 * @brief Encapsulates the upload_account_status request.
	 */
	class UploadAccountStatusReq {
	private:
		/*!
		 * @brief Constructor. Creates an invalid structure.
		 */
		UploadAccountStatusReq(void);

	public:
		/*!
		 * @brief Constructor.
		 *
		 * @param[in] boxId Data box identifier.
		 * @param[in] userId Username used to access the data box.
		 * @param[in] time Time.
		 * @param[in] rcvdNotAccepted Number of received unaccepted messages in box.
		 * @param[in] sntNotAccepted Number of sent unaccepted messages in box.
		 * @param[in] rcvdNotInRegMgmt Number of received messages not sent into records management.
		 * @param[in] sntNotInRegMgmt Number of sent messages not sent into records management.
		 */
		UploadAccountStatusReq(const QString &boxId,
		    const QString &userId, const QDateTime &time,
		    int rcvdNotAccepted, int sntNotAccepted,
		    int rcvdNotInRegMgmt, int sntNotInRegMgmt,
		    const QString &userAccountName);

		/*!
		 * @brief Copy constructor.
		 *
		 * @param[in] other Upload account status request.
		 */
		UploadAccountStatusReq(const UploadAccountStatusReq &other);

		/*!
		 * @brief Get box identifier.
		 *
		 * @return Data box identifier.
		 */
		const QString &boxId(void) const;

		/*!
		 * @brief Get username.
		 *
		 * @return Username used to access the data box.
		 */
		const QString &userId(void) const;

		/*!
		 * @brief Get time.
		 *
		 * @return Time.
		 */
		const QDateTime &time(void) const;

		/*!
		 * @brief Get number of received unaccepted messages.
		 *
		 * @return Number of received unaccepted messages in box.
		 */
		int rcvdNotAccepted(void) const;

		/*!
		 * @brief Get number of sent unaccepted messages.
		 *
		 * @return Number of sent unaccepted messages in box.
		 */
		int sntNotAccepted(void) const;

		/*!
		 * @brief Get number of received messages not sent into records management.
		 *
		 * @return Number of received messages not sent into records management.
		 */
		int rcvdNotInRegMgmt(void) const;

		/*!
		 * @brief Get number of sent messages not sent into records management.
		 *
		 * @return Number of sent messages not sent into records management.
		 */
		int sntNotInRegMgmt(void) const;

		/*!
		 * @brief Get user-defined account name as used by the application.
		 */
		const QString &userAccountName(void) const;

		/*!
		 * @brief Check whether content is valid.
		 *
		 * @return True if content is valid.
		 */
		bool isValid(void) const;

		/*!
		 * @brief Creates a upload account status request structure from
		 *     supplied JSON document.
		 *
		 * @param[in]  json JSON document.
		 * @param[out] ok Set to true on success.
		 * @return Invalid structure on error a valid structure else.
		 */
		static
		UploadAccountStatusReq fromJson(const QByteArray &json,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Converts upload account status request structure into
		 *     a JSON document.
		 *
		 * @note Unspecified values are stores as null into the JSON document.
		 *
		 * @return JSON document containing stored data.
		 */
		QByteArray toJson(void) const;

	private:
		QString m_boxId; /*!< Data box identifier. */
		QString m_userId; /*!< Username used to access the data box. */
		QDateTime m_time; /*!< Time. */
		int m_rcvdNotAccepted; /*!< Number of received unaccepted messages in box. */
		int m_sntNotAccepted; /*!< Number of sent unaccepted messages in box. */
		int m_rcvdNotInRegMgmt; /*!< Number of received messages not sent into records management. */
		int m_sntNotInRegMgmt; /*!< Number of sent messages not sent into records management. */
		QString m_userAccountName; /*!< User-defined account identifier as used in the application. */
	};

	/*!
	 * @brief Encapsulates the upload_account_status response.
	 */
	class UploadAccountStatusResp {
	private:
		/*!
		 * @brief Constructor. Creates an invalid structure.
		 */
		UploadAccountStatusResp(void);

		/*!
		 * @brief Constructor.
		 *
		 * @param[in] error Error entry.
		 */
		UploadAccountStatusResp(const ErrorEntry &error);

	public:
		/*!
		 * @brief Copy constructor.
		 *
		 * @param[in] other Upload account status response.
		 */
		UploadAccountStatusResp(const UploadAccountStatusResp &other);

		/*!
		 * @brief Return error entry.
		 *
		 * @return Error entry.
		 */
		const ErrorEntry &error(void) const;

		/*!
		 * @brief Check whether content is valid.
		 *
		 * @return True if content is valid.
		 */
		bool isValid(void) const;

		/*!
		 * @brief Creates a upload account status response structure
		 *     from supplied JSON document.
		 *
		 * @param[in]  json JSON document.
		 * @param[out] ok Set to true on success.
		 * @return Invalid structure on error a valid structure else.
		 */
		static
		UploadAccountStatusResp fromJson(const QByteArray &json,
		    bool *ok = Q_NULLPTR);

		/*!
		 * @brief Converts upload account status response structure
		 *     into a JSON document.
		 *
		 * @note Unspecified values are stores as null into the JSON document.
		 *
		 * @return JSON document containing stored data.
		 */
		QByteArray toJson(void) const;

	private:
		ErrorEntry m_error; /*!< Error entry. */
	};

}
