/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QHash> /* ::qHash */

#include "src/datovka_shared/identifiers/account_id.h"
#include "src/datovka_shared/identifiers/account_id_p.h"

/* Null objects - for convenience. */
static const QString nullString;

void AcntId::declareTypes(void)
{
	qRegisterMetaType<AcntId>("AcntId");
}

AcntId::AcntId(void)
    : d_ptr(Q_NULLPTR)
{
}

AcntId::AcntId(const AcntId &other)
    : d_ptr((other.d_func() != Q_NULLPTR) ? (new (::std::nothrow) AcntIdPrivate) : Q_NULLPTR)
{
	Q_D(AcntId);
	if (d == Q_NULLPTR) {
		return;
	}

	*d = *other.d_func();
}

#ifdef Q_COMPILER_RVALUE_REFS
AcntId::AcntId(AcntId &&other) Q_DECL_NOEXCEPT
#if (QT_VERSION >= QT_VERSION_CHECK(5, 12, 0))
    : d_ptr(other.d_ptr.release()) //d_ptr(::std::move(other.d_ptr))
#else /* < Qt-5.12 */
    : d_ptr(other.d_ptr.take())
#endif /* >= Qt-5.12 */
{
}
#endif /* Q_COMPILER_RVALUE_REFS */

AcntId::~AcntId(void)
{
}

AcntId::AcntId(const QString &username, bool testing)
    : d_ptr(new (::std::nothrow) AcntIdPrivate)
{
	Q_D(AcntId);
	d->m_username = username;
	d->m_testing = testing;
	d->updateStrId();
}

#ifdef Q_COMPILER_RVALUE_REFS
AcntId::AcntId(QString &&username, bool testing)
    : d_ptr(new (::std::nothrow) AcntIdPrivate)
{
	Q_D(AcntId);
	d->m_username = ::std::move(username);
	d->m_testing = testing;
	d->updateStrId();
}
#endif /* Q_COMPILER_RVALUE_REFS */

/*!
 * @brief Ensures private account identifier presence.
 *
 * @note Returns if private account identifier could not be allocated.
 */
#define ensureAcntIdPrivate(_x_) \
	do { \
		if (Q_UNLIKELY(!ensurePrivate())) { \
			return _x_; \
		} \
	} while (0)

AcntId &AcntId::operator=(const AcntId &other) Q_DECL_NOTHROW
{
	if (other.d_func() == Q_NULLPTR) {
		d_ptr.reset(Q_NULLPTR);
		return *this;
	}
	ensureAcntIdPrivate(*this);
	Q_D(AcntId);

	*d = *other.d_func();

	return *this;
}

#ifdef Q_COMPILER_RVALUE_REFS
AcntId &AcntId::operator=(AcntId &&other) Q_DECL_NOTHROW
{
	swap(*this, other);
	return *this;
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool AcntId::operator==(const AcntId &other) const
{
	Q_D(const AcntId);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return true;
	} else if ((d == Q_NULLPTR) || ((other.d_func() == Q_NULLPTR))) {
		return false;
	}

	return *d == *other.d_func();
}

bool AcntId::operator!=(const AcntId &other) const
{
	return !operator==(other);
}

bool AcntId::operator<(const AcntId &other) const
{
	Q_D(const AcntId);
	if ((d == Q_NULLPTR) && ((other.d_func() == Q_NULLPTR))) {
		return false;
	} else if (d == Q_NULLPTR) {
		return true;
	} else if (other.d_func() == Q_NULLPTR) {
		return false;
	}

	return *d < *other.d_func();
}

bool AcntId::isNull(void) const
{
	Q_D(const AcntId);
	return d == Q_NULLPTR;
}

void AcntId::clear(void)
{
	d_ptr.reset(Q_NULLPTR);
}

bool AcntId::isValid(void) const
{
	return !isNull() && !username().isEmpty();
}

const QString &AcntId::username(void) const
{
	Q_D(const AcntId);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->m_username;
}

void AcntId::setUsername(const QString &n)
{
	ensureAcntIdPrivate();
	Q_D(AcntId);
	d->m_username = n;
	d->updateStrId();
}

#ifdef Q_COMPILER_RVALUE_REFS
void AcntId::setUsername(QString &&n)
{
	ensureAcntIdPrivate();
	Q_D(AcntId);
	d->m_username = ::std::move(n);
	d->updateStrId();
}
#endif /* Q_COMPILER_RVALUE_REFS */

bool AcntId::testing(void) const
{
	Q_D(const AcntId);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return false;
	}

	return d->m_testing;
}

void AcntId::setTesting(bool t)
{
	ensureAcntIdPrivate();
	Q_D(AcntId);
	d->m_testing = t;
	d->updateStrId();
}

const QString &AcntId::strId(void) const
{
	Q_D(const AcntId);
	if (Q_UNLIKELY(d == Q_NULLPTR)) {
		return nullString;
	}

	return d->_m_strId;
}

AcntId::AcntId(AcntIdPrivate *d)
    : d_ptr(d)
{
}

bool AcntId::ensurePrivate(void)
{
	if (Q_UNLIKELY(d_ptr == Q_NULLPTR)) {
		AcntIdPrivate *p = new (::std::nothrow) AcntIdPrivate;
		if (Q_UNLIKELY(p == Q_NULLPTR)) {
			Q_ASSERT(0);
			return false;
		}
		d_ptr.reset(p);
	}
	return true;
}

void swap(AcntId &first, AcntId &second) Q_DECL_NOTHROW
{
	using ::std::swap;
	swap(first.d_ptr, second.d_ptr);
}

#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
size_t qHash(const AcntId &key, size_t seed)
#else /* < Qt-6.0 */
uint qHash(const AcntId &key, uint seed)
#endif /* >= Qt-6.0 */
{
	return ::qHash(key.strId(), seed);
}
