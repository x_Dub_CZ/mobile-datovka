/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QFile>
#include <QList>
#include <QSysInfo>

#include "src/auxiliaries/email_helper.h"
#include "src/datovka_shared/log/log.h"
#include "src/datovka_shared/log/memory_log.h"
#include "src/dialogues/dialogues.h"
#include "src/io/filesystem.h"
#include "src/log.h"

Log::Log(MemoryLog *memLog, QObject *parent)
    : QObject(parent),
    m_memLog(memLog)
{
	if (m_memLog != Q_NULLPTR) {
		connect(m_memLog, SIGNAL(logged(quint64)),
		    this, SLOT(appendNewLog(quint64)));
	}
}

Log::~Log(void)
{
	if (m_memLog != Q_NULLPTR) {
		m_memLog->disconnect(SIGNAL(logged(quint64)),
		    this, SLOT(appendNewLog(quint64)));
	}
}

QString Log::getLogFileLocation(void)
{
	return appLogDirPath();
}

QString Log::loadLogContent(const QString &filePath)
{
	debugFuncCall();

	QString log;

	if (filePath.isEmpty() && (m_memLog != Q_NULLPTR)) {
		const QList<quint64> keys(m_memLog->keys());
		foreach (quint64 key, keys) {
			log.append(m_memLog->message(key));
		}
	} else {
		QFile file(filePath);
		if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
			Q_ASSERT(0);
			logErrorNL("Cannot open file '%s'.",
			    filePath.toUtf8().constData());
			return QString();
		}
		log.append(file.readAll());
		file.close();
	}

	return log;
}

void Log::sendLogViaEmail(const QString &logContent)
{
	debugFuncCall();

	int msgResponse = Dialogues::message(Dialogues::QUESTION,
	    tr("Send log via email"),
	    tr("Do you want to send the log information to developers?"),
	    QString(), Dialogues::NO | Dialogues::YES, Dialogues::NO);
	if (msgResponse == Dialogues::NO) {
		return;
	}

	QString to("datove-schranky@labs.nic.cz");
	QString subject("Mobile Datovka: Log file");
	QString body("Mobile Datovka");
	body.append("\nVersion: " + QStringLiteral(VERSION));
	body.append("\nOS: " + QSysInfo::productType() + " "
	    + QSysInfo::productVersion());
	body.append("\nArch: " + QSysInfo::currentCpuArchitecture());

	removeDirFromDocLoc(DATOVKA_MAIL_DIR_NAME);

	QString targetPath(appEmailDirPath("Log"));

#if defined (Q_OS_ANDROID)
	targetPath = getAndroidFileProviderBasePath();
#endif

	QString filePath(writeFile(targetPath, "mobile-datovka.log",
	    logContent.toUtf8()));
	QStringList attachmentList;
	attachmentList.append(filePath);

	const QString boundary = generateBoundaryString();
	QString emailMessage = createEmailMessage(to, body, subject,  boundary);
	addAttachmentToEmailMessage(emailMessage, "mobile-datovka.log",
	    logContent.toUtf8().toBase64(), boundary);
	finishEmailMessage(emailMessage, boundary);
	sendEmail(emailMessage, attachmentList, to, subject, body, "Log");
}

void Log::appendNewLog(quint64 key)
{
	emit appendNewLogMessage(m_memLog->message(key));
}
