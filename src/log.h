/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>
#include <QString>

class MemoryLog; /* Forward declaration. */

/*
 * Class Log provides interface between QML and log device.
 * Class is initialised in the main function (main.cpp)
 */
class Log : public QObject {
    Q_OBJECT

public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] memLog Memory log.
	 * @param[in] parent Parent object.
	 */
	explicit Log(MemoryLog *memLog, QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Destructor.
	 */
	~Log(void);

	/*!
	 * @brief Get log file location.
	 *
	 * @return Full path to log directory.
	 */
	Q_INVOKABLE static
	QString getLogFileLocation(void);

	/*!
	 * @brief Load log content from file or memory into QML.
	 *
	 * @param[in] filePath Log file path (empty = use memory log).
	 * @return Log content as text.
	 */
	Q_INVOKABLE
	QString loadLogContent(const QString &filePath);

	/*!
	 * @brief Send log via email.
	 *
	 * @param[in] logContent Log content to be sent.
	 */
	Q_INVOKABLE static
	void sendLogViaEmail(const QString &logContent);

signals:
	/*!
	 * @brief Append new log message to QML log bar.
	 *
	 * @param[in] newLog New log message.
	 */
	void appendNewLogMessage(QString newLog);

private slots:
	/*!
	 * @brief Appends newly logged message to QML log bar.
	 *
	 * @param[in] key Log message key.
	 */
	void appendNewLog(quint64 key);

private:
	MemoryLog *m_memLog; /*!< Pointer to memory log. */
};
