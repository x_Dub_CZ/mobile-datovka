/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>
#include <QTimer>

/*!
 * @brief Application locker class.
 */
class Locker : public QObject {
	Q_OBJECT
public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] parent Parent object.
	 */
	explicit Locker(QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Sets internal flag. Following immediate application
	 *     suspension call will be ignored.
	 *
	 * @note This is useful when the control is handed over to another
	 *     application (e.g. when opening an attachment in associated
	 *     application).
	 */
	Q_INVOKABLE
	void ignoreNextSuspension(void);

	/*!
	 * @brief Set inactivity locking interval in seconds.
	 *
	 * @param[in] secs Interval length in seconds.
	 */
	Q_INVOKABLE
	void setInactivityInterval(int secs);

public slots:
	/*!
	 * @brief Processes supplied application state. Calls
	 *     inactivityTimeOut() if suspension state achieved which should
	 *     not be ignored.
	 */
	void processNewState(Qt::ApplicationState state);

private slots:
	/*!
	 * @brief Emits lockApp signal if PIN is set.
	 */
	void inactivityTimeOut(void);

signals:
	/*!
	 * @brief Signal emitted when the application should be locked.
	 */
	void lockApp(void);

protected:
	/*!
	 * @brief Watches for MouseMove, KeyPress and TouchBegin events. Resets
	 *     inactivity timer on those events.
	 *
	 * @param[in] watched Watched object.
	 * @param[in] event Incoming event.
	 * @return Returns whatever the parent class would return.
	 */
	virtual
	bool eventFilter(QObject *watched, QEvent *event) Q_DECL_OVERRIDE;

private:
	int m_inactivityInterval; /*!<
	                           * Interval length in seconds, value less or
	                           * equal to 0 is off.
	                           */
	QTimer m_inactivityTimer; /*!< Inactivity timer. */
	bool m_ignoreImmediateSuspension; /*!<
	                                   * Set true if next suspension should
	                                   * be ignored.
	                                   */
};
