/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include "src/global.h"
#include "src/settings/account.h"
#include "src/settings/accounts.h"
#include "src/sqlite/zfo_db.h"
#include "src/zfo.h"

Zfo::Zfo(QObject *parent) : QObject(parent)
{
}

QByteArray Zfo::getZfoContentFromDb(const QmlAcntId *qAcntId, qint64 msgId)
{
	if ((GlobInstcs::zfoDbPtr != Q_NULLPTR) &&
	        (GlobInstcs::acntMapPtr != Q_NULLPTR) &&
	        (qAcntId != Q_NULLPTR)) {
		return QByteArray::fromBase64(
		    GlobInstcs::zfoDbPtr->getZfoContentFromDb(msgId,
		    qAcntId->testing()));
	} else {
		Q_ASSERT(0);
		return QByteArray();
	}
}

int Zfo::getZfoSizeFromDb(const QmlAcntId *qAcntId, qint64 msgId)
{
	if ((GlobInstcs::zfoDbPtr != Q_NULLPTR) &&
	        (GlobInstcs::acntMapPtr != Q_NULLPTR) &&
	        (qAcntId != Q_NULLPTR)) {
		return GlobInstcs::zfoDbPtr->getZfoSizeFromDb(msgId,
		    qAcntId->testing());
	} else {
		Q_ASSERT(0);
		return -1;
	}
}

void Zfo::reduceZfoDbSize(unsigned int releaseSpaceInMB)
{
	if (Q_UNLIKELY(GlobInstcs::zfoDbPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	// convert MBs from QML to bytes for database resize operation
	GlobInstcs::zfoDbPtr->releaseDb(releaseSpaceInMB * 1000000, true);
}
