/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QDateTime>
#include <QDir>
#include <QDirIterator>
#include <QFileInfoList>
#include <QRegularExpression>
#include <QStandardPaths>
#include <QTextStream>

#include "src/datovka_shared/log/log.h"
#include "src/io/filesystem.h"

/*!
 * @brief Illegal characters in file names without directory separators.
 */
#define ILL_FNAME_CH_NO_SEP ":*?\"<>|"
#define ESCAPED_ILL_FNAME_CH_NO_SEP ":\\*\\?\"<>\\|"
/*!
 * @brief Illegal characters in file name with directory separators.
 */
#define ILL_FNAME_CH "\\/" ILL_FNAME_CH_NO_SEP
#define ESCAPED_ILL_FNAME_CH "\\\\/" ESCAPED_ILL_FNAME_CH_NO_SEP
/*!
 * @brief Replacement character for illegal characters.
 */
#define ILL_FNAME_REP "_"

QString joinDirs(const QString &dirName1, const QString &dirName2)
{
	return dirName1 + QDir::separator() + dirName2;
}

QString appSendDirPath(void)
{
	return existingAppPath(dfltAttachSavingLoc(), DATOVKA_SEND_DIR_NAME);
}

QString appBackupDirPath(void)
{
	return existingAppPath(dfltAttachSavingLoc(), DATOVKA_BACK_DIR_NAME);
}

QString appTransferDirPath(void)
{
	return existingAppPath(dfltAttachSavingLoc(), DATOVKA_TRAN_DIR_NAME);
}

QString appRestoreDirPath(void)
{
	return existingAppPath(dfltAttachSavingLoc(), DATOVKA_REST_DIR_NAME);
}

QString appCertDirPath(void)
{
	return existingAppPath(dfltAttachSavingLoc(), DATOVKA_CERT_DIR_NAME);
}

QString appEmailDirPath(const QString &msgIdStr)
{
	return existingAppPath(dfltAttachSavingLoc(),
	    QLatin1String(DATOVKA_MAIL_DIR_NAME) + QDir::separator() + msgIdStr);
}

QString appLogDirPath(void)
{
	return existingAppPath(dfltAttachSavingLoc(), DATOVKA_LOGS_DIR_NAME);
}

QString appMsgAttachDirPath(const QString &msgIdStr)
{
	return existingAppPath(dfltAttachSavingLoc(), msgIdStr);
}

QString appMsgAttachDirPathiOS(const QString &msgIdStr)
{
	return existingAppPath(dfltAttachSavingLoc(),
	    QStringLiteral(DATOVKA_TEMP_DIR_NAME) + QDir::separator() + msgIdStr);
}

QString appTmpDirPath(void)
{
	return existingAppPath(dfltAttachSavingLoc(), DATOVKA_TEMP_DIR_NAME);
}

void removeDirFromDocLoc(const QString &dirName)
{
	QString folderPath(
	     existingAppPath(dfltAttachSavingLoc(), dirName));

	if (folderPath.isEmpty()) {
		return;
	}

	QDir dir(folderPath);
	dir.removeRecursively();
}

QString existingWritableLoc(int type)
{
	enum QStandardPaths::StandardLocation locationType;

	switch (type) {
	case QStandardPaths::AppDataLocation:
	case QStandardPaths::AppLocalDataLocation:
	case QStandardPaths::DocumentsLocation:
		locationType = (enum QStandardPaths::StandardLocation) type;
		break;
	default: /* All other types are not used. */
		Q_ASSERT(0);
		return QString();
		break;
	}

	QString dirName(QStandardPaths::writableLocation(locationType));
	/* Return if no such path exists. */
	if (dirName.isEmpty()) {
		return QString();
	}

	/* Create the path if it does not exist. */
	QDir dir(dirName);
	if (!dir.exists() && !dir.mkpath(".")) {
		return QString();
	}
	Q_ASSERT(dir.exists());

	return dirName;
}

QString dfltAttachSavingLoc(void)
{
	return existingWritableLoc(QStandardPaths::DocumentsLocation);
}

QString dfltDbAndConfigLoc(void)
{
	return existingWritableLoc(QStandardPaths::AppDataLocation);
}

QStringList zfoFilesFromDir(const QString &dirPath)
{
	QStringList filters("*.zfo");
	QStringList filePathList;

	QDirIterator it(dirPath, filters, QDir::Files,
	    QDirIterator::Subdirectories);
	while (it.hasNext()) {
		filePathList.append(it.next());
	}

	return filePathList;
}

QString nonconflictingFileName(QString filePath)
{
	if (filePath.isEmpty()) {
		return QString();
	}

	if (QFile::exists(filePath)) {

		int fileCnt = 0;
		QFileInfo fi(filePath);

		const QString baseName(fi.baseName());
		const QString path(fi.path());
		const QString suffix(fi.completeSuffix());

		do {
			++fileCnt;
			QString newName(
			    baseName + "_" + QString::number(fileCnt));
			filePath =
			    path + QDir::separator() + newName + "." + suffix;
		} while (QFile::exists(filePath));
	}

	return filePath;
}

QString existingAppPath(const QString &basePath, const QString &dirName)
{
	QString newPath = basePath + QDir::separator() +
	    QLatin1String(DATOVKA_BASE_DIR_NAME) + QDir::separator() + dirName;

	QDir dir(newPath);
	if (!dir.exists() && !dir.mkpath(".")) {
		logErrorNL("Location '%s' could not be created.",
		    newPath.toUtf8().constData());
		return QString();
	}
	Q_ASSERT(dir.exists());

	return newPath;
}

QString writeFile(const QString &filePath, const QString &fileName,
    const QByteArray &data, bool deleteOnError)
{
	QString nameCopy(fileName);
	nameCopy.replace(QRegularExpression("[" ESCAPED_ILL_FNAME_CH "]"),
	    ILL_FNAME_REP);

	QFile fout(nonconflictingFileName(filePath + QDir::separator()
	    + nameCopy));
	if (!fout.open(QIODevice::WriteOnly)) {
		logErrorNL("Cannot open file '%s'.",
		    nameCopy.toUtf8().constData());
		return QString();
	}

	/* Get whole path. */
	QString fullName = fout.fileName();
	int written = fout.write(data);
	bool flushed = fout.flush();
	fout.close();

	if ((written != data.size()) || !flushed) {
		if (deleteOnError) {
			QFile::remove(fullName);
		}
		return QString();
	}

	return fullName;
}

/*!
 * @brief Obtain log file date and time from file name.
 *
 * @param[in] fi File info.
 * @return Valid time information if such could be obtained.
 */
static
QDateTime logDateTime(const QFileInfo &fi)
{
	QDateTime curentDateTime;
	const QStringList pList = fi.baseName().split("_");

	if (pList.count() == 2) {
		curentDateTime = QDateTime::fromString(pList[1],
		    "yyyyMMdd-hhmmss");
	}

	return curentDateTime;
}

void deleteOldestLogFile(void)
{
	debugFuncCall();

	QFileInfoList logFiles(QDir(appLogDirPath()).entryInfoList(
	    QStringList() << "*.log", QDir::Files, QDir::Name));

	if (logFiles.count() <= DATOVKA_MAX_LOG_FILES) {
		return;
	}

	/* Exact match. */
	const QRegularExpression rx("mdatovka_[0-9]{8}-[0-9]{6}.log");
	QString oldestLogFile;
	QDateTime oldestDt;

	foreach (const QFileInfo &fileInfo, logFiles) {
		const QString fileName = fileInfo.fileName();
		if ((fileName.length() > 0) &&
		    (rx.match(fileName).capturedLength() == fileName.length())) {
			QDateTime logDt = logDateTime(fileInfo);
			if (oldestDt.isValid()) {
				if (logDt.isValid() && (logDt < oldestDt)) {
					oldestLogFile = fileInfo.filePath();
					oldestDt = logDt;
				}
			} else {
				oldestLogFile = fileInfo.filePath();
				oldestDt = logDt;
			}
		} else {
			QFile logFile(fileInfo.filePath());
			if (logFile.remove()) {
				logInfoNL("Deleted old version of log file: '%s'.",
				    fileInfo.filePath().toUtf8().constData());
			} else {
				logErrorNL("Couldn't delete file '%s'.",
				    fileInfo.filePath().toUtf8().constData());
			}
		}
	}

	if (oldestLogFile.isEmpty()) {
		return;
	}

	QFile logFile(oldestLogFile);
	if (logFile.remove()) {
		logInfoNL("Deleted oldest log file: '%s'.",
		    oldestLogFile.toUtf8().constData());
	} else {
		logErrorNL("Couldn't delete file '%s'.",
		    oldestLogFile.toUtf8().constData());
	}
}

QString constructLogFileName(void) {

	QString logFileName("mdatovka_"
	    + QDateTime::currentDateTime().toString("yyyyMMdd-hhmmss")
	    + ".log");
	return appLogDirPath() + QDir::separator() + logFileName;
}

QString getAndroidFileProviderBasePath(void) {

	QString basePath = QStandardPaths::standardLocations(
	    QStandardPaths::AppDataLocation).value(0);

	QString fileProviderDir = basePath.append("/share_tmp_files");

	QDir dir(fileProviderDir);
	dir.removeRecursively();

	if (!QDir(fileProviderDir).exists()) {
		if (!QDir("").mkpath(fileProviderDir)) {
			logErrorNL("Failed to create share directory: '%s'.",
			    fileProviderDir.toUtf8().constData());
			return QString();
		}
	}

	return basePath;
}
