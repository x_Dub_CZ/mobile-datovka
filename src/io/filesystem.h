/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QByteArray>
#include <QString>

/*!
 * @brief Datovka file store directory names.
 */
#define DATOVKA_BASE_DIR_NAME "Datovka"
#define DATOVKA_BACK_DIR_NAME "Backup"
#define DATOVKA_TRAN_DIR_NAME "Transfer"
#define DATOVKA_SEND_DIR_NAME "Send"
#define DATOVKA_CERT_DIR_NAME "Cert"
#define DATOVKA_LOGS_DIR_NAME "Logs"
#define DATOVKA_MAIL_DIR_NAME "Email"
#define DATOVKA_REST_DIR_NAME "Restore"
#define DATOVKA_TEMP_DIR_NAME "Temp"

/*!
 * @brief PDF file name of text message for sending.
 */
#define DATOVKA_TEXT_MSG_FILE_NAME "textova-zprava.pdf"

/*!
 * @brief Max number of log files.
 */
#define DATOVKA_MAX_LOG_FILES 5

/*!
 * @brief Join two directoris to path.
 * @param[in] dirName1 First directory name.
 * @param[in] dirName2 Second irectory name.
 * @return Directory path.
 */
QString joinDirs(const QString &dirName1, const QString &dirName2);

/*!
 * @brief Return path to location where the application stores send files.
 *
 * @return Full path.
 */
QString appSendDirPath(void);

/*!
 * @brief Return path to location where the application stores
 *        backup JSON for restoring.
 *
 * @return Full path.
 */
QString appRestoreDirPath(void);

/*!
 * @brief Return path to location where the application stores
 *        backup data.
 *
 * @return Full path.
 */
QString appBackupDirPath(void);

/*!
 * @brief Return path to location where the application stores
 *        transfer data.
 *
 * @return Full path.
 */
QString appTransferDirPath(void);

/*!
 * @brief Return path to location where the application stores certificates.
 *
 * @return Full path.
 */
QString appCertDirPath(void);

/*!
 * @brief Return path to location where email content (attachments) can be
 *     stored.
 *
 * @param[in] msgIdStr String containing message identifier.
 * @return Full path containing the supplied identifier.
 */
QString appEmailDirPath(const QString &msgIdStr);

/*!
 * @brief Return path to location where the application stores log files.
 *
 * @return Full path.
 */
QString appLogDirPath(void);

/*!
 * @brief Return path to location where attachments of a particular message
 *     can be stored.
 *
 * @param[in] msgIdStr String containing message identifier.
 * @return Full path containing the supplied identifier.
 */
QString appMsgAttachDirPath(const QString &msgIdStr);

/*!
 * @brief Return path to location where attachments of a particular message
 *     can be stored for iOS.
 *
 * @param[in] msgIdStr String containing message identifier.
 * @return Full path containing the supplied identifier.
 */
QString appMsgAttachDirPathiOS(const QString &msgIdStr);

/*!
 * @brief Return path to location where temporary files can be stored.
 *
 * @return Full path to temporary folder.
 */
QString appTmpDirPath(void);

/*!
 * @brief Remove directory from application document storage including its
 *     content.
 *
 * @param[in] dirName Directory name.
 */
void removeDirFromDocLoc(const QString &dirName);

/*!
 * @brief Returns name of existing directory to store data to.
 *
 * @param[in] type Location type, must match supported QStandardPaths::StandardLocation.
 * @return Directory name of existing directory or empty string on error.
 */
QString existingWritableLoc(int type);

/*!
 * @brief Return path for attachment saving.
 *
 * @return Path to location where attachment can be stored.
 */
QString dfltAttachSavingLoc(void);

/*!
 * @brief Return default location for database files and datovka.conf.
 *
 * @return Location path or empty string on error.
 */
QString dfltDbAndConfigLoc(void);

/*!
 * @brief Get full paths of ZFO files in supplied directory.
 *
 * @note Does Not follow symbolic links.
 *
 * @param[in] dirPath Directory path.
 * @return List of full paths of found zfo files.
 */
QStringList zfoFilesFromDir(const QString &dirPath);

/*!
 * @brief Check if file with given path exists.
 *
 * @param[in] filePath Full file path.
 * @return Full path with non-conflicting file if file already exists.
 */
QString nonconflictingFileName(QString filePath);

/*!
 * @brief Return path to directory in supplied base path. Create directory if
 *     it does not exist.
 *
 * @note If "Base" and "Path" are supplied, then the resulting directory is
 *     going to be "Base/DATOVKA_BASE_DIR_NAME/Path".
 *
 * @param[in] basePath Base path.
 * @param[in] dirName Directory name.
 * @return Full path to directory in supplied base path if directory exists or
 *     it could be created. Empty sting otherwise.
 */
QString existingAppPath(const QString &basePath, const QString &dirName);

/*!
 * @brief Create file and write data to it.
 *
 * @param[in] filePath File path.
 * @param[in] fileName File name.
 * @param[in] data Data to be written into file.
 * @param[in] deleteOnError Delete created file when cannot be entirely written.
 * @return Full path to written file on success, empty string on failure.
 */
QString writeFile(const QString &filePath, const QString &fileName,
    const QByteArray &data, bool deleteOnError = false);

/*!
 * @brief Delete oldest log file from local storage.
 */
void deleteOldestLogFile(void);

/*!
 * @brief Construct log path and file file.
 *
 * @return Full path including log file name.
 */
QString constructLogFileName(void);

/*!
 * @brief Return Android file provider base path.
 *
 * @return Android file provider base path.
 */
QString getAndroidFileProviderBasePath(void);
