/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/* Free() and set to NULL pointed memory */
#define zfree(memory) { \
    free(memory); \
    (memory) = NULL; \
}

/* Extract data from CMS (successor of PKCS#7)
 * The CMS' signature is is not verified.
 * @cms is input block with CMS structure
 * @cms_length is @cms block length in bytes
 * @data are automatically reallocated bit stream with data found in @cms.
 * You must free them.
 * @data_length is length of @data in bytes
 */
int extract_cms_data(const void *cms, const size_t cms_length,
    void **data, size_t *data_length);

#ifdef __cplusplus
} /* extern "C" */
#endif
