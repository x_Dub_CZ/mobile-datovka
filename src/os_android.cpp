/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QAndroidJniObject>
#include <QtAndroidExtras>

#include "src/os_android.h"

IntentNotification::IntentNotification(InteractionZfoFile &inter,
    QObject *parent)
    : QObject(parent),
    m_inter(inter)
{
}

QStringList IntentNotification::getIntentArguments(void)
{
	/*
	 * http://stackoverflow.com/questions/27771311/how-to-get-application-arguments-on-qt-for-android
	 */

	QAndroidJniObject activity(QtAndroid::androidActivity());
	if (!activity.isValid()) {
		return QStringList();
	}
	QAndroidJniObject intent(activity.callObjectMethod("getIntent",
	    "()Landroid/content/Intent;"));
	if (!intent.isValid()) {
		return QStringList();
	}
	QAndroidJniObject data(intent.callObjectMethod("getData",
	    "()Landroid/net/Uri;"));
	if (!data.isValid()) {
		return QStringList();
	}
	QAndroidJniObject path(data.callObjectMethod("getPath",
	    "()Ljava/lang/String;"));
	if (!path.isValid()) {
		return QStringList();
	}

	/*
	 * TODO -- There is a problem on Android. Opening intent is repeated if
	 * the application is resumed from background. Clearing the intent
	 * will block any later intent and the application must be restarted to
	 * receive any file opening intents.
	 */

#if 0
	jint intentFlags = intent.callMethod<jint>("getFlags", "()I");
	jint flag = QAndroidJniObject::getStaticField<jint>("android/content/Intent",
	    "FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY");
#endif

#if 0
	intent.callObjectMethod("setData",
	    "(Landroid/net/Uri;)Landroid/content/Intent;",
	    QAndroidJniObject().object<jobject>());
#endif

#if 0
	QAndroidJniObject builder("android/net/Uri/Builder");
	builder.callObjectMethod("scheme",
	    "(java/lang/String)Landroid/net/Uri/Builder;",
	    QAndroidJniObject::fromString("https").object<jstring>());
	QAndroidJniObject uri(builder.callObjectMethod("build", "()Landroid/net/Uri;"));
	activity.callObjectMethod("finish", "()V");
#endif

	return QStringList(path.toString());
}

void IntentNotification::scanIntentsForFiles(Qt::ApplicationState newState)
{
	/* Run only when application is newly activated. */
	if (newState != Qt::ApplicationActive) {
		return;
	}

	QStringList fileList(getIntentArguments());

	if (fileList.isEmpty()) {
		return;
	}

	foreach (const QString &filePath, fileList) {
		m_inter.openZfoFile(filePath);
	}
}

bool IntentNotification::checkAndAskStoragePermissions(void)
{
	auto result = QtAndroid::checkPermission(
	    QString("android.permission.WRITE_EXTERNAL_STORAGE"));
	if (result == QtAndroid::PermissionResult::Denied) {
		QtAndroid::PermissionResultMap resultHash =
		    QtAndroid::requestPermissionsSync(
		    QStringList({"android.permission.WRITE_EXTERNAL_STORAGE"}));
		return !(resultHash["android.permission.WRITE_EXTERNAL_STORAGE"]
		    == QtAndroid::PermissionResult::Denied);
	}

	return true;
}
