/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QFile>
#include <QPair>
#include <QPushButton>
#include <QQmlEngine> /* qmlRegisterType */
#include <QStorageInfo>

#include "src/dialogues/dialogues.h"
#include "src/global.h"
#include "src/datovka_shared/isds/type_conversion.h"
#include "src/datovka_shared/log/log.h"
#include "src/messages.h"
#include "src/models/accountmodel.h"
#include "src/models/databoxmodel.h"
#include "src/models/messagemodel.h"
#include "src/setwrapper.h"
#include "src/settings/account.h"
#include "src/settings/accounts.h"
#include "src/settings/prefs_specific.h"
#include "src/sqlite/dbs.h"
#include "src/sqlite/message_db_container.h"
#include "src/sqlite/file_db_container.h"

/*!
 * @brief Translate message type enumeration type into database representation.
 */
static
enum MessageDb::MessageType enumToDbRepr(enum Messages::MessageType msgType)
{
	switch (msgType) {
	case Messages::TYPE_RECEIVED:
		return MessageDb::TYPE_RECEIVED;
		break;
	case Messages::TYPE_SENT:
		return MessageDb::TYPE_SENT;
		break;
	default:
		/*
		 * Any other translates to sent, but forces the application to
		 * crash in debugging mode.
		 */
		Q_ASSERT(0);
		return MessageDb::TYPE_SENT;
		break;
	}
}

void Messages::declareQML(void)
{
	qmlRegisterUncreatableType<Messages>("cz.nic.mobileDatovka.messages", 1, 0, "MessageType", "Access to enums & flags only.");
	qRegisterMetaType<Messages::MessageType>("Messages::MessageType");
	qRegisterMetaType<Messages::MessageTypes>("Messages::MessageTypes");
}

Messages::Messages(QObject *parent) : QObject(parent)
{
}

void Messages::fillContactList(DataboxListModel *databoxModel,
    const QmlAcntId *qAcntId, const QString &dbId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(databoxModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access data box model.");
		Q_ASSERT(0);
		return;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(),
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[*qAcntId]));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return;
	}

	databoxModel->clearAll();
	msgDb->getContactsFromDb(databoxModel, dbId);
}

void Messages::fillMessageList(MessageListModel *messageModel,
    const QmlAcntId *qAcntId, enum MessageType msgType)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(messageModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message model.");
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(),
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[*qAcntId]));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return;
	}

	messageModel->clearAll();
	/* Translate into database enum type. */
	msgDb->setMessageListModelFromDb(messageModel, *qAcntId,
	    enumToDbRepr(msgType));
}

QString Messages::getMessageDetail(const QmlAcntId *qAcntId,
    const QString &msgIdStr)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return QString();
	}

	bool ok = false;
	qint64 msgId = msgIdStr.toLongLong(&ok);
	if (Q_UNLIKELY(!ok || (msgId < 0))) {
		return QString();
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(),
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[*qAcntId]));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return QString();
	}
	return msgDb->getMessageDetailHtmlFromDb(msgId);
}

QmlIsdsEnvelope *Messages::getMsgEnvelopeDataAndSetRecipient(
    const QmlAcntId *qAcntId, qint64 msgId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return Q_NULLPTR;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(),
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[*qAcntId]));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return Q_NULLPTR;
	}

	Isds::Envelope envelope = msgDb->getMessageEnvelopeFromDb(msgId);

	// Return message envelope data into QML
	return new (::std::nothrow) QmlIsdsEnvelope(envelope);
}

void Messages::markMessageAsLocallyRead(MessageListModel *messageModel,
    const QmlAcntId *qAcntId, qint64 msgId, bool isRead)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(),
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[*qAcntId]));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return;
	}
	msgDb->markMessageLocallyRead(msgId, isRead);

	if (Q_UNLIKELY(messageModel == Q_NULLPTR)) {
		logWarningNL("%s", "Cannot access message model.");
		return;
	}
	messageModel->overrideRead(msgId, isRead);
}

void Messages::markMessagesAsLocallyRead(MessageListModel *messageModel,
    const QmlAcntId *qAcntId, enum MessageType msgType, bool isRead)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(),
	    PrefsSpecific::dataOnDisk(
	        *GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[*qAcntId]));
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return;
	}
	msgDb->markMessagesLocallyRead(enumToDbRepr(msgType), isRead);

	if (Q_UNLIKELY(messageModel == Q_NULLPTR)) {
		logWarningNL("%s", "Cannot access message model.");
		return;
	}
	/* The current model should correspond with supplied type. */
	messageModel->overrideReadAll(isRead);
}

void Messages::overrideDownloaded(MessageListModel *messageModel,
    qint64 dmId, bool forceDownloaded)
{
	if (Q_UNLIKELY(messageModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message model.");
		Q_ASSERT(0);
		return;
	}

	messageModel->overrideDownloaded(dmId, forceDownloaded);
}

void Messages::updateRmStatus(MessageListModel *messageModel,
    const QmlAcntId *qAcntId, qint64 dmId, bool isUploadRm)
{
	debugFuncCall();

	if (Q_UNLIKELY(qAcntId == Q_NULLPTR)) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(messageModel == Q_NULLPTR)) {
		logWarningNL("%s", "Cannot access message model.");
		return;
	}
	messageModel->updateRmStatus(dmId, isUploadRm);
	emit updateMessageDetail(getMessageDetail(qAcntId, QString::number(dmId)));
}

void Messages::deleteMessageFromDbs(AccountListModel *accountModel,
    MessageListModel *messageModel, const QmlAcntId *qAcntId, qint64 msgId)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR) ||
	        (qAcntId == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	if (Q_UNLIKELY(!qAcntId->isValid())) {
		return;
	}

	int msgResponse = Dialogues::message(Dialogues::QUESTION,
	    tr("Delete message: %1").arg(msgId),
	    tr("Do you want to delete the message '%1'?").arg(msgId),
	    tr("Note: It will delete all attachments and message information from the local database."),
	    Dialogues::NO | Dialogues::YES, Dialogues::NO);
	if (msgResponse == Dialogues::NO) {
		return;
	}

	if (Q_UNLIKELY(!GlobInstcs::acntMapPtr->contains(*qAcntId))) {
		return;
	}

	const bool dataOnDisk = PrefsSpecific::dataOnDisk(*GlobInstcs::prefsPtr,
	    (*GlobInstcs::acntMapPtr)[*qAcntId]);

	MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
	    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(), dataOnDisk);
	if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message database.");
		return;
	}

	FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
	    GlobalSettingsQmlWrapper::dbPath(), qAcntId->username(), dataOnDisk);
	if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
		logErrorNL("Cannot access file database for %s.",
		    qAcntId->username().toUtf8().constData());
		return;
	}

	if (Q_UNLIKELY(accountModel == Q_NULLPTR)) {
		logWarningNL("%s", "Cannot access account model.");
	}

	if (Q_UNLIKELY(messageModel == Q_NULLPTR)) {
		logWarningNL("%s", "Cannot access message model.");
	}

	if (fDb->deleteFilesFromDb(msgId)) {
		if (msgDb->deleteMessageFromDb(msgId, true)) {
			/* Remove row from model, don't regenerate data. */
			if (accountModel != Q_NULLPTR) {
				accountModel->updateCounters(*qAcntId,
				    msgDb->getNewMessageCountFromDb(MessageDb::TYPE_RECEIVED),
				    msgDb->getMessageCountFromDb(MessageDb::TYPE_RECEIVED),
				    msgDb->getNewMessageCountFromDb(MessageDb::TYPE_SENT),
				    msgDb->getMessageCountFromDb(MessageDb::TYPE_SENT));
			}
			if (messageModel != Q_NULLPTR) {
				messageModel->removeMessage(msgId);
			}
		}
	}
}

bool Messages::moveOrCreateNewDbsToNewLocation(const QString &newLocation)
{
	debugFuncCall();

	int allDbSize = 0;

	/* Select action what to do with the currently used databases. */
	enum ReloactionAction action = askAction();
	switch (action) {
	case RA_NONE:
		/* Do nothing and exit. */
		return false;
		break;
	case RA_RELOCATE:
		allDbSize = getAllDbSize();
		break;
	case RA_CREATE_NEW:
		allDbSize = estimateAllDbSize();
		break;
	default:
		/* Unsupported action. */
		Q_ASSERT(0);
		return false;
		break;
	}

	if (Q_UNLIKELY(allDbSize <= 0)) {
		/* Expecting a positive value. */
		return false;
	}

	/* Check whether new location has required size for database storing. */
	{
		QStorageInfo si(newLocation);
		if (si.bytesAvailable() < allDbSize) {
			Dialogues::errorMessage(Dialogues::CRITICAL,
			    tr("New location error"),
			    tr("It is not possible to store databases in the new location because there is not enough free space left."),
			    tr("Databases size is %1 MB.").arg(allDbSize / (1024 * 1024)));
			return false;
		}
	}

	return relocateDatabases(newLocation, action);
}

void Messages::deleteExpiredMessagesFromDbs(int days)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return;
	}

	QList<qint64> msgIDList;
	const QList<AcntId> acntIdList(GlobInstcs::acntMapPtr->keys());
	foreach (const AcntId &acntId, acntIdList) {
		const bool dataOnDisk = PrefsSpecific::dataOnDisk(
		    *GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[acntId]);

		MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
		    dataOnDisk);
		if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
			logErrorNL("Cannot access message database for %s.",
			    acntId.username().toUtf8().constData());
			return;
		}
		/*
		 * Messages should be deleted from the database with a delay -
		 * after they have been really deleted from the server.
		 */
		msgIDList = msgDb->getExpireMessageListFromDb(days + 1);

		if (msgIDList.isEmpty()) {
			continue;
		}

		FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
		    dataOnDisk);
		if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
			logErrorNL("Cannot access file database for %s.",
			    acntId.username().toUtf8().constData());
			return;
		}

		fDb->beginTransaction();
		msgDb->beginTransaction();
		foreach (qint64 msgId, msgIDList) {
			if (fDb->deleteFilesFromDb(msgId)) {
				msgDb->deleteMessageFromDb(msgId, false);
			}
		}
		fDb->commitTransaction();
		msgDb->commitTransaction();
	}
}

enum Messages::ReloactionAction Messages::askAction(void)
{
	QList< QPair<QString, int> > customButtons;
	customButtons.append(QPair<QString, int>(tr("Move"), RA_RELOCATE));
	customButtons.append(QPair<QString, int>(tr("Create new"), RA_CREATE_NEW));
	int customVal = -1;

	Dialogues::message(Dialogues::QUESTION,
	    tr("Change database location"),
	    tr("What do you want to do with the currently used database files?"),
	    tr("You have the option to move the current database files to a new location or you can delete them and create new empty databases."),
	    Dialogues::CANCEL, Dialogues::CANCEL,
	    customButtons, &customVal);

	if (customVal == RA_RELOCATE) {
		return RA_RELOCATE;
	} else if (customVal == RA_CREATE_NEW) {
		return RA_CREATE_NEW;
	} else {
		return RA_NONE;
	}
}

int Messages::getAllDbSize(void)
{
	int size = 0;

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return -1;
	}

	const QList<AcntId> acntIdList(GlobInstcs::acntMapPtr->keys());
	foreach (const AcntId &acntId, acntIdList) {
		const bool dataOnDisk = PrefsSpecific::dataOnDisk(
		    *GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[acntId]);

		MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(), dataOnDisk);
		if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
			logErrorNL(
			    "Cannot access message database for user name '%s'.",
			    acntId.username().toUtf8().constData());
			return -1;
		}
		size += msgDb->getDbSizeInBytes();

		FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(), dataOnDisk);
		if (Q_UNLIKELY(fDb == Q_NULLPTR)) {
			logErrorNL(
			    "Cannot access file database for user name '%s'.",
			    acntId.username().toUtf8().constData());
			return -1;
		}
		size += fDb->getDbSizeInBytes();
	}

	return size;
}

int Messages::estimateAllDbSize(void)
{
	if (Q_UNLIKELY(GlobInstcs::acntMapPtr == Q_NULLPTR)) {
		Q_ASSERT(0);
		return -1;
	}

	/* 1 MB free disk space is required for new database */
	return 2 * GlobInstcs::acntMapPtr->size() * 1000000;
}

bool Messages::relocateDatabases(const QString &newLocationDir,
    enum ReloactionAction action)
{
	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::fileDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return false;
	}

	if ((action != RA_RELOCATE) && (action != RA_CREATE_NEW)) {
		return false;
	}

	/* List of message databases and old locations. */
	typedef QPair<MessageDb *, QString> MsgDbListEntry;
	QList< MsgDbListEntry > relocatedMsgDbs;
	typedef QPair<FileDb *, QString> FileDbListEntry;
	QList< FileDbListEntry > relocatedFileDbs;

	bool relocationSucceeded = true;

	const QList<AcntId> acntIdList(GlobInstcs::acntMapPtr->keys());
	foreach (const AcntId &acntId, acntIdList) {
		const bool dataOnDisk = PrefsSpecific::dataOnDisk(
		    *GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[acntId]);

		/* Ignore accounts with data stored in memory. */
		if (!dataOnDisk) {
			continue;
		}

		MessageDb *mDb = GlobInstcs::messageDbsPtr->accessMessageDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
		    dataOnDisk);
		relocationSucceeded = (mDb != Q_NULLPTR);
		if (!relocationSucceeded) {
			logWarningNL(
			    "Cannot access message database for user name '%s'.",
			    acntId.username().toUtf8().constData());
			break; /* Break the for cycle. */
		}
		FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
		    dataOnDisk);
		relocationSucceeded = (fDb != Q_NULLPTR);
		if (!relocationSucceeded) {
			logWarningNL(
			    "Cannot access file database for user name '%s'.",
			    acntId.username().toUtf8().constData());
			break; /* Break the for cycle. */
		}

		QString oldPath, oldFileName;

		/* Message database file. */
		oldPath = mDb->fileName();
		oldFileName = QFileInfo(oldPath).fileName();
		switch (action) {
		case RA_RELOCATE:
			relocationSucceeded = mDb->copyDb(
			    newLocationDir + QDir::separator() + oldFileName);
			break;
		case RA_CREATE_NEW:
			relocationSucceeded = mDb->reopenDb(
			    newLocationDir + QDir::separator() + oldFileName);
			break;
		default:
			Q_ASSERT(0);
			relocationSucceeded = false;
			break;
		}
		if (relocationSucceeded) {
			/*
			 * Remember original file name as the file still
			 * exists.
			 */
			relocatedMsgDbs.append(
			    MsgDbListEntry(mDb, oldPath));
		} else {
			break; /* Break the for cycle. */
		}

		/* File database file. */
		oldPath = fDb->fileName();
		oldFileName = QFileInfo(oldPath).fileName();
		switch (action) {
		case RA_RELOCATE:
			relocationSucceeded = fDb->copyDb(
			    newLocationDir + QDir::separator() + oldFileName);
			break;
		case RA_CREATE_NEW:
			relocationSucceeded = fDb->reopenDb(
			    newLocationDir + QDir::separator() + oldFileName);
			break;
		default:
			Q_ASSERT(0);
			relocationSucceeded = false;
			break;
		}
		if (relocationSucceeded) {
			/*
			 * Remember original file name as the file still
			 * exists.
			 */
			relocatedFileDbs.append(
			    FileDbListEntry(fDb, oldPath));
		} else {
			break; /* Break the for cycle. */
		}
	}

	if (relocationSucceeded) {
		/* Delete all original files. */
		foreach (const MsgDbListEntry &entry, relocatedMsgDbs) {
			QFile::remove(entry.second);
		}
		foreach (const FileDbListEntry &entry, relocatedFileDbs) {
			QFile::remove(entry.second);
		}
	} else {
		/*
		 * Revert all databases to original locations and delete the
		 * new locations.
		 */
		QString newLocation;
		foreach (const MsgDbListEntry &entry, relocatedMsgDbs) {
			newLocation = entry.first->fileName();
			if (entry.first->openDb(entry.second, true)) {
				QFile::remove(newLocation);
			} else {
				logErrorNL(
				    "Cannot revert to original message database file '%s'.",
				    entry.second.toUtf8().constData());
				/* TODO -- Critical. Cannot revert. */
			}
		}
		foreach (const FileDbListEntry &entry, relocatedFileDbs) {
			newLocation = entry.first->fileName();
			if (entry.first->openDb(entry.second, true)) {
				QFile::remove(newLocation);
			} else {
				logErrorNL(
				    "Cannot revert to original file database file '%s'.",
				    entry.second.toUtf8().constData());
				/* TODO -- Critical. Cannot revert. */
			}
		}
	}

	return relocationSucceeded;
}

int Messages::searchMsg(MessageListModel *messageModel, const QString &phrase,
    MessageTypes msgTypes)
{
	debugFuncCall();

	if (Q_UNLIKELY((GlobInstcs::messageDbsPtr == Q_NULLPTR) ||
	        (GlobInstcs::acntMapPtr == Q_NULLPTR))) {
		Q_ASSERT(0);
		return 0;
	}

	int msgs = 0;

	if (Q_UNLIKELY(messageModel == Q_NULLPTR)) {
		logErrorNL("%s", "Cannot access message model.");
		Q_ASSERT(0);
		return 0;
	}

	messageModel->clearAll();

	const QList<AcntId> acntIdList(GlobInstcs::acntMapPtr->keys());
	foreach (const AcntId &acntId, acntIdList) {
		const bool dataOnDisk = PrefsSpecific::dataOnDisk(
		    *GlobInstcs::prefsPtr, (*GlobInstcs::acntMapPtr)[acntId]);

		/* First step: Search in attachment names, return msg ID list */
		QList<qint64> msgIds;

		FileDb *fDb = GlobInstcs::fileDbsPtr->accessFileDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
		    dataOnDisk);
		if (fDb != Q_NULLPTR) {
			msgIds = fDb->searchAttachmentName(phrase);
		} else {
			logWarningNL("%s", "Cannot access file database.");
		}

		/*
		 * Second step: Search in message envelopes and
		 *              add messages from first step into model.
		 */
		MessageDb *msgDb = GlobInstcs::messageDbsPtr->accessMessageDb(
		    GlobalSettingsQmlWrapper::dbPath(), acntId.username(),
		    dataOnDisk);
		if (Q_UNLIKELY(msgDb == Q_NULLPTR)) {
			logErrorNL("%s", "Cannot access message database.");
			continue;
		}

		if (msgTypes & TYPE_RECEIVED) {
			msgs += msgDb->searchMessagesAndSetModelFromDb(
			    messageModel, acntId, phrase,
			    MessageDb::TYPE_RECEIVED, msgIds);
		}
		if (msgTypes & TYPE_SENT) {
			msgs += msgDb->searchMessagesAndSetModelFromDb(
			    messageModel, acntId, phrase,
			    MessageDb::TYPE_SENT, msgIds);
		}
	}

	return msgs;
}
