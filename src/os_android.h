/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#pragma once

#include <QObject>

#include "src/qml_interaction/interaction_zfo_file.h"

/*!
 * @brief Wraps Android intent retrieval.
 *
 * @todo See https://bugreports.qt.io/browse/QTBUG-44945 ,
 *     https://bugreports.qt.io/browse/QTBUG-36014 ,
 *     http://grokbase.com/t/gg/android-qt/1316xratx9/canonical-way-to-handle-intent-action-view-on-start ,
 *     http://www.opengis.ch/2015/12/03/passing-android-intents-to-qt/ ,
 *     http://blog.qt.io/blog/2016/06/30/intents-with-qt-for-android-part-1/
 *     whether there could be a way how to handle incoming intents via a
 *     signal-slot mechanism.
 */
class IntentNotification : public QObject {
	Q_OBJECT
public:
	/*!
	 * @brief Constructor.
	 *
	 * @param[in] inter ZFO file interaction object.
	 * @param[in] parent Parent object.
	 */
	explicit IntentNotification(InteractionZfoFile &inter,
	    QObject *parent = Q_NULLPTR);

	/*!
	 * @brief Obtain intent arguments.
	 *
	 * @return List of files to be opened.
	 */
	static
	QStringList getIntentArguments(void);

	/*!
	 * @brief Check and ask android storage permissions.
	 *
	 * @return True if storage permissions are granted.
	 */
	static
	bool checkAndAskStoragePermissions(void);

public slots:
	/*!
	 * @brief Scans intent and process the obtained files.
	 *
	 * @param[in] newState New application state.
	 */
	void scanIntentsForFiles(Qt::ApplicationState newState);

private:
	InteractionZfoFile &m_inter; /*!< QML interaction object. */
};
