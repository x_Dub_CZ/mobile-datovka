#!/bin/bash
#
# -----------------------------------------------------------------------------
# The script build OpenSSL for android (diferent types of architectures).
# 
# Usage: User must set some variables below and run script.
#
# 1. Set required Openssl version, e.x."openssl-1.0.2x".
OPENSSL_VERSION="openssl-1.0.2t"
# 2. Set full path to android NDK base dir on your build host platform.
#    (e.x. BASE_ANDROID_NDK_PATH_LINUX to /home/user/android/android-ndk-r15c) for linux.
#    NOTE: It is recommended to use NDK version r13b or r15c. It were tested.
BASE_ANDROID_NDK_PATH_WINDOWS="C:/Users/Martin/AppData/Local/Android/android-ndk-r15c"
BASE_ANDROID_NDK_PATH_LINUX="/home/martin/Android/android-ndk-r15c"
BASE_ANDROID_NDK_PATH_MACOS="/Users/martin/Android/android-ndk-r15c"
# 3. Add required android architectures into brackets.
#    Available architectures are: armeabi-v7a arm64-v8a mips mips64 x86_64 x86
ANDROID_ARCHS=(armeabi-v7a x86)
# That is all! Now you can run the script :)
# ----------------------------------------------------------------------------



# Do not change these variables if you dont know what you do!
BASE_ANDROID_TARGET="android-16"
BUILD_HOST_WINDOWS="windows-x86_64"
BUILD_HOST_LINUX="linux-x86_64"
BUILD_HOST_MACOS="darwin-x86_64"

# Detect host build OS type and set path and android build host.
if [[ "$OSTYPE" == "linux-gnu" ]]; then
	BUILD_HOST=${BUILD_HOST_LINUX}
	BASE_ANDROID_NDK_PATH=${BASE_ANDROID_NDK_PATH_LINUX}
elif [[ "$OSTYPE" == "darwin"* ]]; then
	BUILD_HOST=${BUILD_HOST_MACOS}
	BASE_ANDROID_NDK_PATH=${BASE_ANDROID_NDK_PATH_MACOS}
elif [[ "$OSTYPE" == "cygwin" ]]; then
	BUILD_HOST=${BUILD_HOST_WINDOWS}
	BASE_ANDROID_NDK_PATH=${BASE_ANDROID_NDK_PATH_WINDOWS}
elif [[ "$OSTYPE" == "msys" ]]; then
	BUILD_HOST=${BUILD_HOST_WINDOWS}
	BASE_ANDROID_NDK_PATH=${BASE_ANDROID_NDK_PATH_WINDOWS}
elif [[ "$OSTYPE" == "win32" ]]; then
	BUILD_HOST=${BUILD_HOST_WINDOWS}
	BASE_ANDROID_NDK_PATH=${BASE_ANDROID_NDK_PATH_WINDOWS}
fi

set -e
rm -rf libs_openssl_1.0.x
mkdir libs_openssl_1.0.x

# Download and unzip openssl package.
curl -O "https://www.openssl.org/source/${OPENSSL_VERSION}.tar.gz"
tar xfz "${OPENSSL_VERSION}.tar.gz"

# Build all required android architecture.
for arch in ${ANDROID_ARCHS[@]}; do
    case ${arch} in
        "armeabi-v7a")
            _ANDROID_TARGET_SELECT=arch-arm
            _ANDROID_ARCH=arch-arm
            _ANDROID_EABI=arm-linux-androideabi-4.9
            _ANDROID_GCC=arm-linux-androideabi-gcc
            _ANDROID_AR=arm-linux-androideabi-ar
            configure_platform="android-armv7" ;;
        "arm64-v8a")
            _ANDROID_TARGET_SELECT=arch-arm64-v8a
            _ANDROID_ARCH=arch-arm64
            _ANDROID_EABI=aarch64-linux-android-4.9
            _ANDROID_GCC=aarch64-linux-android-gcc
            _ANDROID_AR=aarch64-linux-android-ar
            configure_platform="linux-generic64 -DB_ENDIAN" ;;
        "mips")
            _ANDROID_TARGET_SELECT=arch-mips
            _ANDROID_ARCH=arch-mips
            _ANDROID_EABI=mipsel-linux-android-4.9
            _ANDROID_GCC=mipsel-linux-android-gcc
            _ANDROID_AR=mipsel-linux-android-ar
            configure_platform="android -DB_ENDIAN" ;;
        "mips64")
            _ANDROID_TARGET_SELECT=arch-mips64
            _ANDROID_ARCH=arch-mips64
            _ANDROID_EABI=mips64el-linux-android-4.9
            _ANDROID_GCC=mips64el-linux-android-gcc
            _ANDROID_AR=mips64el-linux-android-ar
            configure_platform="linux-generic64 -DB_ENDIAN" ;;
        "x86")
            _ANDROID_TARGET_SELECT=arch-x86
            _ANDROID_ARCH=arch-x86
            _ANDROID_EABI=x86-4.9
            _ANDROID_GCC=i686-linux-android-gcc
            _ANDROID_AR=i686-linux-android-ar
            configure_platform="android-x86" ;;
        "x86_64")
            _ANDROID_TARGET_SELECT=arch-x86_64
            _ANDROID_ARCH=arch-x86_64
            _ANDROID_EABI=x86_64-4.9
            _ANDROID_GCC=x86_64-linux-android-gcc
            _ANDROID_AR=x86_64-linux-android-ar
            configure_platform="linux-generic64" ;;
        *)
            configure_platform="linux-elf" ;;
    esac

    mkdir libs_openssl_1.0.x/${arch}

    cd "${OPENSSL_VERSION}"

    export CC=${BASE_ANDROID_NDK_PATH}/toolchains/${_ANDROID_EABI}/prebuilt/${BUILD_HOST}/bin/${_ANDROID_GCC}
    export AR=${BASE_ANDROID_NDK_PATH}/toolchains/${_ANDROID_EABI}/prebuilt/${BUILD_HOST}/bin/${_ANDROID_AR}
    export ANDROID_DEV=${BASE_ANDROID_NDK_PATH}/platforms/${BASE_ANDROID_TARGET}/${_ANDROID_ARCH}/usr
    ./Configure shared threads no-asm no-zlib no-ssl2 no-ssl3 no-hw $configure_platform

    perl -pi -e 's/SHLIB_EXT=\.so\.\$\(SHLIB_MAJOR\)\.\$\(SHLIB_MINOR\)/SHLIB_EXT=\.so/g' Makefile
    perl -pi -e 's/SHARED_LIBS_LINK_EXTS=\.so\.\$\(SHLIB_MAJOR\) \.so//g' Makefile
    perl -pi -e 's/SHLIB_MAJOR=1/SHLIB_MAJOR=`/g' Makefile
    perl -pi -e 's/SHLIB_MINOR=0.0/SHLIB_MINOR=`/g' Makefile
    make clean
    make build_libs -j4

    file libcrypto.so
    file libssl.so
    file libcrypto.a
    file libssl.a
    cp libcrypto.so ../libs_openssl_1.0.x/${arch}/libcrypto.so
    cp libssl.so ../libs_openssl_1.0.x/${arch}/libssl.so
    cp libcrypto.a ../libs_openssl_1.0.x/${arch}/libcrypto.a
    cp libssl.a ../libs_openssl_1.0.x/${arch}/libssl.a
    cd ..

done

    cp -r -L ${OPENSSL_VERSION}/include/ libs_openssl_1.0.x/include
    rm -rf "${OPENSSL_VERSION}"
    rm "${OPENSSL_VERSION}.tar.gz"

    echo "BUILD DONE!"

exit 0

