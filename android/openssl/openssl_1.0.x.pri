INCLUDEPATH += \
    $$PWD \
    src \
    android/openssl/libs_openssl_1.0.x/include/
    equals(ANDROID_TARGET_ARCH, armeabi) {
        LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/armeabi/libcrypto.a \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/armeabi/libssl.a
    }
    equals(ANDROID_TARGET_ARCH, armeabi-v7a) {
        LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/armeabi-v7a/libcrypto.a \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/armeabi-v7a/libssl.a
    }
    equals(ANDROID_TARGET_ARCH, x86) {
        LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/x86/libcrypto.a \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/x86/libssl.a
    }
    equals(ANDROID_TARGET_ARCH, arm64-v8a) {
        LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/arm64-v8a/libcrypto.a \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/arm64-v8a/libssl.a
    }
    equals(ANDROID_TARGET_ARCH, x86_64) {
        LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/x86_64/libcrypto.a \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/x86_64/libssl.a
    }


contains(ANDROID_TARGET_ARCH, armeabi) {
    ANDROID_EXTRA_LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/armeabi/libcrypto.so \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/armeabi/libssl.so
}
contains(ANDROID_TARGET_ARCH, armeabi-v7a) {
    ANDROID_EXTRA_LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/armeabi-v7a/libcrypto.so \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/armeabi-v7a/libssl.so
}
contains(ANDROID_TARGET_ARCH, x86) {
    ANDROID_EXTRA_LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/x86/libcrypto.so \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/x86/libssl.so
}
contains(ANDROID_TARGET_ARCH, arm64-v8a) {
    ANDROID_EXTRA_LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/arm64-v8a/libcrypto.so \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/arm64-v8a/libssl.so
}
contains(ANDROID_TARGET_ARCH, x86_64) {
    ANDROID_EXTRA_LIBS = \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/x86_64/libcrypto.so \
        $${_PRO_FILE_PWD_}/android/openssl/libs_openssl_1.0.x/x86_64/libssl.so
}
