/*
 * Copyright (C) 2014-2021 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In addition, as a special exception, the copyright holders give
 * permission to link the code of portions of this program with the
 * OpenSSL library under certain conditions as described in each
 * individual source file, and distribute linked combinations including
 * the two.
 */

#include <QAndroidJniObject>
#include <QtAndroidExtras>

#include <QDesktopServices>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QUrl>

#include "android/src/android_io.h"

AndroidIO::AndroidIO(void)
{
}

bool AndroidIO::isSDKVersion24OrNewest(void)
{
	jboolean ok = QAndroidJniObject::callStaticMethod<jboolean>(
	     "cz/nic/mobiledatovka/java/QFileProvider",
	     "isSDKVersion24OrNewest");

	return (ok);
}

bool AndroidIO::openFile(const QString &filePath)
{
	/*
	 * TODO - must be tested and reimplement.
	 * Use file provider only because SDK detection fails.
	 * QDesktopService generat empty files.
	 */
/*
	if (!isSDKVersion24OrNewest()) {
		return openWithQDesktopServices(filePath);
	}
*/
	return openWithFileProvider(filePath);
}

bool AndroidIO::openWithQDesktopServices(const QString &filePath)
{
	return QDesktopServices::openUrl(QUrl::fromLocalFile(filePath));
}

bool AndroidIO::openWithFileProvider(const QString &filePath)
{
	QAndroidJniObject jsPath = QAndroidJniObject::fromString(filePath);
	jboolean ok = QAndroidJniObject::callStaticMethod<jboolean>(
	     "cz/nic/mobiledatovka/java/QFileProvider",
	     "viewFile", "(Ljava/lang/String;)Z",
	     jsPath.object<jstring>());

	return (ok);
}

bool AndroidIO::createEmail(const QString &subject, const QString &to,
    const QString &body, const QStringList &filePaths)
{
	QAndroidJniObject jsSubject = QAndroidJniObject::fromString(subject);
	QAndroidJniObject jsTo = QAndroidJniObject::fromString(to);
	QAndroidJniObject jsBody = QAndroidJniObject::fromString(body);
	QJsonArray arr = QJsonArray::fromStringList(filePaths);
	QJsonObject jsonObj;
	jsonObj[QLatin1String("attachments")] = arr;
	QJsonDocument doc(jsonObj);
	QAndroidJniObject jsJsonPathList = QAndroidJniObject::fromString(doc.toJson(QJsonDocument::Compact));

	jboolean ok = QAndroidJniObject::callStaticMethod<jboolean>(
	     "cz/nic/mobiledatovka/java/QFileProvider",
	     "sendEmail", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z",
	     jsSubject.object<jstring>(), jsTo.object<jstring>(),
	     jsBody.object<jstring>(), jsJsonPathList.object<jstring>());

	return (ok);
}
